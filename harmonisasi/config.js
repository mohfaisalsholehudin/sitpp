window.ENV = {
    // DEVELOPMENT
    // "SLICE_URL" : 35,
    // "SLICE_ZIP" : 46,
    // "SLICE_FILE":24,
    // "SLICE_UPLOAD":43,
    // "DOWNLOAD_URL": "http://10.244.66.59/api/filesdownloadzip/",
    // "URL_DOWNLOAD": "http://10.244.66.59/api/filesdownload/",
    // "STREAM_URL_VIDEO" : "http://10.244.66.85:8086/api/v1/pengetahuans/stream/video/",
    // "STREAM_URL_AUDIO" : "http://10.244.66.85:8086/api/v1/pengetahuans/stream/audio/",
    // "BACKEND_URL": "http://10.244.66.59",
    // "FILE_URL": "http://10.244.66.59/api/files/",

    //SIMULATION
    // "SLICE_URL" : 35,
    // "SLICE_ZIP" : 46,
    // "SLICE_FILE":24,
    // "SLICE_UPLOAD":43,
    // "DOWNLOAD_URL": "http://10.244.98.68/api/filesdownloadzip/",
    // "URL_DOWNLOAD": "http://10.244.98.68/api/filesdownload/",
    // "STREAM_URL_VIDEO" : "http://10.244.66.85:8086/api/v1/pengetahuans/stream/video/",
    // "STREAM_URL_AUDIO" : "http://10.244.66.85:8086/api/v1/pengetahuans/stream/audio/",
    // "BACKEND_URL": "http://10.244.98.68",
    // "FILE_URL": "http://10.244.98.68/api/files/",

    //PRODUCTION
    "SLICE_URL" : 35,
    "SLICE_ZIP" : 46,
    "SLICE_FILE":43,
    "SLICE_UPLOAD":43,
    "DOWNLOAD_URL": "https://kms.intranet.pajak.go.id/api/filesdownloadzip/",
    "URL_DOWNLOAD": "https://kms.intranet.pajak.go.id/api/filesdownload/",
    "STREAM_URL_VIDEO" : "http://10.244.66.85:8086/api/v1/pengetahuans/stream/video/",
    "STREAM_URL_AUDIO" : "http://10.244.66.85:8086/api/v1/pengetahuans/stream/audio/",
    "BACKEND_URL": "https://kms.intranet.pajak.go.id",
    "FILE_URL": "https://kms.intranet.pajak.go.id/api/files/",


    //SSO
    "SSO_URL":"http://10.244.66.4:8080",
    "LOCAL_URL":"http://localhost:3000",
}

//35 itu dari http://10.244.66.59:8080/api/files/
//https://kms.intranet.pajak.go.id/

//SLICE_FILE pake yg 43 kalo di Prod
//SLICE_FILE pake yg 24 kalo yg di Dev 10.244.66.59

//SLICE_URL pake yg 35 kalo pake 10.244.66.59 
//SLICE_URL pake yg 35 kalo di prod
//SLICE_URL pake yg 40 kalo pake kms-test

//SLICE_FILE pake yg 24 kalo pake 10.244.66.59
//SLICE_FILE pake yg 43 kalo di prod
//SLICE_FILE pake yg 29 kalo pake kms-test

//SLICE_ZIP pake yg 46 kalo pake 10.244.66.59
//SLICE_ZIP pake yg 46 kalo di prod 
//SLICE_ZIP pake yg 51 kalo pake kms-test

//SLICE_UPLOAD pake yg 43 kalo pake 10.244.66.59
//SLICE_UPLOAD pake yg 43 kalo di prod
//SLICE_UPLOAD pake yg 48 kalo pake kms-test


// URL_SIMULASI : 10.244.98.68
// URL_DEVELOPMENT : 10.244.66.59
// URL_PRODUCTION :  10.245.10.38
// DOMAIN_PRODUCTION : https://kms.intranet.pajak.go.id

