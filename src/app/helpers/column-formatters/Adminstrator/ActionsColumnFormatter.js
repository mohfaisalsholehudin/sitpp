// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function ActionsColumnFormatterAdminJenisPajak(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
    <a
      title="Edit Jenis Pajak"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openEditDialog(row.id_jnspajak)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
      <i className="fas fa-edit text-primary"></i>
      </span>
    </a>
    <> </>

    {/* <a
      title="Hapus Jenis Pajak"
      className="btn btn-icon btn-light btn-hover-danger btn-sm"
      onClick={() => openDeleteDialog(row.id_jnspajak)}
    >
      <span className="svg-icon svg-icon-md svg-icon-danger">
      <i className="fas fa-trash text-danger"></i>
      </span>
    </a> */}
  </>
  );
  // const checkStatus = (status) => {
  //   switch (status) {
  //     case "Aktif":
  //       return (
  //         <>
  //         <a
  //           title="Edit Proposal"
  //           className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
  //           onClick={() => openEditDialog(row.id_usulan)}
  //         >
  //           <span className="svg-icon svg-icon-md svg-icon-primary">
  //             <SVG
  //               src={toAbsoluteUrl(
  //                 "/media/svg/icons/Communication/Write.svg"
  //               )}
  //             />
  //           </span>
  //         </a>
  //         <> </>

  //         <a
  //           title="Hapus Proposal"
  //           className="btn btn-icon btn-light btn-hover-danger btn-sm"
  //           onClick={() => openDeleteDialog(row.id)}
  //         >
  //           <span className="svg-icon svg-icon-md svg-icon-danger">
  //             <SVG
  //               src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
  //             />
  //           </span>
  //         </a>
  //       </>
  //       );

  //     case "Pasif":
  //       return (
  //         <>
  //           <a
  //             title="Edit Proposal"
  //             className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
  //             onClick={() => openEditDialog(row.id_usulan)}
  //           >
  //             <span className="svg-icon svg-icon-md svg-icon-primary">
  //               <SVG
  //                 src={toAbsoluteUrl(
  //                   "/media/svg/icons/Communication/Write.svg"
  //                 )}
  //               />
  //             </span>
  //           </a>
  //           <> </>

  //           <a
  //             title="Hapus Proposal"
  //             className="btn btn-icon btn-light btn-hover-danger btn-sm"
  //             onClick={() => openDeleteDialog(row.id)}
  //           >
  //             <span className="svg-icon svg-icon-md svg-icon-danger">
  //               <SVG
  //                 src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
  //               />
  //             </span>
  //           </a>
  //         </>
  //       );
  //     default:
  //       break;
  //   }
  // };

  // return <>{checkStatus(row.status)}</>;
}

export function ActionsColumnFormatterAdminJenisPeraturan(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
    <a
      title="Edit Jenis Peraturan"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openEditDialog(row.id_jnsperaturan)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
      <i className="fas fa-edit text-primary"></i>
      </span>
    </a>
    <> </>

    {/* <a
      title="Hapus Jenis Peraturan"
      className="btn btn-icon btn-light btn-hover-danger btn-sm"
      onClick={() => openDeleteDialog(row.id_jnsperaturan)}
    >
      <span className="svg-icon svg-icon-md svg-icon-danger">
      <i className="fas fa-trash text-danger"></i>
      </span>
    </a> */}
  </>
  );
}
export function ActionsColumnFormatterAdminMasterJenis(
  cellContent,
  row,
  rowIndex,
  { openDeleteDialog }
) {
  return (
    <>
    <a
      title="Hapus Master Jenis"
      className="btn btn-icon btn-light btn-hover-danger btn-sm"
      onClick={() => openDeleteDialog(row.id)}
    >
      <span className="svg-icon svg-icon-md svg-icon-danger">
      <i className="fas fa-trash text-danger"></i>
      </span>
    </a>
  </>
  );
}

export function ActionsColumnFormatterAdminDetilJenis(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
    <a
      title="Edit Detil Jenis"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openEditDialog(row.id)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
      <i className="fas fa-edit text-primary"></i>
      </span>
    </a>
    <> </>

    <a
      title="Hapus Detil Jenis"
      className="btn btn-icon btn-light btn-hover-danger btn-sm"
      onClick={() => openDeleteDialog(row.id)}
    >
      <span className="svg-icon svg-icon-md svg-icon-danger">
      <i className="fas fa-trash text-danger"></i>
      </span>
    </a>
  </>
  );
}

export function ActionsColumnFormatterAdminRoleList(
  cellContent,
  row,
  rowIndex,
  { openDialog }
) {
  return (
    <>
    <a
      title="Open Detil Role"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openDialog(row.id_detil)}
    >
      <span className="svg-icon svg-icon-md svg-icon-dark">
      <i className="fas fa-eye text-dark"></i>
      </span>
    </a>
  </>
  );
}
export function ActionsColumnFormatterAdminRolePenjaminKualitasPengetahuan(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
    <a
      title="Edit Role Penjamin Kualitas Pengetahuan"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openEditDialog(row.id_pkp)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
      <i className="fas fa-edit text-primary"></i>
      </span>
    </a>
    <> </>

    <a
      title="Hapus Role Penjamin Kualitas Pengetahuan"
      className="btn btn-icon btn-light btn-hover-danger btn-sm"
      onClick={() => openDeleteDialog(row.id_pkp)}
    >
      <span className="svg-icon svg-icon-md svg-icon-danger">
      <i className="fas fa-trash text-danger"></i>
      </span>
    </a>
  </>
  );
}
export function ActionsColumnFormatterAdminRolePenjaminKualitasPengetahuanProbis(
  cellContent,
  row,
  rowIndex,
  { openDeleteDialog }
) {
  return (
    <>
    <a
      title="Hapus Probis Penjamin Kualitas Pengetahuan"
      className="btn btn-icon btn-light btn-hover-danger btn-sm"
      onClick={() => openDeleteDialog(row.pkpDetil.id_pkp_detil)}
    >
      <span className="svg-icon svg-icon-md svg-icon-danger">
      <i className="fas fa-trash text-danger"></i>
      </span>
    </a>
  </>
  );
}
export function ActionsColumnFormatterAdminRoleKnowledgeOwner(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
    <a
      title="Edit Role Knowledge Owner"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openEditDialog(row.id_ko)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
      <i className="fas fa-edit text-primary"></i>
      </span>
    </a>
    <> </>

    <a
      title="Hapus Role Knowledge Owner"
      className="btn btn-icon btn-light btn-hover-danger btn-sm"
      onClick={() => openDeleteDialog(row.id_ko)}
    >
      <span className="svg-icon svg-icon-md svg-icon-danger">
      <i className="fas fa-trash text-danger"></i>
      </span>
    </a>
  </>
  );
}
export function ActionsColumnFormatterAdminRoleKnowledgeOwnerProbis(
  cellContent,
  row,
  rowIndex,
  { openDeleteDialog }
) {
  return (
    <>
    <a
      title="Hapus Probis Knowledge Owner"
      className="btn btn-icon btn-light btn-hover-danger btn-sm"
      onClick={() => openDeleteDialog(row.kownerDetil.id_ko_detil)}
    >
      <span className="svg-icon svg-icon-md svg-icon-danger">
      <i className="fas fa-trash text-danger"></i>
      </span>
    </a>
  </>
  );
}
export function ActionsColumnFormatterAdminRoleSubjectMatterExpert(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
    <a
      title="Edit Role Subject Matter Expert"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openEditDialog(row.id_sme)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
      <i className="fas fa-edit text-primary"></i>
      </span>
    </a>
    <> </>

    <a
      title="Hapus Role Subject Matter Expert"
      className="btn btn-icon btn-light btn-hover-danger btn-sm"
      onClick={() => openDeleteDialog(row.id_sme)}
    >
      <span className="svg-icon svg-icon-md svg-icon-danger">
      <i className="fas fa-trash text-danger"></i>
      </span>
    </a>
  </>
  );
}
export function ActionsColumnFormatterAdminRoleSubjectMatterExpertProbis(
  cellContent,
  row,
  rowIndex,
  { openDeleteDialog }
) {
  return (
    <>
    <a
      title="Hapus Probis Knowledge Owner"
      className="btn btn-icon btn-light btn-hover-danger btn-sm"
      onClick={() => openDeleteDialog(row.smiDetil.id_sme_detil)}
    >
      <span className="svg-icon svg-icon-md svg-icon-danger">
      <i className="fas fa-trash text-danger"></i>
      </span>
    </a>
  </>
  );
}
export function ActionsColumnFormatterAdminRolePengelolaPortalPengetahuan(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
    <a
      title="Edit Role Pengelola Portal Pengetahuan"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openEditDialog(row.id_pp_kms)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
      <i className="fas fa-edit text-primary"></i>
      </span>
    </a>
    <> </>

    <a
      title="Hapus Role Pengelola Portal Pengetahuan"
      className="btn btn-icon btn-light btn-hover-danger btn-sm"
      onClick={() => openDeleteDialog(row.id_pp_kms)}
    >
      <span className="svg-icon svg-icon-md svg-icon-danger">
      <i className="fas fa-trash text-danger"></i>
      </span>
    </a>
  </>
  );
}

export function ActionsColumnFormatterAdminSettingProbis(
  cellContent,
  row,
  rowIndex,
  { openEditDialog }
) {
  return (
    <>
    <a
      title="Edit Setting Probis"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openEditDialog(row.id_probis)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
      <i className="fas fa-edit text-primary"></i>
      </span>
    </a>
  </>
  );
}
export function ActionsColumnFormatterAdminSettingCasename(
  cellContent,
  row,
  rowIndex,
  { openEditDialog }
) {
  return (
    <>
    <a
      title="Edit Setting Casename"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openEditDialog(row.caseName.id_case_name)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
      <i className="fas fa-edit text-primary"></i>
      </span>
    </a>
  </>
  );
}

export function ActionsColumnFormatterAdminSettingSubcase(
  cellContent,
  row,
  rowIndex,
  { openEditDialog }
) {
  return (
    <>
    <a
      title="Edit Setting Subcase"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openEditDialog(row.subCase.id_subcase)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
      <i className="fas fa-edit text-primary"></i>
      </span>
    </a>
  </>
  );
}
export function ActionsColumnFormatterAdminSettingSektorBisnis(
  cellContent,
  row,
  rowIndex,
  { openEditDialog }
) {
  return (
    <>
    <a
      title="Edit Setting Sektor Bisnis"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openEditDialog(row.id_sektor)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
      <i className="fas fa-edit text-primary"></i>
      </span>
    </a>
  </>
  );
}
export function ActionsColumnFormatterAdminSettingTipeKnowledge(
  cellContent,
  row,
  rowIndex,
  { openEditDialog }
) {
  return (
    <>
    <a
      title="Edit Setting Tipe Knowledge"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openEditDialog(row.id_tipe_km)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
      <i className="fas fa-edit text-primary"></i>
      </span>
    </a>
  </>
  );
}
export function ActionsColumnFormatterAdminSettingTusi(
  cellContent,
  row,
  rowIndex,
  { openEditDialog }
) {
  return (
    <>
    <a
      title="Edit Setting Tugas dan Fungsi"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openEditDialog(row.id_tusi)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
      <i className="fas fa-edit text-primary"></i>
      </span>
    </a>
  </>
  );
}

export function ActionsColumnFormatterAdminPengumuman(
  cellContent,
  row,
  rowIndex,
  {
    editDialog,
    editPublish,
    deleteDialog,
    apply,
    showReview,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Publish Non Aktif":
        return (
          <>
            <a
              title="Edit Pengumuman"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                editPublish(
                  row.id_pengumuman,
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
            <a
              title="Hapus Pengumuman"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() =>
                deleteDialog(
                  row.id_pengumuman
                  // row.knowledgeProses.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
          </>
        );

      case "Draft":
        return (
          <>
            <a
              title="Edit Pengumuman"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                editDialog(
                  row.id_pengumuman,
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Pengumuman"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() =>
                deleteDialog(
                  row.id_pengumuman
                  // row.knowledgeProses.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>
            <a
              title="Publish Pengumuman"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                apply(
                  row.id_pengumuman
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fab fa-telegram-plane text-success"></i>
              </span>
            </a>
          </>
        );

        case "Publish Aktif":
        return (
          <>
            <a
              title="Edit Pengumuman"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                editPublish(
                  row.id_pengumuman,
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
            <a
              title="Hapus Pengumuman"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() =>
                deleteDialog(
                  row.id_pengumuman
                  // row.knowledgeProses.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
          </>
        );
      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function ActionsColumnFormatterAdminPengumumanTarget(
  cellContent,
  row,
  rowIndex,
  { deleteDialog }
) {
  return (
    <>
    <a
      title="Hapus Pegawai"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => deleteDialog(row.id_target)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
      <i className="fas fa-trash text-danger"></i>
      </span>
    </a>
  </>
  );
}