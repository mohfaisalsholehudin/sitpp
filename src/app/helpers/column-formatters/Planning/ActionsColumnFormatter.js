// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function ActionsColumnFormatterPlanProposal(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog, showProposal, applyProposal, showReject }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showProposal(row.id_perencanaan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Eselon 3":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showProposal(row.id_perencanaan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

      case "Draft":
        return (
          <>
            <a
              title="Edit Perencanaan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_perencanaan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Perencanaan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => openDeleteDialog(row.id_perencanaan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>
            <a
              title="Ajukan Perencanaan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal(row.id_perencanaan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>
          </>
        );

      case "Tolak":
        return (
          <>
            <a
              title="Show Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReject(row.id_perencanaan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            <> </>
            <a
              title="Edit Perencanaan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_perencanaan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
          </>
        );

      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function ActionsColumnFormatterPlanResearch(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog, showProposal }
) {
  return (
    <>
      <a
        title="Open Perencanaan"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showProposal(row.id_perencanaan)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
          /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterPlanMonitoring(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog, showProposal }
) {
  return (
    <>
      <a
        title="Open Data"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showProposal(row.id_perencanaan)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
          /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}
