// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
import React from "react";
const {SLICE_ZIP, DOWNLOAD_URL} = window.ENV;

export function ProposalFileColumnFormatter(cellContent, row) {
  return (
    <a href={DOWNLOAD_URL+row.file_kajian.slice(SLICE_ZIP)} target="_blank" rel="noopener noreferrer">Download</a>
  );
}
// export function RevalidateFileValidasiColumnFormatter(cellContent, row) {
//   return (
//     <a href={row.file_upload} target="_blank" rel="noopener noreferrer">Lihat</a>
//   );
// }
