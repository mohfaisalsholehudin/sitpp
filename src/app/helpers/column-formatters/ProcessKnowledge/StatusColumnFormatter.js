// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
import React from "react";
export function StatusColumnFormatterProcessKnowledgeAdded(cellContent, row) {
  const CustomerStatusCssClasses = [
    "success",
    "warning",
    "warning",
    "warning",
    "warning",
    "warning",
    "info",
    "info",
    "primary",
    "primary",
    "primary",
    "danger",
    "info",
    "info",
    "info",
    "info",
    "info",
    "info",
    ""
  ];
  const CustomerStatusTitles = [
    "Draft Knowledge",
    "Review PKP Tacit",
    "Review PKP Explicit",
    "Review KO Tacit",
    "Review KO Explicit",
    "Review SME Tacit",
    "Siap Publish PKP Tacit",
    "Siap Publish PKP Explicit",
    "Publish Knowledge",
    "Telah Review Knowledge",
    "Siap Review Knowledge",
    "Tolak",
    "Update Review PKP Tacit",
    "Update Review PKP Explicit",
    "Update Review KO Tacit",
    "Update Review KO Explicit",
    "Update Review SME Tacit",
    "Siap Publish Review PKP",
    ""
    ];

  let status = "";

  switch (row.statusKm.nama) {
    case "Draft Knowledge":
      status = 0;
      break;

    case "Review PKP Tacit":
      status = 1;
      break;

      case "Review PKP Explicit":
      status = 2;
      break;

    case "Review KO Tacit":
      status = 3;
      break;

    case "Review KO Explicit":
      status = 4;
      break;

    case "Review SME Tacit":
      status = 5;
      break;

    case "Siap Publish PKP Tacit":
      status = 6;
      break;
      
    case "Siap Publish PKP Explicit":
      status = 7;
      break;
      
    case "Publish Knowledge":
      status = 8;
      break;

    case "Telah Review Knowledge":
      status = 9;
      break;

    case "Siap Review Knowledge":
      status = 10;
      break;

    case "Tolak":
      status = 11;
      break;
    
    case "Update Review PKP Tacit":
      status = 12;
      break;

    case "Update Review PKP Explicit":
      status = 13;
      break;

    case "Update Review KO Tacit":
      status = 14;
      break;

    case "Update Review KO Explicit":
      status = 15;
      break;

    case "Update Review SME Tacit":
      status = 16;
      break;

    case "Siap Publish Review PKP":
      status = 17;
      break;

    default:
      status = 18;
      break;
  }
  const getLabelCssClasses = () => {
    if (row.statusKm.nama === "") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}

export function StatusColumnFormatterReviewUpdate(cellContent, row) {
  const CustomerStatusCssClasses = [
    "success",
    "success",
    "warning",
    "primary",
    "primary",
    "primary",
    "info",
    "info",
    "primary",
    "primary",
    "primary",
    ""
  ];
  const CustomerStatusTitles = [
    "Update Review PKP Tacit",
    "Update Review PKP Explicit",
    "Review PKP Explicit",
    "Update Review KO Tacit",
    "Update Review KO Explicit",
    "Update Review SME Tacit",
    "Siap Publish PKP Tacit",
    "Siap Publish PKP Explicit",
    "Siap Publish Review PKP",
    "Telah Review Knowledge",
    "Siap Review Knowledge",
    ""
    ];

  let status = "";

  switch (row.statusKm.nama) {
    case "Update Review PKP Tacit":
      status = 0;
      break;

    case "Update Review PKP Explicit":
      status = 1;
      break;

      case "Review PKP Explicit":
      status = 2;
      break;

    case "Update Review KO Tacit":
      status = 3;
      break;

    case "Update Review KO Explicit":
      status = 4;
      break;

    case "Update Review SME Tacit":
      status = 5;
      break;

      case "Siap Publish PKP Tacit":
        status = 6;
        break;
      
        case "Siap Publish PKP Explicit":
          status = 7;
          break;
      
          case "Siap Publish Review PKP":
            status = 8;
            break;

      case "Telah Review Knowledge":
        status = 9;
        break;

      case "Siap Review Knowledge":
        status = 10;
        break;

      default:
        status = 11
  }
  const getLabelCssClasses = () => {
    if (row.statusKm.nama === "") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}


export function StatusColumnFormatterProcessKnowledgeFeedback(cellContent, row) {
  const CustomerStatusCssClasses = [
    "warning",
    "success",
    ""
  ];
  const CustomerStatusTitles = [
    "Tidak Aktif",
    "Aktif",
    ""
    ];

  let status = "";

  switch (row.status) {
    case 0:
      status = 0;
      break;

    case 1:
      status = 1;
      break;

    default:
      status = 2;
      break;
  }
  const getLabelCssClasses = () => {
    if (row.status === "") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}

export function StatusColumnFormatterMonitoringReview(cellContent, row) {
  const CustomerStatusCssClasses = [
    "primary",
    "primary",
    "danger",
    "info",
    "info",
    "info",
    "info",
    "info",
    ""
  ];
  const CustomerStatusTitles = [
    "Telah Review Knowledge",
    "Siap Review Knowledge",
    "Tolak",
    "Update Review PKP Tacit",
    "Update Review PKP Explicit",
    "Update Review KO Tacit",
    "Update Review KO Explicit",
    "Update Review SME Tacit",
    "Siap Publish Review PKP",
    ""
    ];

  let status = "";

  switch (row.statusKm.nama) {
    case "Telah Review Knowledge":
      status = 0;
      break;

    case "Siap Review Knowledge":
      status = 1;
      break;

    case "Tolak":
      status = 2;
      break;
    
    case "Update Review PKP Tacit":
      status = 3;
      break;

    case "Update Review PKP Explicit":
      status = 4;
      break;

    case "Update Review KO Tacit":
      status = 5;
      break;

    case "Update Review KO Explicit":
      status = 6;
      break;

    case "Update Review SME Tacit":
      status = 7;
      break;

    case "Siap Publish Review PKP":
      status = 8;
      break;

    default:
      status = 9;
      break;
  }
  const getLabelCssClasses = () => {
    if (row.statusKm.nama === "") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}

export function StatusColumnFormatterReviewUpdateKo(cellContent, row) {
  const CustomerStatusCssClasses = [
    "success",
    "success",
    "warning",
    "success",
    "success",
    "primary",
    "info",
    "info",
    "primary",
    "primary",
    "primary",
    ""
  ];
  const CustomerStatusTitles = [
    "Update Review PKP Tacit",
    "Update Review PKP Explicit",
    "Review PKP Explicit",
    "Update Review KO Tacit",
    "Update Review KO Explicit",
    "Update Review SME Tacit",
    "Siap Publish PKP Tacit",
    "Siap Publish PKP Explicit",
    "Siap Publish Review PKP",
    "Telah Review Knowledge",
    "Siap Review Knowledge",
    ""
    ];

  let status = "";

  switch (row.statusKm.nama) {
    case "Update Review PKP Tacit":
      status = 0;
      break;

    case "Update Review PKP Explicit":
      status = 1;
      break;

      case "Review PKP Explicit":
      status = 2;
      break;

    case "Update Review KO Tacit":
      status = 3;
      break;

    case "Update Review KO Explicit":
      status = 4;
      break;

    case "Update Review SME Tacit":
      status = 5;
      break;

      case "Siap Publish PKP Tacit":
        status = 6;
        break;
      
        case "Siap Publish PKP Explicit":
          status = 7;
          break;
      
          case "Siap Publish Review PKP":
            status = 8;
            break;

      case "Telah Review Knowledge":
        status = 9;
        break;

      case "Siap Review Knowledge":
        status = 10;
        break;

      default:
        status = 11
  }
  const getLabelCssClasses = () => {
    if (row.statusKm.nama === "") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}

export function StatusColumnFormatterReviewUpdateSme(cellContent, row) {
  const CustomerStatusCssClasses = [
    "success",
    "success",
    "warning",
    "success",
    "success",
    "success",
    "info",
    "info",
    "primary",
    "primary",
    "primary",
    ""
  ];
  const CustomerStatusTitles = [
    "Update Review PKP Tacit",
    "Update Review PKP Explicit",
    "Review PKP Explicit",
    "Update Review KO Tacit",
    "Update Review KO Explicit",
    "Update Review SME Tacit",
    "Siap Publish PKP Tacit",
    "Siap Publish PKP Explicit",
    "Siap Publish Review PKP",
    "Telah Review Knowledge",
    "Siap Review Knowledge",
    ""
    ];

  let status = "";

  switch (row.statusKm.nama) {
    case "Update Review PKP Tacit":
      status = 0;
      break;

    case "Update Review PKP Explicit":
      status = 1;
      break;

      case "Review PKP Explicit":
      status = 2;
      break;

    case "Update Review KO Tacit":
      status = 3;
      break;

    case "Update Review KO Explicit":
      status = 4;
      break;

    case "Update Review SME Tacit":
      status = 5;
      break;

      case "Siap Publish PKP Tacit":
        status = 6;
        break;
      
        case "Siap Publish PKP Explicit":
          status = 7;
          break;
      
          case "Siap Publish Review PKP":
            status = 8;
            break;

      case "Telah Review Knowledge":
        status = 9;
        break;

      case "Siap Review Knowledge":
        status = 10;
        break;

      default:
        status = 11
  }
  const getLabelCssClasses = () => {
    if (row.statusKm.nama === "") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}


export function StatusColumnFormatterProcessKnowledgeUsulanTemplate(cellContent, row) {
  const CustomerStatusCssClasses = [
    "success",
    "warning",
    "primary",
    ""
  ];
  const CustomerStatusTitles = [
    "Draft Pengajuan Template",
    "Review Pengajuan Template",
    "Terima Pengajuan Template",
    ""
    ];

  let status = "";

  switch (row.status) {
    case "Draft Pengajuan Template":
      status = 0;
      break;

    case "Review Pengajuan Template":
      status = 1;
      break;

      case "Terima Pengajuan Template":
      status = 2;
      break;

    default:
      status = 3;
      break;
  }
  const getLabelCssClasses = () => {
    if (row.status === "") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}