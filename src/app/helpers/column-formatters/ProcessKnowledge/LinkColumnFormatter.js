// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
import React from "react";
const {SLICE_URL, DOWNLOAD_URL, SLICE_FILE, FILE_URL, URL_DOWNLOAD} = window.ENV;


export function AddKnowledgeReferensiLinkColumnFormatter(cellContent, row) {
  return (
    <a href={row.link} target="_blank" rel="noopener noreferrer">{row.link}</a>
  );
}
export function FileColumnFormatterProcessKnowledgeSuccessStoryUploadLampiran(cellContent, row) {
  // const temp = row.link.slice(SLICE_FILE);
  // const url = BACKEND_URL + temp;
  return (
    // <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
    // <a href={row.link} target="_blank" rel="noopener noreferrer">{row.link.slice(SLICE_URL)}</a>
    // <a href={DOWNLOAD_URL+row.link.slice(SLICE_URL)} target="_blank" rel="noopener noreferrer">{row.link.slice(SLICE_URL)}</a>
    <a href={URL_DOWNLOAD + row.link} target="_blank" rel="noopener noreferrer">{row.link}</a>
    
    );
}
// export function RevalidateFileValidasiColumnFormatter(cellContent, row) {
//   return (
//     <a href={row.file_upload} target="_blank" rel="noopener noreferrer">Lihat</a>
//   );
// }

export function FileColumnFormatterProcessKnowledgeMappingPeraturanDokumenLainnya(
  cellContent,
  row,
  rowIndex
) {

  const str = row.file_upload
  const pieces = str.split("/")
  const last = pieces[pieces.length - 1]

    return (
        <>
          <a href={FILE_URL + row.file_upload} target="_blank" rel="noopener noreferrer"
            >
                {last}
          </a>
        </>
      );
}
