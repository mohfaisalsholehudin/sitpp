// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
import React from "react";
const {SLICE_ZIP, DOWNLOAD_URL, SLICE_FILE, BACKEND_URL, BACKEND_URL2, SLICE_URL} = window.ENV;

export function FileColumnFormatterEvaResearch(cellContent, row) {

  const temp = row.file_upload.slice(SLICE_FILE);
  const url = BACKEND_URL + temp;
 
  return (
    // <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
    <a href={url} target="_blank" rel="noopener noreferrer">Lihat</a>
  );
}


export function FileColumnFormatterEvaValidate(cellContent, row) {
  const temp = row.usulan.file_upload.slice(SLICE_FILE);
  const url = BACKEND_URL + temp;
  return (
    <a href={url} target="_blank" rel="noopener noreferrer">Lihat</a>
  );
}
export function FileColumnFormatterEvaValidateKajian(cellContent, row) {
  return (
    <a href={row.file_upload ? DOWNLOAD_URL+row.file_upload.slice(SLICE_ZIP) : null} target="_blank" rel="noopener noreferrer"> {row.file_upload ? 'Download' : ""}</a>
  );
}

export function FileColumnFormatterEvaRevalidateSurat(cellContent, row) {
  const temp = row.usulan.file_upload.slice(SLICE_FILE);
  const url = BACKEND_URL + temp;
  return (
    <a href={url} target="_blank" rel="noopener noreferrer">Lihat</a>
  );
}

export function FileColumnFormatterEvaRevalidateValidasi(cellContent, row) {
  return (
    <a href={DOWNLOAD_URL+row.file_upload.slice(SLICE_ZIP)} target="_blank" rel="noopener noreferrer">Download</a>
  );
}
