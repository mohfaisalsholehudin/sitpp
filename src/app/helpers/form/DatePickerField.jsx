import React, { forwardRef, useState } from "react";
import { useField, useFormikContext } from "formik";
import DatePicker from "react-datepicker";

const getFieldCSSClasses = (touched, errors) => {
  const classes = ["form-control"];
  if (touched && errors) {
    classes.push("is-invalid");
  }

  if (touched && !errors) {
    classes.push("is-valid");
  }

  return classes.join(" ");
};


export function DatePickerField({ ...props }) {
  const { setFieldValue, touched, errors } = useFormikContext();
  const [field] = useField(props);
  const handleChange = (val) => {
    // let date = new Date(field.value)
    // console.log(val)
    if (val) {
      val.setHours((-1 * val.getTimezoneOffset()) / 60);
    }
    setFieldValue(field.name, val)
    // console.log(val)
  }
  const CustomDate = forwardRef(({ value, onClick, onChange }, ref) => {
    return (
      <div>
        <div className="input-group input-group-lg ">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="far fa-calendar-alt"></i>
            </span>
          </div>
          <input
            type="text"
            placeholder={`Pilih ${props.label}`}
            className={getFieldCSSClasses(
              touched[field.name],
              errors[field.name]
            )}
            {...field}
            {...props}
            name={field.name}
            onClick={onClick}
            onChange={onChange}
            ref={ref}
            value={value}
          />
        </div>
        {errors[field.name] && touched[field.name] ? (
          <div className="invalid-feedback" style={{ display: "block" }}>
            {errors[field.name].toString()}
          </div>
        ) : null}
      </div>
    );
  });
  return (
    <>
      {props.label && (
        <label className="col-xl-3 col-lg-3 col-form-label">
          {props.label}
        </label>
      )}
      <div className="col-lg-9 col-xl-6">
        <DatePicker
          style={{ width: "100%" }}
          {...field}
          {...props}
          selected={(field.value && new Date(field.value)) || null}
          dateFormat="dd-MM-yyyy"
          customInput={<CustomDate />}
          // onChange={(val) => {
          //   setFieldValue(field.name, val);
          // }}
          onChange={(val)=>handleChange(val)}
          // popperClassName="some-custom-class"
          // popperPlacement="right-end"
          // popperModifiers={[
          //   {
          //     name: "offset",
          //     options: {
          //       offset: [5, 10],
          //     },
          //   },
          //   {
          //     name: "preventOverflow",
          //     options: {
          //       rootBoundary: "viewport",
          //       tether: false,
          //       altAxis: true,
          //     },
          //   },
          // ]}
        />
      </div>
    </>
  );
}


export function DatePickerFieldSplit({ ...props }) {
  const { setFieldValue, touched, errors } = useFormikContext();
  const [field] = useField(props);

  const CustomDate = forwardRef(({ value, onClick, onChange }, ref) => {
    return (
      <div>
        <div className="input-group input-group-lg ">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="far fa-calendar-alt"></i>
            </span>
          </div>
          <input
            type="text"
            placeholder={`Pilih ${props.label}`}
            className={getFieldCSSClasses(
              touched[field.name],
              errors[field.name]
            )}
            {...field}
            {...props}
            name={field.name}
            onClick={onClick}
            onChange={onChange}
            ref={ref}
            value={value}
          />
        </div>
        {errors[field.name] && touched[field.name] ? (
          <div className="invalid-feedback" style={{ display: "block" }}>
            {errors[field.name].toString()}
          </div>
        ) : null}
      </div>
    );
  });
  return (
    <>
      {props.label && (
        <label >
          {props.label}
        </label>
      )}
        <DatePicker
          style={{ width: "100%" }}
          {...field}
          {...props}
          selected={(field.value && new Date(field.value)) || null}
          dateFormat="dd-MM-yyyy"
          customInput={<CustomDate />}
          onChange={(val) => {
            setFieldValue(field.name, val);
          }}
        />
    </>
  );
}

export function DatePickerFieldRowStart({ ...props }) {
  const { setFieldValue, touched, errors } = useFormikContext();
  const [field] = useField(props);
  // const [startDate, setStartDate] = useState(new Date());
  // const [endDate, setEndDate] = useState(new Date());


  const CustomDate = forwardRef(({ value, onClick, onChange }, ref) => {
    return (
      <div>
        <div className="input-group input-group-lg ">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="far fa-calendar-alt"></i>
            </span>
          </div>
          <input
            type="text"
            placeholder={`Pilih ${props.label}`}
            // className={getFieldCSSClasses(
            //   touched[field.name],
            //   errors[field.name]
            // )}
            className={"form-control"}
            {...field}
            {...props}
            name={field.name}
            onClick={onClick}
            onChange={onChange}
            ref={ref}
            value={value}
          />
        </div>
        {errors[field.name] && touched[field.name] ? (
          <div className="invalid-feedback" style={{ display: "block" }}>
            {errors[field.name].toString()}
          </div>
        ) : null}
      </div>
    );
  });
  return (
    <>
      {props.show === 'true' && (
        <label className="col-xl-3 col-lg-3 col-form-label">
          {props.label}
        </label>
      )}
      <div className="col-lg-3 col-xl-3">
        <DatePicker
          style={{ width: "100%" }}
          {...field}
          {...props}
          // selected={(field.value && new Date(field.value)) || null}
          selected={props.selected}
          dateFormat="dd-MM-yyyy"
          customInput={<CustomDate />}
          // onChange={(val) => {
          //   setFieldValue(field.name, val);
          // }}
          onChange={(val) => {
            // props.setstartdate(val);
            setFieldValue(field.name, val);
          }}
          startDate={props.selected}
          endDate={props.enddate}
          selectsStart
          // popperClassName="some-custom-class"
          // popperPlacement="right-end"
          // popperModifiers={[
          //   {
          //     name: "offset",
          //     options: {
          //       offset: [5, 10],
          //     },
          //   },
          //   {
          //     name: "preventOverflow",
          //     options: {
          //       rootBoundary: "viewport",
          //       tether: false,
          //       altAxis: true,
          //     },
          //   },
          // ]}
        />
      </div>
    </>
  );
}

export function DatePickerFieldRowEnd({ ...props }) {
  const { setFieldValue, touched, errors } = useFormikContext();
  const [field] = useField(props);
  // const [startDate, setStartDate] = useState(new Date());
  // const [endDate, setEndDate] = useState(new Date());


  const CustomDate = forwardRef(({ value, onClick, onChange }, ref) => {
    return (
      <div>
        <div className="input-group input-group-lg ">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="far fa-calendar-alt"></i>
            </span>
          </div>
          <input
            type="text"
            placeholder={`Pilih ${props.label}`}
            // className={getFieldCSSClasses(
            //   touched[field.name],
            //   errors[field.name]
            // )}
            className={"form-control"}
            {...field}
            {...props}
            name={field.name}
            onClick={onClick}
            onChange={onChange}
            ref={ref}
            value={value}
          />
        </div>
        {errors[field.name] && touched[field.name] ? (
          <div className="invalid-feedback" style={{ display: "block" }}>
            {errors[field.name].toString()}
          </div>
        ) : null}
      </div>
    );
  });
  return (
    <>
      {props.show === 'true' && (
        <label className="col-xl-3 col-lg-3 col-form-label">
          {props.label}
        </label>
      )}
      <div className="col-lg-3 col-xl-3">
        <DatePicker
          style={{ width: "100%" }}
          {...field}
          {...props}
          // selected={(field.value && new Date(field.value)) || null}
          selected={props.selected}
          dateFormat="dd-MM-yyyy"
          customInput={<CustomDate />}
          // onChange={(val) => {
          //   setFieldValue(field.name, val);
          // }}
          onChange={(val) => {
            // props.setenddate(val);
            setFieldValue(field.name, val);
          }}
          startDate={props.startdate}
          endDate={props.selected}
          minDate={props.startdate}
          selectsEnd
          // popperClassName="some-custom-class"
          // popperPlacement="right-end"
          // popperModifiers={[
          //   {
          //     name: "offset",
          //     options: {
          //       offset: [5, 10],
          //     },
          //   },
          //   {
          //     name: "preventOverflow",
          //     options: {
          //       rootBoundary: "viewport",
          //       tether: false,
          //       altAxis: true,
          //     },
          //   },
          // ]}
        />
      </div>
    </>
  );
}