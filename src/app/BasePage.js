import React, { Suspense, useEffect, useState, lazy } from "react";
import { useSelector } from "react-redux";
import { Redirect, Switch, Route } from "react-router-dom";
import { LayoutSplashScreen, ContentRoute } from "../_metronic/layout";
// import { BuilderPage } from "./pages/BuilderPage";
import { MyPage } from "./pages/MyPage";
import { DashboardPage } from "./pages/DashboardPage";
import axios from "axios";
import ErrorsPage from "./modules/ErrorsExamples/ErrorsPage";
import Pengumuman from "./pages/Pengumuman/Pengumuman";
import PengumumanDetil from "./pages/Pengumuman/PengumumanDetil";

// import Evaluation from "./modules/Evaluation/Evaluation"
// import {Administrator} from "./modules/Administrator/Administrator"
// import Administrator from "./modules/Administrator/Administrator"

const EvaluationPage = lazy(() => import("./modules/Evaluation/Evaluation"));

const PlanningPage = lazy(() => import("./modules/Planning/Planning"));

const ComposingPage = lazy(() => import("./modules/Composing/Composing"));

const KnowledgePage = lazy(() => import("./modules/Knowledge/Knowledge"));

const AdministratorPage = lazy(() =>
  import("./modules/Administrator/Administrator")
);

const ProcessKnowledgePage = lazy(() =>
  import("./modules/ProcessKnowledge/ProcessKnowledge")
);

export default function BasePage() {
  // useEffect(() => {
  //   console.log('Base page');
  // }, []) // [] - is required if you need only one call
  // https://reactjs.org/docs/hooks-reference.html#useeffect
  // const {REACT_APP_MENU_URL} = process.env;
  // const [menu, setMenu] = useState([])
  const { role, user } = useSelector((state) => state.auth);
  const konseptor = role.includes("ROLE_PERATURAN_KONSEPTOR");
  const es4 = role.includes("ROLE_PERATURAN_PENELITI_LVL1");
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const admin = role.includes("ROLE_ADMIN_PERATURAN");
  const isGranted =
    user.kantor === "Direktorat Peraturan Perpajakan I" ||
    user.kantor === "Direktorat Peraturan Perpajakan II" ||
    user.kantor === "Direktorat Perpajakan Internasional" ||
    user.nip9 === "917330342";

    // console.log(user)

  // const getMenu = async () => {
  //   try {
  //     const data = {
  //           role:['99','8'],
  //           route: 'dashboard'
  //     }
  //   const response = await axios.post(`${REACT_APP_MENU_URL}`, data);
  //   return response;
  // } catch (err) {
  //   if (err.response) {
  //      console.log(err.response);
  //   } else {
  //     console.log(err);
  //   }
  // }
  // };

  // useEffect(() => {
  //   // getMenu()
  //   // .then(({data: {data}})=> {
  //   //   setMenu(data.menu);
  //   // })
  // }, [])

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          <Redirect exact from="/" to="/dashboard" />
        }
        {/* {isGranted ? (
          <ContentRoute path="/dashboard" component={DashboardPage} />
        ) : (
          <Redirect to="error/error-v1" />
        )} */}
        {/* <ContentRoute path="/dashboard" component={isGranted ? DashboardPage : ErrorsPage} /> */}
        <ContentRoute path="/dashboard" component={DashboardPage} />

        {/* <ContentRoute path="/dashboard" component={DashboardPage} /> */}
        <Route path="/evaluation" component={EvaluationPage} />
        <Route path="/plan" component={PlanningPage} />
        <Route path="/compose" component={ComposingPage} />
        <Route path="/knowledge" component={KnowledgePage} />
        <Route path="/admin" component={AdministratorPage} />
        <Route path="/process-knowledge" component={ProcessKnowledgePage} />
        <ContentRoute path="/announcement/:id/detail" component={PengumumanDetil}/>
        <ContentRoute path="/announcement" component={Pengumuman}/>

        {/* <ContentRoute path="/builder" component={BuilderPage} /> */}
        <ContentRoute path="/my-page" component={MyPage} />
        <Redirect to="error/error-v1" />
      </Switch>
    </Suspense>
  );
}
