import axios from "axios";
// import { useSelector } from "react-redux";
// const use = useSelector;
const { BACKEND_URL } = window.ENV;
// const { iamToken } = useSelector(state => state.auth);


export function uploadKmi(formData) {
    const config = {
      headers: {
        "Content-type": "multipart/form-data"
      }
    };
    return axios.post(`${BACKEND_URL}/api/uploadkmi`, formData, config);
  }

/* Begin Bisnis Proses */
export function getProbis() {
    return axios.get(`${BACKEND_URL}/api/bisnisproses/`);
}
export function getProbisById(id_probis){
    return axios.get(`${BACKEND_URL}/api/bisnisproses/${id_probis}`)
}
export function getProbisByStatus(status){
    return axios.get(`${BACKEND_URL}/api/bisnisproses/bystatus/${status}`)
}

export function saveProbis(nama, keterangan, status, ctas){
    return axios.post(`${BACKEND_URL}/api/bisnisproses/`, {
       nama, keterangan,status, ctas
      });
}

export function updateProbis(id, nama, keterangan, status, ctas){
    return axios.put(`${BACKEND_URL}/api/bisnisproses/${id}`,{
        nama, keterangan, status, ctas
    })
}

export function getCaseName(){
    return axios.get(`${BACKEND_URL}/api/casename/`)
}

export function getCaseNameById(id_casename) {
    return axios.get(`${BACKEND_URL}/api/casename/${id_casename}`)
}
export function getCaseNameByIdProbis(id_probis) {
    return axios.get(`${BACKEND_URL}/api/casename/probis/${id_probis}`)
}

export function saveCaseName(id_probis, nama, keterangan, status, ctas){
    return axios.post(`${BACKEND_URL}/api/casename/`, {
       id_probis,nama, keterangan,status, ctas
      });
}
export function updateCaseName(id_casename, id_probis, nama, keterangan, status, ctas){
    return axios.put(`${BACKEND_URL}/api/casename/${id_casename}`, {
       id_probis,nama, keterangan,status, ctas
      });
}

export function getSubcaseByIdCasename(id_casename){
    return axios.get(`${BACKEND_URL}/api/subcase/bycasename/${id_casename}`)
}
export function getSubcaseById(id_subcase){
    return axios.get(`${BACKEND_URL}/api/subcase/${id_subcase}`)
}
export function getSubcase(){
    return axios.get(`${BACKEND_URL}/api/subcase/`)
}
export function saveSubcase(id_case_name, nama, keterangan, status){
    return axios.post(`${BACKEND_URL}/api/subcase/`, {
       id_case_name,nama, keterangan,status
      });
}
export function updateSubcase(id_subcase, id_case_name, nama, keterangan, status){
    return axios.put(`${BACKEND_URL}/api/subcase/${id_subcase}`, {
       id_case_name,nama, keterangan,status
      });
}
/* End of Bisnis Proses */

/* Begin Bisnis Sektor */

export function getBisnisSektor(){
    return axios.get(`${BACKEND_URL}/api/bisnisektor/`)
}
export function getBisnisSektorByStatus(status){
    return axios.get(`${BACKEND_URL}/api/bisnisektor/bystatus/${status}`)
}

export function getBisnisSektorById(id){
    return axios.get(`${BACKEND_URL}/api/bisnisektor/${id}`)
}
export function saveBisnisSektor(nama, keterangan, status){
    return axios.post(`${BACKEND_URL}/api/bisnisektor/`, {
       nama, keterangan,status
      });
}
export function updateBisnisSektor(id, nama, keterangan, status){
    return axios.put(`${BACKEND_URL}/api/bisnisektor/${id}`, {
       nama, keterangan,status
      });
}

/* End of Bisnis Proses */

/* Begin Tipe Knowledge */

export function getTipeKnowledge(){
    return axios.get(`${BACKEND_URL}/api/tipeknowledge/`)
}

export function getTipeKnowledgeByStatus(){
    return axios.get(`${BACKEND_URL}/api/tipeknowledge/bystatus/1`)
}

export function getTipeKnowledgeById(id){
    return axios.get(`${BACKEND_URL}/api/tipeknowledge/${id}`)
}
export function saveTipeKnowledge(nama, jenis, template, keterangan, status){
    return axios.post(`${BACKEND_URL}/api/tipeknowledge/`, {
       nama, jenis, template, keterangan, status
      });
}
export function updateTipeKnowledge(id, nama, jenis, template, keterangan, status){
    return axios.put(`${BACKEND_URL}/api/tipeknowledge/${id}`, {
       nama, jenis, template, keterangan, status
      });
}

/* End of Tipe Knowledge */


/* Begin Tusi */

export function getTusi(){
    return axios.get(`${BACKEND_URL}/api/tusi/`)
}
export function getTusiById(id){
    return axios.get(`${BACKEND_URL}/api/tusi/${id}`)
}
export function saveTusi(nama, keterangan, status){
    return axios.post(`${BACKEND_URL}/api/tusi/`, {
       nama, keterangan, status
      });
}
export function updateTusi(id, nama, keterangan, status){
    return axios.put(`${BACKEND_URL}/api/tusi/${id}`, {
       nama, keterangan, status
      });
}

/* End of Tusi */

/* Begin Penjamin Kualitas Pengetahuan*/

export function getPkp(){
    return axios.get(`${BACKEND_URL}/api/pkp/`)
}
export function getPkpById(id){
    return axios.get(`${BACKEND_URL}/api/pkp/${id}`)
}
export function getPkpDetilById(id){
    return axios.get(`${BACKEND_URL}/api/pkpdetil/${id}`)
}

export function getPkpByNip(nip){
    return axios.get(`${BACKEND_URL}/api/pkp/bynip/${nip}`)
}

export function getPkpDetilByIdPkp(id){
    return axios.get(`${BACKEND_URL}/api/pkpdetil/byidpkp/${id}`)
}
export function savePkp(nip, nama, unit, seksi, kodeseksi, kodeunit){
    // return axios.post(`${BACKEND_URL}/api/pkp/`, {
    //    nama, nip, seksi, unit, kodeseksi, kodeunit
    //   });
    return axios.post(`${BACKEND_URL}/api/pkp/`, {
        nip, nama, unit, seksi
       });
}

export function savePkpDetil(id_pkp, id_probis){
    return axios.post(`${BACKEND_URL}/api/pkpdetil/`,{
        id_pkp, id_probis
    })
}

export function updatePkp(id, nip, nama, unit, seksi, kodeseksi, kodeunit){
    return axios.put(`${BACKEND_URL}/api/pkp/${id}`,{
        nip, nama, unit, seksi
    })
}

export function deletePkp(id){
    return axios.delete(`${BACKEND_URL}/api/pkp/${id}`)
}

export function deletePkpDetil(id){
    return axios.delete(`${BACKEND_URL}/api/pkpdetil/${id}`)
}

export function getNotifPkp(name){
    return axios.get(`${BACKEND_URL}/api/knowledgeproses/notifpkpbyname/${name}`)
}

/* End of Penjamin Kualitas Pengetahuan */

/* Begin Knowledge Owner*/

export function getKowner(){
    return axios.get(`${BACKEND_URL}/api/kowner/`)
}
export function getKownerByUnit(es4, es3, es2, jabatan){
    return axios.get(`${BACKEND_URL}/api/kowner/byunit/${es4}/${es3}/${es2}/${jabatan}`)
}
export function getNotifKo(name){
    return axios.get(`${BACKEND_URL}/api/knowledgeproses/notifkobyname/${name}`)
}

export function getKownerById(id_ko){
    return axios.get(`${BACKEND_URL}/api/kowner/${id_ko}`)
}

export function getKownerDetilById(id_ko){
    return axios.get(`${BACKEND_URL}/api/kownerdetil/idKo/${id_ko}`)
}

export function saveKowner(nm_es2, nm_es3, nm_es4, kd_unit_es2, kd_unit_es3, kd_unit_es4){
    return axios.post(`${BACKEND_URL}/api/kowner/`, {
       nm_es2, nm_es3, nm_es4, kd_unit_es2, kd_unit_es3, kd_unit_es4});
}

export function updateKowner(id_ko, nm_es2, nm_es3, nm_es4, kd_unit_es2, kd_unit_es3, kd_unit_es4){
    return axios.put(`${BACKEND_URL}/api/kowner/${id_ko}`, {
       nm_es2, nm_es3, nm_es4, kd_unit_es2, kd_unit_es3, kd_unit_es4});
}

export function saveKownerDetil(id_csname, id_ko){
    return axios.post(`${BACKEND_URL}/api/kownerdetil/`, {
        id_csname, id_ko
      });
}

export function deleteKownerDetil(id){
    return axios.delete(`${BACKEND_URL}/api/kownerdetil/${id}`)
}


/* End of Knowledge Owner */

/* Begin Subject Matter Expert*/

export function getSme(){
    return axios.get(`${BACKEND_URL}/api/sme/`)
}
export function getSmeById(id_sme){
    return axios.get(`${BACKEND_URL}/api/sme/${id_sme}`)
}
export function getSmeByNip(nip){
    return axios.get(`${BACKEND_URL}/api/sme/bynip/${nip}`)
}

export function getSmeByIdSme(id_sme){
    return axios.get(`${BACKEND_URL}/api/smedetil/idSme/${id_sme}`)
}
export function saveSme(nip, nama, nm_unit, nm_jabatan){
    return axios.post(`${BACKEND_URL}/api/sme/`, {
        nip, nama, nm_unit, nm_jabatan
       });
}
export function updateSme(id, nip, nama, nm_unit, nm_jabatan){
    return axios.put(`${BACKEND_URL}/api/sme/${id}`,{
        nip, nama, nm_unit, nm_jabatan
    })
}
export function saveSmeDetil(id_csname, id_sme){
    return axios.post(`${BACKEND_URL}/api/smedetil/`, {
        id_csname, id_sme
      });
}
export function deleteSmeDetil(id){
    return axios.delete(`${BACKEND_URL}/api/smedetil/${id}`)
}
/* End of Subject Matter Expert */

/* Begin Pengelola Portal Pengetahuan*/

export function getPengPortal(){
    return axios.get(`${BACKEND_URL}/api/pengportal/`)
}

export function getPengPortalById(id_pp_kms){
    return axios.get(`${BACKEND_URL}/api/pengportal/${id_pp_kms}`)
}

export function savePengPortal(nm_unit_es2, nm_unit_es3, nm_unit_es4, kd_unit_es2, kd_unit_es3, kd_unit_es4){
    return axios.post(`${BACKEND_URL}/api/pengportal/`,{nm_unit_es2, nm_unit_es3, nm_unit_es4, kd_unit_es2, kd_unit_es3, kd_unit_es4})
}

export function updatePengPortal(id_pp_kms, nm_unit_es2, nm_unit_es3, nm_unit_es4, kd_unit_es2, kd_unit_es3, kd_unit_es4){
    return axios.put(`${BACKEND_URL}/api/pengportal/${id_pp_kms}`,{nm_unit_es2, nm_unit_es3, nm_unit_es4, kd_unit_es2, kd_unit_es3, kd_unit_es4})
}

/* End of Pengelola Portal Pengetahuan */


export function saveTusiKm(id_km_pro, id_tusi_km){
    return axios.post(`${BACKEND_URL}/api/tusiknowledge/`,{id_km_pro, id_tusi_km})
}

/* Begin Tambah Knowledge*/
export function getKnowledgeProcess() {
    return axios.get(`${BACKEND_URL}/api/knowledgeproses/`)
}

export function getKnowledgeProcessByNip(nip){
    return axios.get(`${BACKEND_URL}/api/knowledgeproses/bynip/${nip}`)
}

export function getKnowledgeProcessByNipStatus(values){
    return axios.post(`${BACKEND_URL}/api/knowledgeproses/bynipandstatus`,values)
}

export function getKnowledgeProcessByMenu(values){
    return axios.post(`${BACKEND_URL}/api/knowledgeproses/bynipandstatusandmenu`,values)
}

export function getKnowledgeProcessById(id_km_pro){
    return axios.get(`${BACKEND_URL}/api/knowledgeproses/${id_km_pro}`)
}

export function saveKnowledgeProcess(id_tipe_km, tipe_konten, jenis, nip_kontri, nama_kontri, kodekantor_kontri, nm_unit_kontri, jns_menu_knowledge){
    return axios.post(`${BACKEND_URL}/api/knowledgeproses/`,{id_tipe_km, tipe_konten, jenis, nip_kontri, nama_kontri, kodekantor_kontri, created_by: nip_kontri, nm_unit_kontri, jns_menu_knowledge})
}

export function updateKnowledgeProcess(id, id_tipe_km, tipe_konten, id_sektor, id_probis, id_csname, id_subcase, judul, updated_by, link_djpforum){
    return axios.put(`${BACKEND_URL}/api/knowledgeproses/${id}`,{id_tipe_km, tipe_konten, id_sektor, id_probis, id_csname, id_subcase, judul, updated_by, link_djpforum})
}

export function updateKnowledgeProcessMedia(id, id_tipe_km, tipe_konten, id_sektor, id_probis, id_csname, id_subcase, judul, file, updated_by, link_djpforum){
    return axios.put(`${BACKEND_URL}/api/knowledgeproses/${id}`,{id_tipe_km, tipe_konten, id_sektor, id_probis, id_csname, id_subcase, judul, media_upload: file, updated_by, link_djpforum})
}

export function updateKnowledgeProcessKategoriLevel(id_km_pro, kategori, level_knowledge){
    return axios.put(`${BACKEND_URL}/api/knowledgeproses/${id_km_pro}`,{kategori, level_knowledge})
}

export function updateStatusKnowledgeProcess(id_km_pro, status, cat_review = null){
    return axios.put(`${BACKEND_URL}/api/knowledgeproses/updateknowledgeprosesstatus/${id_km_pro}/${status}`,{cat_review})
}
export function updateStatusKnowledgeProcessDisposisi(id_km_pro, status, id_ko, alasan = null){
    if(status === 18){
        return axios.put(`${BACKEND_URL}/api/knowledgeproses/updateknowledgeprosesstatus/${id_km_pro}/${status}`,{});
    } else {
        return axios.put(`${BACKEND_URL}/api/knowledgeproses/updateknowledgeprosesstatus/${id_km_pro}/${status}`,
                { id_ko: id_ko, catatan_pkp: alasan }
            );
    } 
}
export function updateStatusKnowledgeProcessDisposisiKo(id_km_pro, status, id_sme, alasan = null){
    if(status === 18){
        return axios.put(`${BACKEND_URL}/api/knowledgeproses/updateknowledgeprosesstatus/${id_km_pro}/${status}`,{});
    } else {
        return axios.put(`${BACKEND_URL}/api/knowledgeproses/updateknowledgeprosesstatus/${id_km_pro}/${status}`,
                { id_sme: id_sme, catatan_ko: alasan }
            );
    } 
}

export function deleteKnowledgeProcess(id_km_pro){
    return axios.delete(`${BACKEND_URL}/api/knowledgeproses/${id_km_pro}`)
}

export function getSuccessStoryByIdKmPro(id_km_pro){
    return axios.get(`${BACKEND_URL}/api/successstory/bykmpro/${id_km_pro}`)
}

export function saveSuccessStory(id_km_pro, id_tipe_km, pendahuluan, daftar_isi, definisi, tagging){
    return axios.post(`${BACKEND_URL}/api/successstory/`,{id_km_pro, id_tipe_km, pendahuluan, daftar_isi, definisi, tagging})
}

export function updateSuccessStory(id_ss_temp, id_km_pro, id_tipe_km, pendahuluan, daftar_isi, definisi, tagging){
    return axios.put(`${BACKEND_URL}/api/successstory/${id_ss_temp}`,{id_km_pro, id_tipe_km, pendahuluan, daftar_isi, definisi, tagging})
}

export function updateSuccessStoryMateri(id_ss_temp, materi){
    return axios.put(`${BACKEND_URL}/api/successstory/${id_ss_temp}`,{materi})
}

export function saveSopMateri(id_km_pro, id_tipe_km, materi){
    return axios.post(`${BACKEND_URL}/api/soptemp/`,{id_km_pro, id_tipe_km, materi})
}

export function updateSopMateri(id_sop_temp, materi){
    return axios.put(`${BACKEND_URL}/api/soptemp/${id_sop_temp}`,{materi})
}

export function saveOtherKnowledgeMateri(id_km_pro, id_tipe_km, materi){
    return axios.post(`${BACKEND_URL}/api/defaulttemp/`,{id_km_pro, id_tipe_km, materi})
}

export function updateOtherKnowledgeMateri(id_default_temp, materi){
    return axios.put(`${BACKEND_URL}/api/defaulttemp/${id_default_temp}`,{materi})
}

export function getReferensiByIdKmPro(id_km_pro){
    return axios.get(`${BACKEND_URL}/api/referensi/byidknowledgeproses/${id_km_pro}`)
}

export function getReferensiByIdRefrensi(id_refrensi){
    return axios.get(`${BACKEND_URL}/api/referensi/${id_refrensi}`)
}

export function saveReferensi(id_km_pro, judul, link){
    return axios.post(`${BACKEND_URL}/api/referensi/`,{id_km_pro, judul, link})
}

export function deleteReferensi(id_km_pro){
    return axios.delete(`${BACKEND_URL}/api/referensi/${id_km_pro}`)
}

export function getPeraturanTerkaitByIdKmPro(id_km_pro){
    return axios.get(`${BACKEND_URL}/api/peraturanterkait/byidknowledgeproses/${id_km_pro}`)
}

export function savePeraturanTerkaitKM(id_km_pro, id_peraturan){
    return axios.post(`${BACKEND_URL}/api/peraturanterkait/`,{id_km_pro,id_peraturan})
}

export function deletePeraturanTerkait(id_km_pro){
    return axios.delete(`${BACKEND_URL}/api/peraturanterkait/${id_km_pro}`)
}

export function getUploadTerkaitByIdKmPro(id_km_pro){
    return axios.get(`${BACKEND_URL}/api/uploadterkait/byidknowledgeproses/${id_km_pro}`)
}

export function saveUploadTerkait(id_km_pro, link){
    return axios.post(`${BACKEND_URL}/api/uploadterkait/`,{id_km_pro, link})
}

export function deleteUploadTerkait(id_km_pro){
    return axios.delete(`${BACKEND_URL}/api/uploadterkait/${id_km_pro}`)
}

export function getSopByIdKmPro(id_km_pro){
    return axios.get(`${BACKEND_URL}/api/soptemp/bykmpro/${id_km_pro}`)
}

export function getDasarHukumByIdKmPro(id_km_pro){
    return axios.get(`${BACKEND_URL}/api/dasarhukum/byidknowledgeproses/${id_km_pro}`)
}
export function getDasarHukumByIdDasarHukum(id_dasar_hukum){
    return axios.get(`${BACKEND_URL}/api/dasarhukum/${id_dasar_hukum}`)
}
export function saveDasarHukum(id_km_pro, judul, link){
    return axios.post(`${BACKEND_URL}/api/dasarhukum/`,{id_km_pro, judul, link})
}
export function deleteDasarHukum(id_km_pro){
    return axios.delete(`${BACKEND_URL}/api/dasarhukum/${id_km_pro}`)
}
export function getOtherKnowledgeByIdKmPro(id_km_pro){
    return axios.get(`${BACKEND_URL}/api/defaulttemp/bykmpro/${id_km_pro}`)
}
/* End of Tambah Knowledge */


/* Begin Pencarian Knowledge */
export function getKonten(values, page=1, size=100){
    return axios.post(`${BACKEND_URL}/api/pencariankonten/cariknowledgeproses?page=${page}&size=${size}`,values)
}

export function getFeedbackByIdKmPro(id_km_pro){
    return axios.get(`${BACKEND_URL}/api/feedback/byid_km_pro/${id_km_pro}`)
}
/* End of Pencarian Knowledge */


/* Begin Mapping Knowledge */

export function saveMappingProbis(values) {
    return axios.post(`${BACKEND_URL}/api/mappingprobis/`,values)
}
export function updateMappingProbis(values){
    return axios.put(`${BACKEND_URL}/api/mappingprobis/`,values)
}

export function getMappingProbisByIdKnowledge(id_km_pro){
    return axios.get(`${BACKEND_URL}/api/mappingprobis/byidknowledgeproses/${id_km_pro}`)
}
export function getMappingProbisByIdPeraturan(id_peraturan){
    return axios.get(`${BACKEND_URL}/api/mappingprobis/byidperaturan/${id_peraturan}`)
}

export function deleteMappingProbis(id_map_knowledge){
    return axios.delete(`${BACKEND_URL}/api/mappingprobis/${id_map_knowledge}`)
}

export function getCasenameByArrayProbis(values){
    return axios.post(`${BACKEND_URL}/api/casename/byarrayidprobis`,values)
}

export function saveMappingCasename(values){
    return axios.post(`${BACKEND_URL}/api/mappingcasename/`,values)
}

export function updateMappingCasename(values){
    return axios.put(`${BACKEND_URL}/api/mappingcasename/`,values)
}

export function getMappingCasenameByIdKnowledge(id_km_pro){
    return axios.get(`${BACKEND_URL}/api/mappingcasename/byidknowledgeproses/${id_km_pro}`)
}
export function getMappingCasenameByIdPeraturan(id_peraturan){
    return axios.get(`${BACKEND_URL}/api/mappingcasename/byidperaturan/${id_peraturan}`)
}

export function deleteMappingCasename(id_mapping_casename){
    return axios.delete(`${BACKEND_URL}/api/mappingcasename/${id_mapping_casename}`)
}

export function getSubcaseByArrayCasename(values){
    return axios.post(`${BACKEND_URL}/api/subcase/byarrayidcasename`,values)
}

export function saveMappingSubcase(values){
    return axios.post(`${BACKEND_URL}/api/mappingsubcase/`,values)
}
export function updateMappingSubcase(values){
    return axios.put(`${BACKEND_URL}/api/mappingsubcase/`,values)
}

export function getMappingSubcaseByIdKnowledge(id_km_pro){
    return axios.get(`${BACKEND_URL}/api/mappingsubcase/byidknowledgeproses/${id_km_pro}`)
}
export function getMappingSubcaseByIdPeraturan(id_peraturan){
    return axios.get(`${BACKEND_URL}/api/mappingsubcase/byidperaturan/${id_peraturan}`)
}

export function deleteMappingSubcase(id_map_subcase){
    return axios.delete(`${BACKEND_URL}/api/mappingsubcase/${id_map_subcase}`)
}

export function saveMappingSektor(values){
    return axios.post(`${BACKEND_URL}/api/mappingbisnissektor/`,values)
}

export function updateMappingSektor(values){
    return axios.put(`${BACKEND_URL}/api/mappingbisnissektor/`,values)
}

export function getMappingSektorByIdKnowledge(id_km_pro){
    return axios.get(`${BACKEND_URL}/api/mappingbisnissektor/byidknowledgeproses/${id_km_pro}`)
}
export function getMappingSektorByIdPeraturan(id_peraturan){
    return axios.get(`${BACKEND_URL}/api/mappingbisnissektor/byidperaturan/${id_peraturan}`)
}

export function deleteMappingSektor(id_map_sektor){
    return axios.delete(`${BACKEND_URL}/api/mappingbisnissektor/${id_map_sektor}`)
}
/* End of Mapping Knowledge */


/* Begin Mapping Peraturan */

export function getPeraturanByStatus(page=1, size=30, status){
    return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/all/status?page=${page}&size=${size}&status=${status}`)
  }
  
export function getPeraturanByJudul(page=1, size=30, pencarian){
    return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/terima/by_no?page=${page}&pencarian=${pencarian}&size=${size}`)
  }

  export function getRegulasiById(id) {
    return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/${id}`);
  }
  export function getPeraturanTerkait(id) {
    return axios.get(`${BACKEND_URL}/api/kmperaturanterkait/regulasi/${id}`);
  }

  export function updateRegulasi(
    id,
    alasan_tolak,
    body,
    file_upload,
    id_topik,
    jns_regulasi,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_regulasi,
    perihal,
    status,
    tgl_regulasi
  ) {
    return axios.put(`${BACKEND_URL}/api/kmregulasiperpajakan/${id}`, {
      file_upload,
      alasan_tolak,
      body,
      file_upload,
      id_topik,
      jns_regulasi,
      kd_kantor,
      kd_unit_org,
      nip_pengusul,
      nip_pjbt_es3,
      nip_pjbt_es4,
      no_regulasi,
      perihal,
      status,
      tgl_regulasi,
    });
  }

  export function saveBodyRegulasi(id, body) {
    return axios.put(`${BACKEND_URL}/api/kmregulasiperpajakan/updatebody/${id}`, {
      body: body,
    });
  }

  export function getJenisPeraturan() {
    return axios.get(`${BACKEND_URL}/api/jnsperaturan/`);
  }
  
  export function getTopik() {
    //return axios.get(`{BACKEND_URL}/api/reftopik/`);
    return axios.get(`${BACKEND_URL}/api/detiljenis/byidjenis/1`);
  }

  export function getDokumenPer(id) {
    return axios.get(`${BACKEND_URL}/api/kmdoklainnya/per/${id}`);
  }

  export function addPerTerkaitById(refKm, id, idPer) {
    if (refKm === 1) {
      return axios.post(`${BACKEND_URL}/api/kmperaturanterkait/${refKm}`, {
        id_peraturan: id,
        id_peraturan_terkait: idPer,
      });
    } else if (refKm === 3) {
      return axios.post(`${BACKEND_URL}/api/kmperaturanterkait/${refKm}`, {
        id_kajian: id,
        id_peraturan_terkait: idPer,
      });
    } else if (refKm === 4) {
      return axios.post(`${BACKEND_URL}/api/kmperaturanterkait/${refKm}`, {
        id_putusan: id,
        id_peraturan_terkait: idPer,
      });
    }
  }

  export function deletePeraturanTerkaitMapping(id) {
    return axios.delete(`${BACKEND_URL}/api/kmperaturanterkait/${id}`);
  }

  export function deleteDokLainMapping(id) {
    return axios.delete(`${BACKEND_URL}/api/kmdoklainnya/${id}`);
  }
  export function uploadFileNew(formData) {
    const config = {
      headers: {
        "Content-type": "multipart/form-data"
      }
    };
    return axios.post(`${BACKEND_URL}/api/newupload`, formData, config);
  }

  export function saveDokLainPeraturanMapping(file_upload, id_per, keterangan) {
    return axios.post(`${BACKEND_URL}/api/kmdoklainnya/1`, {
      file_upload,
      id_per,
      keterangan,
    });
  }
/* End of Mapping Peraturan */



/* Begin Usulan Perubahan */

export function getUsulanPerubahan(){
    return axios.get(`${BACKEND_URL}/api/usulantemplate/`)
}

export function getUsulanPerubahanById(id){
    return axios.get(`${BACKEND_URL}/api/usulantemplate/${id}`)
}

export function deleteUsulanPerubahan(id){
    return axios.delete(`${BACKEND_URL}/api/usulantemplate/${id}`)
}

export function saveUsulanPerubahan(
    isi,
    jenis_usulan,
    judul_usulan,
    nip_pengusul,
    status,
    tipe
    ){
    return axios.post(`${BACKEND_URL}/api/usulantemplate/`,{
        isi,
        jenis_usulan,
        judul_usulan,
        nip_pengusul,
        status,
        tipe
    })
}

export function updateUsulanPerubahan(
    id,
    isi,
    jenis_usulan,
    judul_usulan,
    nip_pengusul,
    status,
    tipe
    ){
    return axios.put(`${BACKEND_URL}/api/usulantemplate/${id}`,{
        isi,
        jenis_usulan,
        judul_usulan,
        nip_pengusul,
        status,
        tipe
    })
}
export function updateStatusUsulanPerubahan(
    id,
    catatan=null,
    status
    ){
    return axios.put(`${BACKEND_URL}/api/usulantemplate/${id}`,{
        catatan,
        status
    })
}

/* End of Usulan Perubahan */


/* Begin Pengumuman */
export function getPengumuman(){
    return axios.get(`${BACKEND_URL}/api/pengumuman/`)
}

export function getPengumumanById(id){
    return axios.get(`${BACKEND_URL}/api/pengumuman/${id}`)
}

export function savePengumuman(isi, judul, target, status){
    return axios.post(`${BACKEND_URL}/api/pengumuman/`,{isi, judul, target, status})
}

export function updatePengumuman(id, isi, judul, target, status){
    return axios.put(`${BACKEND_URL}/api/pengumuman/${id}`,{isi, judul, target, status})
}

export function deletePengumuman(id){
    return axios.delete(`${BACKEND_URL}/api/pengumuman/${id}`)
}
export function updateStatusPengumuman(id, status){
    return axios.put(`${BACKEND_URL}/api/pengumuman/${id}`,{status})
}

export function getPengumumanTarget(){
    return axios.get(`${BACKEND_URL}/api/pengumumantarget/`)
}
export function getPengumumanTargetByIdPengumuman(id_pengumuman){
    return axios.get(`${BACKEND_URL}/api/pengumumantarget/getbyidpengumuman/${id_pengumuman}`)
}

export function savePengumumanTarget(id_pengumuman, nama, nip, nip_pendek, unit_organisasi){
    return axios.post(`${BACKEND_URL}/api/pengumumantarget/`,{id_pengumuman, nama, nip, nip_pendek, unit_organisasi})
}

export function deletePengumumanTarget(id_target){
    return axios.delete(`${BACKEND_URL}/api/pengumumantarget/${id_target}`)
}

export function getPengumumanByNip(nip){
    return axios.get(`${BACKEND_URL}/api/pengumuman/bynipandrole?nip=${nip}`)
}

/* End of Pengumuman */

/* Begin Matrix Knowledge */
    export function getMatrix(){
        return axios.get(`${BACKEND_URL}/api/knowledgeproses/getmatrix`)
    }
/* End of Matrix Knowledge */

