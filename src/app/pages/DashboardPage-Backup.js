import React, {useEffect} from "react";
import { useHistory } from "react-router-dom";
import {useSelector } from "react-redux";
import { checkToken } from "../modules/Evaluation/Api";
import { Builder } from "../../_metronic/_partials/builder/Builder";
export function DashboardPage() {



  const { token, user, role } = useSelector((state) => state.auth);
  const history = useHistory();

const checkStatus = async () => {
  try {
    await checkToken(token).then((data)=> {
      return data;
    }).catch(()=>history.push('/logout'))
  } catch (error) {
    window.alert("Ooops Something Went Wrong !!!")
  }
}

  useEffect(() => {
    // checkStatus();
    // return() => {
    //   checkStatus();
    // }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [history])
  return <>
  <Builder />
  </>;
}
