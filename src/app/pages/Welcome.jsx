import React from "react";
import { Slide } from "react-slideshow-image";
import { Zoom } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";
import "./styles.css";
import { Card, CardBody, CardHeader, Notice } from "./controls";

const {BACKEND_URL} = window.ENV;

// const activeTab = localStorage.getItem(localStorageActiveTabKey);
// const [key, setKey] = useState(activeTab ? +activeTab : 0);
// const saveCurrentTab = (_tab) => {
//   localStorage.setItem(localStorageActiveTabKey, _tab);
// };

const slideImages = [
  {
    url:
      `${BACKEND_URL}/api/filesckeditor/12312312312_0317103552.jpeg`,
    caption: "UU HPP",
  },
  {
    url:
      `${BACKEND_URL}/api/filesckeditor/12311231321_0317104047.jpeg`,
    caption: "Slide 2",
  },
  {
    url:
      `${BACKEND_URL}/api/filesckeditor/12321213231123_0317104145.jpeg`,
    caption: "Slide 3",
  },
];

const Slideshow = () => {
  return (
    <div className="slide-container">
      <Slide>
        {slideImages.map((slideImage, index) => (
          <div className="each-slide" key={index}>
            <div style={{ backgroundImage: `url(${slideImage.url})` }}></div>
          </div>
        ))}
      </Slide>
    </div>
  );
};

export default Slideshow;
