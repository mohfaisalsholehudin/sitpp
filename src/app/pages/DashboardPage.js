import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { checkToken } from "../modules/Evaluation/Api";
import Slideshow from "./Welcome.jsx";
import "./marquee.css";
import { Card, CardBody, CardHeader } from "../../_metronic/_partials/controls";
import Pengumuman from "./Pengumuman/Pengumuman.jsx";
// import Builder from "../../_metronic/_partials/builder/Builder";
export function DashboardPage() {
  //window.location.reload();
  const { token, user, role } = useSelector((state) => state.auth);
  const history = useHistory();
  // window.onload = function() {
  //   if (!window.location.hash) {
  //     window.location = window.location + "#loaded";
  //     window.location.reload();
  //   }
  // };

  const checkStatus = async () => {
    try {
      await checkToken(token)
        .then((data) => {
          return data;
          window.location.reload();
        })
        .catch(() => history.push("/logout"));
    } catch (error) {
      window.alert("Ooops Something Went Wrong !!!");
    }
  };

  useEffect(() => {
    // checkStatus();
    // return() => {
    //   checkStatus();
    // }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [history]);
  return (
    <>
      <Card>
        <CardHeader 
        //  title="Pengumuman"
         style={{ backgroundColor: "#FFC91B" }}>
         {" "}
          <br></br>
          <div className="marquee">
            <div className="marquee__inner" aria-hidden="true">
              <span>
                Selamat Datang di Sistem Informasi Tata Peraturan Perpajakan
              </span>
            </div>
          </div> 
        </CardHeader>
        <CardBody>
          {/* <Pengumuman /> */}
          <Slideshow />
        </CardBody>
      </Card>
    </>
  );
}
