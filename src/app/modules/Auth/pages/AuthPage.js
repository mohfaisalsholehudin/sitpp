/* eslint-disable jsx-a11y/anchor-is-valid */
import React,{useEffect, useState} from "react";
import { Switch, Redirect } from "react-router-dom";
import { ContentRoute } from "../../../../_metronic/layout";
import Login from "./Login";
import { useSelector } from "react-redux";
import "../../../../_metronic/_assets/sass/pages/login/classic/login-1.scss";
import { LayoutSplashScreen } from "../../../../_metronic/layout";
import { withRouter } from "react-router-dom";



import qs from "qs";
import axios from "axios";
import { connect } from "react-redux";
import * as auth from "../_redux/authRedux";
import { getRole, getUserByToken, login } from "../_redux/authCrud";



export function AuthPage(props) {
  const { auth } = useSelector((state) => state.auth);
  const {LOCAL_URL, SSO_URL, BACKEND_URL} = window.ENV;

  const str = "kmi:kmi";
  const pass = btoa(str);
  const [token, setToken] = useState("");

  const data = {
    grant_type: "authorization_code",
    code: props.code,
    // redirect_uri: `${REACT_APP_LOCAL_URL}/auth/login`,
    redirect_uri: `${LOCAL_URL}/auth/login`,
  };

  const config = {
    headers: {
      "Content-type": "application/x-www-form-urlencoded",
      Authorization: `Basic ${pass}`,
    },
  };

  const dataClient = {
    clientId: "kmi",
    clientSecret: "kmi",
    token,
  };

  //After hit this function, you will get access_token
  const postCodeToSso = async () => {
    return await axios.post(
      `${SSO_URL}/oauth/token`,
      // `${REACT_APP_SSO_URL}/oauth/token`,
      qs.stringify(data),
      config
    );
  };

  //Check access_token to SSO, and you will get token IAM
  const checkToken = async () => {
    return await axios.post(
      `${SSO_URL}/oauth/check_token`,
      // `${REACT_APP_SSO_URL}/oauth/check_token`,
      qs.stringify(dataClient),
      config
    );
  };

  const [loading] = useState(true);
  // useEffect(() => {
  //   const response = postCodeToSso();
  //   response
  //     .then(({ data: { access_token } }) => {
  //       setToken(access_token);
  //       //set token sso
  //       props.sso(access_token);
  //     })
  //     .catch((e) => {
  //       console.log(e);
  //     });
  // // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  // useEffect(() => {
  //   if (auth !== undefined) {
  //     const response = checkToken();
  //     response
  //       .then(({ data, status }) => {
  //         if (status === 200) {
  //           props.getTokenIam(data.token_iam);
  //           const options = { headers: { 'Authorization' : data.token_iam}}
  //           axios
  //             .post(`${BACKEND_URL}/api/iam/getwhoami`, {}, options)
  //             .then(({ data }) => {
  //               props.fulfillUser(data.data);
  //             })
  //             getRole(data.token_iam).then(({data})=> {
  //               props.setRole(data)
  //             })
  //             .catch((e) => {
  //               console.log(e);
  //             });
              
  //         }
  //       })
  //       .catch((e) => {
  //         console.log(e);
  //       });
  //   }
  // // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [auth]);

  return (
    <>
     {/* {loading ? (
        <LayoutSplashScreen />
      ) : (
        <div className="d-flex flex-column flex-root">
          <div
            className="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-row-fluid bg-white"
            id="kt_login"
          >
            <div
              className="flex-row-fluid d-flex flex-column position-relative p-7 overflow-hidden"
              style={{ backgroundColor: "#212C5F" }}
            >
              <div
                className="d-flex flex-column-fluid flex-center mt-30 mt-lg-0"
                style={{ backgroundColor: "#212C5F" }}
              ></div>
              <div className="d-flex d-lg-none flex-column-auto flex-column flex-sm-row justify-content-between align-items-center mt-5 p-5"></div>
            </div>
          </div>
        </div>
      )} */}
    <div className="d-flex flex-column flex-root">
      {/* begin::Login */}
      <div
          className="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-row-fluid bg-white"
          id="kt_login"
      >
       
        <div className="flex-row-fluid d-flex flex-column position-relative p-7 overflow-hidden" style={{backgroundColor: "#263787"}}>
          
          <div className="d-flex flex-column-fluid flex-center mt-30 mt-lg-0" style={{backgroundColor: "#263787"}}>
            <Switch>
            <ContentRoute path="/auth/login" component={Login}/>
            <Redirect from="/auth" exact={true} to="/auth/login"/>
            <Redirect to="/auth/login"/>
          </Switch>
          </div>
          <div
              className="d-flex d-lg-none flex-column-auto flex-column flex-sm-row justify-content-between align-items-center mt-5 p-5">
          </div>
        </div>
      </div>
    </div>
  </>
  );
}

export default withRouter(connect(null, auth.actions)(AuthPage));
