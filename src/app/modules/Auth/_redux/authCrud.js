import axios from "axios";
const {BACKEND_URL} = window.ENV;

// export const LOGIN_URL = "api/auth/login";
export const LOGIN_URL = `${BACKEND_URL}/api/iam/auth`
// export const LOGIN_URL = `/api/iam/auth`
export const REGISTER_URL = "api/auth/register";
export const REQUEST_PASSWORD_URL = "api/auth/forgot-password";

export const ME_URL = `${BACKEND_URL}/api/iam/getwhoami`;
// export const ME_URL = `/api/iam/getwhoami`;
// export const GET_ROLE = `/api/iam/getrole`;
export const GET_ROLE = `${BACKEND_URL}/api/iam/getrole`;

export function login(username, password) {
  return axios.post(LOGIN_URL, { username, password });
}

export function register(email, fullname, username, password) {
  return axios.post(REGISTER_URL, { email, fullname, username, password });
}

export function requestPassword(email) {
  return axios.post(REQUEST_PASSWORD_URL, { email });
}

export function getUserByToken(token, jabatan_iri) {

  const options ={ headers: { 'Authorization' : token, "Content-Type": "application/json"}}
  // Authorization head should be fulfilled in interceptor.
  return axios.post(ME_URL,jabatan_iri,options);
}

export function getJabatan(token){
  const options ={ headers: { 'Authorization' : token}}

  return axios.post(`${BACKEND_URL}/api/iam/getjabatan`,{},options)
}

export function getRole(token){
  const options = { headers: { 'Authorization' : token}}

  return axios.post(GET_ROLE,{},options);
}
