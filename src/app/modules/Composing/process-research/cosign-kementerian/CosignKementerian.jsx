import React, { useEffect, useState } from "react";
import {  Route } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";

/* Helpers */
import { Pagination } from "../../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../../../helpers/column-formatters";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../../_metronic/_partials/controls";
import { DateFormat } from "../../../../helpers/DateFormat";
import {
  getPenyusunanById,
  getCosignKementerian
} from "../../../Evaluation/Api";
import CosignKementerianReprocessOpen from "./CosignKementerianOpen";

function CosignKementerianReprocess({
  history,
  match: {
    params: { id }
  }
}) {
  const [detil, setDetil] = useState([]);
  const [content, setContent] = useState([]);
  const showDialog = id_cosign_kl =>
    history.push(
      `/compose/reprocess/detils/${id}/cosign-kementerian/${id_cosign_kl}/open`
    );

  useEffect(() => {
    getCosignKementerian(id).then(({ data }) => {
      setContent(data);
    });
  }, [id]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nama_unit",
      text: "Nama Unit",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_surat",
      text: "Tgl Jawaban",
      sort: true,
      formatter: columnFormatters.DateFormatterComposeProcessCosignDjp,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterComposeReProcessCosignKementerian,
      formatExtraData: {
        showDetil: showDialog
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "nama_unit",
    pageNumber: 1,
    pageSize: 5
  };
  const defaultSorted = [{ dataField: "nama_unit", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  useEffect(() => {
    getPenyusunanById(id).then(({ data }) => {
      setDetil(data);
    });
  }, [id]);
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Cosign Jawaban KL / Asosiasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <>
            <div className="row">
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    No Penyusunan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${detil.no_penyusunan}`}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Jenis Peraturan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${detil.jns_peraturan}`}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Tanggal Penyusunan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${DateFormat(detil.tgl_penyusunan)}`}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Judul Peraturan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${detil.judul_peraturan}`}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </>
          <>
            <PaginationProvider pagination={paginationFactory(pagiOptions)}>
              {({ paginationProps, paginationTableProps }) => {
                return (
                  <>
                    <ToolkitProvider
                      keyField="id_cosign_kl"
                      data={content}
                      columns={columns}
                      search
                    >
                      {props => (
                        <div>
                          <div className="row">
                            <div className="col-lg-6 col-xl-6 mb-3">
                              <SearchBar
                                {...props.searchProps}
                                style={{ width: "500px" }}
                              />
                              <br />
                            </div>
                            <div
                              className="col-lg-6 col-xl-6 mb-3"
                              style={{ textAlign: "right" }}
                            >
                              <button
                                type="button"
                                className="btn btn-light mx3"
                                style={{
                                  float: "right"
                                }}
                                onClick={() =>
                                  history.push(`/compose/reprocess/${id}/detil`)
                                }
                              >
                                <i className="fa fa-arrow-left"></i>
                                Kembali
                              </button>
                            </div>
                          </div>
                          <BootstrapTable
                            {...props.baseProps}
                            wrapperClasses="table-responsive"
                            bordered={false}
                            headerWrapperClasses="thead-light"
                            classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                            defaultSorted={defaultSorted}
                            bootstrap4
                            noDataIndication={emptyDataMessage}
                            {...paginationTableProps}
                          ></BootstrapTable>
                          <Pagination paginationProps={paginationProps} />
                        </div>
                      )}
                    </ToolkitProvider>
                  </>
                );
              }}
            </PaginationProvider>
          </>
          <Route path="/compose/reprocess/detils/:id/cosign-kementerian/:id_cosign_kl/open">
            {({ history, match }) => (
              <CosignKementerianReprocessOpen
                show={match != null}
                id={match && match.params.id}
                id_cosign_kl={match && match.params.id_cosign_kl}
                after={false}
                onHide={() => {
                  history.push(
                    `/compose/reprocess/detils/${id}/cosign-kementerian`
                  );
                }}
                onRef={() => {
                  history.push(
                    `/compose/reprocess/detils/${id}/cosign-kementerian`
                  );
                }}
              />
            )}
          </Route>
        </CardBody>
      </Card>
    </>
  );
}

export default CosignKementerianReprocess;
