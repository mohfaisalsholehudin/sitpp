import React, { useState, useEffect } from "react";
import { useHistory, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../../../../helpers/column-formatters";
import {
  getAnalisisHarmon,
  getDraftComposingById,
  getPenyusunanById,
  updateStatusPenyusunan
} from "../../../Evaluation/Api";
import TindakLanjutSahliDraftOpen from "./TindakLanjutSahliDraftOpen";
import TindakLanjutSahliKajianOpen from "./TindakLanjutSahliKajianOpen";
import ProposalOpen from "../../proposal/ProposalOpen";
import TindakLanjutSahliDraftOpenReprocess from "./TindakLanjutSahliDraftOpen";
import TindakLanjutSahliReject from "./TindakLanjutSahliReject"
import TindakLanjutSahliApprove from "./TindakLanjutSahliApprove";

function TindakLanjutSahliDraftReprocess({ id }) {
  const history = useHistory();
  const [draft, setDraft] = useState([]);
  const { role } = useSelector(state => state.auth);
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");



  const openAnalisis = (id_harmon) => {
      history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-sahli/${id_harmon}/hasil-analisis-harmonisasi`);
  };

  const openDraft = id_draft => {
    history.push(
      `/compose/reprocess/detils/${id}/tindak-lanjut-sahli/${id_draft}/draft`
    );
  };
  const openProcess = () => {
    history.push(
      `/compose/reprocess/detils/${id}/tindak-lanjut-sahli/kajian`
    );
  };
  const showStatus = () => {
    history.push(
      `/compose/reprocess/detils/${id}/tindak-lanjut-sahli/status`
    );

  }
  const handleApprove = (val) => {
    getPenyusunanById(id).then(({ data }) => {
      updateStatusPenyusunan(id, 13, data.kd_kantor, null, null, null, val.catatan_sahli).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace("/compose/reprocess");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(`/compose/reprocess/${id}/process`);
          });
        }
      });
    });
  }
  useEffect(() => {
    getDraftComposingById(id).then(({ data }) => {
      data.map(dt => {
        getPenyusunanById(dt.id_penyusunan).then(({ data }) => {
          getAnalisisHarmon(data.id_penyusunan).then((data_1)=> {
            setDraft(draft => [
              ...draft,
              {
                id_draftperaturan: dt.id_draftperaturan,
                jns_draft: dt.jns_draft,
                file_kajian: data.update_file_kajian,
                status: data.status,
                id_harmon: data_1.data.length > 0 ? data_1.data[0].id_analisis_harmon : null 
              }
            ]);
            
          })
        });
      });
    });
  }, [id]);
  const rejectProposal = () => {
    history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-sahli/reject`);
  };
  const approveProposal = () => {
    history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-sahli/approve`);
  };
  const handleReject = (val) => {
    getPenyusunanById(id).then(({ data }) => {
      updateStatusPenyusunan(id, 11, data.kd_kantor, val.alasan_penolakan).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace("/compose/reprocess");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(`/compose/reprocess`);
          });
        }
      });
    });
  }



  const columns = [
    {
      dataField: "draft_peraturan",
      text: "Draft Peraturan",
      sort: true,
      formatter:
        columnFormatters.FileColumnFormatterComposeProcessTindakLanjutDirektoratDraftPeraturan,
      formatExtraData: {
        openDraft: openDraft
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3"
    },
    {
      dataField: "hasil_analisis",
      text: "Hasil Analisis Harmonisasi",
      sort: true,
      formatter:
        columnFormatters.FileColumnFormatterComposeProcessTindakLanjutDirektoratHasilAnalisis,
      formatExtraData: {
        openAnalisis: openAnalisis
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        textAlign: "center"
      }
    },
    {
      dataField: "kajian",
      text: "Kajian",
      formatter:
        columnFormatters.ActionsColumnFormatterComposeProcessTindakLanjutDirektoratKajian,
      formatExtraData: {
        openProcess: openProcess
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      formatter:
        columnFormatters.StatusColumnFormatterComposeProcessTindakLanjutDirektoratKajian,
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3"
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        es3 ? columnFormatters.ActionsColumnFormatterComposeReProcessTindakLanjutSahliKajianAksiEs3 : columnFormatters.ActionsColumnFormatterComposeReProcessTindakLanjutSahliKajianAksi,
      formatExtraData: {
        openProcess: approveProposal,
        showStatus: showStatus,
        rejectProposal: rejectProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const emptyDataMessage = () => {
    return "No Data";
  };


  return (
    <>
    <>
    <BootstrapTable
          wrapperClasses="table-responsive"
          bordered={false}
          headerWrapperClasses="thead-light"
          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
          keyField="id_draftperaturan"
          data={draft}
          columns={columns}
          bootstrap4
          noDataIndication={emptyDataMessage}
        ></BootstrapTable>
    </>
      {/*Route to Open Draft*/}
      <Route path="/compose/reprocess/detils/:id/tindak-lanjut-sahli/:id_draft/draft">
        {({ history, match }) => (
          <TindakLanjutSahliDraftOpenReprocess
            show={match != null}
            id={match && match.params.id}
            id_draft={match && match.params.id_draft}
            onHide={() => {
              history.push(
                `/compose/reprocess/detils/${id}/tindak-lanjut-sahli`
              );
            }}
            onRef={() => {
              history.push(
                `/compose/reprocess/detils/${id}/tindak-lanjut-sahli`
              );
            }}
          />
        )}
      </Route>
       {/*Route to Open Kajian*/}
       <Route path="/compose/reprocess/detils/:id/tindak-lanjut-sahli/kajian">
        {({ history, match }) => {
          if(match){
            return(
              (
                <TindakLanjutSahliKajianOpen
                  show={match != null}
                  id={match && match.params.id}
                  onHide={() => {
                    history.push(
                      `/compose/reprocess/detils/${id}/tindak-lanjut-sahli`
                    );
                  }}
                  onRef={() => {
                    history.push(
                      `/compose/reprocess/detils/${id}/tindak-lanjut-sahli`
                    );
                  }}
                />
              )
            )
          }
          return(<></>)}}
      </Route>
       {/*Route to Open Status*/}
       <Route path="/compose/reprocess/detils/:id/tindak-lanjut-sahli/status">
        {({ history, match }) => (
          <ProposalOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-sahli`);
            }}
            onRef={() => {
              history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-sahli`);
            }}
          />
        )}
      </Route>
        {/*Route to Reject Proposal*/}
        <Route path="/compose/reprocess/detils/:id/tindak-lanjut-sahli/reject">
        {({ history, match }) => (
          <TindakLanjutSahliReject
            show={match != null}
            id={match && match.params.id}
            reject={handleReject}
            onHide={() => {
              history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-sahli`);
             
            }}
            onRef={() => {
              history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-sahli`);
             
            }}
          />
        )}
      </Route>
        {/*Route to Approve Proposal*/}
        <Route path="/compose/reprocess/detils/:id/tindak-lanjut-sahli/approve">
        {({ history, match }) => (
          <TindakLanjutSahliApprove
            show={match != null}
            id={match && match.params.id}
            approve={handleApprove}
            onHide={() => {
              history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-sahli`);
             
            }}
            onRef={() => {
              history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-sahli`);
             
            }}
          />
        )}
      </Route>
    </>
  );
}

export default TindakLanjutSahliDraftReprocess;
