import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import ReProcessTable from "./ReProcessTable";

function ReProcess() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Penelitian Penyusunan Peraturan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <ReProcessTable />
        </CardBody>
      </Card>
    </>
  );
}

export default ReProcess;
