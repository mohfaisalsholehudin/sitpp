/* Library */
import React, { useEffect, useState } from "react";
import { useHistory, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";

/* Component */
// import ProposalOpen from "./ProposalOpen";
// import ProposalReject from "./ProposalReject";

/* Utility */



function BkfTable() {
  const history = useHistory();
  const { user } = useSelector(state => state.auth);


  const add = () => history.push("/compose/bkf/add");
  const open = id => history.push(`/compose/bkf/${id}/open`);
  const reject = id => history.push(`/compose/bkf/${id}/reject`);
  const edit = id => history.push(`/compose/bkf/${id}/edit`);
  const process = id => history.push(`/compose/bkf/${id}/process`);
//   const applyProposal = id => {
//     applyPerencanaan(id, user.kantorLegacyKode)
//       .then(({status})=> {
//         if (status === 201 || status === 200) {
//           swal("Berhasil", "Data berhasil disimpan", "success").then(
//             () => {
//               history.push("/dashboard");
//               history.replace("/plan/proposal");
//             }
//           );
//         } else {
//           swal("Gagal", "Data gagal disimpan", "error").then(() => {
//             history.push("/dashboard");
//             history.replace("/plan/proposal");
//           });
//         }
//       })
//   };

const date = new Date();

const proposal =[
    {
        id_penyusunan: '1',
        no_bkf: 'No-1/PJ.12/KP.0101/2020/2021',
        tgl_penyusunan: date,
        jns_peraturan: 'PPN',
        judul_peraturan: 'Peraturan 1',
        file: "",
        status: 'Draft'
    },
    {
        id_penyusunan: '2',
        no_bkf: 'No-2/PJ.12/KP.0101/2020/2021',
        tgl_penyusunan: date,
        jns_peraturan: 'PPh',
        judul_peraturan: 'Peraturan 2',
        file: "",

        status: 'Eselon 4'
    },
    {
        id_penyusunan: '3',
        no_bkf: 'No-3/PJ.12/KP.0101/2020/2021',
        tgl_penyusunan: date,
        jns_peraturan: 'Prakerja 3',
        judul_peraturan: 'Peraturan 3',
        file: "",
        status: 'Tolak'
    }
]

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "no_bkf",
      text: "No BKF",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_penyusunan",
      text: "Tgl Penyusunan",
      sort: true,
      formatter: columnFormatters.DateFormatterComposeBkf,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jns_peraturan",
      text: "Jenis Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul_peraturan",
      text: "Judul Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
        dataField: "file",
        text: "File",
        sort: true,
        sortCaret: sortCaret,
        formatter: columnFormatters.FileColumnFormatterComposeBkf,
        headerSortingClasses
      },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatterComposeBkf,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeBkf,
      formatExtraData: {
          openProcess: process
        // openEditDialog: edit,
        // openDeleteDialog: deleteAction,
        // showProposal: open,
        // showReject: reject,
        // applyProposal: applyProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_bkf",
    pageNumber: 1,
    pageSize: 5
  };
  const defaultSorted = [{ dataField: "no_bkf", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: proposal.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

//   useEffect(() => {
//     getPerencanaan().then(({ data }) => {
//       data.map(data => {
//         return data.status !== 'Terima' ? setPlan(plan => [...plan, data]) : null;
//       })
//     });
//   }, []);

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_penyusunan"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right"
                            }}
                            onClick={add}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      {/* <Route path="/plan/proposal/:id/reject">
        {({ history, match }) => (
          <ProposalReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/plan/proposal");
            }}
            onRef={() => {
              history.push("/plan/proposal");
            }}
          />
        )}
      </Route>
      <Route path="/compose/proposal/:id/open">
        {({ history, match }) => (
          <ProposalOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/compose/proposal");
            }}
            onRef={() => {
              history.push("/compose/proposal");
            }}
          />
        )}
      </Route> */}

     
    </>
  );
}

export default BkfTable;
