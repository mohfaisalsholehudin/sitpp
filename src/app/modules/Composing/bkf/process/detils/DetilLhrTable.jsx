import React from "react";
import BootstrapTable from "react-bootstrap-table-next";

import * as columnFormatters from "../../../../../helpers/column-formatters";

function DetilLhrTable() {
  const before = [
    {
      id_pokok: "1",
      pokok: "Pengaturan 1"
    },
    {
      id_pokok: "2",
      pokok: "Pengaturan 2"
    }
  ];
  const after = [
    {
      id_pokok: "1",
      pokok: "Pengaturan 1"
    },
    {
      id_pokok: "2",
      pokok: "Pengaturan 2"
    }
  ];
  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter
    },
    {
      dataField: "pokok",
      text: "Pokok Pengaturan",
      sort: true
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeDetilLhrPokok,
      formatExtraData: {
        openProcess: process
        // openEditDialog: edit,
        // openDeleteDialog: deleteAction,
        // showProposal: open,
        // showReject: reject,
        // applyProposal: applyProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const columnsAfter = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter
    },
    {
      dataField: "pokok",
      text: "Pokok Pengaturan",
      sort: true
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeDetilLhrPokok,
      formatExtraData: {
        openProcess: process
        // openEditDialog: edit,
        // openDeleteDialog: deleteAction,
        // showProposal: open,
        // showReject: reject,
        // applyProposal: applyProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const emptyDataMessage = () => {
    return <div className="text-center">No Data to Display</div>;
  };
  return (
    <>
      <>
        <div className="row">
        <div className="col-lg-6 col-xl-6 mt-4">
        <h4> Pokok Pengaturan Sebelum Pembahasan</h4>
        </div>
          <div
            className="col-lg-6 col-xl-6 mb-3"
            style={{ textAlign: "right" }}
          >
            <button
              type="button"
              className="btn btn-primary ml-3"
              style={{
                float: "right"
              }}
              // onClick={() => history.push(`/compose/proposal/process/detils/${id}/lhr/add`)}
            >
              <i className="fa fa-plus"></i>
              Tambah
            </button>
          </div>
        </div>
        <BootstrapTable
          wrapperClasses="table-responsive"
          bordered={false}
          headerWrapperClasses="thead-light"
          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
          keyField="id_pokok"
          data={before}
          columns={columns}
          bootstrap4
          noDataIndication={emptyDataMessage}
        ></BootstrapTable>
      </>
      <>
      <div className="row mt-8">
        <div className="col-lg-6 col-xl-6 mt-4">
        <h4> Pokok Pengaturan Sesudah Pembahasan</h4>
        </div>
          <div
            className="col-lg-6 col-xl-6 mb-3"
            style={{ textAlign: "right" }}
          >
            <button
              type="button"
              className="btn btn-primary ml-3"
              style={{
                float: "right"
              }}
              // onClick={() => history.push(`/compose/proposal/process/detils/${id}/lhr/add`)}
            >
              <i className="fa fa-plus"></i>
              Tambah
            </button>
          </div>
        </div>
        <BootstrapTable
          wrapperClasses="table-responsive"
          bordered={false}
          headerWrapperClasses="thead-light"
          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
          keyField="id_pokok"
          data={after}
          columns={columnsAfter}
          bootstrap4
          noDataIndication={emptyDataMessage}
        ></BootstrapTable>
      </>
    </>
  );
}

export default DetilLhrTable;
