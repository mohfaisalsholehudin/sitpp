import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { DatePickerFieldSplit, InputSplit } from "../../../../../helpers";
import Select from "react-select";
import { unitKerja } from "../../../../../references/UnitKerja";
import CustomFileInput from "../../../../../helpers/form/CustomFileInput";

// import {unitKerja}

function DetilLhrForm({ initValues, btnRef }) {
  const [kantor, setKantor] = useState([]);

  useEffect(() => {
    unitKerja.map(data => {
      setKantor(kantor => [
        ...kantor,
        {
          label: data.nm_UNIT_KERJA,
          value: data.kd_unit_kerja,
          alamat: data.alamat
        }
      ]);
    });
  }, []);

  const LhrEditSchema = Yup.object().shape({
    no_lhr: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No LHR is required"),
    no_surat: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No Surat is required"),
    tgl_rapat: Yup.mixed()
      .nullable(false)
      .required("Tanggal Rapat is required"),
    tempat_pembahasan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Tempat Pembahasan is required"),
    unit_penyelenggara: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Unit Penyelenggara Penerbit is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initValues}
        validationSchema={LhrEditSchema}
        onSubmit={values => {
          console.log(values);
          // saveProposal(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          const handleChangeUnit = val => {
            setFieldValue("unit_penyelenggara", val.label);
          };
          return (
            <>
              <Form className="form form-label-right">
                <div className="form-group row">
                  {/* FIELD TANGGAL SURAT */}
                  <div className="col-lg-6 col-xl-6 mb-3">
                    <DatePickerFieldSplit
                      name="tgl_rapat"
                      label="Tanggal Rapat"
                    />
                  </div>
                  {/* FIELD NO SURAT */}
                  <div className="col-lg-6 col-xl-6 mb-3">
                    <Field
                      name="no_surat"
                      component={InputSplit}
                      placeholder="No Surat Undangan"
                      label="No Surat Undangan"
                    />
                  </div>
                </div>
                {/* FIELD PERIHAL */}
                <div className="form-group row">
                  <div className="col-lg-6 col-xl-6 mb-3">
                    <Field
                      name="tempat_pembahasan"
                      component={InputSplit}
                      placeholder="Tempat Pembahasan"
                      label="Tempat Pembahasan"
                    />
                  </div>
                  <div className="col-lg-6 col-xl-6 mb-3">
                    {/* FIELD NO LHR */}
                    <Field
                      name="no_lhr"
                      component={InputSplit}
                      placeholder="No Surat LHR"
                      label="No Surat LHR"
                    />
                  </div>
                </div>
                <div className="form-group row">
                  {/* FIELD UNIT PENYELENGGARA */}
                  <div className="col-lg-6 col-xl-6 mb-3">
                    <label>Unit Penyelenggara Rapat</label>
                    <Select
                      options={kantor}
                      onChange={value => handleChangeUnit(value)}
                      value={kantor.filter(
                        data => data.label === values.unit_kerja
                      )}
                    />
                  </div>
                  <div className="col-lg-6 col-xl-6 mb-3">
                    <label>Upload LHR</label>
                    <Field
                      name="file"
                      component={CustomFileInput}
                      title="Select a file"
                      label="File"
                      style={{ display: "flex" }}
                    />
                  </div>
                </div>
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default DetilLhrForm;
