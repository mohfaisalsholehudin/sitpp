import React, { useEffect, useState, useRef } from "react";

import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../../_metronic/layout";
import DetilLhrForm from "./DetilLhrForm";
import DetilLhrFooter from "./DetilLhrFooter";
import DetilLhrTable from "./DetilLhrTable";


export function DetilLhrEdit({history,
    match: {
      params: { id_surat,id }
    }
  }) {
    const initValues = {
        no_lhr: "",
        no_surat: "",
        tgl_rapat: "",
        tempat_pembahasan: "",
        unit_penyelenggara: "",
        file: ""
      };

  const [title, setTitle] = useState("");
  const suhbeader = useSubheader();

  useEffect(() => {
    let _title = id_surat ? "Edit LHR" : "Tambah LHR";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [id_surat, suhbeader]);
  const btnRef = useRef();

  const handleBack = () => {
    history.push(`/compose/bkf/process/detils/${id}/lhr`)
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
        <DetilLhrForm 
        initValues={initValues}
        btnRef={btnRef}
        />
        <DetilLhrTable />
        </>
      </CardBody>

      <CardFooter style={{ borderTop: "none" }}>
          <DetilLhrFooter 
          btnRef={btnRef}
          backAction={handleBack}
          />
      </CardFooter>
    </Card>
  );
}

export default DetilLhrEdit;