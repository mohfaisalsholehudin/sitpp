/* Library */
import React, { useEffect, useState, useRef } from "react";
import Select from "react-select";
import { useSelector } from "react-redux";

import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import ProposalEditForm from "./ProposalEditForm";
import ProposalEditFooter from "./ProposalEditFooter";
import {
  getPerencanaan,
  getPerencanaanById,
  savePenyusunan,
  uploadFile,
  getPenyusunanById,
  updatePenyusunan,
  uploadZip,
  getPerencanaanSort
} from "../../../Evaluation/Api";

/* Component */
// import ProposalEditForm from "./ProposalEditForm";
// import ProposalEditFooter from "./ProposalEditFooter";

function ProposalEdit({
  history,
  match: {
    params: { id }
  }
}) {
  const initValues = {
    no_perencanaan: "",
    tgl_penyusunan: "",
    jns_peraturan: "",
    judul_peraturan: "",
    no_peraturan: "",
    tentang: "",
    simfoni: "",
    unit_incharge: "",
    file_kajian: "",
    update_kajian: ""
  };

  // Subheader
  const suhbeader = useSubheader();

  const { user } = useSelector(state => state.auth);

  const [title, setTitle] = useState("");
  const [loading, setLoading] = useState(false);
  const [actionsLoading] = useState(true);
  const [show, setShow] = useState(false);
  const [proposal, setProposal] = useState([]);
  const [content, setContent] = useState([]);

  useEffect(() => {
    let _title = id
      ? "Edit Pembuatan Penyusunan Regulasi Perpajakan"
      : "Tambah Pembuatan Penyusunan Regulasi Perpajakan";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
      getPenyusunanById(id).then(({ data }) => {
        setContent({
          no_perencanaan: data.no_perencanan,
          tgl_penyusunan: data.tgl_penyusunan,
          jns_peraturan: data.jns_peraturan,
          judul_peraturan: data.judul_peraturan,
          unit_incharge: data.unit_incharge,
          file_upload: data.update_file_kajian,
          simfoni: data.simfoni,
          no_peraturan: data.no_peraturan,
          tentang: data.tentang,
          id_usulan: data.id_usulan,
          id_perencanaan: data.id_perencanaan,
          id_validator: data.id_validator,
          kd_kantor: data.kd_kantor,
          kd_unit_org: data.kd_unit_org
        });
        setShow(true);
      });
    }
  }, [id, suhbeader]);

  useEffect(() => {
    getPerencanaanSort(user.kantorLegacyKode).then(({ data }) => {
      data.map(data => {
        if (data.no_perencanaan) {
          setProposal(proposal => [
            ...proposal,
            {
              label: data.no_perencanaan,
              value: data.id_perencanaan
            }
          ]);
        }
      });
    });
  }, [user.kantorLegacyKode]);
  const btnRef = useRef();
  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  const saveProposal = values => {
    // console.log(values);
    if (!id) {
      if (values.file.name) {
        enableLoading()
        const formData = new FormData();
        formData.append("file", values.file);
        uploadZip(formData)
          .then(({ data }) =>{
            disableLoading();
            savePenyusunan(
              values.id_perencanaan,
              values.jns_peraturan,
              values.judul_peraturan,
              values.kd_kantor,
              "", //values.no_penyusunan,
              values.no_peraturan,
              values.no_perencanaan,
              values.simfoni,
              values.tentang,
              values.tgl_penyusunan,
              values.unit_incharge,
              data.message, //file_upload,
              values.kd_unit_org
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push("/compose/proposal");
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push("/compose/proposal/add");
                });
              }
            })
          })
          .catch(() => window.alert("Oops Something went wrong !"));
      } else {
        savePenyusunan(
          values.id_perencanaan,
          values.jns_peraturan,
          values.judul_peraturan,
          values.kd_kantor,
          "", //values.no_penyusunan,
          values.no_peraturan,
          values.no_perencanaan,
          values.simfoni,
          values.tentang,
          values.tgl_penyusunan,
          values.unit_incharge,
          values.file_upload, //file_upload
          values.kd_unit_org
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push("/compose/proposal");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/compose/proposal/add");
            });
          }
        });
      }
    } else {
      if (values.file.name) {
        enableLoading();
        const formData = new FormData();
        formData.append("file", values.file);
        uploadZip(formData).then(({ data }) => {
          disableLoading();
          updatePenyusunan(
            id,
            values.id_perencanaan,
            values.jns_peraturan,
            values.judul_peraturan,
            values.kd_kantor,
            "", //values.no_penyusunan,
            values.no_peraturan,
            values.no_perencanaan,
            values.simfoni,
            values.tentang,
            values.tgl_penyusunan,
            values.unit_incharge,
            data.message, //file_upload
            values.kd_unit_org
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push("/compose/proposal");
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push("/compose/proposal/add");
              });
            }
          });
        });
      } else {
        updatePenyusunan(
          id,
          values.id_perencanaan,
          values.jns_peraturan,
          values.judul_peraturan,
          values.kd_kantor,
          "", //values.no_penyusunan,
          values.no_peraturan,
          values.no_perencanaan,
          values.simfoni,
          values.tentang,
          values.tgl_penyusunan,
          values.unit_incharge,
          values.file_upload, //file_upload
          values.kd_unit_org
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push("/compose/proposal");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/compose/proposal/add");
            });
          }
        });
      }
    }
  };
  const backToProposalList = () => {
    if (id) {
      history.push("/compose/proposal");
    } else {
      setShow(false);
    }
  };
  const getCurrentDate = () => {
    const date = new Date();
    return date;
  };

  const handleChangePerencanaan = val => {
    getPerencanaanById(val.value).then(({ data }) => {
      setContent({
        no_perencanaan: data.no_perencanaan,
        tgl_penyusunan: getCurrentDate(),
        jns_peraturan: data.jns_peraturan,
        judul_peraturan: data.judul_peraturan,
        unit_incharge: data.unit_incharge,
        file_upload: data.file_kajian,
        no_peraturan: data.no_peraturan,
        simfoni: data.simfoni,
        tentang: data.tentang,
        id_usulan: data.id_usulan,
        id_perencanaan: data.id_perencanaan,
        id_validator: data.id_validator,
        kd_kantor: user.kantorLegacyKode,
        kd_unit_org: user.unitEs4LegacyKode,
      });
      setShow(true);
    });
  };


  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        {show ? (
          <div className="mt-5">
            <ProposalEditForm
              actionsLoading={actionsLoading}
              proposal={content || initValues}
              btnRef={btnRef}
              saveProposal={saveProposal}
              loading={loading}
              backAction={backToProposalList}
            />
          </div>
        ) : (
          <div className="form-group row">
            <label className="col-xl-3 col-lg-3 col-form-label">
              No Perencanaan
            </label>
            <div className="col-lg-9 col-xl-6">
              <Select
                options={proposal}
                onChange={value => handleChangePerencanaan(value)}
              />
            </div>
          </div>
        )}
      </CardBody>
      {show ? (
        <CardFooter style={{ borderTop: "none" }}>
          {/* <ProposalEditFooter
            backAction={backToProposalList}
            btnRef={btnRef}
            disabledTrue={val}
            loading={loading}
          ></ProposalEditFooter> */}
        </CardFooter>
      ) : null}
    </Card>
  );
}

export default ProposalEdit;
