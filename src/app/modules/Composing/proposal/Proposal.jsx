import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import ProposalTable from "./ProposalTable";

function Proposal() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Usulan Penyusunan Peraturan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <ProposalTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Proposal;
