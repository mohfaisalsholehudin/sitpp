import React, { useEffect, useState } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../../../_metronic/_partials/controls";
import {
  getDraft,
  saveDraftComposing,
  updateDraftComposing,
  uploadFileNew,
} from "../../../../Evaluation/Api";
import CustomFileInput from "../../../../../helpers/form/CustomFileInput";
import CustomLampiranInput from "../../../../../helpers/form/CustomLampiranInput";
import DocumentEditor from "../../../../../helpers/editor/DocumentEditor";

function DraftEditor({
  history,
  match: {
    params: { id, id_draft, name },
  },
}) {
  const [draft, setDraft] = useState("");
  const [content, setContent] = useState();
  const [loading, setLoading] = useState(false);

  const initialValues = {
    file: "",
    lamp: "",
  };

  const validationSchema = Yup.object().shape({
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        // value => value && SUPPORTED_FORMATS.includes(value.type)
        (value) =>
          value && SUPPORTED_FORMATS.some((a) => value.type.includes(a))
      ),
  });
  const FILE_SIZE = 50000000;
  const SUPPORTED_FORMATS = [
    "application/pdf",
    "application/x-rar-compressed",
    "application/octet-stream",
    "application/zip",
    "application/octet-stream",
    "application/x-zip-compressed",
    "multipart/x-zip",
    "application/vnd.rar",
    "application/rar",
    "application/x-rar",
  ];

  useEffect(() => {
    if (id_draft) {
      getDraft(id_draft).then(({ data }) => {
        setContent({
          body_draft: data.body_draft,
          jns_draft: data.jns_draft,
          file_upload: data.fileupload,
          lampiran: data.filelampiranr,
        });
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_draft]);

  const saveDraft = (values) => {
    if (!id_draft) {
      if (!values.lampiran) {
        enableLoading();
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFileNew(formData).then((data_1) => {
          disableLoading();
          saveDraftComposing(draft, name, id, data_1.data.message).then(
            ({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push(`/compose/proposal/${id}/process`);
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push(`/compose/proposal/draft/${id}/add/${name}`);
                });
              }
            }
          );
          // const formLampiran = new FormData();
          // formLampiran.append("file", values.lampiran);
          // uploadFile(formLampiran).then(data_2 => {

          // });
        });
      } else {
        enableLoading();
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFileNew(formData).then((data_1) => {
          disableLoading();
          const formLampiran = new FormData();
          formLampiran.append("file", values.lampiran);
          uploadFileNew(formLampiran).then((data_2) => {
            saveDraftComposing(
              draft,
              name,
              id,
              data_1.data.message,
              data_2.data.message
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push(`/compose/proposal/${id}/process`);
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push(`/compose/proposal/draft/${id}/add/${name}`);
                });
              }
            });
          });
        });
      }
    } else {
      if (values.file.name) {
        enableLoading();
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFileNew(formData).then((data_1) => {
          disableLoading();
          if (!values.lampiran.name) {
            updateDraftComposing(draft, id_draft, data_1.data.message).then(
              ({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Data berhasil disimpan", "success").then(
                    () => {
                      history.push(`/compose/proposal/${id}/process`);
                    }
                  );
                } else {
                  swal("Gagal", "Data gagal disimpan", "error").then(() => {
                    history.push(`/compose/proposal/draft/${id}/add/${name}`);
                  });
                }
              }
            );
          } else {
            enableLoading();
            const formLampiran = new FormData();
            formLampiran.append("file", values.lampiran);
            uploadFileNew(formLampiran).then((data_2) => {
              disableLoading();
              updateDraftComposing(
                draft,
                id_draft,
                data_1.data.message,
                data_2.data.message
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Data berhasil disimpan", "success").then(
                    () => {
                      history.push(`/compose/proposal/${id}/process`);
                    }
                  );
                } else {
                  swal("Gagal", "Data gagal disimpan", "error").then(() => {
                    history.push(`/compose/proposal/draft/${id}/add/${name}`);
                  });
                }
              });
            });
          }
        });
      } else if (values.lampiran.name) {
        enableLoading();
        const formLampiran = new FormData();
        formLampiran.append("file", values.lampiran);
        uploadFileNew(formLampiran).then((data_1) => {
          disableLoading();
          if (!values.file.name) {
            updateDraftComposing(
              draft,
              id_draft,
              values.file_upload,
              data_1.data.message
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push(`/compose/proposal/${id}/process`);
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push(`/compose/proposal/draft/${id}/add/${name}`);
                });
              }
            });
          } else {
            enableLoading();
            const formData = new FormData();
            formData.append("file", values.file);
            uploadFileNew(formData).then((data_2) => {
              disableLoading();
              updateDraftComposing(
                draft,
                id_draft,
                data_2.data.message,
                data_1.data.message
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Data berhasil disimpan", "success").then(
                    () => {
                      history.push(`/compose/proposal/${id}/process`);
                    }
                  );
                } else {
                  swal("Gagal", "Data gagal disimpan", "error").then(() => {
                    history.push(`/compose/proposal/draft/${id}/add/${name}`);
                  });
                }
              });
            });
          }
        });
      } else {
        updateDraftComposing(
          draft,
          id_draft,
          values.file_upload,
          values.lampiran
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/proposal/${id}/process`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/compose/proposal/draft/${id}/add/${name}`);
            });
          }
        });
      }
    }
  };
  const handleBack = () => {
    history.push(`/compose/proposal/${id}/process`);
  };

  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  return (
    <Card>
      <CardHeader
        title="Buat Draft Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <DocumentEditor content={content} setDraft={setDraft} />
          <div className="mt-4">
            <Formik
              enableReinitialize={true}
              initialValues={content || initialValues}
              validationSchema={validationSchema}
              onSubmit={(values) => {
                saveDraft(values);
                // console.log(values);
              }}
            >
              {({
                handleSubmit,
                values,
                setFieldValue,
                errors,
                setFieldTouched,
                touched,
              }) => {
                const handleClick = () => {
                  if (!values.file) {
                    if (values.file_upload) {
                      setFieldValue("file", {
                        file: values.file_upload,
                        size: 5000000,
                        type: [
                          "application/pdf",
                          "application/x-rar-compressed",
                          "application/octet-stream",
                          "application/zip",
                          "application/octet-stream",
                          "application/x-zip-compressed",
                          "multipart/x-zip",
                          "application/vnd.rar",
                        ],
                      });
                    } else if (errors) {
                      setFieldTouched("file", true);
                    }
                  }
                };

                return (
                  <Form className="form form-label-right">
                    <>
                      <div className="form-group row">
                        <label className="col-xl-3 col-lg-3 col-form-label">
                          Upload File
                        </label>
                        <div className="col-lg-9 col-xl-6">
                          <Field
                            name="file"
                            component={CustomFileInput}
                            title="Select a file"
                            label="File"
                            style={{ display: "flex" }}
                          />
                        </div>
                      </div>
                    </>
                    {draft ? (
                      <div className="form-group row">
                        <label className="col-xl-3 col-lg-3 col-form-label">
                          Upload Lampiran
                        </label>
                        <div className="col-lg-9 col-xl-6">
                          <Field
                            name="lampiran"
                            component={CustomLampiranInput}
                            title="Select a file"
                            label="Lampiran"
                            style={{ display: "flex" }}
                          />
                        </div>
                      </div>
                    ) : null}

                    <div className="col-lg-12" style={{ textAlign: "right" }}>
                      <button
                        type="button"
                        onClick={handleBack}
                        className="btn btn-light"
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                        }}
                      >
                        <i className="fa fa-arrow-left"></i>
                        Kembali
                      </button>
                      {`  `}
                      {loading ? (
                        <button
                          type="submit"
                          className="btn btn-success spinner spinner-white spinner-left ml-2"
                          onSubmit={() => handleSubmit()}
                          onClick={() => handleClick()}
                          disabled={true}
                          style={{
                            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                          }}
                        >
                          <span>Simpan</span>
                        </button>
                      ) : (
                        <button
                          type="submit"
                          className="btn btn-success ml-2"
                          onSubmit={() => handleSubmit()}
                          onClick={() => handleClick()}
                          style={{
                            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                          }}
                        >
                          <i className="fas fa-save"></i>
                          Simpan
                        </button>
                      )}
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </>
      </CardBody>
    </Card>
  );
}

export default DraftEditor;
