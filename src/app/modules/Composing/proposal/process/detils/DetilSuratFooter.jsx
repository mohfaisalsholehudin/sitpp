import React from "react";

function DetilSuratFooter({ backAction, btnRef, loading }) {
  const saveForm = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };
  return (
    <>
      <div className="col-lg-12" style={{ textAlign: "right" }}>
        {loading ? (
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
            disabled={true}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
        ) : (
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
        )}
        {`  `}
        {loading ? (
          <button
            type="submit"
            className="btn btn-success spinner spinner-white spinner-left ml-2"
            onClick={saveForm}
            disabled={true}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <span>Simpan</span>
          </button>
        ) : (
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveForm}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
            // disabled={disabled}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
        )}
      </div>
    </>
  );
}

export default DetilSuratFooter;
