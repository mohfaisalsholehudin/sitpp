import React, { useEffect, useState, useRef } from "react";
import { useSelector } from "react-redux";


import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../../_metronic/layout";
import DetilSuratForm from "./DetilSuratForm";
import DetilSuratFooter from "./DetilSuratFooter";
import {
  getSuratByIdSurat,
  saveSuratUndangan,
  updateSuratUndangan,
} from "../../../../Evaluation/Api";
import swal from "sweetalert";
import { uploadFileNew } from "../../../../../references/Api";

function DetilSuratEdit({
  history,
  match: {
    params: { id_surat, id }
  }
}) {
  const [content, setContent] = useState();
  const { user } = useSelector(state => state.auth);
  const [loading, setLoading] = useState(false);

  const initValues = {
    nomor_surat: "",
    tgl_surat: "",
    perihal: "",
    unit_penerima: "",
    unit_penerbit: "",
    id_jenis: "",
    jenis_instansi:"",
    jenis_instansi_2: "",
    status: "draft",
    file: ""
  };
  const [title, setTitle] = useState("");
  const suhbeader = useSubheader();

  useEffect(() => {
    let _title = id_surat ? "Edit Dokumentasi Administrasi Persuratan" : "Tambah Dokumentasi Administrasi Persuratan";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_surat, suhbeader]);

  useEffect(() => {
    if (id_surat) {
      getSuratByIdSurat(id_surat).then(({ data }) => {
        setContent({
          nomor_surat: data.nomor_surat, 
          tgl_surat: data.tgl_surat,
          perihal: data.perihal,
          unit_penerbit: data.unit_penerbit,
          unit_penerima: data.unit_penerima,
          id_jenis: data.id_jenis,
          jenis_instansi: data.jenis_instansi,
          status: data.status,
          file_upload: data.file_upload,
          jenis_instansi_2: data.jenis_instansi_2,
        });
      });
    }
  }, [id_surat]);
  const btnRef = useRef();

  const handleBack = () => {
    history.push(`/compose/proposal/process/detils/${id}/surat`);
  };

  const saveSurat = values => {
    if (!id_surat) {
      enableLoading();
      const formData = new FormData();
      formData.append("file", values.file);
      uploadFileNew(formData).then(({ data }) => {
        disableLoading();
        saveSuratUndangan(
          id,
          values.nomor_surat,
          values.perihal,
          values.tgl_surat,
          values.unit_penerbit,
          values.unit_penerima,
          values.id_jenis,
          values.jenis_instansi,
          values.status,
          data.message,
          values.jenis_instansi_2
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/proposal/process/detils/${id}/surat`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/compose/proposal/process/detils/${id}/surat/add`);
            });
          }
        });
      })
      
    } else {
      if(values.file.name){
        enableLoading();
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFileNew(formData).then(({ data }) => {
          disableLoading();
          updateSuratUndangan(
            id_surat,
            id,
            values.nomor_surat,
            values.perihal,
            values.tgl_surat,
            values.unit_penerbit,
            values.unit_penerima,
            values.id_jenis,
            values.jenis_instansi,
            values.status,
            data.message,
          values.jenis_instansi_2
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push(`/compose/proposal/process/detils/${id}/surat`);
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push(`/compose/proposal/process/detils/${id}/surat/add`);
              });
            }
          });
        })
      } else {
        updateSuratUndangan(
          id_surat,
          id,
          values.nomor_surat,
          values.perihal,
          values.tgl_surat,
          values.unit_penerbit,
          values.unit_penerima,
          values.id_jenis,
          values.jenis_instansi,
          values.status,
          values.file_upload,
          values.jenis_instansi_2
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/proposal/process/detils/${id}/surat`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/compose/proposal/process/detils/${id}/surat/add`);
            });
          }
        });
      }
    }
  };


  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <DetilSuratForm
            initValues={content || initValues}
            btnRef={btnRef}
            saveSurat={saveSurat}
          />
        </>
      </CardBody>

      <CardFooter style={{ borderTop: "none" }}>
        <DetilSuratFooter btnRef={btnRef} backAction={handleBack} loading={loading} />
      </CardFooter>
    </Card>
  );
}

export default DetilSuratEdit;
