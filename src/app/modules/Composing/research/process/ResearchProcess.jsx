import React, {useState, useEffect} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import{getPenyusunanById} from "../../../Evaluation/Api"
import { DateFormat } from "../../../../helpers/DateFormat";
import ResearchProcessDetils from "./ResearchProcessDetils";
import ResearchDraft from "./ResearchDraft";

function ResearchProcess({
    history,
    match: {
      params: { id }
    }
  }) {

    const [detil, setDetil] = useState([]);


    useEffect(() => {
      getPenyusunanById(id).then(({ data }) => {
        setDetil(data)
      })
    }, [id])
  return (
    <Card>
      <CardHeader
        title="Daftar Proses Penyusunan Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="row">
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Tanggal Penyusunan
                </label>
                <div className="col-lg-9 col-xl-6">
                  <p className="text-muted pt-2">
                  {`: ${DateFormat(detil.tgl_penyusunan)}`}
                  </p>
                </div>
              </div>
            </div>
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Jenis Peraturan
                </label>
                <div className="col-lg-9 col-xl-6">
                  <p className="text-muted pt-2"> 
                  {`: ${detil.jns_peraturan}`}
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Unit In Charge
                </label>
                <div className="col-lg-9 col-xl-6">
                  <p className="text-muted pt-2">
                  {`: ${detil.unit_incharge}`}
                    
                  </p>
                </div>
              </div>
            </div>
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Judul Peraturan
                </label>
                <div className="col-lg-9 col-xl-6">
                  <p className="text-muted pt-2"> 
                  {`: ${detil.judul_peraturan}`}
                   </p>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Usulan Simfoni
                </label>
                <div className="col-lg-9 col-xl-6">
                  <p className="text-muted pt-2">
                  {`: ${detil.simfoni}`}

                  </p>
                </div>
              </div>
            </div>
          </div>
        </>
      <ResearchDraft
      id={id}
       />
       <ResearchProcessDetils id={id} />
      </CardBody>

      <CardFooter style={{ borderTop: "none" }} />
    </Card>
  );
}

export default ResearchProcess;
