import React, { useEffect, useState } from "react";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { DatePickerField, Input} from "../../../../helpers";
import CustomFileInput from "../../../../helpers/form/CustomFileInput";
import { getMintaJawab } from "../../../Evaluation/Api";


function CosignKementerianForm({ initValues, btnRef, id, saveProposal }) {
  const [surat, setSurat] = useState([]);
  const [valSurat, setValSurat] = useState();

  const CosignEditSchema = Yup.object().shape({
    no_surat: Yup.string()
      .min(2, "Minimum 2 symbols")
      // .max(50, "Maximum 50 symbols")
      .required("No Surat is required"),
    id_minta_jawab: Yup.string()
      .required("ND Permintaaan is required"),
    tgl_surat: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),
    nama_unit: Yup.string()
      .min(2, "Minimum 2 symbols")
      // .max(50, "Maximum 50 symbols")
      .required("Nama Unit is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        // value => value && SUPPORTED_FORMATS.includes(value.type)
        value => value && SUPPORTED_FORMATS.some(a=> value.type.includes(a))
      )
  });
  const FILE_SIZE = 50000000;
  const SUPPORTED_FORMATS = [
    "application/pdf",
    "application/x-rar-compressed",
    "application/octet-stream",
    "application/zip",
    "application/octet-stream",
    "application/x-zip-compressed",
    "multipart/x-zip",
    "application/vnd.rar",
  ];
  useEffect(() => {
    getMintaJawab(id).then(({ data }) => {
      data.map(data => {
        setSurat(surat => [
          ...surat,
          { label: data.nomor_surat, value: data.id_minta_jawab }
        ]);
      });
    });
  }, [id]);
  useEffect(() => {
    if (initValues.id_minta_jawab) {
      surat
        .filter(data => data.value === initValues.id_minta_jawab)
        .map(data => {
          setValSurat(data.label);
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [initValues.id_minta_jawab]);
  return (
    <>
    <Formik
      enableReinitialize={true}
      initialValues={initValues}
      validationSchema={CosignEditSchema}
      onSubmit={values => {
        // console.log(values);
        saveProposal(values);
      }}
    >
      {({
        handleSubmit,
        setFieldValue,
        handleBlur,
        handleChange,
        errors,
        touched,
        values,
        isValid
      }) => {
        const handleChangeSurat = val => {
          setFieldValue("id_minta_jawab", val.value);
          setValSurat(val.label);
        };

        return (
          <>
            <Form className="form form-label-right">
              {/* FIELD NO ND PERMINTAAN */}
              <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  No ND Permintaan
                </label>
                <div className="col-lg-9 col-xl-6">
                  <Select
                    options={surat}
                    onChange={value => handleChangeSurat(value)}
                    value={surat.filter(data => data.label === valSurat)}
                  />
                </div>
              </div>
              {/* FIELD UNIT KERJA */}

            <div className="form-group row">
                <Field
                  name="nama_unit"
                  component={Input}
                  placeholder="Nama Instansi"
                  label="Nama Instansi"
                />
              </div>
              {/* FIELD NO SURAT */}
              <div className="form-group row">
                <Field
                  name="no_surat"
                  component={Input}
                  placeholder="No Surat"
                  label="No Surat"
                />
              </div>
              {/* FIELD TANGGAL SURAT */}
              <div className="form-group row">
                <DatePickerField name="tgl_surat" label="Tanggal Surat" />
              </div>

              {/* FIELD UPLOAD FILE */}
              {values.no_surat ? (
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Upload ND Jawaban
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Field
                      name="file"
                      component={CustomFileInput}
                      title="Select a file"
                      label="File"
                      style={{ display: "flex" }}
                    />
                  </div>
                </div>
              ) : null}

              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
              {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
            </Form>
          </>
        );
      }}
    </Formik>
  </>
  );
}

export default CosignKementerianForm;

