import React, { useState, useEffect, useRef } from "react";
import swal from "sweetalert";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../_metronic/layout";
import CosignKementerianFooter from "./CosignKementerianFooter";
import CosignKementerianForm from "./CosignKementerianForm";
import {
  getPenyusunanById,
  saveCosignKementerian,
  updateCosignKementerian,
  uploadFile,
  getCosignKementerianByIdCosign,
  uploadFileNew
} from "../../../Evaluation/Api";

function CosignKementerianEdit({
  history,
  match: {
    params: { id_cosign, id }
  }
}) {
  const initValues = {
    nama_unit: "",
    no_surat: "",
    tgl_surat: "",
    file: ""
  };

  const [title, setTitle] = useState("");
  const [content, setContent] = useState();
  const [val, setVal] = useState([]);
  const suhbeader = useSubheader();
  const btnRef = useRef();
  const handleBack = () => {
    history.push(`/compose/process/detils/${id}/cosign-kementerian`);
  };

  useEffect(() => {
    let _title = id_cosign
      ? "Edit Jawaban Cosign KL / Asosiasi"
      : "Tambah Jawaban Cosign KL / Asosiasi";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if(id_cosign){
      getCosignKementerianByIdCosign(id_cosign).then(({data})=> {
        setContent({
          nama_unit: data.nama_unit,
          no_surat: data.no_surat,
          tgl_surat: data.tgl_surat,
          file_upload: data.file_cosign,
          id_minta_jawab: data.id_minta_jawab
        });
      })
    }
  }, [id_cosign, suhbeader]);

  useEffect(() => {
    getPenyusunanById(id).then(({ data }) => {
      setVal(data);
    });
  }, [id]);

  const saveProposal = values => {
    if (!id_cosign) {
      const formData = new FormData();
      formData.append("file", values.file);
      uploadFileNew(formData).then(({ data }) => {
        saveCosignKementerian(
          data.message,
          values.id_minta_jawab,
          id,
          values.nama_unit,
          values.no_surat,
          values.tgl_surat
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/process/detils/${id}/cosign-kementerian`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(
                `/compose/process/detils/${id}/cosign-kementerian/add`
              );
            });
          }
        });
      });
    } else {
      if (values.file.name) {
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData).then(({ data }) => {
          updateCosignKementerian(
            id_cosign,
            data.message,
            values.id_minta_jawab,
            id,
            values.nama_unit,
            values.no_surat,
            values.tgl_surat
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push(
                  `/compose/process/detils/${id}/cosign-kementerian`
                );
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push(
                  `/compose/process/detils/${id}/cosign-kementerian/add`
                );
              });
            }
          });
        });
      } else {
        updateCosignKementerian(
          id_cosign,
          values.file_upload,
          values.id_minta_jawab,
          id,
          values.nama_unit,
          values.no_surat,
          values.tgl_surat
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/process/detils/${id}/cosign-kementerian`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(
                `/compose/process/detils/${id}/cosign-kementerian/add`
              );
            });
          }
        });
      }
    }
  };
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <CosignKementerianForm
            initValues={content || initValues}
            btnRef={btnRef}
            id={id}
            saveProposal={saveProposal}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <CosignKementerianFooter backAction={handleBack} btnRef={btnRef} />
      </CardFooter>
    </Card>
  );
}

export default CosignKementerianEdit;
