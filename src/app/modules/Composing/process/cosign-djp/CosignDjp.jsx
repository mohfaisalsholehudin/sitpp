import React, {useState, useEffect} from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../../_metronic/_partials/controls";
import CosignDjpTable from "./CosignDjpTable";
import { DateFormat } from "../../../../helpers/DateFormat";
import {getPenyusunanById} from "../../../Evaluation/Api"

function CosignDjp({
    history,
    match: {
      params: { id }
    }
  }) {

    const [detil, setDetil] = useState([]);

    useEffect(() => {
     getPenyusunanById(id).then(({ data }) => {
       setDetil(data)
     })
    }, [id])
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Cosign Jawaban Unit DJP"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <>
            <div className="row">
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    No Penyusunan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                    {`: ${detil.no_penyusunan}`}


                    </p>
                  </div>
                </div>
              </div>
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Jenis Peraturan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                  {`: ${detil.jns_peraturan}`}
                      
                       </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Tanggal Penyusunan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">

                  {`: ${DateFormat(detil.tgl_penyusunan)}`}

                    </p>
                  </div>
                </div>
              </div>
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Judul Peraturan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                    {`: ${detil.judul_peraturan}`}

                    </p>
                  </div>
                </div>
              </div>
            </div>
          </>
          <CosignDjpTable id={id} />
        </CardBody>
      </Card>
    </>
  );
}

export default CosignDjp;
