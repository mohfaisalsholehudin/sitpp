import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import ProcessTable from "./ProcessTable";

function Process() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Penyusunan Peraturan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <ProcessTable />
        </CardBody>
      </Card>
    </>
  );
}

export default Process;
