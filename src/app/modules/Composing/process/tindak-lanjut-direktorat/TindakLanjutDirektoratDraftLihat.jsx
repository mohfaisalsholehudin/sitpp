import React, { useEffect, useState, useRef } from "react";
import EmailEditor from "react-email-editor";
import { useSelector } from "react-redux";
import axios from "axios";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../../_metronic/_partials/controls";
import { getDraft } from "../../../Evaluation/Api";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";
import DocumentEditor from "../../../../helpers/editor/DocumentEditor";
// import "../../proposal/process/drafting/decoupled.css"
const {BACKEND_URL, FILE_URL} = window.ENV;

function TindakLanjutDirektoratDraftLihat({
  history,
  match: {
    params: { id, id_draft, name }
  }
}) {
  const [draft, setDraft] = useState("");
  const [show, setShow] = useState(false);
  const [content, setContent] = useState({
    file: "",
    lampiran: ""
  });

  const validationSchema = Yup.object().shape({
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];
  useEffect(() => {
    // if (id_draft) {
    //   axios.get(`${BACKEND_URL}/api/draftperaturan/byidpenyusunan/${id}`, {
    //     transformResponse: res => {
    //       name === "usulan"
    //         ? setDraft(JSON.parse(res[0].body_draft))
    //         : setDraft(JSON.parse(res[0].body_cosign));
    //       // setDraft(JSON.parse(res[0].body_draft));
    //       return res;
    //     },
    //     responseType: "json"
    //   });
    //   getDraft(id_draft).then(({ data }) => {
    //     switch (name) {
    //       case "usulan":
    //         setContent({
    //           file_upload: data.fileupload,
    //           lampiran: data.filelampiranr
    //         });
    //         break;
    //       case "cosign unit":
    //         setContent({
    //           file_upload: data.fileupload_cosign,
    //           lampiran: data.filelampiran_cosign
    //         });
    //         break;

    //       default:
    //         break;
    //     }
    //   });
    // }
    if (id_draft) {
      getDraft(id_draft).then(({ data }) => {
        switch(name){
          case "usulan":
            setContent({
              body_draft: data.body_draft,
              file_upload: data.fileupload,
              lampiran: data.filelampiranr
            })
            break;
          case "cosign unit":
            setContent({
              body_draft: data.body_cosign,
              file_upload: data.fileupload_cosign,
              lampiran: data.filelampiran_cosign
            })
            break;
            default:
              break;
        }
      });
    }

  }, [id_draft, name, id]);

  return (
    <Card>
      <CardHeader
        title="Lihat Draft Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
        <DocumentEditor
        content={content}
        setDraft={setDraft}
        isReadOnly={true} 
        />
          <Formik
            enableReinitialize={true}
            initialValues={content}
            validationSchema={validationSchema}
            onSubmit={values => {
              // saveDraft(values);
              // console.log(values);
            }}
          >
            {({ handleSubmit, values }) => {
              return (
                <Form className="form form-label-right">
                  {/* Field File */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      File Tersimpan
                    </label>
                    <div
                      className="col-lg-9 col-xl-6"
                      style={{ marginTop: "10px" }}
                    >
                      <a
                        href={FILE_URL + values.file_upload}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {values.file_upload
                          ? values.file_upload
                          : null}
                      </a>
                    </div>
                  </div>
                  {/* Field Lampiran */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Lampiran Tersimpan
                    </label>
                    <div
                      className="col-lg-9 col-xl-6"
                      style={{ marginTop: "10px" }}
                    >
                      <a
                        href={FILE_URL + values.lampiran}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {values.lampiran ? values.lampiran : null}
                      </a>
                    </div>
                  </div>

                  <div className="col-lg-12 " style={{ textAlign: "right" }}>
                    <button
                      type="button"
                      className="btn btn-light-success ml-2"
                      // onSubmit={() => handleSubmit()}
                      onClick={() =>
                        history.push(
                          `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                        )
                      }
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="fa fa-arrow-left"></i>
                      Kembali
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </CardBody>
    </Card>
  );
}

export default TindakLanjutDirektoratDraftLihat;
