import React, { useState, useEffect, useRef } from "react";
import swal from "sweetalert";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../_metronic/layout";
import CosignSahliForm from "./CosignSahliForm";
import CosignSahliFooter from "./CosignSahliFooter";
import {
  getCosignSahliByIdCosign,
  getPenyusunanById,
  saveCosignSahli,
  saveCosignSahliCatatan,
  updateCosignSahli,
  updateCosignSahliCatatan,
  uploadFile
} from "../../../Evaluation/Api";
import CosignSahliFormNotes from "./CosignSahliFormNotes";

function CosignSahliNotesEdit({
  history,
  match: {
    params: { id_cosign_sahli, id }
  }
}) {
  const initValues = {
    catatan: "",
    nama_unit: "Staff Ahli Menteri"
  };

  const [title, setTitle] = useState("");
  const [content, setContent] = useState();
  const [val, setVal] = useState([]);

  const suhbeader = useSubheader();
  useEffect(() => {
    let _title = id_cosign_sahli ? "Edit Cosign Sahli" : "Tambah Cosign Sahli";

    setTitle(_title);
    suhbeader.setTitle(_title);
    if (id_cosign_sahli) {
      getCosignSahliByIdCosign(id_cosign_sahli).then(({ data }) => {
        setContent({
          nama_unit: data.nama_unit,
          catatan: data.catatan
        });
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_cosign_sahli, suhbeader]);

  useEffect(() => {
    getPenyusunanById(id).then(({ data }) => {
      setVal(data);
    });
  }, [id]);
  const btnRef = useRef();

  const handleBack = i => {
    history.push(`/compose/process/detils/${i}/cosign-sahli`);
  };

  const saveCosign = values => {
    if (!id_cosign_sahli) {
        saveCosignSahliCatatan(
          id,
          values.nama_unit,
          values.catatan
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/process/detils/${id}/cosign-sahli`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/compose/process/detils/${id}/cosign-sahli/add`);
            });
          }
        });
    } else {
        updateCosignSahliCatatan(
          id,
          id_cosign_sahli,
          values.nama_unit,
          values.catatan,
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/process/detils/${id}/cosign-sahli`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/compose/process/detils/${id}/cosign-sahli/add`);
            });
          }
        });
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="form-group row">
            <label className="col-xl-3 col-lg-3 col-form-label">
              No Penyusunan
            </label>
            <div className="col-lg-9 col-xl-6" style={{ marginTop: "10px" }}>
              {val.no_penyusunan}
            </div>
          </div>
          <div className="mt-5">
            <CosignSahliFormNotes
              initValues={content || initValues}
              btnRef={btnRef}
              saveCosign={saveCosign}
              id={id}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <CosignSahliFooter backAction={handleBack} btnRef={btnRef} id={id} />
      </CardFooter>
    </Card>
  );
}

export default CosignSahliNotesEdit;
