import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import {
  getCosignSahliByIdCosign,
  getCosignUnitByIdCosign,
} from "../../../Evaluation/Api";
import { DateFormat } from "../../../../helpers/DateFormat";
const {FILE_URL} = window.ENV;


function MonitoringCosignSahliDraftOpen ({ id, show, onHide, id_cosign }) {
  const history = useHistory();
  const [data, setData] = useState([]);

  useEffect(() => {
    if (id_cosign) {
      getCosignSahliByIdCosign(id_cosign).then(({ data }) => {
        setData(data);
      });
    }
  }, [id_cosign]);

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detil Cosign Sahli
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Nama Unit</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.nama_unit}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Nomor Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.no_surat ? data.no_surat : "-"}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Tanggal Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${DateFormat(data.tgl_surat)}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Catatan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.catatan ? data.catatan : "-"}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">File Permintaan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              <a
                href={FILE_URL + data.file_permintaan}
                target="_blank"
                rel="noopener noreferrer"
              >
                {data.file_permintaan ? data.file_permintaan : "-"}
              </a>
            </span>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-danger"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}

export default MonitoringCosignSahliDraftOpen ;
