import React,{useEffect, useState} from "react";
import { Modal } from "react-bootstrap";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { getJenisSurat, getPeraturanTerkaitByIdPeraturan, getSuratByIdSurat } from "../../../Evaluation/Api";
import { DateFormat } from "../../../../helpers/DateFormat";


function MonitoringPerekamanPeraturanOpen({ id, show, onHide, id_perter }) {

  const history = useHistory();
  const [data, setData] = useState([]);
  const [surat, setSurat] = useState([]);
  const [isReject, setIsReject] = useState(false);
  const [isShow, setIsShow] = useState(true);

  useEffect(() => {
    if(id_perter){
      getPeraturanTerkaitByIdPeraturan(id_perter).then(({data})=> {
        setData(data)
      })
    }
  }, [id_perter])


  const handleReject = () => {
    setIsReject(true);
    setIsShow(false);
  }

  const handleCancel = () => {
    setIsReject(false);
    setIsShow(true);
  }



  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detil Peraturan Terkait
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Nomor Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
            {`: ${data.nomor_peraturan}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Tanggal Ditetapkan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
            {`: ${DateFormat(data.tgl_ditetapkan)}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Tanggal Diundangkan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
            {`: ${DateFormat(data.tgl_diundangkan)}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Perihal</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
                {`: ${data.perihal}`}
            </span>
          </div>
        </div>
       
      </Modal.Body>
     <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-danger"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
        </div>
      </Modal.Footer> 
      
    </Modal>
  );
}

export default MonitoringPerekamanPeraturanOpen;
