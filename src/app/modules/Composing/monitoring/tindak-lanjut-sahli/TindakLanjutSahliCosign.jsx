import React, { useState, useEffect } from "react";
import { useHistory, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../../../../helpers/column-formatters";
import { getCosignSahli } from "../../../Evaluation/Api";
import CosignSahliDraftOpen from "./TindakLanjutSahliCosignOpen";
import TindakLanjutSahliCosignOpen from "./TindakLanjutSahliCosignOpen";
import MonitoringTindakLanjutSahliCosignOpen from "./TindakLanjutSahliCosignOpen";

function MonitoringTindakLanjutSahliCosign({ id }) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const { status } = useSelector(state => state.auth);

  const showDetil = id_cosign => {
    history.push(
      `/compose/monitoring/detils/${id}/tindak-lanjut-sahli/${id_cosign}/cosign/open`
    );
  };

  useEffect(() => {
    getCosignSahli(id).then(({ data }) => {
      setContent(data);
    });
  }, [id]);

  const columns = [
    {
      dataField: "nama_unit",
      text: "Nama Unit",
      sort: true
    },
    {
      dataField: "tgl_surat",
      text: "Tanggal Surat",
      formatter: columnFormatters.DateFormatterComposeProcessCosignDjp,
      sort: true
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
      columnFormatters.ActionsColumnFormatterComposeProcessCosignSahliJustView,
        // status === "Cosign Sahli"
        //   ? columnFormatters.ActionsColumnFormatterComposeProcessCosignSahli
        //   : columnFormatters.ActionsColumnFormatterComposeProcessCosignSahliJustView,
      formatExtraData: {
        showDetil: showDetil
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];


  const columnsNotes = [
    {
      dataField: "nama_unit",
      text: "Nama Unit",
      sort: true
    },
    {
      dataField: "catatan",
      text: "Catatan",
      sort: true
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
      columnFormatters.ActionsColumnFormatterComposeProcessCosignSahliJustView,
        // status === "Cosign Sahli"
        //   ? columnFormatters.ActionsColumnFormatterComposeProcessCosignSahli
        //   : columnFormatters.ActionsColumnFormatterComposeProcessCosignSahliJustView,
      formatExtraData: {
        showDetil: showDetil
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <div className="row">
          <div className="col-lg-12 col-xl-12 mb-3 mt-3">
            <h4> Cosign Sahli</h4>
          </div>
        </div>
        <BootstrapTable
          wrapperClasses="table-responsive"
          bordered={false}
          headerWrapperClasses="thead-light"
          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
          keyField="id_cosign_sahli"
          data={content}
          // columns={columns}
          columns={content[0] ? content[0].tgl_surat ? columns : columnsNotes : columns}
          bootstrap4
          noDataIndication={emptyDataMessage}
        ></BootstrapTable>
      </>
      <Route path="/compose/monitoring/detils/:id/tindak-lanjut-sahli/:id_cosign/cosign/open">
        {({ history, match }) => (
          <MonitoringTindakLanjutSahliCosignOpen
            show={match != null}
            id={match && match.params.id}
            id_cosign={match && match.params.id_cosign}
            after={false}
            onHide={() => {
              history.push(`/compose/monitoring/detils/${id}/tindak-lanjut-sahli`);
            }}
            onRef={() => {
              history.push(`/compose/monitoring/detils/${id}/tindak-lanjut-sahli`);
            }}
          />
        )}
      </Route>
    </>
  );
}

export default MonitoringTindakLanjutSahliCosign;
