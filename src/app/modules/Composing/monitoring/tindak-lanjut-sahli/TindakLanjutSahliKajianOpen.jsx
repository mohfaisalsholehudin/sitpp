/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import swal from "sweetalert";
import { useSelector } from "react-redux";
import { Modal, Table } from "react-bootstrap";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import {
  getPenyusunanById,
  updatePenyusunanKajianUnit,
  uploadFile,
  updateStatusPenyusunan,
  updatePenyusunanKajianSahli,
  uploadZip
} from "../../../Evaluation/Api";
// import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import CustomZipInput from "../../../../helpers/form/CustomZipInput";
const {SLICE_ZIP, DOWNLOAD_URL} = window.ENV;
// import { getPerencanaanById, getUsulanById, updateStatusPerencanaan } from "../../Evaluation/Api";

// import ResearchReject from "./ResearchReject";
// import {DateFormat} from "../../../helpers/DateFormat"

function MonitoringTindakLanjutSahliKajianOpen({ id, show, onHide }) {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const { status } = useSelector(state => state.auth);
  const [content, setContent] = useState([]);
  const [isShow, setIsShow] = useState(true);
  const [isUpload, setIsUpload] = useState(false);

  const initialValues = {
    file: ""
  };

  const validationSchema = Yup.object().shape({
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });
  const FILE_SIZE = 50000000;
  const SUPPORTED_FORMATS = [
    "application/x-rar-compressed",
    "application/octet-stream",
    "application/zip",
    "application/octet-stream",
    "application/x-zip-compressed",
    "multipart/x-zip",
    "application/vnd.rar"
  ];
  useEffect(() => {
    getPenyusunanById(id).then(({ data }) => {
      setContent(data);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const uploadAction = () => {
    setIsUpload(true);
    setIsShow(false);
  };
  const handleCancel = () => {
    setIsUpload(false);
    setIsShow(true);
  };
  const handleDownload = values => {
    window.open(DOWNLOAD_URL + values);
  };
  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Riwayat Kajian
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <>
            <div className="row">
              <Table responsive hover>
                <thead
                  style={{
                    border: "1px solid #3699FF",
                    textAlign: "center",
                    backgroundColor: "#3699FF"
                  }}
                >
                  <tr>
                    <th>NO</th>
                    <th style={{ textAlign: "left" }}>KAJIAN</th>
                    <th style={{ textAlign: "left" }}>NAMA FILE</th>
                    <th style={{ textAlign: "left" }}>
                      <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        AKSI
                      </div>
                    </th>
                  </tr>
                </thead>
                <tbody
                  style={{ border: "1px solid lightgrey", textAlign: "center" }}
                >
                  <tr style={{ borderBottom: "1px solid lightgrey" }}>
                    <td>1</td>
                    <td style={{ textAlign: "left" }}>Kajian Usulan</td>
                    <td style={{ textAlign: "left" }}>
                      {content.update_file_kajian
                        ? content.update_file_kajian
                        : null}
                    </td>
                    <td style={{ textAlign: "left" }}>
                      <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3">
                        <a
                          className="label label-lg label-light-primary label-inline"
                          title="Open Detils"
                          // href={content.update_file_kajian}
                          onClick={()=> handleDownload(content.update_file_kajian)}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          Download
                        </a>
                      </div>
                    </td>
                  </tr>
                  <tr style={{ borderBottom: "1px solid lightgrey" }}>
                    <td>2</td>
                    <td style={{ textAlign: "left" }}>Kajian Unit</td>
                    <td style={{ textAlign: "left" }}>
                      {content.file_kajian_unit
                        ? content.file_kajian_unit
                        : "-"}
                    </td>
                    <td style={{ textAlign: "left" }}>
                      <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3">
                        {content.file_kajian_unit ? (<a
                          className="label label-lg label-light-primary label-inline"
                          title="Open Detils"
                          // href={content.file_kajian_unit}
                          onClick={()=> handleDownload(content.file_kajian_unit)}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          Download
                        </a>): "-"}
                      </div>
                    </td>
                  </tr>
                  <tr style={{ borderBottom: "1px solid lightgrey" }}>
                    <td>3</td>
                    <td style={{ textAlign: "left" }}>Kajian sahli</td>
                    <td style={{ textAlign: "left" }}>
                      {content.file_kajian_sahli
                        ? content.file_kajian_sahli
                        : "-"}
                    </td>
                    <td style={{ textAlign: "left" }}>
                      {content.file_kajian_sahli ? (
                        <>
                          <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3">
                            <a
                              className="label label-lg label-light-primary label-inline"
                              title="Open Detils"
                              // href={content.file_kajian_sahli}
                              onClick={()=> handleDownload(content.file_kajian_sahli)}
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              Download
                            </a>
                          </div>

                          {status !== "Cosign Sahli" ? null : (
                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3">
                              <a
                                className="label label-lg label-light-success label-inline"
                                title="Open Detils"
                                onClick={() => uploadAction()}
                              >
                                Upload
                              </a>
                            </div>
                          )}
                        </>
                      ) : status !== "Cosign Sahli" ? "-" : (
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3">
                          <a
                            className="label label-lg label-light-success label-inline"
                            title="Open Detils"
                            onClick={() => uploadAction()}
                          >
                            Upload
                          </a>
                        </div>
                      )}
                    </td>
                  </tr>
                </tbody>
                <tfoot
                  style={{
                    border: "1px solid #3699FF",
                    textAlign: "center",
                    backgroundColor: "#3699FF"
                  }}
                >
                  <tr style={{ height: "40px" }}>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tfoot>
              </Table>
            </div>
          </>
          {isUpload ? (
            <Formik
              enableReinitialize={true}
              initialValues={initialValues}
              validationSchema={validationSchema}
              onSubmit={values => {
                //   console.log(values);
              }}
            >
              {({ handleSubmit, isSubmitting }) => {
                return (
                  <Form className="form form-label-right">
                    <div
                      className="form-group row"
                      style={{ marginBottom: "0px" }}
                    ></div>
                    {/* FIELD UPLOAD FILE */}
                    <div className="form-group row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Upload Kajian
                      </label>
                      <div className="col-lg-9 col-xl-9">
                        <Field
                          name="file"
                          component={CustomZipInput}
                          title="Select a file"
                          label="File"
                          style={{ display: "flex" }}
                        />
                      </div>
                    </div>
                    <div className="col-lg-12" style={{ textAlign: "center" }}>
                      <button
                        type="button"
                        onClick={handleCancel}
                        className="btn btn-light"
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                        }}
                      >
                        <i className="flaticon2-cancel icon-nm"></i>
                        Batal
                      </button>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          ) : (
            ""
          )}
        </>
      </Modal.Body>
      {isShow ? (
        <Modal.Footer style={{ borderTop: "none" }}>
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              // onClick={backAction}
              onClick={onHide}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="flaticon2-cancel icon-nm"></i>
              Tutup
            </button>
          </div>
        </Modal.Footer>
      ) : (
        ""
      )}
    </Modal>
  );
}

export default MonitoringTindakLanjutSahliKajianOpen;
