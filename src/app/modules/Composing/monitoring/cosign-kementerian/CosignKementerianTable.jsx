/* Library */
import React, { useEffect, useState } from "react";
import swal from "sweetalert";
import { useHistory, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";

/* Helpers */
import { Pagination } from "../../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../../../helpers/column-formatters";
import { deleteCosignKementerian, getCosignKementerian } from "../../../Evaluation/Api";
import CosignKementerianOpen from "./CosignKementerianOpen";
import MonitoringCosignKementerianOpen from "./CosignKementerianOpen";

/* Component */
// import ProposalOpen from "./ProposalOpen";
// import ProposalReject from "./ProposalReject";

/* Utility */
function MonitoringCosignKementerianTable({ id }) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const { status } = useSelector(state => state.auth);

  const showDetil = id_cosign_kl =>
    history.push(
      `/compose/monitoring/detils/${id}/cosign-kementerian/${id_cosign_kl}/open`
    );

  useEffect(() => {
    getCosignKementerian(id).then(({ data }) => {
      setContent(data);
    });
  }, [id]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nama_unit",
      text: "Nama Unit",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_surat",
      text: "Tgl Jawaban",
      sort: true,
      formatter: columnFormatters.DateFormatterComposeProcessCosignDjp,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        status === "Proses Penyusunan"
          ? columnFormatters.ActionsColumnFormatterComposeProcessCosignKementerian
          : columnFormatters.ActionsColumnFormatterComposeProcessCosignKementerianJustView,
      formatExtraData: {
        showDetil: showDetil
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "nama_unit",
    pageNumber: 1,
    pageSize: 5
  };
  const defaultSorted = [{ dataField: "nama_unit", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  //   useEffect(() => {
  //     getPerencanaan().then(({ data }) => {
  //       data.map(data => {
  //         return data.status !== 'Terima' ? setPlan(plan => [...plan, data]) : null;
  //       })
  //     });
  //   }, []);

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_cosign_kl"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div
                          className="col-lg-6 col-xl-6 mb-3"
                          style={{ textAlign: "right" }}
                        >
                          <button
                            type="button"
                            className="btn btn-light mx3"
                            style={{
                              float: "right"
                            }}
                            onClick={() =>
                              history.push(`/compose/monitoring/${id}/detil`)
                            }
                          >
                            <i className="fa fa-arrow-left"></i>
                            Kembali
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      <Route path="/compose/monitoring/detils/:id/cosign-kementerian/:id_cosign_kl/open">
        {({ history, match }) => (
          <MonitoringCosignKementerianOpen
            show={match != null}
            id={match && match.params.id}
            id_cosign_kl={match && match.params.id_cosign_kl}
            after={false}
            onHide={() => {
              history.push(`/compose/monitoring/detils/${id}/cosign-kementerian`);
            }}
            onRef={() => {
              history.push(`/compose/monitoring/detils/${id}/cosign-kementerian`);
            }}
          />
        )}
      </Route>
    </>
  );
}

export default MonitoringCosignKementerianTable;
