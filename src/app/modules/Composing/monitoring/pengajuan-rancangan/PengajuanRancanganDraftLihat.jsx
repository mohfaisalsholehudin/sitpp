import React, { useEffect, useState, useRef } from "react";
import EmailEditor from "react-email-editor";
import axios from "axios";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../../_metronic/_partials/controls";
import { getDraft } from "../../../Evaluation/Api";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";
// import "../../proposal/process/drafting/decoupled.css"
const {BACKEND_URL, SLICE_URL} = window.ENV;


function MonitoringPengajuanRancanganDraftLihat({
  history,
  match: {
    params: { id, id_draft, name }
  }
}) {
  const [draft, setDraft] = useState("");
  const [content, setContent] = useState({
    file: "",
    lampiran: ""
  });

  const validationSchema = Yup.object().shape({
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    // if (id_draft) {
    //   axios.get(`${BACKEND_URL}/api/draftperaturan/byidpenyusunan/${id}`, {
    //     transformResponse: res => {
    //       switch (name) {
    //         case "usulan":
    //           setDraft(JSON.parse(res[0].body_draft));
    //           break;
    //         case "cosign unit":
    //           setDraft(JSON.parse(res[0].body_cosign));
    //           break;
    //         case "cosign sahli":
    //           setDraft(JSON.parse(res[0].body_sahli));
    //           break;
    //         case "cosign dirjen":
    //           setDraft(JSON.parse(res[0].body_direktur));
    //           break;

    //         default:
    //           break;
    //       }
    //       return res;
    //     },
    //     responseType: "json"
    //   });
    //   getDraft(id_draft).then(({ data }) => {
    //     switch (name) {
    //       case "usulan":
    //         setContent({
    //           file_upload: data.fileupload,
    //           lampiran: data.filelampiranr
    //         });
    //         break;
    //       case "cosign unit":
    //         setContent({
    //           file_upload: data.fileupload_cosign,
    //           lampiran: data.filelampiran_cosign
    //         });
    //         break;
    //       case "cosign sahli":
    //         setContent({
    //           file_upload: data.fieupload_sahli,
    //           lampiran: data.filelampiran_sahli
    //         });
    //         break;
    //       case "cosign dirjen":
    //         setContent({
    //           file_upload: data.fileupload_direktur,
    //           lampiran: data.filelampiran_direktur
    //         });
    //         break;

    //       default:
    //         break;
    //     }
    //   });
    // }
    if(id_draft){
      getDraft(id_draft).then(({ data }) => {
        switch(name){
          case "usulan":
            setContent({
              body_draft: data.body_draft,
              file_upload: data.fileupload,
              lampiran: data.filelampiranr
            })
            break;
          case "cosign unit":
            setContent({
              body_draft: data.body_cosign,
              file_upload: data.fileupload_cosign,
              lampiran: data.filelampiran_cosign
            })
            break;
          case "cosign sahli":
            setContent({
              body_draft: data.body_sahli,
              file_upload: data.fieupload_sahli,
              lampiran: data.filelampiran_sahli
            })
              break;
          case "cosign dirjen":
            setContent({
              body_draft: data.body_direktur,
              file_upload: data.fileupload_direktur,
              lampiran: data.filelampiran_direktur
            });
            break;
            default:
              break;
        }
      });
    }
  }, [id_draft, name, id]);
  return (
    <Card>
      <CardHeader
        title="Lihat Draft Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
        <div className="row">
            <div className="document-editor" style={{ width: "100%" }}>
              <div className="document-editor__toolbar"></div>
              <div className="document-editor__editable-container">
                <CKEditor
                  onReady={editor => {
                    window.editor = editor;

                    // Add these two lines to properly position the toolbar
                    const toolbarContainer = document.querySelector(
                      ".document-editor__toolbar"
                    );
                    toolbarContainer.appendChild(
                      editor.ui.view.toolbar.element
                    );
                  }}
                  config={{
                    simpleUpload: {
                      // The URL that the images are uploaded to.
                      uploadUrl: `${BACKEND_URL}/api/uploadckeditor`,

                      // Enable the XMLHttpRequest.withCredentials property.
                      withCredentials: false,

                      // Headers sent along with the XMLHttpRequest to the upload server.
                      headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": "true"
                      }
                    },
                    isReadOnly: true
                  }}
                  disabled = {true}
                  onChange={(event, editor) => {setDraft(editor.getData()); }}
                  editor={Editor}
                  data={content.body_draft ? content.body_draft : null}
                />
              </div>
            </div>
          </div>
          <Formik
            enableReinitialize={true}
            initialValues={content}
            validationSchema={validationSchema}
            onSubmit={values => {}}
          >
            {({ values }) => {
              return (
                <Form className="form form-label-right">
                  {/* Field File */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      File Tersimpan
                    </label>
                    <div
                      className="col-lg-9 col-xl-6"
                      style={{ marginTop: "10px" }}
                    >
                      <a
                        href={values.file_upload}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {values.file_upload
                          ? values.file_upload.slice(SLICE_URL)
                          : null}
                      </a>
                    </div>
                  </div>
                  {/* Field Lampiran */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Lampiran Tersimpan
                    </label>
                    <div
                      className="col-lg-9 col-xl-6"
                      style={{ marginTop: "10px" }}
                    >
                      <a
                        href={values.lampiran}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {values.lampiran ? values.lampiran.slice(SLICE_URL) : null}
                      </a>
                    </div>
                  </div>

                  <div className="col-lg-12" style={{ textAlign: "right" }}>
                    <button
                      type="button"
                      className="btn btn-light-success ml-2"
                      // onSubmit={() => handleSubmit()}
                      onClick={() =>
                        history.push(
                          `/compose/monitoring/detils/${id}/pengajuan-rancangan`
                        )
                      }
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="fa fa-arrow-left"></i>
                      Kembali
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </CardBody>
    </Card>
  );
}

export default MonitoringPengajuanRancanganDraftLihat;
