/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import { Modal, Table } from "react-bootstrap";
import { getDraft } from "../../../Evaluation/Api";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
// import { getPerencanaanById, getUsulanById, updateStatusPerencanaan } from "../../Evaluation/Api";

// import ResearchReject from "./ResearchReject";
// import {DateFormat} from "../../../helpers/DateFormat"

function PengajuanRancanganDraftOpen({ id, show, onHide, id_draft }) {
  const history = useHistory();
  const { status } = useSelector(state => state.auth);
  const [content, setContent] = useState([]);

  useEffect(() => {
    if (id_draft) {
      getDraft(id_draft).then(({ data }) => {
        // console.log(data);
        setContent(data);
      });
    }
  }, [id_draft]);

  const tampilTanggal = val => {
    const date = new Date(val);
    const tanggal = String(date.getDate()).padStart(2, "0");
    var bulan = String(date.getMonth() + 1).padStart(2, "0");
    const tahun = date.getFullYear();

    const jam = date.getHours();
    const menit = date.getMinutes();

    return tanggal + "-" + bulan + "-" + tahun + " - " + jam + ":" + menit;
  };

  const goLihat = name => {
    history.push(
      `/compose/monitoring/detils/${id}/pengajuan-rancangan/${id_draft}/draft/${name}/open`
    );
  };
  const goEdit = () => {
    history.push(
      `/compose/monitoring/detils/${id}/pengajuan-rancangan/${id_draft}/draft/edit`
    );
  };

  const checkStatus = status => {
    switch (status) {
      case "Cosign Dirjen":
        return (
          <a
            className="label label-lg label-light-success label-inline"
            title="Open Detils"
            onClick={() => goEdit()}
          >
            Edit
          </a>
        );
      case "Penelitian Cosign Unit/KL Es4":
        return (
          <a
            className="label label-lg label-light-primary label-inline"
            title="Open Detils"
            onClick={() => goLihat("cosign unit")}
          >
            Lihat
          </a>
        );
      case "Proses Penyusunan":
        return (
          <a
            className="label label-lg label-light-primary label-inline"
            title="Open Detils"
            onClick={() => goLihat("cosign unit")}
          >
            Lihat
          </a>
        );
      case "Penelitian Cosign Sahli":
        return (
          <a
            className="label label-lg label-light-primary label-inline"
            title="Open Detils"
            onClick={() => goLihat("cosign unit")}
          >
            Lihat
          </a>
        );
      case "Cosign Sahli":
        return (
          <a
            className="label label-lg label-light-primary label-inline"
            title="Open Detils"
            onClick={() => goLihat("cosign sahli")}
          >
            Lihat
          </a>
        );
      case "Penelitian Cosign Dirjen":
        return content.body_direktur ? (
          <a
            className="label label-lg label-light-primary label-inline"
            title="Open Detils"
            onClick={() => goLihat("cosign dirjen")}
          >
            Lihat
          </a>
        ) : (
          "-"
        );
      case "Penetapan":
        return content.body_direktur ? (
          <a
            className="label label-lg label-light-primary label-inline"
            title="Open Detils"
            onClick={() => goLihat("cosign dirjen")}
          >
            Lihat
          </a>
        ) : (
          "-"
        );
        case "Terima":
          return content.body_direktur ? (
            <a
              className="label label-lg label-light-primary label-inline"
              title="Open Detils"
              onClick={() => goLihat("cosign dirjen")}
            >
              Lihat
            </a>
          ) : (
            "-"
          );

      default:
        break;
    }
  };

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Riwayat Draft
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <Table responsive hover>
            <thead
              style={{
                border: "1px solid #3699FF",
                textAlign: "center",
                backgroundColor: "#3699FF"
              }}
            >
              <tr>
                <th>NO</th>
                <th style={{ textAlign: "left" }}>DRAFT</th>
                <th style={{ textAlign: "left" }}>TANGGAL</th>
                <th style={{ textAlign: "left" }}>AKSI</th>
              </tr>
            </thead>
            <tbody
              style={{ border: "1px solid lightgrey", textAlign: "center" }}
            >
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>1</td>
                <td style={{ textAlign: "left" }}>Draft Usulan</td>
                <td style={{ textAlign: "left" }}>
                  {content.wkt_create
                    ? tampilTanggal(content.wkt_create)
                    : null}
                </td>
                <td style={{ textAlign: "left" }}>
                  <a
                    className="label label-lg label-light-primary label-inline"
                    title="Open Detils"
                    onClick={() => goLihat("usulan")}
                  >
                    Lihat
                  </a>
                </td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>2</td>
                <td style={{ textAlign: "left" }}>Draft Cosign</td>
                <td style={{ textAlign: "left" }}>
                  {content.wkt_cosign ? tampilTanggal(content.wkt_cosign) : "-"}
                </td>
                <td style={{ textAlign: "left" }}>
                  {content.body_cosign ? (
                    <a
                      className="label label-lg label-light-primary label-inline"
                      title="Open Detils"
                      onClick={() => goLihat("cosign unit")}
                    >
                      Lihat
                    </a>
                  ) : (
                    "-"
                  )}
                </td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>3</td>
                <td style={{ textAlign: "left" }}>Draft Sahli</td>
                <td style={{ textAlign: "left" }}>
                  {content.wkt_sahli ? tampilTanggal(content.wkt_sahli) : "-"}
                </td>
                <td style={{ textAlign: "left" }}>
                  {content.body_sahli ? (
                    <a
                      className="label label-lg label-light-primary label-inline"
                      title="Open Detils"
                      onClick={() => goLihat("cosign sahli")}
                    >
                      Lihat
                    </a>
                  ) : (
                    "-"
                  )}
                </td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>4</td>
                <td style={{ textAlign: "left" }}>Draft Dirjen</td>
                <td style={{ textAlign: "left" }}>
                  {content.wkt_direktur
                    ? tampilTanggal(content.wkt_direktur)
                    : "-"}
                </td>
                <td style={{ textAlign: "left" }}>
                  {checkStatus(status)}
                  {/* <a
                    className="label label-lg label-light-success label-inline"
                    title="Open Detils"
                    onClick={() => goEdit()}
                  >
                    Edit
                  </a> */}
                </td>
              </tr>
            </tbody>
            <tfoot
              style={{
                border: "1px solid #3699FF",
                textAlign: "center",
                backgroundColor: "#3699FF"
              }}
            >
              <tr style={{ height: "40px" }}>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tfoot>
          </Table>
        </div>
      </Modal.Body>
      <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            // onClick={backAction}
            onClick={onHide}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}

export default PengajuanRancanganDraftOpen;
