/* eslint-disable no-restricted-imports */
import React, { useEffect, useState } from "react";
import Select from "react-select";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepConnector from "@material-ui/core/StepConnector";
import Button from "@material-ui/core/Button";
import {
  Input,
  Textarea,
  Select as Sel,
  DatePickerField,
  Checkbox,
  Radio
} from "../../../../helpers";
import { Field } from "formik";
import CustomZipInput from "../../../../helpers/form/CustomZipInput";
import {
  getJenisPajak,
  getJenisPeraturan,
  getRegulasiPerpajakanTerima
} from "../../../Evaluation/Api";
import CircularProgress from "@material-ui/core/CircularProgress";
import { green } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
  root: {
    width: "90%"
  },
  button: {
    marginRight: theme.spacing(1)
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  connectorActive: {
    "& $connectorLine": {
      borderColor: theme.palette.secondary.main
    }
  },
  connectorCompleted: {
    "& $connectorLine": {
      borderColor: theme.palette.primary.main
    }
  },
  connectorDisabled: {
    "& $connectorLine": {
      borderColor: theme.palette.grey[100]
    }
  },
  connectorLine: {
    transition: theme.transitions.create("border-color")
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));

function getSteps() {
  return [
    "Detil Isu Permasalahan",
    "Peraturan / Penegasan Terkait",
    "Usulan Penyusunan / Perubahan / Penggantian Peraturan / Penegasan"
  ];
}

export default function FormStepper({
  btnRef,
  check,
  setShow,
  isEdit,
  per,
  handlePeraturan, handleDownload,
  loading
}) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();
  const [showPer, setShowPer] = useState(false);
  const [peraturan, setPeraturan] = useState([]);
  const [jenisPeraturan, setJenisPeraturan] = useState([]);
  const [jenisPajak, setJenisPajak] = useState([]);

  // const [isShow, setIsShow] = useState(false);

  function handleNext() {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  }

  function handleBack() {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  }

  const connector = (
    <StepConnector
      classes={{
        active: classes.connectorActive,
        completed: classes.connectorCompleted,
        disabled: classes.connectorDisabled,
        line: classes.connectorLine
      }}
    />
  );

  const simf = [
    { name: "Ya", value: 'Ya' },
    { name: "Tidak", value: 'Tidak' }
  ];

  useEffect(() => {

    getJenisPeraturan().then(({ data }) => {
      // setJenisPeraturan(data);
      data.map(dt=> {
        return dt.status === 'AKTIF' ? setJenisPeraturan(peraturan => [...peraturan, dt]) : null
      })
    });

    getJenisPajak().then(({ data }) => {
      setJenisPajak(data);
    });
    getRegulasiPerpajakanTerima().then(({ data }) => {
      data.content.map(data => {
        if (data.no_regulasi) {
          setPeraturan(peraturan => [
            ...peraturan,
            {
              label: data.no_regulasi,
              value: data.id_peraturan
            }
          ]);
        }
      });
    });
  }, []);
  useEffect(() => {
    if (per) {
      setShowPer(true);
    }
  }, [per]);
  const handleChangePeraturan = val => {
    handlePeraturan(val);
    setShowPer(true);
  };


  function getStepContent(step) {
    switch (step) {
      case 0:
        return (
          <>
            {/* Field No Evaluasi */}
            <div className="form-group row">
              <Field
                name="no_evaluasi"
                component={Input}
                placeholder="No Evaluasi"
                label="No Evaluasi"
                disabled
                solid={"true"}
              />
            </div>

            <>
              {/* Field Tanggal Usulan */}
              <div className="form-group row">
                <DatePickerField
                  name="tgl_perencanaan"
                  label="Tanggal Perencanaan"
                />
              </div>

              {/* Field Simfoni */}
              <div className="form-group row">
              <label className="col-xl-3 col-lg-3 col-form-label">
                  Simfoni
                </label>
                <div className="col-lg-9 col-xl-6">
                {simf.map((data, index) => (
                    <Field
                      name="simfoni"
                      component={Radio}
                      type="radio"
                      value={data.value}
                      key={index}
                      content={data.name}
                    />
                ))}
                </div>
              </div>
              {/* Field Unit Incharge */}
              <div className="form-group row">
                <Field
                  name="unit_incharge"
                  component={Input}
                  placeholder="Unit Incharge"
                  label="Unit Incharge"
                  disabled
                  solid={"true"}
                />
              </div>

              {/* Field Jenis Pajak */}
              <div className="form-group row align-items-center">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Jenis Pajak
                </label>
                <div className="col-lg-9 col-xl-6">
                  <div className="checkbox-inline">
                    {jenisPajak.map((data, index) => (
                      <Field
                        component={Checkbox}
                        name="jns_pajak"
                        type="checkbox"
                        value={data.nm_jnspajak}
                        content={data.nm_jnspajak}
                        key={index}
                        // check={proposal.jns_pajak.split(',').includes(data.nm_jnspajak) }
                      />
                    ))}
                  </div>
                </div>
              </div>
              {/* Field Isu Masalah */}
              <div className="form-group row">
                <Field
                  name="isu_masalah"
                  component={Textarea}
                  placeholder="Isu Masalah"
                  label="Isu Masalah"
                />
              </div>
            </>
          </>
        );
      case 1:
        return (
          <>
            {showPer ? (
              <>
                {/* Field No Peraturan */}
                <div className="form-group row">
                  <Field
                    name="no_peraturan"
                    component={Input}
                    placeholder="Nomor Peraturan"
                    label="Nomor Peraturan"
                    disabled
                  />
                  <button
                    type="button"
                    onClick={() => setShowPer(false)}
                    className="btn btn-light-warning"
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                    }}
                  >
                    <i className="fa fa-redo"></i>
                    Reset
                  </button>
                </div>
                {/* Field Judul Peraturan */}
                <div className="form-group row">
                  <Field
                    name="judul_peraturan"
                    component={Textarea}
                    placeholder="Judul Peraturan"
                    label="Judul Peraturan"
                  />
                </div>

                {/* Field Tentang */}
                <div className="form-group row">
                  <Field
                    name="tentang"
                    component={Textarea}
                    placeholder="Tentang"
                    label="Tentang"
                  />
                </div>
              </>
            ) : (
              <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  No Peraturan
                </label>
                <div className="col-lg-9 col-xl-6">
                  <Select
                    options={peraturan}
                    onChange={value => handleChangePeraturan(value)}
                  />
                </div>
              </div>
            )}
          </>
        );
      case 2:
        return (
          <>
            {/* Field Jenis Peraturan */}
            <div className="form-group row">
              <Sel
                name="jns_peraturan"
                label="Jenis Peraturan"
              >
                <option value="">Pilih Jenis Peraturan</option>
                {jenisPeraturan.map(data => (
                  <option
                    key={data.id_jnsperaturan}
                    value={data.nm_jnsperaturan}
                  >
                    {data.nm_jnsperaturan}
                  </option>
                ))}
              </Sel>
            </div>
            {/* Field Konten Peraturan */}
            <div className="form-group row">
              <Field
                name="konten_peraturan"
                component={Textarea}
                placeholder="Konten Peraturan"
                label="Konten Peraturan"
              />
            </div>
            {/* Field Alasan */}
            <div className="form-group row">
              <Field
                name="alasan"
                component={Textarea}
                placeholder="Alasan"
                label="Alasan"
              />
            </div>
            {/* Field Analisa Dampak */}
            <div className="form-group row">
              <Field
                name="analisa_dampak"
                component={Textarea}
                placeholder="Analisis Dampak"
                label="Analisis Dampak"
              />
            </div>
              {/* FIELD UPLOAD FILE */}
              <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Unggah Dokumen Kajian
                </label>
                <div className="col-lg-9 col-xl-6">
                  <Field
                    name="file"
                    component={CustomZipInput}
                    title="Select a file"
                    label="File"
                    style={{ display: "flex" }}
                  />
                   <button
                  type="button"
                  onClick={handleDownload}
                  className="btn btn-light-primary"
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                  }}
                >
                  <i className="fas fa-download"></i>
                  Download
                </button>
                </div>
              </div>
          </>
        );

      default:
        return "Unknown step";
    }
  }
  const saveForm = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  const reset = () => {
    setShow();
  };

  return (
    <div className={classes.root}>
      <Stepper alternativeLabel activeStep={activeStep} connector={connector}>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        <div>
          {getStepContent(activeStep)}
          <div className="text-right">

            {isEdit ? 
            <Button
              disabled={activeStep === 0}
              onClick={handleBack}
              className={classes.button}
            >
              Kembali
            </Button> : 
            activeStep === 0 ? (
              <Button onClick={reset} className={classes.button}>
                Reset
              </Button>
            ) : (
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.button}
              >
                Kembali
              </Button>
            )
            }
            
            {activeStep === steps.length - 1 ? (
                 <Button
                 variant="contained"
                 color="secondary"
                 onClick={saveForm}
                 disabled={check}
                 className={classes.button}
               >
                 {loading && <CircularProgress size={24} className={classes.buttonProgress} color="secondary" />}
                 {loading ? 'Menyimpan. .' : 'Simpan'}
               </Button>
            ) : (
              <Button
                variant="contained"
                color="secondary"
                onClick={handleNext}
                className={classes.button}
              >
                Selanjutnya
              </Button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
