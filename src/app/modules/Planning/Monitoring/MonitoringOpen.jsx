/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import { Modal, Table } from "react-bootstrap";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import SVG from "react-inlinesvg";
import { getPerencanaanById, getValidasiById } from "../../Evaluation/Api";
const { DOWNLOAD_URL, SLICE_ZIP } = window.ENV;

function MonitoringOpen({ id, show, onHide }) {
  const [validasi, setValidasi] = useState([]);
  const [perencanaan, setPerencanaan] = useState([]);

  useEffect(() => {
    if (id) {
      getPerencanaanById(id).then(({ data }) => {
        setPerencanaan(data);
        getValidasiById(data.id_validator).then(({ data }) => {
          setValidasi(data);
        });
      });
    }
  }, [id]);
  const handleDownload = values => {
    window.open(DOWNLOAD_URL + values.slice(SLICE_ZIP));
  };

  return (
    <Modal
      size="xl"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detil Usulan Perencanaan
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <Table responsive hover>
            <thead
              style={{
                border: "1px solid #3699FF",
                textAlign: "center",
                backgroundColor: "#3699FF"
              }}
            >
              <tr>
                <th>NO</th>
                <th style={{ textAlign: "left" }}>Nama</th>
                <th style={{ textAlign: "left" }}>Usulan</th>
                <th style={{ textAlign: "left" }}>Perencanaan</th>
              </tr>
            </thead>
            <tbody
              style={{ border: "1px solid lightgrey", textAlign: "center" }}
            >
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>1</td>
                <td style={{ textAlign: "left" }}>No. Peraturan</td>
                <td style={{ textAlign: "left" }}>{validasi.no_peraturan}</td>
                <td style={{ textAlign: "left" }}>
                  {perencanaan.no_peraturan}
                </td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>2</td>
                <td style={{ textAlign: "left" }}>Judul Peraturan</td>
                <td style={{ textAlign: "left" }}>
                  {validasi.judul_peraturan}
                </td>
                <td style={{ textAlign: "left" }}>
                  {perencanaan.judul_peraturan}
                </td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>3</td>
                <td style={{ textAlign: "left" }}>Jenis Peraturan</td>
                <td style={{ textAlign: "left" }}>{validasi.jns_peraturan}</td>
                <td style={{ textAlign: "left" }}>
                  {perencanaan.jns_peraturan}
                </td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>4</td>
                <td style={{ textAlign: "left" }}>Konten Peraturan</td>
                <td style={{ textAlign: "left" }}>
                  {validasi.konten_peraturan}
                </td>
                <td style={{ textAlign: "left" }}>
                  {perencanaan.konten_peraturan}
                </td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>5</td>
                <td style={{ textAlign: "left" }}>Jenis Pajak</td>
                <td style={{ textAlign: "left" }}>
                  {validasi.usulan ? validasi.usulan.jns_pajak : null}
                </td>
                <td style={{ textAlign: "left" }}>{perencanaan.jns_pajak}</td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>6</td>
                <td style={{ textAlign: "left" }}>Isu Masalah</td>
                <td style={{ textAlign: "left" }}>{validasi.isu_masalah}</td>
                <td style={{ textAlign: "left" }}>{perencanaan.isu_masalah}</td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>7</td>
                <td style={{ textAlign: "left" }}>Tentang</td>
                <td style={{ textAlign: "left" }}>{validasi.tentang}</td>
                <td style={{ textAlign: "left" }}>{perencanaan.tentang}</td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>8</td>
                <td style={{ textAlign: "left" }}>Alasan</td>
                <td style={{ textAlign: "left" }}>{validasi.alasan}</td>
                <td style={{ textAlign: "left" }}>{perencanaan.alasan}</td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>9</td>
                <td style={{ textAlign: "left" }}>Analisa Dampak</td>
                <td style={{ textAlign: "left" }}>{validasi.analisa_dampak}</td>
                <td style={{ textAlign: "left" }}>
                  {perencanaan.analisa_dampak}
                </td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>10</td>
                <td style={{ textAlign: "left" }}>Simfoni</td>
                <td style={{ textAlign: "left" }}>-</td>
                <td style={{ textAlign: "left" }}>{perencanaan.simfoni}</td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>11</td>
                <td style={{ textAlign: "left" }}>File</td>
                <td style={{ textAlign: "left" }}>
                  {validasi.file_upload
                    ? validasi.file_upload.slice(SLICE_ZIP)
                    : null}
                  <a
                    title="Open Proposal"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() => handleDownload(validasi.file_upload)}
                  >
                    <span className="svg-icon svg-icon-md svg-icon-primary">
                      <SVG
                        src={toAbsoluteUrl(
                          "/media/svg/icons/Files/Download.svg"
                        )}
                      />
                    </span>
                  </a>
                </td>
                <td style={{ textAlign: "left" }}>
                  {perencanaan.file_kajian
                    ? perencanaan.file_kajian.slice(SLICE_ZIP)
                    : null}
                  <a
                    title="Open Proposal"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() => handleDownload(validasi.file_upload)}
                  >
                    <span className="svg-icon svg-icon-md svg-icon-primary">
                      <SVG
                        src={toAbsoluteUrl(
                          "/media/svg/icons/Files/Download.svg"
                        )}
                      />
                    </span>
                  </a>
                </td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>12</td>
                <td style={{ textAlign: "left" }}>Konseptor</td>
                <td style={{ textAlign: "left" }}>{validasi.nm_pegawai}</td>
                <td style={{ textAlign: "left" }}>{perencanaan.nm_perekam}</td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>13</td>
                <td style={{ textAlign: "left" }}>Unit Konseptor</td>
                <td style={{ textAlign: "left" }}>{validasi.unit_incharge}</td>
                <td style={{ textAlign: "left" }}>
                  {perencanaan.unit_incharge}
                </td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>14</td>
                <td style={{ textAlign: "left" }}>Catatan Eselon 4</td>
                <td style={{ textAlign: "left" }}>{validasi.catatan_es4}</td>
                <td style={{ textAlign: "left" }}>{perencanaan.catatan_es4}</td>
              </tr>
              <tr style={{ borderBottom: "1px solid lightgrey" }}>
                <td>15</td>
                <td style={{ textAlign: "left" }}>Catatan Eselon 3</td>
                <td style={{ textAlign: "left" }}>{validasi.catatan_es3}</td>
                <td style={{ textAlign: "left" }}>{perencanaan.catatan_es3}</td>
              </tr>
            </tbody>
            <tfoot
              style={{
                border: "1px solid #3699FF",
                textAlign: "center",
                backgroundColor: "#3699FF"
              }}
            >
              <tr style={{ height: "40px" }}>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tfoot>
          </Table>
        </div>

        {/* <div className="row">
          <div className="col-lg-9 col-xl-6">
            <h5 className="mb-3 mt-2" style={{ fontWeight: "bold" }}>
              USULAN
            </h5>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Nomor Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${usulan.no_surat}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Tgl Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${DateFormat(usulan.tgl_surat)}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Perihal</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${usulan.perihal}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">
              Instansi Penerbit/ Unit Kerja
            </span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${usulan.instansi_unit}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-9 col-xl-6">
            <h5 className="mb-3 mt-2" style={{ fontWeight: "bold" }}>
              PERENCANAAN
            </h5>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">No Evaluasi</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.no_evaluasi}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">No Perencanaan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.no_perencanaan}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Tgl Perencanaan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${DateFormat(data.tgl_perencanaan)}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Jenis Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.jns_peraturan}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Nomor Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.no_peraturan}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Judul Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.judul_peraturan}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Jenis Pajak</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.jns_pajak}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Simfoni</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.simfoni}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Isu Permasalahan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.isu_masalah}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Tentang</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.tentang}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Konten Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.konten_peraturan}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Alasan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.alasan}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">
              Analisa Dampak Kebijakan
            </span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.analisa_dampak}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Unit Incharge</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.unit_incharge}`}
            </span>
          </div>
        </div>
        {data.catatan ? (
          <div className="row">
            <div className="col-lg-6 col-xl-6 mb-3">
              <span className="font-weight-bold mr-2">Catatan</span>
            </div>
            <div className="col-lg-6 col-xl-6 mb-3">
              <span className="text-muted text-hover-primary">
                {`: ${data.catatan}`}
              </span>
            </div>
          </div>
        ) : null} */}
      </Modal.Body>
      <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}

export default MonitoringOpen;
