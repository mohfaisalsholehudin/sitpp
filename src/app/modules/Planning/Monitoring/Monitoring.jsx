import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import MonitoringTable from "./MonitoringTable";

function Monitoring() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Surat Usulan Evaluasi Regulasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <MonitoringTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Monitoring;
