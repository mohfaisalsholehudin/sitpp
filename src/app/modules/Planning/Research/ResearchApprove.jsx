import React from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Textarea } from "../../../helpers";

function ResearchApprove({ handleCancel, approve }) {
  const initialValues = {
    catatan: ""
  };

  const validationSchema = Yup.object().shape({
    catatan: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Catatan is required")
  });

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={values => {
        //   console.log(values);
        approve(values)
        }}
      >
        {({ handleSubmit }) => {
          return (
            <Form className="form form-label-right">
              <div className="form-group row" style={{ marginBottom: "0px" }}>
                <div className="col-lg-9 col-xl-6">
                  <h5 className="mt-6" style={{ fontWeight: "600" }}>
                    CATATAN
                  </h5>
                </div>
              </div>
              <div className="form-group row">
                <Field
                  name="catatan"
                  component={Textarea}
                  placeholder="Catatan"
                  custom={"custom"}
                />
              </div>
              <div className="col-lg-12" style={{ textAlign: "center" }}>
                <button
                  type="button"
                  onClick={handleCancel}
                  className="btn btn-light"
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                  }}
                >
                  <i className="flaticon2-cancel icon-nm"></i>
                  Batal
                </button>
                {`  `}
                <button
                  type="submit"
                  onSubmit={() => handleSubmit()}
                  className="btn btn-success ml-2"
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                  }}
                >
                  <i className="fas fa-check"></i>
                  Kirim
                </button>
              </div>
            </Form>
          );
        }}
      </Formik>
    </>
  );
}

export default ResearchApprove;
