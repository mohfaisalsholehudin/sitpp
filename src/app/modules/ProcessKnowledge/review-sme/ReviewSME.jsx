import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls"; 
import ReviewSMETable from "./ReviewSMETable";

function ReviewSME() {

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Review Subject Matter Expert"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <ReviewSMETable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default ReviewSME;
