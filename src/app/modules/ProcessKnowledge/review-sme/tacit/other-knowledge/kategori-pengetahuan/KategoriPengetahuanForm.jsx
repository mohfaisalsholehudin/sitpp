import React, { useEffect, useState } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import KategoriPengetahuanTable from "../kategori-pengetahuan/KategoriPengetahuanTable";
import Select from "react-select";
import { Input, Textarea, Select as Sel} from "../../../../../../helpers";
import { getKnowledgebyId, updateKnowledge, updateKnowledgePublishPKP, updateStatusPKP, updateStatusSME } from "../../../../Api";
import { useHistory } from "react-router-dom";

function KategoriPengetahuanForm({
  content,
  jenis,
  btnRef,
  btnPublish,
  media,
  step,
  id_km_pro,
  showButton,
  hideButton,
  showForm,
  hideForm
}) {
  const history = useHistory();
  const [knowledge, setKnowledge] = useState([]);
  const [isShowPengetahuan, setIsShowPengetahuan] = useState(false);
  const [table, setTable] = useState(false);
  const [isShowForm, setIsShowForm] = useState(true);
  
  useEffect(() => {
    getKnowledgebyId(id_km_pro).then(({data}) => {
      setKnowledge({
            tipe_konten: data.tipe_konten,
            id_probis: data.id_probis,
            id_sektor: data.id_sektor,
            id_csname: data.id_csname,
            id_subcase: data.id_subcase,
            id_tipe_km: data.id_tipe_km,
            id_km_pro: data.id_km_pro,
            judul: data.judul,
            media_upload: data.media_upload,
            catatan_pkp: data.catatan_pkp,
            catatan_ko: data.catatan_ko,
            kategori: data.kategori,
            level_knowledge: data.level_knowledge,
        });
    });
  })

  const initialValues = {
    id_km_pro: id_km_pro,
    kategori: knowledge.kategori,
    level_knowledge: knowledge.level_knowledge
  }

  const ValidateSchema = Yup.object().shape({
    kategori: Yup.string()
    .required("Kategori Pengetahuan is required"),
    level_knowledge: Yup.string()
    .required("Level Akses is required")
  });

  console.log(knowledge.level_knowledge)
  useEffect(() => {
    knowledge.level_knowledge != 1 ? setTable(false) : setTable(true);
    isShowPengetahuan ? showButton() : hideButton();
    isShowForm ? showForm() : hideForm();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [knowledge, isShowPengetahuan, isShowForm]);

  const saveSimpan = (values) => {
    swal({
      title: "Setuju",
      text: "Apakah Anda Yakin Menyimpan Draft Usulan Ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
        updateKnowledgePublishPKP(
          id_km_pro,
          content.id_csname,
          content.id_ko,
          content.id_probis,
          content.id_sektor,
          content.id_sme,
          content.id_subcase,
          content.id_tipe_km,
          values.kategori,
          values.level_knowledge
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
                swal("Berhasil", "Draft Usulan Berhasil Disimpan", "success").then(() => {
                  history.push("/dashboard");
                  history.replace(`/process-knowledge/review-sme`);
            })
        } else {
          swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(`/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/5/review`);
          });
      }
    })
    }
      })
}

const savePublish = (values) => {
  if(values.kategori && values.level_knowledge){
  swal({
    title: "Setuju",
    text: "Apakah Anda Ingin Menyetujui Usulan Ini ?",
    icon: "warning",
    buttons: true
  }).then(ret => {
    if (ret == true) {
      updateKnowledgePublishPKP(
        id_km_pro,
        content.id_csname,
        content.id_ko,
        content.id_probis,
        content.id_sektor,
        content.id_sme,
        content.id_subcase,
        content.id_tipe_km,
        values.kategori,
        values.level_knowledge
  ).then(({ status }) => {
    if (status === 201 || status === 200) {
      updateStatusSME(content.id_km_pro).then(({status}) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan Berhasil Disetujui", "success").then(() => {
              history.push("/dashboard");
              history.replace(`/process-knowledge/review-sme`);
        })
    } else {
      swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
        history.push("/dashboard");
        history.replace(`/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/5/review`);
      });
    }
  });
  }
})
}
  })
}
}

  return (
    <>
      <>
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          validationSchema={ValidateSchema}
          onSubmit={values => {
            savePublish(values);
          }}
        >
          {({ handleSubmit, setFieldValue, values, errors }) => {
            return (
              <>
                <Form className="form form-label-right">
                  {/* FIELD Kategori Pengetahuan */}
                  <div className="form-group row">
                      <Sel 
                        name="kategori" 
                        label="Kategori Pengetahuan"
                        disabled
                        >
                          <option key ={knowledge.kategori} value={knowledge.kategori}>
                            {knowledge.kategori === "mudah" ? "Mudah"
                              : "Sulit"}
                          </option>
                      </Sel>
                  </div>
                  {/* FIELD Level Akses */}
                    <div className="form-group row">
                      <Sel 
                          name="level_knowledge" 
                          label="Level Akses"
                          disabled
                          >
                          <option key ={knowledge.level_knowledge} value={knowledge.level_knowledge}>
                            {knowledge.level_knowledge}
                          </option>
                      </Sel>
                    <button
                      type="submit"
                      style={{ display: "none" }}
                      ref={btnPublish}
                      onSubmit={() => handleSubmit()}
                    ></button>
                    <button
                      type="button"
                      style={{ display: "none" }}
                      ref={btnRef}
                      onClick={() => saveSimpan(values)}
                    ></button>
                  </div>
                </Form>
              </>
            );
          }}
        </Formik>
      </>
      {table ? <KategoriPengetahuanTable mdeia={media} step={step} jenis={jenis} id_km_pro={id_km_pro}/> : null}
    </>
  );
}

export default KategoriPengetahuanForm;
