import React, { useEffect, useState, useRef } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
// import { DatePickerField, Input, Textarea, Select } from "../../helpers";
// import CustomFileInput from "../../helpers/CustomFileInput";
import CustomFileInput from "../../../../../helpers/form/CustomFileInput";
import { Input, Textarea, Select as Sel} from "../../../../../helpers";
import MateriPokok from "./materi-pokok/MateriPokok";

function ReviewSMEOtherKnowledgeFormTwo({
  content,
  handleChangeEvaluation,
  setShow,
  btnRef,
  saveFormTwo,
  draft,
  setDraft,
  savePlan,
  isEdit,
  loading,
  media
}) {
  // const [ValidateSchema, setValidateSchema] = useState();

  // useEffect(() => {
  //   if (media === "dokumen") {
  //     setValidateSchema(
  //       Yup.object().shape({
  //         tipe_knowledge: Yup.string().required("Tipe Knowledge is required"),
  //         proses_bisnis: Yup.string().required("Proses Bisnis is required"),
  //         binis_sektor: Yup.string().required("Bisnis Sektor is required"),
  //         casename: Yup.string().required("Case Name is required"),
  //         subcase: Yup.string().required("Sub Case is required"),
  //         judul: Yup.string()
  //           .min(2, "Minimum 2 symbols")
  //           .required("Judul is required"),
  //         pendahuluan: Yup.string()
  //           .min(2, "Minimum 2 symbols")
  //           .required("Pendahuluan is required"),
  //         daftar_isi: Yup.string()
  //           .min(2, "Minimum 2 symbols")
  //           .required("Daftar Isi is required"),
  //         definisi: Yup.string()
  //           .min(2, "Minimum 2 symbols")
  //           .required("Definisi is required"),
  //         tagging: Yup.string()
  //           .min(2, "Minimum 2 symbols")
  //           .required("Tagging is required"),
  //         // materi_pokok: Yup.string()
  //         //   .min(2, "Minimum 2 symbols")
  //         //   .required("Materi Pokok is required")
  //         // file: Yup.mixed()
  //         //   .required("A file is required")
  //         //   .test(
  //         //     "fileSize",
  //         //     "File too large",
  //         //     value => value && value.size <= FILE_SIZE
  //         //   )
  //         //   .test(
  //         //     "fileFormat",
  //         //     "Unsupported Format",
  //         //     value => value && SUPPORTED_FORMATS.includes(value.type)
  //         //   )
  //       })
  //     );
  //   } else {
  //     setValidateSchema(
  //       Yup.object().shape({
  //         tipe_knowledge: Yup.string().required("Tipe Knowledge is required"),
  //         proses_bisnis: Yup.string().required("Proses Bisnis is required"),
  //         binis_sektor: Yup.string().required("Bisnis Sektor is required"),
  //         casename: Yup.string().required("Case Name is required"),
  //         subcase: Yup.string().required("Sub Case is required"),
  //         judul: Yup.string()
  //           .min(2, "Minimum 2 symbols")
  //           .required("Judul is required"),
  //         pendahuluan: Yup.string()
  //           .min(2, "Minimum 2 symbols")
  //           .required("Pendahuluan is required"),
  //         daftar_isi: Yup.string()
  //           .min(2, "Minimum 2 symbols")
  //           .required("Daftar Isi is required"),
  //         definisi: Yup.string()
  //           .min(2, "Minimum 2 symbols")
  //           .required("Definisi is required"),
  //         tagging: Yup.string()
  //           .min(2, "Minimum 2 symbols")
  //           .required("Tagging is required"),
  //         // materi_pokok: Yup.string()
  //         //   .min(2, "Minimum 2 symbols")
  //         //   .required("Materi Pokok is required"),
  //         file: Yup.mixed()
  //           .required("A file is required")
  //           .test(
  //             "fileSize",
  //             "File too large",
  //             value => value && value.size <= FILE_SIZE
  //           )
  //           .test(
  //             "fileFormat",
  //             "Unsupported Format",
  //             value => value && SUPPORTED_FORMATS.includes(value.type)
  //           )
  //       })
  //     );
  //   }
  // // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [media]);
  // const ValidateSchema =  
  //   Yup.object().shape({
  //     materi_pokok: Yup.string()
  //       .min(2, "Minimum 2 symbols")
  //       .required("Materi Pokok is required")
  //   })

  // const FILE_SIZE = 50000000;
  // const SUPPORTED_FORMATS = [
  //   "application/x-rar-compressed",
  //   "application/octet-stream",
  //   "application/zip",
  //   "application/octet-stream",
  //   "application/x-zip-compressed",
  //   "multipart/x-zip",
  //   "application/vnd.rar"
  // ];
  // const btnRef = useRef();

  return (
    <>
      <MateriPokok
      draft={draft}
      setDraft={setDraft}
       />
      <button
        type="submit"
        style={{ display: "none" }}
        ref={btnRef}
        onClick={() => saveFormTwo()}
      ></button>
      {/* <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ValidateSchema}
        onSubmit={values => {
          // console.log(values);
          saveForm(values);
        }}
      >
        {({ handleSubmit, setFieldValue, isValid, values, isSubmitting }) => {
          // const handleDownload =() => {
          //   window.open(DOWNLOAD_URL + values.file.file.slice(SLICE_ZIP));
          // }
          return (
            <>
              <Form className="form form-label-right">
               <MateriPokok />
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
              </Form>
            </>
          );
        }}
      </Formik> */}
    </>
  );
}

export default ReviewSMEOtherKnowledgeFormTwo;
