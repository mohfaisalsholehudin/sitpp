import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import { Textarea, Select as Sel } from "../../../../../helpers";

function ReviewSMESuccessStoryTolakForm({ content, btnRef, saveForm }) {

  const [ValidateSchema, setValidateSchema] = useState();

  const initialValues = {
    id_km_pro: content.id_km_pro,
    catatan_tolak: ""
  }

  useEffect(() => {
      setValidateSchema(
        Yup.object().shape({
          catatan_tolak: Yup.string().required("Catatan Tolak is required"),
        })
        );
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={ValidateSchema}
        onSubmit={values => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          {/* console.log(errors) */}
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Catatan Tolak */}
                <div className="form-group row">
                <Field
                    name="catatan_tolak"
                    component={Textarea}
                    placeholder="Alasan Tolak"
                    label="Alasan Tolak"
                />
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ReviewSMESuccessStoryTolakForm;
