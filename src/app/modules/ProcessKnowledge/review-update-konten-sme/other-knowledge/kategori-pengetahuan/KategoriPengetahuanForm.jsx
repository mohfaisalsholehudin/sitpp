import React, { useEffect, useState } from "react";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import KategoriPengetahuanTable from "../kategori-pengetahuan/KategoriPengetahuanTable";
import Select from "react-select";
import { getLevelAksesbyIdKM, updateKnowledge, updateKnowledgePublishPKP, updateStatusPKP } from "../../../Api";
import { useHistory } from "react-router-dom";
import { updateStatusKnowledgeProcessDisposisi } from "../../../../../references/Api";

function KategoriPengetahuanForm({
  content,
  jenis,
  btnRef,
  btnPublish,
  media,
  step,
  id_km_pro,
  showButton,
  hideButton,
  showForm,
  hideForm,
  lastPath,
  id_tipe_km,
  path,
  showButtonTable,
  hideButtonTable
}) {
  const history = useHistory();
  const [isShow, setIsShow] = useState(false);
  const [isShowPengetahuan, setIsShowPengetahuan] = useState(false);
  const [table, setTable] = useState(false);
  const [isShowForm, setIsShowForm] = useState(true);
  const [valKategori, setValKategori] = useState([]);
  const [valLevel, setValLevel] = useState([]);
  
  // const initialValues = {
  //   id_km_pro: id_km_pro,
  //   kategori: "",
  //   level_knowledge: ""
  // }

  const ValidateSchema = Yup.object().shape({
    kategori: Yup.string()
    .required("Kategori Pengetahuan is required"),
    level_knowledge: Yup.string()
    .required("Level Akses is required")
  });

  useEffect(() => {
    if(content ? content.kategori : null)
    {
      if(content.kategori === "sulit"){
        setIsShowPengetahuan(true);
        showButton();
        setIsShowForm(false);
        hideForm();
      } else {
        hideButton();
        changeForm();
      }
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [content ? content.kategori : null]);

  useEffect(() => {
    if(content ? content.level_knowledge : null)
    {
      if(content.level_knowledge === "1"){
        setIsShow(true);
        setTable(true);
        getLevelAksesbyIdKM(id_km_pro).then(({data})=> {
          if(data.length > 0){
           showButtonTable()
          }else{
           hideButtonTable()
          }
       })
      } else {
        change();
      }
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [content ? content.level_knowledge : null]);

  // console.log(table)

  const dataLevel = [
    {
      label: "1 - Secret",
      value: "1"
    },
    {
      label: "2 - Confidential",
      value: "2"
    },
    {
      label: "3 - Shareable",
      value: "3"
    },
    {
      label: "4 - Public",
      value: "4"
    }
  ];

  const dataPengetahuan = [
    {
      label: "Mudah",
      value: "mudah"
    },
    {
      label: "Sulit",
      value: "sulit"
    }
  ];

  useEffect(()=> {
    // console.log(content ? content.kategori : 'tidak ada')
    if(content ? content.kategori : null){
      dataPengetahuan.filter(data=>data.value === content.kategori)
      .map(data => {
        setValKategori(data.label);
        // console.log(data.label)
        // console.log('masuk sini')
      });
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[content ? content.kategori : null])

  useEffect(()=> {
    // console.log(content ? content.kategori : 'tidak ada')
    if(content ? content.level_knowledge : null){
      dataLevel.filter(data=>data.value === content.level_knowledge)
      .map(data => {
        setValLevel(data.label);
        // console.log(data.label)
        // console.log('masuk sini')
      });
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[content ? content.level_knowledge : null])


  // console.log(valKategori)
  const change = () => {
    setTable(false);
    setIsShow(false);
    showButtonTable()
  };

  const changePengetahuan = () => {
    setIsShowPengetahuan(false);
    hideButton();
  };

  const changeForm = () => {
    setIsShowForm(true)
    showForm();
  }

  const renderError = (message) => <p style={{ color: "red" }}>{message}</p>;

  const saveSimpan = (values) => {
    swal({
      title: "Setuju",
      text: "Apakah Anda Yakin Menyimpan Draft Usulan Ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret === true) {
        updateKnowledgePublishPKP(
          id_km_pro,
          content.id_csname,
          content.id_ko,
          content.id_probis,
          content.id_sektor,
          content.id_sme,
          content.id_subcase,
          content.id_tipe_km,
          values.kategori,
          values.level_knowledge
        ).then(({ status }) => {
      if (status === 201 || status === 200) {
              swal("Berhasil", "Draft Usulan Berhasil Disimpan", "success").then(() => {
                history.push("/dashboard");
                history.replace(`/process-knowledge/sme-update-review`);
          })
      } else {
        swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace(`/process-knowledge/sme-update-review/${id_tipe_km}/${id_km_pro}/5/detil`);
        });
    }
  })
  }
    })
  }

  const savePublish = (values) => {
    if(values.kategori && values.level_knowledge){
    swal({
      title: "Setuju",
      text: "Apakah Anda Ingin Menyetujui Usulan Ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret === true) {
        updateKnowledgePublishPKP(
          id_km_pro,
          content.id_csname,
          content.id_ko,
          content.id_probis,
          content.id_sektor,
          content.id_sme,
          content.id_subcase,
          content.id_tipe_km,
          values.kategori,
          values.level_knowledge
      ).then(({ status }) => {
      if (status === 201 || status === 200) {
        updateStatusKnowledgeProcessDisposisi(
          content.id_km_pro, 
          18,
          ).then(({status}) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan Berhasil Disetujui", "success").then(() => {
                history.push("/dashboard");
                history.replace(`/process-knowledge/sme-update-review`);
          })
      } else {
        swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace(`/process-knowledge/sme-update-review/${id_tipe_km}/${id_km_pro}/5/detil`);
        });
      }
    });
    }
  })
}
    })
  }
}
// console.log(content)

  return (
    <>
      <>
        <Formik
          enableReinitialize={true}
          initialValues={content}
          validationSchema={ValidateSchema}
          onSubmit={values => {
            // console.log(values);
            savePublish(values);
          }}
        >
          {({ handleSubmit, setFieldValue, values, errors }) => {
            const handleChangeLevel = val => {
              // console.log(val.value);
              setFieldValue("level_knowledge", val.value);
              setValLevel(val.label);
              if(val.value === '1'){
                setIsShow(true);
                setTable(true);
                hideButtonTable()
                getLevelAksesbyIdKM(id_km_pro).then(({data})=> {
                  if(data.length > 0){
                   showButtonTable()
                  }else{
                   hideButtonTable()
                  }
               })
              } else {
                change();
              }
            };

            const handleChangePengetahuan = val => {
              setFieldValue("kategori", val.value);
              setValKategori(val.label);
                if(val.value === "sulit"){
                  setIsShowPengetahuan(true);
                  showButton();
                  setTable(false)
                } else {
                  hideButton();
                }
            };

            const handleChangeForm = val => {
              if(val.value === 'sulit'){
                setIsShowForm(false);
                hideForm();
              } else {
                changeForm();
              }
            }
            return (
              <>
                <Form className="form form-label-right">
                  {/* FIELD Kategori Pengetahuan */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Kategori Pengetahuan
                    </label>
                    <div className="col-lg-9 col-xl-6">
                    {path === "view" ? (
                        <Select
                          options={dataPengetahuan}
                          onChange={(value) => {
                            handleChangePengetahuan(value);
                            handleChangeForm(value);
                          }}
                          value={dataPengetahuan.filter(
                            (data) => data.label === valKategori
                          )}
                          isDisabled
                        />
                      ) : (
                        <Select
                          options={dataPengetahuan}
                          onChange={(value) => {
                            handleChangePengetahuan(value);
                            handleChangeForm(value);
                          }}
                          value={dataPengetahuan.filter(
                            (data) => data.label === valKategori
                          )}
                          isDisabled
                        />
                      )}
                      <ErrorMessage name="kategori" render={renderError} />
                    </div>
                  </div>
                  {/* FIELD Level Akses */}
                  {/* { isShowForm ? ( */}
                    <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Level Akses
                    </label>
                    <div className="col-lg-9 col-xl-6">
                    {path === "view" ? (
                          <Select
                            options={dataLevel}
                            onChange={(value) => handleChangeLevel(value)}
                            value={dataLevel.filter(
                              (data) => data.label === valLevel
                            )}
                            isDisabled
                          />
                        ) : (
                          <Select
                            options={dataLevel}
                            onChange={(value) => handleChangeLevel(value)}
                            value={dataLevel.filter(
                              (data) => data.label === valLevel
                            )}
                            isDisabled
                          />
                        )}
                      <ErrorMessage name="level_knowledge" render={renderError} />
                    </div>
                   
                  </div>
                  {/* ) : null} */}
                   <button
                      type="submit"
                      style={{ display: "none" }}
                      ref={btnPublish}
                      onSubmit={() => handleSubmit()}
                    ></button>
                    <button
                      type="button"
                      style={{ display: "none" }}
                      ref={btnRef}
                      onClick={() => saveSimpan(values)}
                    ></button>
                </Form>
              </>
            );
          }}
        </Formik>
      </>
      {table ? <KategoriPengetahuanTable mdeia={media} step={step} jenis={jenis} id_km_pro={id_km_pro} id_tipe_km={id_tipe_km} lastPath={lastPath} knowledge={content}/> : null}
    </>
  );
}

export default KategoriPengetahuanForm;
