/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";
import { useSelector } from "react-redux";

/* Helper */
import { useSubheader } from "../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../_metronic/_partials/controls";
import { saveKnowledgeProcess } from "../../../references/Api";

/* Content */
import SimpleKnowledgeEditForm from "./SimpleKnowledgeEditForm";

function SimpleKnowledgeEdit({
  history,
  match: {
    params: { id },
  },
}) {
  const initValues = {
    id_tipe_km: "",
    tipe_konten: "",
  };

  // Subheader
  const suhbeader = useSubheader();
  const { user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");

  useEffect(() => {
    let _title = id ? "Edit Simple Knowledge" : "Tambah Simple Knowledge";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, suhbeader]);

  const btnRef = useRef();
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  const backAction = () => {
    history.push("/process-knowledge/simple-knowledge");
  };
  const saveForm = (values) => {
    if (!id) {
      saveKnowledgeProcess(
        values.id_tipe_km,
        values.tipe_konten,
        values.jenis,
        user.nip9,
        user.namaPegawai,
        user.kantorLegacyKode,
        user.unit,
        values.jenis === "Tacit" ? "simple_tacit" : "simple_explicit"
      ).then(({ status, data }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            return history.push(
              `/process-knowledge/simple-knowledge/${data.id_tipe_km}/${data.id_km_pro}/1/add`
            );
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/process-knowledge/simple-knowledge/add");
          });
        }
      });
    } else {
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <SimpleKnowledgeEditForm
              content={initValues}
              btnRef={btnRef}
              saveForm={saveForm}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <DetilJenisEditFooter backAction={backAction} btnRef={btnRef} /> */}
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
            // disabled={disabled}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
        </div>
      </CardFooter>
    </Card>
  );
}

export default SimpleKnowledgeEdit;
