import React, { useEffect, useState } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import KategoriPengetahuanTable from "../kategori-pengetahuan/KategoriPengetahuanTable";
import { Select as Sel } from "../../../../../helpers";
import { getKnowledgebyId } from "../../../Api";

function KategoriPengetahuanDetailForm({
  jenis,
  btnRef,
  media,
  step,
  id_km_pro,
  showButton,
  hideButton,
  showForm,
  hideForm,
  lastPath
}) {
    const [knowledge, setKnowledge] = useState([]);
    const [isShowPengetahuan, setIsShowPengetahuan] = useState(false);
    const [table, setTable] = useState(false);
    const [isShowForm, setIsShowForm] = useState(true);

    useEffect(() => {
      getKnowledgebyId(id_km_pro).then(({data}) => {
        setKnowledge({
              kategori: data.kategori,
              level_knowledge: data.level_knowledge,
          });
      });
    })
  
    const initialValues = {
      id_km_pro: id_km_pro,
      kategori: knowledge.kategori,
      level_knowledge: knowledge.level_knowledge
    }
  
    const ValidateSchema = Yup.object().shape({
      kategori: Yup.string()
      .required("Kategori Pengetahuan is required"),
      level_knowledge: Yup.string()
      .required("Level Akses is required")
    });
  
    useEffect(() => {
      knowledge.level_knowledge != 1 ? setTable(false) : setTable(true);
      isShowPengetahuan ? showButton() : hideButton();
      isShowForm ? showForm() : hideForm();
    }, [knowledge, isShowPengetahuan, isShowForm, showButton, hideButton, showForm, hideForm]);
  
    return (
      <>
        <>
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            validationSchema={ValidateSchema}
          >
            {({ handleSubmit }) => {
              return (
                <>
                  <Form className="form form-label-right">
                    {/* FIELD Kategori Pengetahuan */}
                    <div className="form-group row">
                        <Sel 
                          name="kategori" 
                          label="Kategori Pengetahuan"
                          disabled
                          >
                            <option key ={knowledge.kategori} value={knowledge.kategori}>
                              {knowledge.kategori === "mudah"
                              ? "Mudah" : "Sulit"}
                            </option>
                        </Sel>
                    </div>
                    {/* FIELD Level Akses */}
                      <div className="form-group row">
                        <Sel 
                            name="level_knowledge" 
                            label="Level Akses"
                            disabled
                            >
                            <option key ={knowledge.level_knowledge} value={knowledge.level_knowledge}>
                              {knowledge.level_knowledge}
                            </option>
                        </Sel>
                      <button
                        type="submit"
                        style={{ display: "none" }}
                        ref={btnRef}
                        onSubmit={() => handleSubmit()}
                      ></button>
                    </div>
                  </Form>
                </>
              );
            }}
          </Formik>
        </>
        {table ? <KategoriPengetahuanTable mdeia={media} step={step} jenis={jenis} id_km_pro={id_km_pro} lastPath={lastPath}/> : null}
      </>
    );
  }

export default KategoriPengetahuanDetailForm;
