/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import KategoriPengetahuanTable from "../kategori-pengetahuan/KategoriPengetahuanTable";
import Select from "react-select";
import {
  getLevelAksesbyIdKM,
  updateKnowledgePublishPKP,
  updateStatusPKP,
} from "../../../Api";
import { useHistory } from "react-router-dom";

function KategoriPengetahuanForm({
  content,
  jenis,
  btnRef,
  btnPublish,
  media,
  step,
  id_km_pro,
  path,
  // showButton,
  // hideButton,
  // showForm,
  // hideForm,
  lastPath,
  // showButtonTable,
  // hideButtonTable,
  id_tipe_km,
  saveForm,
}) {
  const history = useHistory();
  const [isShow, setIsShow] = useState(false);
  const [isShowPengetahuan, setIsShowPengetahuan] = useState(false);
  const [table, setTable] = useState(false);
  const [isShowForm, setIsShowForm] = useState(true);
  const [valKategori, setValKategori] = useState([]);
  const [valLevel, setValLevel] = useState([]);
  const [tempKategori, setTempKategori] = useState("");
  const [tempLevel, setTempLevel] = useState("");

  const initialValues = {
    id_km_pro: id_km_pro,
    kategori: "",
    level_knowledge: "",
  };

  const ValidateSchema = Yup.object().shape({
    kategori: Yup.string().required("Kategori Pengetahuan is required"),
    level_knowledge: Yup.string().required("Level Akses is required"),
  });

  useEffect(() => {
    if (content ? content.kategori : null) {
      setTempKategori(content.kategori);
      if (content.kategori === "sulit") {
        setIsShowPengetahuan(true);
        // showButton();
        // setIsShowForm(false);
        // hideForm();
      } else {
        // hideButton();
        changeForm();
      }
    }
  }, [content ? content.kategori : null]);

  useEffect(() => {
    if (content ? content.level_knowledge : null) {
      setTempLevel(content.level_knowledge);
      if (content.level_knowledge === "1") {
        setIsShow(true);
        setTable(true);
        // getLevelAksesbyIdKM(id_km_pro).then(({ data }) => {
        //   if (data.length > 0) {
        //     showButtonTable();
        //   } else {
        //     hideButtonTable();
        //   }
        // });
      } else {
        change();
      }
    }
  }, [content ? content.level_knowledge : null]);

  const dataLevel = [
    {
      label: "1 - Secret",
      value: "1",
    },
    {
      label: "2 - Confidential",
      value: "2",
    },
    {
      label: "3 - Shareable",
      value: "3",
    },
    {
      label: "4 - Public",
      value: "4",
    },
  ];

  const dataPengetahuan = [
    {
      label: "Mudah",
      value: "mudah",
    },
    {
      label: "Sulit",
      value: "sulit",
    },
  ];

  useEffect(() => {
    if (content ? content.kategori : null) {
      dataPengetahuan
        .filter((data) => data.value === content.kategori)
        .map((data) => {
          setValKategori(data.label);
        });
    }
  }, [content ? content.kategori : null]);

  useEffect(() => {
    if (content ? content.level_knowledge : null) {
      dataLevel
        .filter((data) => data.value === content.level_knowledge)
        .map((data) => {
          setValLevel(data.label);
        });
    }
  }, [content ? content.level_knowledge : null]);

  const change = () => {
    setTable(false);
    setIsShow(false);
    // showButtonTable();
  };

  const changeForm = () => {
    setIsShowForm(true);
    // showForm();
  };

  const renderError = (message) => <p style={{ color: "red" }}>{message}</p>;

  return (
    <>
      <>
        <Formik
          enableReinitialize={true}
          initialValues={content || initialValues}
          validationSchema={ValidateSchema}
          onSubmit={(values) => {
            saveForm(values);
            // console.log(values)
          }}
        >
          {({ handleSubmit, setFieldValue, values, errors }) => {
            const handleChangeLevel = (val) => {
              setFieldValue("level_knowledge", val.value);
              setTempLevel(val.value);
              setValLevel(val.label);
              if (val.value === "1") {
                setIsShow(true);
                setTable(true);
                // hideButtonTable();
                // getLevelAksesbyIdKM(id_km_pro).then(({ data }) => {
                //   if (data.length > 0) {
                //     showButtonTable();
                //   } else {
                //     hideButtonTable();
                //   }
                // });
              } else {
                change();
              }
            };

            const handleChangePengetahuan = (val) => {
              setFieldValue("kategori", val.value);
              setTempKategori(val.value);
              setValKategori(val.label);
              if (val.value === "sulit") {
                // setValLevel([]);
                setIsShowPengetahuan(true);
                // showButton();
                setTable(false);
                // showButtonTable();
              } else {
                // hideButton();
              }
            };

            const handleChangeForm = (val) => {
              if (val.value === "sulit") {
                // setValLevel([]);
                // setIsShowForm(false);
                // hideForm();
              } else {
                changeForm();
              }
            };
            return (
              <>
                <Form className="form form-label-right">
                  {/* FIELD Kategori Pengetahuan */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Kategori Pengetahuan
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <Select
                        isDisabled={path === "view" ? true : false}
                        options={dataPengetahuan}
                        onChange={(value) => {
                          handleChangePengetahuan(value);
                          handleChangeForm(value);
                        }}
                        value={dataPengetahuan.filter(
                          (data) => data.label === valKategori
                        )}
                      />
                      <ErrorMessage name="kategori" render={renderError} />
                    </div>
                  </div>
                  {/* FIELD Level Akses */}
                  {isShowForm ? (
                    <div className="form-group row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Level Akses
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <Select
                          isDisabled={path === "view" ? true : false}
                          options={dataLevel}
                          onChange={(value) => handleChangeLevel(value)}
                          value={dataLevel.filter(
                            (data) => data.label === valLevel
                          )}
                        />
                        <ErrorMessage
                          name="level_knowledge"
                          render={renderError}
                        />
                      </div>
                    </div>
                  ) : null}
                  <button
                    type="submit"
                    style={{ display: "none" }}
                    ref={btnRef}
                    onSubmit={() => handleSubmit()}
                  ></button>
                  {/* <button
                    type="button"
                    style={{ display: "none" }}
                    ref={btnRef}
                    onClick={() => saveSimpan(values)}
                  ></button> */}
                </Form>
              </>
            );
          }}
        </Formik>
      </>
      {table ? (
        <KategoriPengetahuanTable
          mdeia={media}
          step={step}
          jenis={jenis}
          id_km_pro={id_km_pro}
          id_tipe_km={id_tipe_km}
          lastPath={lastPath}
          knowledge={content}
          path={path}
          tempKategori={tempKategori}
          tempLevel={tempLevel}
        />
      ) : null}
    </>
  );
}

export default KategoriPengetahuanForm;
