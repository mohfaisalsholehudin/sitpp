import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { Route, useHistory } from "react-router-dom";

//Helpers
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../../../_metronic/_helpers";
import { Pagination } from "../../../../../helpers/pagination/Pagination";
import * as columnFormatters from "../../../../../helpers/column-formatters";
import KategoriPengetahuanModal from "./KategoriPengetahuanModal";
import { deleteLevelAkses, getLevelAksesbyIdKM } from "../../../Api";

function KategoriPengetahuanTable({
  id_km_pro,
  id_tipe_km,
  jenis,
  step,
  lastPath,
  knowledge,
  tempKategori,
  tempLevel,
  path,
}) {
  const history = useHistory();
  const [content, setContent] = useState([]);

  useEffect(() => {
    getLevelAksesbyIdKM(id_km_pro).then(({ data }) => {
      setContent(data);
    });
  }, [id_km_pro]);

  const add = () => {
    history.push(
      // `/process-knowledge/review-pkp/${id_km_pro}/success-story/${jenis}/${step}/review/kategori-pengetahuan/open`
      `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add/level-akses/open`
    );
  };

  const deleteAction = (id_level_detil) => {
    swal({
      title: "Apakah Anda Ingin Menghapus Level Akses Ini?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        deleteLevelAkses(id_level_detil).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              // history.replace(`/process-knowledge/review-pkp/${id_km_pro}/success-story/${jenis}/${step}/review`);
              history.replace(
                `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`
              );
            });
          } else {
            swal("Gagal", "Data gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              // history.replace(`/process-knowledge/review-pkp/${id_km_pro}/success-story/${jenis}/${step}/review`);
              history.replace(
                `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`
              );
            });
          }
        });
      }
    });
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "nm_es2",
      text: "Eselon 2",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "nm_es3",
      text: "Eselon 3",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "nm_es4",
      text: "Eselon 4",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "nm_jabatan",
      text: "Jabatan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterProcessKnowledgeSuccessStoryLevel,
      formatExtraData: {
        openDeleteDialog: deleteAction,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
      hidden: path === "view" ? true : false,
    },
  ];

  const columnsDetail = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "nm_es2",
      text: "Direktorat",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "nm_es3",
      text: "Eselon 3",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "nm_es4",
      text: "Eselon 4",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "nm_jabatan",
      text: "Jabatan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
  ];

  const emptyDataMessage = () => {
    return <div className="text-center">No Data to Display</div>;
  };
  const { SearchBar } = Search;
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_level_detil",
    pageNumber: 1,
    pageSize: 50,
  };
  const defaultSorted = [{ dataField: "id_level_detil", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                {path !== "view" ? (
                  <ToolkitProvider
                    keyField="id_level_detil"
                    data={content}
                    columns={columns}
                    search
                  >
                    {(props) => (
                      <div>
                        <div className="row">
                          <div className="col-lg-6 col-xl-6 mb-3">
                            <SearchBar
                              {...props.searchProps}
                              style={{ width: "500px" }}
                            />
                            <br />
                          </div>
                          <div
                            className="col-lg-6 col-xl-6 mb-3"
                            style={{ textAlign: "right" }}
                          >
                            <button
                              type="button"
                              className="btn btn-primary ml-3"
                              style={{
                                float: "right",
                              }}
                              onClick={add}
                            >
                              <i className="fa fa-plus"></i>
                              Tambah
                            </button>
                          </div>
                        </div>
                        <BootstrapTable
                          {...props.baseProps}
                          wrapperClasses="table-responsive"
                          bordered={false}
                          headerWrapperClasses="thead-light"
                          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                          defaultSorted={defaultSorted}
                          bootstrap4
                          noDataIndication={emptyDataMessage}
                          {...paginationTableProps}
                        ></BootstrapTable>
                        <Pagination paginationProps={paginationProps} />
                      </div>
                    )}
                  </ToolkitProvider>
                ) : (
                  <ToolkitProvider
                    keyField="id_level_detil"
                    data={content}
                    columns={columnsDetail}
                    search
                  >
                    {(props) => (
                      <div>
                        <div className="row">
                          <div className="col-lg-6 col-xl-6 mb-3">
                            <SearchBar
                              {...props.searchProps}
                              style={{ width: "500px" }}
                            />
                            <br />
                          </div>
                        </div>
                        <BootstrapTable
                          {...props.baseProps}
                          wrapperClasses="table-responsive"
                          bordered={false}
                          headerWrapperClasses="thead-light"
                          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                          defaultSorted={defaultSorted}
                          bootstrap4
                          noDataIndication={emptyDataMessage}
                          {...paginationTableProps}
                        ></BootstrapTable>
                        <Pagination paginationProps={paginationProps} />
                      </div>
                    )}
                  </ToolkitProvider>
                )}
              </>
            );
          }}
        </PaginationProvider>
      </>
      {/*Route to Open Details*/}
      <Route path="/process-knowledge/simple-knowledge/:id_tipe_km/:id_km_pro/:step/add/level-akses/open">
        {({ history, match }) => (
          <KategoriPengetahuanModal
            show={match != null}
            content={content}
            knowledge={knowledge}
            id_level_detil={match && match.params.id_level_detil}
            id_km_pro={id_km_pro}
            id_tipe_km={id_tipe_km}
            jenis={jenis}
            step={step}
            after={false}
            tempKategori={tempKategori}
            tempLevel={tempLevel}
            onHide={() => {
              history.push(
                `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`
              );
            }}
            onRef={() => {
              history.push(
                `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`
              );
            }}
          />
        )}
      </Route>
      {/*Route to Edit Details*/}
      {/* <Route path="/process-knowledge/review-pkp/:id_km_pro/success-story/:jenis/:step/review/kategori-pengetahuan/:id_level_detil/edit">
        {({ history, match }) => (
          <KategoriPengetahuanModal
            show={match != null}
            id_level_detil={match && match.params.id_level_detil}
            content={content}
            knowledge={knowledge}
            id_km_pro={id_km_pro}
            jenis={jenis}
            step={step}
            after={false}
            onHide={() => {
              history.push(
                `/process-knowledge/review-pkp/${id_km_pro}/success-story/${jenis}/${step}/review`
              );
            }}
            onRef={() => {
              history.push(
                `/process-knowledge/review-pkp/${id_km_pro}/success-story/${jenis}/${step}/review`
              );
            }}
          />
        )}
      </Route> */}
    </>
  );
}

export default KategoriPengetahuanTable;
