import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Checkbox } from "../../../../../helpers";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import {
  getMappingProbisByIdKnowledge,
  saveMappingProbis,
  updateMappingProbis,
} from "../../../../../references/Api";
import { getProbisByStatus } from "../../../Api";

function ProbisModal({
  show,
  onHide,
  after,
  id_detil,
  id_km_pro,
  id_tipe_km,
  step,
  idMap
}) {
  const history = useHistory();
  const [content, setContent] = useState();
  const [probis, setProbis] = useState([]);
  const [isCheck, setIsCheck] = useState([]);
  const [isCheckAll, setIsCheckAll] = useState(false);
  const [isUpdate, setIsUpdate] = useState(false);

  useEffect(() => {
    getMappingProbisByIdKnowledge(id_km_pro).then(({ data }) => {
      if (data.length > 0) {
        setIsUpdate(true);
        setContent({
          id_km_pro: id_km_pro,
          idProbis: data.map((dt) => "" + dt.id_probis),
        });
      }
    });
    getProbisByStatus(1).then(({ data }) => {
      data.map((data) => {
        return setProbis((probis) => [...probis, data]);
      });
    });
  }, [id_km_pro]);
  const validationSchema = Yup.object().shape({
    idProbis: Yup.array()
      .min(1, "Proses Bisnis is required")
      .required("Proses Bisnis is required"),
  });

  const saveProbis = (values) => {
    if(!idMap){
      saveMappingProbis(values).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push(`/dashboard`);
            history.replace(
              `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`
            );
          });
        }
      });
    } else {
      updateMappingProbis(values).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push(`/dashboard`);
            history.replace(`/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`);
          });
        }
      });
    }
  };

  const initialValues = {
    idProbis: "",
    id_km_pro: id_km_pro,
  };

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Tambah Proses Bisnis
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <Formik
            enableReinitialize={true}
            initialValues={content || initialValues}
            validationSchema={validationSchema}
            onSubmit={(values) => {
              // console.log(values);
              saveProbis(values);
            }}
          >
            {({ handleSubmit, setFieldValue, values }) => {
              const handleSelectAll = (e) => {
                setIsCheckAll(!isCheckAll);
                setIsCheck(probis.map((li) => "" + li.id_probis));
                setFieldValue(
                  "idProbis",
                  probis.map((li) => "" + li.id_probis)
                );
                if (isCheckAll) {
                  setIsCheck([]);
                  setFieldValue("idProbis", []);
                }
              };

              const handleClick = (e) => {
                const { id, checked } = e.target;
                setIsCheck([...isCheck, id]);
                if (!checked) {
                  setIsCheck(isCheck.filter((item) => item !== id));
                }
              };
              return (
                <Form className="form form-label-right">
                  {/* FIELD PROSES BISNIS */}
                  <div className="form-group row align-items-center">
                    <div className="col-lg-12 col-xl-12">
                      <div className="checkbox-inline">
                        {probis.map((data, index) => (
                          <Field
                            component={Checkbox}
                            name="idProbis"
                            type="checkbox"
                            value={"" + data.id_probis}
                            content={data.nama}
                            key={index}
                          />
                        ))}
                      </div>
                    </div>
                  </div>
                  <div className="form-group row align-items-center">
                    <div className="col-lg-12 col-xl-12">
                      <label
                        className="checkbox"
                        style={{
                          marginBottom: "15px",
                          float: "left",
                          width: "25%",
                          marginLeft: "18px",
                          marginTop: "-20px",
                        }}
                      >
                        <input
                          id="selectAll"
                          name="selectAll"
                          type="checkbox"
                          className="form-control form-control-lg is-valid"
                          onChange={handleSelectAll}
                          checked={isCheckAll}
                        />
                        <span style={{ marginRight: "10px" }}></span>Pilih Semua
                      </label>
                    </div>
                  </div>
                  <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Batal
                    </button>
                    {`  `}
                    <button
                      type="submit"
                      onSubmit={() => handleSubmit()}
                      className="btn btn-success ml-2"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                    >
                      <i className="fas fa-check"></i>
                      Kirim
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </Modal.Body>
    </Modal>
  );
}

export default ProbisModal;
