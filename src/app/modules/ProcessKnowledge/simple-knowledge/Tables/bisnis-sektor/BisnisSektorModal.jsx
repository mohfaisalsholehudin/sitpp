import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Checkbox } from "../../../../../helpers";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import {
  getBisnisSektorByStatus,
  getMappingProbisByIdKnowledge,
  getMappingSektorByIdKnowledge,
  saveMappingProbis,
  saveMappingSektor,
  updateMappingSektor,
} from "../../../../../references/Api";

function BisnisSektorModal({
  show,
  onHide,
  after,
  id_detil,
  id_km_pro,
  id_tipe_km,
  step,
  idMap
}) {
  const history = useHistory();
  const [content, setContent] = useState();
  const [sektor, setSektor] = useState([]);
  const [isCheck, setIsCheck] = useState([]);
  const [isCheckAll, setIsCheckAll] = useState(false);
  const [isUpdate, setIsUpdate] = useState(false);

  useEffect(() => {
    getMappingSektorByIdKnowledge(id_km_pro).then(({ data }) => {
      if (data.length > 0) {
        setIsUpdate(true);
        setContent({
          id_km_pro: id_km_pro,
          idSektor: data.map((dt) => "" + dt.id_sektor),
        });
      }
    });
    getBisnisSektorByStatus(1).then(({ data }) => {
      data.map((data) => {
        return setSektor((sektor) => [...sektor, data]);
      });
    });
  }, [id_km_pro]);
  const validationSchema = Yup.object().shape({
    idSektor: Yup.array()
      .min(1, "Bisnis Sektor is required")
      .required("Bisnis Sektor is required"),
  });

  const saveSektor = (values) => {
    if(!idMap){
      saveMappingSektor(values).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push(`/dashboard`);
            history.replace(
              `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`
            );
          });
        }
      });
    } else {
      updateMappingSektor(values).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push(`/dashboard`);
            history.replace(
              `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`
            );
          });
        }
      });
    }
  };

  const initialValues = {
    idSektor: "",
    id_km_pro: id_km_pro,
  };

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Tambah Bisnis Sektor
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <Formik
            enableReinitialize={true}
            initialValues={content || initialValues}
            validationSchema={validationSchema}
            onSubmit={(values) => {
              // console.log(values);
              saveSektor(values);
            }}
          >
            {({ handleSubmit, setFieldValue, values }) => {
              const handleSelectAll = (e) => {
                setIsCheckAll(!isCheckAll);
                setIsCheck(sektor.map((li) => "" + li.id_sektor));
                setFieldValue(
                  "idSektor",
                  sektor.map((li) => "" + li.id_sektor)
                );
                if (isCheckAll) {
                  setIsCheck([]);
                  setFieldValue("idSektor", []);
                }
              };

              return (
                <Form className="form form-label-right">
                  {/* FIELD BISNIS SEKTOR */}
                  <div className="form-group row align-items-center">
                    <div className="col-lg-12 col-xl-12">
                      <div className="checkbox-inline">
                        {sektor.map((data, index) => (
                          <Field
                            component={Checkbox}
                            name="idSektor"
                            type="checkbox"
                            value={"" + data.id_sektor}
                            content={data.nama}
                            key={index}
                          />
                        ))}
                      </div>
                    </div>
                  </div>
                  <div className="form-group row align-items-center">
                    <div className="col-lg-12 col-xl-12">
                      <label
                        className="checkbox"
                        style={{
                          marginBottom: "15px",
                          float: "left",
                          width: "25%",
                          marginLeft: "18px",
                          marginTop: "-20px",
                        }}
                      >
                        <input
                          id="selectAll"
                          name="selectAll"
                          type="checkbox"
                          className="form-control form-control-lg is-valid"
                          onChange={handleSelectAll}
                          checked={isCheckAll}
                        />
                        <span style={{ marginRight: "10px" }}></span>Pilih Semua
                      </label>
                    </div>
                  </div>
                  <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Batal
                    </button>
                    {`  `}
                    <button
                      type="submit"
                      onSubmit={() => handleSubmit()}
                      className="btn btn-success ml-2"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                    >
                      <i className="fas fa-check"></i>
                      Kirim
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </Modal.Body>
    </Modal>
  );
}

export default BisnisSektorModal;
