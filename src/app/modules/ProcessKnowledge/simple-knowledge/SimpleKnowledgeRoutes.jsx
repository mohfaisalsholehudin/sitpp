import React, { useEffect, useState } from "react";
import { getTipeKnowledgeById } from "../../../references/Api";
import SimpleKnowledgeOtherKnowledge from "./other-knowledge/SimpleKnowledgeOtherKnowledge";
import SimpleKnowledgeSop from "./sop/SimpleKnowledgeSop";
import SimpleKnowledgeSuccessStory from "./success-story/SimpleKnowledgeSuccessStory";

function SimpleKnowledgeRoutes({
  history,
  match: {
    params: { id, step, id_km_pro, id_tipe_km },
  },
}) {
  const [template, setTemplate] = useState("");
  const [tipeKm, setTipeKm] = useState("");
  let thePath = document.URL;
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);

  useEffect(() => {
    getTipeKnowledgeById(id_tipe_km).then(({ data }) => {
      setTemplate(data.template);
      setTipeKm(data.nama);
    });
  }, [id_tipe_km]);

  const checkType = (template) => {
    switch (template) {
      case "Success Story":
        return (
          <SimpleKnowledgeSuccessStory
            id={id}
            id_km_pro={id_km_pro}
            id_tipe_km={id_tipe_km}
            step={step}
            tipe_knowledge={tipeKm}
            path={lastPath}
          />
        );
      case "SOP":
        return (
          <SimpleKnowledgeSop
            id={id}
            id_km_pro={id_km_pro}
            id_tipe_km={id_tipe_km}
            step={step}
            tipe_knowledge={tipeKm}
            path={lastPath}
          />
        );
      case "Other Knowledge":
        return (
          <SimpleKnowledgeOtherKnowledge
            id={id}
            id_km_pro={id_km_pro}
            id_tipe_km={id_tipe_km}
            step={step}
            tipe_knowledge={tipeKm}
            path={lastPath}
          />
        );
      default:
        break;
    }
  };
  return <div>{checkType(template)}</div>;
}

export default SimpleKnowledgeRoutes;
