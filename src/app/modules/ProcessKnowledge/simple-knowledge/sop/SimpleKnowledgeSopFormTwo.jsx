import React from "react";
import DocumentEditorKM from "../../../../helpers/editor/DocumentEditorKM";

function SimpleKnowledgeSopFormTwo({ btnRef, saveForm, draft, setDraft, path }) {
  return (
    <>
      <DocumentEditorKM
      content={draft}
      setDraft={setDraft}
      isReadOnly={path === "view" ? true : false}
      />
      <button
        type="submit"
        style={{ display: "none" }}
        ref={btnRef}
        onClick={() => saveForm()}
      ></button>
    </>
  );
}

export default SimpleKnowledgeSopFormTwo;
