/* Library */
import React, { useEffect, useState, useRef } from "react";
import { Modal, Spinner } from "react-bootstrap";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";

/* Helper */
import { useSubheader } from "../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";
import {
  getKnowledgeProcessById,
  getOtherKnowledgeByIdKmPro,
  saveOtherKnowledgeMateri,
  updateKnowledgeProcess,
  updateKnowledgeProcessKategoriLevel,
  updateKnowledgeProcessMedia,
  updateOtherKnowledgeMateri,
  uploadKmi,
} from "../../../../references/Api";
import SimpleKnowledgeOtherKnowledgeForm from "./SimpleKnowledgeOtherKnowledgeForm";
import SimpleKnowledgeOtherKnowledgeFormTwo from "./SimpleKnowledgeOtherKnowledgeFormTwo";
import { getLevelAksesbyIdKM } from "../../Api";
import BisnisSektorTable from "../Tables/bisnis-sektor/BisnisSektorTable";
import ProbisTable from "../Tables/probis/ProbisTable";
import CasenameTable from "../Tables/casename/CasenameTable";
import SubcaseTable from "../Tables/subcase/SubcaseTable";
import PeraturanTerkaitTable from "../Tables/peraturan-terkait/PeraturanTerkaitTable";
import UploadFileTable from "../Tables/upload-file/UploadFileTable";
import UnitOrganisasiTable from "../Tables/unit-organisasi/UnitOrganisasiTable";
import KategoriPengetahuanForm from "../Tables/kategori-pengetahuan/KategoriPengetahuanForm";
import TugasFungsiTable from "../Tables/tugas-fungsi/TugasFungsiTable";

function SimpleKnowledgeOtherKnowledge({
  id,
  step,
  id_km_pro,
  id_tipe_km,
  tipe_knowledge,
  path,
  // history,
}) {
  // Subheader
  const suhbeader = useSubheader();
  const history = useHistory();
  const { user } = useSelector((state) => state.auth);
  const { SLICE_UPLOAD } = window.ENV;

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [loading, setLoading] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);
  const [content, setContent] = useState();
  const [draft, setDraft] = useState("");
  const [idTemp, setIdTemp] = useState();
  const [materi, setMateri] = useState();
  const [media, setMedia] = useState("");

  const initValues = {
    tipe_knowledge: tipe_knowledge,
    tipe_konten: media,
    id_probis: "",
    id_sektor: "",
    id_csname: "",
    id_subcase: "",
    id_tipe_km: id_tipe_km,
    id_km_pro: id_km_pro,
    judul: "",
    file: "",
    link_djpforum: ""
  };
  useEffect(() => {
    if (path === "view") {
      let _title = "Detail Knowledge";
      setTitle(_title);
      suhbeader.setTitle(_title);
    } else {
      // let _title = "Review Knowledge";
      let _title = id ? "Edit KM Tacit" : "Tambah KM Tacit";
      setTitle(_title);
      suhbeader.setTitle(_title);
    }
    // let _title = id ? "Edit KM Tacit" : "Tambah KM Tacit";

    // setTitle(_title);
    // suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id_km_pro) {
      getKnowledgeProcessById(id_km_pro).then(({ data }) => {
        setMedia(data.tipe_konten);
        if (data.judul) {
          getOtherKnowledgeByIdKmPro(id_km_pro).then((data_2) => {
            // console.log(data_2.data[0].successStoryTemp);
            setContent({
              tipe_knowledge: data.tipeKnowledge.nama,
              level_knowledge: data.level_knowledge
                ? data.level_knowledge
                : null,
              kategori: data.kategori ? data.kategori : null,
              tipe_konten: data.tipe_konten,
              id_probis: data.id_probis,
              id_sektor: data.id_sektor,
              id_csname: data.id_csname,
              id_subcase: data.id_subcase,
              id_tipe_km: data.id_tipe_km,
              id_km_pro: data.id_km_pro,
              judul: data.judul,
              file_upload: data.media_upload,
              catatan_tolak: data.catatan_tolak,
              link_djpforum: data.link_djpforum,
            });
            setMateri(
              data_2.data[0] ? data_2.data[0].defaultTemp.materi : null
            );
          });
          checkIdTemp();
        } else {
          checkIdTemp();
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, id_km_pro, id_tipe_km, media, suhbeader]);

  const checkIdTemp = () => {
    getOtherKnowledgeByIdKmPro(id_km_pro).then(({ data }) => {
      if (data.length > 0) {
        setIdTemp(data[0].defaultTemp.id_default_temp);
      }
    });
  };
  const btnRef = useRef();

  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  const backAction = () => {
    switch (step) {
      case "2":
        history.push(
          `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/1/${path}`
        );
        break;
      case "3":
        history.push(
          `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/2/${path}`
        );
        break;
      case "4":
        history.push(
          `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/3/${path}`
        );
        break;
      default:
        break;
    }
  };

  const saveForm = (values) => {
    switch (step) {
      case "1":
        saveFormOne(values);
        break;
      case "2":
        saveFormTwo();
        break;
      case "3":
        history.push(
          `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/4/${path}`
        );
        break;
      case "4":
        // history.push(`/process-knowledge/simple-knowledge`);
        saveFormFour(values);
        break;

      default:
        break;
    }
  };
  const saveFormOne = (values) => {
    if (!id) {
      if (values.file) {
        if (values.file.name) {
          enableLoading();
          const formData = new FormData();
          formData.append("file", values.file);
          uploadKmi(formData).then(({ data }) => {
            disableLoading();
            updateKnowledgeProcessMedia(
              values.id_km_pro,
              values.id_tipe_km,
              values.tipe_konten,
              values.id_sektor,
              values.id_probis,
              values.id_csname,
              values.id_subcase,
              values.judul,
              data.message,
              user.nip18,
              values.link_djpforum
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                switch (step) {
                  case "1":
                    history.push(
                      `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/2/${path}`
                    );
                    break;
                  case "2":
                    history.push(
                      `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/3/${path}`
                    );
                    break;
                  case "3":
                    history.push(`/process-knowledge/simple-knowledge`);
                    break;

                  default:
                    break;
                }
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push(`/process-knowledge/simple-knowledge/${path}`);
                });
              }
            });
          });
        } else {
          updateKnowledgeProcessMedia(
            values.id_km_pro,
            values.id_tipe_km,
            values.tipe_konten,
            values.id_sektor,
            values.id_probis,
            values.id_csname,
            values.id_subcase,
            values.judul,
            values.file_upload,
            user.nip18,
            values.link_djpforum
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              switch (step) {
                case "1":
                  history.push(
                    `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/2/${path}`
                  );
                  break;
                case "2":
                  history.push(
                    `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/3/${path}`
                  );
                  break;
                case "3":
                  history.push(`/process-knowledge/simple-knowledge`);
                  break;

                default:
                  break;
              }
            }
          });
        }
      } else {
        updateKnowledgeProcess(
          values.id_km_pro,
          values.id_tipe_km,
          values.tipe_konten,
          values.id_sektor,
          values.id_probis,
          values.id_csname,
          values.id_subcase,
          values.judul,
          user.nip18,
          values.link_djpforum
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            switch (step) {
              case "1":
                history.push(
                  `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/2/${path}`
                );
                break;
              case "2":
                history.push(
                  // `/process-knowledge/simple-knowledge/success-story/3/simple`
                  `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/3/${path}`
                );
                break;
              case "3":
                history.push(`/process-knowledge/simple-knowledge`);
                break;

              default:
                break;
            }
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/process-knowledge/simple-knowledge/${path}`);
            });
          }
        });
      }
    }
  };

  const saveFormTwo = () => {
    if (idTemp) {
      updateOtherKnowledgeMateri(idTemp, draft ? draft : materi).then(
        ({ status }) => {
          if (status === 201 || status === 200) {
            switch (step) {
              case "1":
                history.push(
                  `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/2/${path}`
                );
                break;
              case "2":
                history.push(
                  `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/3/${path}`
                );
                break;
              case "3":
                history.push(`/process-knowledge/simple-knowledge`);
                break;
              default:
                break;
            }
          }
        }
      );
    } else {
      saveOtherKnowledgeMateri(id_km_pro, id_tipe_km, draft).then(
        ({ status }) => {
          if (status === 201 || status === 200) {
            switch (step) {
              case "1":
                history.push(
                  `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/2/${path}`
                );
                break;
              case "2":
                history.push(
                  `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/3/${path}`
                );
                break;
              case "3":
                history.push(`/process-knowledge/simple-knowledge`);
                break;
              default:
                break;
            }
          }
        }
      );
    }
  };
  const saveFormFour = (values) => {
    // console.log(values)
    if (values.level_knowledge === "1") {
      getLevelAksesbyIdKM(id_km_pro).then(({ data }) => {
        if (data.length < 1) {
          swal({
            title: "Harap Masukkan Minimal 1 Detil Level",
            text: "Klik OK untuk melanjutkan",
            icon: "info",
            closeOnClickOutside: false,
          }).then((willApply) => {
            if (willApply) {
              // history.push("/logout");
            }
          });
        } else {
          updateKnowledgeProcessKategoriLevel(
            id_km_pro,
            values.kategori,
            values.level_knowledge
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Knowledge berhasil disimpan", "success").then(
                () => {
                  history.push("/dashboard");
                  history.replace(`/process-knowledge/simple-knowledge`);
                }
              );
            }
          });
        }
      });
    } else {
      // console.log(values)
      updateKnowledgeProcessKategoriLevel(
        id_km_pro,
        values.kategori,
        values.level_knowledge
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Knowledge berhasil disimpan", "success").then(
            () => {
              history.push("/dashboard");
              history.replace(`/process-knowledge/simple-knowledge`);
            }
          );
        }
      });
    }
  };
  const checkStep = (val) => {
    switch (val) {
      case "1":
        return (
          <SimpleKnowledgeOtherKnowledgeForm
            content={content || initValues}
            btnRef={btnRef}
            saveForm={saveForm}
            media={media}
            path={path}
          />
        );
      case "2":
        return (
          <SimpleKnowledgeOtherKnowledgeFormTwo
            content={initValues}
            btnRef={btnRef}
            saveForm={saveForm}
            draft={materi}
            setDraft={setDraft}
            path={path}
          />
        );
      case "3":
        return (
          <>
            <BisnisSektorTable
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <ProbisTable
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <CasenameTable
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <SubcaseTable
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <PeraturanTerkaitTable
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <UploadFileTable
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <UnitOrganisasiTable
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <TugasFungsiTable
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <button
              type="submit"
              style={{ display: "none" }}
              ref={btnRef}
              onClick={() => saveForm()}
            ></button>
          </>
        );
      case "4":
          return (
            <KategoriPengetahuanForm
              id_tipe_km={id_tipe_km}
              id_km_pro={id_km_pro}
              btnRef={btnRef}
              saveForm={saveForm}
              media={media}
              path={path}
              step={step}
              content={content}
            />
          );
      default:
        break;
    }
  };

  const enableLoading = () => {
    setLoading(true);
    setIsDisabled(true);
  };

  const disableLoading = () => {
    setLoading(false);
    setIsDisabled(false);
  };
  const homeDialog = (id) => {
    swal({
      title: "Kembali",
      text: "Apakah Anda Ingin Kembali Ke Halaman Tambah Simple Knowledge ?",
      icon: "warning",
      buttons: true,
    }).then((ret) => {
      if (ret == true) {
        // acceptAction(penegasan.status);
        history.push(`/process-knowledge/simple-knowledge/`);
      }
    });
  };

  return (
    <>
      <Card>
        <CardHeader
          title={title}
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <>
            <div className="mt-5">{checkStep(step)}</div>
          </>
        </CardBody>
        <CardFooter style={{ borderTop: "none" }}>
          <div className="col-lg-12" style={{ textAlign: "right" }}>
            {step !== "1" ? (
              <button
                type="button"
                onClick={backAction}
                className="btn btn-light"
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                }}
              >
                <i className="fa fa-arrow-left"></i>
                Kembali
              </button>
            ) : null}
            {`  `}

            {step === "4" ? (
              path === "view" ? (
                <button
                  type="button"
                  className="btn btn-primary ml-2"
                  onClick={homeDialog}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={disabled}
                >
                  <i className="fas fa-home"></i>
                  Kembali ke Halaman Utama
                </button>
              ) : (
                <button
                  type="submit"
                  className="btn btn-success ml-2"
                  onClick={saveButton}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                >
                  <i className="fas fa-save"></i>
                  Simpan
                </button>
              )
            ) : loading ? (
              <button
                type="submit"
                className="btn btn-success spinner spinner-white spinner-left ml-2"
                onClick={saveButton}
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                }}
                disabled={isDisabled}
              >
                <span>Selanjutnya</span>
              </button>
            ) : (
              <button
                type="submit"
                onClick={saveButton}
                className="btn btn-success ml-2"
                disabled={isDisabled}
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                }}
              >
                <i className="fa fa-arrow-right"></i>
                Selanjutnya
              </button>
            )}
          </div>
        </CardFooter>
      </Card>
      <Modal
        show={loading}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Body style={{ textAlign: "center" }}>
          <h1>
            <Spinner
              animation="border"
              variant="primary"
              role="status"
              style={{ width: "50px", height: "50px" }}
            >
              <span className="sr-only">Loading...</span>
            </Spinner>
          </h1>
          <h1>Sedang Upload File. . .</h1>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default SimpleKnowledgeOtherKnowledge;
