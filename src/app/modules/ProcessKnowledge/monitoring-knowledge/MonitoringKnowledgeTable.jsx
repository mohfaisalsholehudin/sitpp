/* Library */
import React, { useEffect, useState } from "react";
import { Redirect, useHistory } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import swal from "sweetalert";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import {getReview, updateStatusPKP, updateStatusPublish} from "../Api"

/* Component */
// import ProposalOpen from "./ProposalOpen";
// import ProposalReject from "./ProposalReject";

/* Utility */

function MonitoringKnowledgeTable() {
  const history = useHistory();
  const [content, setContent] = useState([]);

  useEffect(()=> {
    getReview().then(({ data }) => {
      data.map(dt => {
        return dt.statusKm.id_status_km === 2 ?
        setContent(content => [...content, dt])
      : dt.statusKm.id_status_km === 3 ?
        setContent(content => [...content, dt])
      : dt.statusKm.id_status_km === 4 ?
        setContent(content => [...content, dt])
      : dt.statusKm.id_status_km === 5 ?
        setContent(content => [...content, dt])
      : dt.statusKm.id_status_km === 6 ?
        setContent(content => [...content, dt])
      : dt.statusKm.id_status_km === 7 ?
        setContent(content => [...content, dt])
      : dt.statusKm.id_status_km === 8 ?
        setContent(content => [...content, dt])
      : dt.statusKm.id_status_km === 9 ?
        setContent(content => [...content, dt])
      : null
      })
      })
    },[])

  const detailKnowledge = (id_km_pro, jenis, template) => {
    switch (template) {
      case "Success Story":
        history.push(`/process-knowledge/monitoring-knowledge/${id_km_pro}/success-story/${jenis}/1/detail`);
        break;
      case "Other Knowledge":
        history.push(`/process-knowledge/monitoring-knowledge/${id_km_pro}/other-knowledge/${jenis}/1/detail`);
        break;
      case "SOP":
        history.push(`/process-knowledge/monitoring-knowledge/${id_km_pro}/sop/${jenis}/1/detail`);
        break;
      default:
        break;
    }
  };

  // const ratingKnowledge = (id_km_pro) => {
  //   history.replace(`/process-knowledge/monitoring-knowledge/${id_km_pro}/rating`);
  // };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul",
      text: "Judul",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tipeKnowledge.template",
      text: "Tipe Knowledge",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tipeKnowledge.jenis",
      text: "Jenis",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "statusKm.nama",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeAdded,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterProcessKnowledgeMonitoring,
      formatExtraData: {
        detailProposal: detailKnowledge,
        // ratingKnowledge: ratingKnowledge
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_km_pro",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "id_km_pro", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_km_pro"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default MonitoringKnowledgeTable;
