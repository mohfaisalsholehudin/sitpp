import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls"; 
import MonitoringKnowledgeTable from "./MonitoringKnowledgeTable";

function MonitoringKnowledge() {

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Monitoring Knowledge"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <MonitoringKnowledgeTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default MonitoringKnowledge;
