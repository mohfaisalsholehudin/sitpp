import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import { Route, useHistory } from "react-router-dom";

//Helpers
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../../../_metronic/_helpers";
import { Pagination } from "../../../../../../helpers/pagination/Pagination";
import * as columnFormatters from "../../../../../../helpers/column-formatters";
import DasarHukumModal from "./DasarHukumModal";
import { deleteDasarHukum, getDasarHukumbyIdKM } from "../../../../Api";

function DasarHukumTable({ id, id_km_pro, jenis, id_lhr, media, step, lastPath }) {
  const history = useHistory();
    const [content, setContent] = useState([]);
  // const content = [
  //   { id: 1, judul: "referensi 1", link: "http://referensi1.com" },
  //   { id: 2, judul: "referensi 2", link: "http://referensi2.com" },
  //   { id: 3, judul: "referensi 3", link: "http://referensi3.com" }
  // ];

  useEffect(() => {
    getDasarHukumbyIdKM(id_km_pro).then(({ data }) => {
      setContent(data);
    });
  }, []);

  // console.log(content)

  const add = () => {
    history.push(
      `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/${step}/review/dasar-hukum/open`
    );
  };
  const edit = (id_dasar_hukum) => {
    history.push(
      `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/${step}/review/dasar-hukum/${id_dasar_hukum}/edit`
    );
  };

  const deleteDialog = id_dasar_hukum => {
    swal({
      title: "Hapus",
      text: "Apakah Anda yakin menghapus data ini ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((ret) => {
        if (ret == true) {
          deleteDasarHukum(id_dasar_hukum)
            .then(({ data }) => {
              if (data.deleted == true) {
                swal("Berhasil", "Data berhasil dihapus", "success").then(
                  () => {
                    history.push('/dashboard');
                    history.replace(`/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/${step}/review`);
                  }
                );
              } else {
                swal("Gagal", "Data gagal dihapus", "error").then(
                  () => {
                    history.push('/dashboard');
                    history.replace(`/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/${step}/review`);
                  }
                );
              }
            })
        }
      });
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul",
      text: "Nomor Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "link",
      text: "Perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterProcessKnowledgeSopDasarHukum,
      formatExtraData: {
        openEditDialog: edit,
        openDeleteDialog: deleteDialog
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const columnsDetail = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul",
      text: "Nomor Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "link",
      text: "Perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    }
  ];

  const emptyDataMessage = () => {
    return <div className="text-center">No Data to Display</div>;
  };
  const { SearchBar } = Search;
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_dasar_hukum",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "id_dasar_hukum", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
              {lastPath === "review" ?
                <ToolkitProvider
                  keyField="id_upd_terkait"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-12 col-xl-12 mb-3">
                          <h4> File Lampiran</h4>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div
                          className="col-lg-6 col-xl-6 mb-3"
                          style={{ textAlign: "right" }}
                        >
                          <button
                            type="button"
                            className="btn btn-primary ml-3"
                            style={{
                              float: "right"
                            }}
                            onClick={add}
                          >
                            <i className="fa fa-plus"></i>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
                :
                <ToolkitProvider
                  keyField="id_dasar_hukum"
                  data={content}
                  columns={columnsDetail}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-12 col-xl-12 mb-3">
                          <h4> Dasar Hukum</h4>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
                }
              </>
            );
          }}
        </PaginationProvider>
      </>
      {/*Route to Open Details*/}
      <Route path="/process-knowledge/review-pkp/:id_km_pro/sop/:jenis/:step/review/dasar-hukum/open">
        {({ history, match }) => (
          <DasarHukumModal
            show={match != null}
            id_dasar_hukum={match && match.params.id_dasar_hukum}
            id_km_pro={id_km_pro}
            jenis={jenis}
            step={step}
            after={false}
            onHide={() => {
              history.push(
                `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/${step}/review`
              );
            }}
            onRef={() => {
              history.push(
                `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/${step}/review`
              );
            }}
          />
        )}
      </Route>
      {/*Route to Edit Details*/}
      <Route path="/process-knowledge/review-pkp/:id_km_pro/sop/:jenis/:step/review/dasar-hukum/:id_dasar_hukum/edit">
        {({ history, match }) => (
          <DasarHukumModal
            show={match != null}
            id_dasar_hukum={match && match.params.id_dasar_hukum}
            id_km_pro={id_km_pro}
            jenis={jenis}
            step={step}
            after={false}
            onHide={() => {
              history.push(
                `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/${step}/review`
              );
            }}
            onRef={() => {
              history.push(
                `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/${step}/review`
              );
            }}
          />
        )}
      </Route>
    </>
  );
}

export default DasarHukumTable;
