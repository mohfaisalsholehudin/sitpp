import React from "react";
import MateriPokok from "./materi-pokok/MateriPokok";

function MonitoringSuccessStoryFormTwo({
  draft,
  setDraft,
  lastPath
}) {
  
  return (
    <>
      <MateriPokok
      draft={draft}
      setDraft={setDraft}
      lastPath={lastPath}
       />
      {/* <button
        type="submit"
        style={{ display: "none" }}
        ref={btnRef}
        onClick={() => saveFormTwo()}
      ></button> */}
    </>
  );
}

export default MonitoringSuccessStoryFormTwo;
