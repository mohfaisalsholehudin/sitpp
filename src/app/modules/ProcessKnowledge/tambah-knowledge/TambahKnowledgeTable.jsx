/* Library */
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import {
  deleteKnowledgeProcess,
  getKnowledgeProcess,
  getKnowledgeProcessByMenu,
  getKnowledgeProcessByNip,
  getKnowledgeProcessByNipStatus,
  updateStatusKnowledgeProcess,
} from "../../../references/Api";

/* Component */
// import ProposalOpen from "./ProposalOpen";
// import ProposalReject from "./ProposalReject";

/* Utility */

function TambahKnowledgeTable({ tab = "proses" }) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);
  const { user } = useSelector((state) => state.auth);

  const add = () => history.push("/process-knowledge/add-knowledge/add");
  const edit = (id_km_pro, id_tipe_km) =>
    history.push(
      `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/1/edit`
    );
  const review = (id_km_pro, id_tipe_km) =>
    history.push(
      `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/1/view`
    );
  const deleteAction = (id) => {
    swal({
      title: "Apakah Anda Yakin?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        deleteKnowledgeProcess(id).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/process-knowledge/add-knowledge");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/process-knowledge/add-knowledge");
            });
          }
        });
      }
      // else {
      //   swal("Your imaginary file is safe!");
      // }
    });
  };
  const apply = (id_km_pro, jenis) => {
    swal({
      title: "Apakah Anda Yakin?",
      text: "Klik OK untuk melanjutkan",
      icon: "info",
      buttons: true,
      dangerMode: false,
    }).then((willDelete) => {
      if (willDelete) {
        switch (jenis) {
          case "Tacit":
            updateStatusKnowledgeProcess(id_km_pro, 2).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Knowledge berhasil diajukan", "success").then(
                  () => {
                    history.push("/dashboard");
                    history.replace("/process-knowledge/add-knowledge");
                  }
                );
              } else {
                swal("Gagal", "Usulan gagal diajukan", "error").then(() => {
                  history.push("/dashboard");
                  history.replace("/process-knowledge/add-knowledge");
                });
              }
            });
            break;

          case "Explicit":
            updateStatusKnowledgeProcess(id_km_pro, 3).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Knowledge berhasil diajukan", "success").then(
                  () => {
                    history.push("/dashboard");
                    history.replace("/process-knowledge/add-knowledge");
                  }
                );
              } else {
                swal("Gagal", "Usulan gagal diajukan", "error").then(() => {
                  history.push("/dashboard");
                  history.replace("/process-knowledge/add-knowledge");
                });
              }
            });
            break;

          default:
            swal("Gagal", "Usulan gagal diajukan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/process-knowledge/add-knowledge");
            });
            break;
        }
      }
    });

    // getUsulanById(id).then(({ data }) => {
    //   updateStatusUsulan(data.id_usulan, 2).then(({ status }) => {
    //     if (status === 201 || status === 200) {
    //       swal("Berhasil", "Usulan berhasil diajukan", "success").then(() => {
    //         history.push("/dashboard");
    //         history.replace("/evaluation/proposal");
    //       });
    //     } else {
    //       swal("Gagal", "Usulan gagal diajukan", "error").then(() => {
    //         history.push("/dashboard");
    //         history.replace("/evaluation/proposal");
    //       });
    //     }
    //   });
    // });
  };

  useEffect(() => {
    switch (tab) {
      case "proses":
        getKnowledgeProcessByMenu({
          menu: ["kontri_tacit", "kontri_explicit"],
          nip: user.nip9,
          status: [1, 2, 3, 4, 5, 6, 7, 8, 11, 19],
        }).then(({ data }) => {
          setContent(data);
        });
        break;
      case "Tolak":
        getKnowledgeProcessByMenu({
          menu: ["kontri_tacit", "kontri_explicit"],
          nip: user.nip9,
          status: [12],
        }).then(({ data }) => {
          setContent(data);
        });
        break;
      case "Publish":
        getKnowledgeProcessByMenu({
          menu: ["kontri_tacit", "kontri_explicit"],
          nip: user.nip9,
          status: [9,10],
        }).then(({ data }) => {
          setContent(data);
        });
        break;

      default:
        break;
    }

    // getKnowledgeProcessByNip(user.nip9).then(({ data }) => {
    //   // setContent(data);
    //   switch (tab) {
    //     case "proses":
    //       // return setContent(data);
    //       data.map((dt) => {
    //         return dt.statusKm.nama !== "Tolak" &&
    //           dt.statusKm.nama !== "Publish Knowledge" &&
    //           dt.statusKm.nama !== "Telah Review Knowledge" &&
    //           dt.statusKm.nama !== "Update Review PKP Explicit" &&
    //           dt.statusKm.nama !== "Update Review KO Tacit" &&
    //           dt.statusKm.nama !== "Update Review KO Explicit" &&
    //           dt.statusKm.nama !== "Update Review PKP Tacit" &&
    //           dt.statusKm.nama !== "Update Review SME Tacit" &&
    //           dt.statusKm.nama !== "Siap Publish Review PKP"
    //           ? setContent((content) => [...content, dt])
    //           : null;
    //       });
    //       break;
    //     case "Tolak":
    //       data.map((dt) => {
    //         return dt.statusKm.nama === "Tolak"
    //           ? setContent((content) => [...content, dt])
    //           : null;
    //       });
    //       break;
    //       case "Publish":
    //         data.map((dt) => {
    //           return dt.statusKm.nama === "Publish Knowledge" || dt.statusKm.nama === "Telah Review Knowledge"
    //             ? setContent((content) => [...content, dt])
    //             : null;
    //         });
    //         break;
    //     default:
    //       break;
    //   }
    // });
  }, [user.nip9, tab]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
    },
    {
      dataField: "id_km_pro",
      text: "id",
      sort: true,
      hidden: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "judul",
      text: "Judul",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tipeKnowledge.nama",
      text: "Tipe Knowledge",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tipeKnowledge.jenis",
      text: "Jenis",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "updated_at",
      text: "Waktu Tambah / Edit",
      sort: true,
      formatter: columnFormatters.DateFormatterTambahKnowledge,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "reviewer_tolak",
      text: "Reviewer Tolak",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      hidden: tab !== "Tolak" ? true : false,
    },
    {
      dataField: "catatan_tolak",
      text: "Alasan Tolak",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      hidden: tab !== "Tolak" ? true : false,
    },
    {
      dataField: "statusKm.nama",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeAdded,
    },
    {
      dataField: "statusKm.id_status_km",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeAdded,
      hidden: true,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterProcessKnowledgeAdded,
      formatExtraData: {
        openEditDialog: edit,
        openDeleteDialog: deleteAction,
        applyProposal: apply,
        showReview: review,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "statusKm.id_status_km",
    pageNumber: currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "statusKm.id_status_km", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_km_pro"
                  data={content}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={add}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default TambahKnowledgeTable;
