import React, { useEffect, useState } from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";
// import "./decoupled.css";

const { BACKEND_URL } = window.ENV;

function MateriPokok(
  { draft, setDraft, path
    // history,
    // match: {
    //   params: { id, id_draft, name }
    // }
  }
) {
  // const [draft, setDraft] = useState("");
  const [show, setShow] = useState(false);
  const [data, setData] = useState([]);

  return (
    <>
      <div className="form-group row mb-4" style={{ marginBottom: "0px" }}>
        <div className="col-lg-9 col-xl-6">
          <h5 style={{ fontWeight: "600" }}>
            Materi Pokok
          </h5>
        </div>
      </div>
      <div className="row mb-4">
        <div className="document-editor" style={{ width: "100%" }}>
          <div className="document-editor__toolbar"></div>
          <div className="document-editor__editable-container">
          {path === 'view' ? (
             <CKEditor
             disabled
             onReady={editor => {
               window.editor = editor;
               setShow(true);

               // Add these two lines to properly position the toolbar
               const toolbarContainer = document.querySelector(
                 ".document-editor__toolbar"
               );
               toolbarContainer.appendChild(editor.ui.view.toolbar.element);
             }}
             config={{
                       removePlugins: ["Heading", "Link"],
                       toolbar: [],
                       isReadOnly: true,
                     }}
             editor={Editor}
             data={draft ? draft : null}
           />
            ) : 
            <CKEditor
              onReady={editor => {
                window.editor = editor;
                setShow(true);

                // Add these two lines to properly position the toolbar
                const toolbarContainer = document.querySelector(
                  ".document-editor__toolbar"
                );
                toolbarContainer.appendChild(editor.ui.view.toolbar.element);
              }}
              config={{
                simpleUpload: {
                  // The URL that the images are uploaded to.
                  uploadUrl: `${BACKEND_URL}/api/uploadckeditor`,

                  // Enable the XMLHttpRequest.withCredentials property.
                  withCredentials: false,

                  // Headers sent along with the XMLHttpRequest to the upload server.
                  headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Credentials": "true"
                  }
                }
              }}
              onChange={(event, editor) => {
                setDraft(editor.getData());
                setShow(true);
              }}
              editor={Editor}
              // data={content.body_draft ? content.body_draft : null}
              data={draft ? draft : null}
            />}
          </div>
        </div>
      </div>
    </>
  );
}

export default MateriPokok;
