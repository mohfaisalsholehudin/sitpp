import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Checkbox } from "../../../../../helpers";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import {
  getCasenameByArrayProbis,
  getMappingCasenameByIdKnowledge,
  getMappingProbisByIdKnowledge,
  saveMappingCasename,
  updateMappingCasename,
} from "../../../../../references/Api";

function CasenameModal({
  show,
  onHide,
  after,
  id_detil,
  id_km_pro,
  id_tipe_km,
  step,
  idMap
}) {
  const history = useHistory();
  const [content, setContent] = useState();
  // const [probis, setProbis] = useState([]);
  const [casename, setCasename] = useState([]);
  const [isCheck, setIsCheck] = useState([]);
  const [isCheckAll, setIsCheckAll] = useState(false);
  const [isUpdate, setIsUpdate] = useState(false);
  const [histProbis, setHistProbis] = useState([])



  useEffect(() => {
    getMappingCasenameByIdKnowledge(id_km_pro).then(({ data }) => {
        if(data.length > 0) {
          setContent({
            id_km_pro: id_km_pro,
            idCasename: data.map((dt) => "" + dt.id_csname),
          });
        }
    })


    getMappingProbisByIdKnowledge(id_km_pro).then(({ data }) => {
      getCasenameByArrayProbis([...new Map(data.map((m) => [m.id_probis, m])).values()].map((dt) => (
        "" + dt.id_probis
      ))).then(({ data }) => {
        setCasename(data)
      })
    });
   
  }, [id_km_pro]);
  const validationSchema = Yup.object().shape({
    idCasename: Yup.array()
      .min(1, "Casename is required")
      .required("Casename is required"),
  });

  const saveCasename = (values) => {
    if(!idMap){
      saveMappingCasename(values).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push(`/dashboard`);
            history.replace(
              `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`
            );
          });
        }
      });
    }else {
      updateMappingCasename(values).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push(`/dashboard`);
            history.replace(`/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`);
          });
        }
      });
    }
  };

  const initialValues = {
    idCasename: "",
    id_km_pro: id_km_pro,
  };
  
  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Tambah Casename
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <Formik
            enableReinitialize={true}
            initialValues={content || initialValues}
            validationSchema={validationSchema}
            onSubmit={(values) => {
              // console.log(values);
              saveCasename(values);
            }}
          >
            {({ handleSubmit, setFieldValue, values }) => {
              const handleSelectAll = (e) => {
                setIsCheckAll(!isCheckAll);
                setIsCheck(casename.map((li) => "" + li.id_case_name));
                setFieldValue(
                  "idCasename",
                  casename.map((li) => "" + li.id_case_name)
                );
                if (isCheckAll) {
                  setIsCheck([]);
                  setFieldValue("idCasename", []);
                }
              };

              return (
                <Form className="form form-label-right">
                  {/* FIELD PROSES BISNIS */}
                  <div className="form-group row align-items-center">
                    <div className="col-lg-12 col-xl-12">
                      <div className="checkbox-inline">
                        {casename.map((data, index) => (
                          <Field
                            component={Checkbox}
                            name="idCasename"
                            type="checkbox"
                            value={"" + data.id_case_name}
                            content={data.nama}
                            key={index}
                          />
                        ))}
                      </div>
                    </div>
                  </div>
                  <div className="form-group row align-items-center">
                    <div className="col-lg-12 col-xl-12">
                      <label
                        className="checkbox"
                        style={{
                          marginBottom: "15px",
                          float: "left",
                          width: "25%",
                          marginLeft: "18px",
                          marginTop: "-20px",
                        }}
                      >
                        <input
                          id="selectAll"
                          name="selectAll"
                          type="checkbox"
                          className="form-control form-control-lg is-valid"
                          onChange={handleSelectAll}
                          checked={isCheckAll}
                        />
                        <span style={{ marginRight: "10px" }}></span>Pilih Semua
                      </label>
                    </div>
                  </div>
                  <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Batal
                    </button>
                    {`  `}
                    <button
                      type="submit"
                      onSubmit={() => handleSubmit()}
                      className="btn btn-success ml-2"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                    >
                      <i className="fas fa-check"></i>
                      Kirim
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </Modal.Body>
    </Modal>
  );
}

export default CasenameModal;
