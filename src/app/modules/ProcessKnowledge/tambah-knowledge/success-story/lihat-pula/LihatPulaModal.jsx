import React, { useEffect, useState } from "react";
import {
  Modal,
  Table,
  FormControl,
  InputGroup,
  Button,
} from "react-bootstrap";
import SVG from "react-inlinesvg";
import swal from "sweetalert";
import { toAbsoluteUrl } from "../../../../../../_metronic/_helpers";
import { useHistory } from "react-router-dom";
import { useSubheader } from "../../../../../../_metronic/layout";
import { addLihatPula, getKnowledgebyId, getKnowledgebyJudul } from "../../../Api";

function LihatPulaModal({ id_lihatpula, id_km_pro, jenis, step, show, onHide, id_tipe_km}) {

  const history = useHistory();
  const [val, setVal] = useState();
  const [lihatpula, setLihatPula] = useState([]);
  const subheader = useSubheader();

  const [title, setTitle] = useState("");

  useEffect(() => {
    let _title = id_lihatpula ? "Edit Lihat Pula" : "Tambah Lihat Pula";
    setTitle(_title);
    subheader.setTitle(_title);
  }, [id_lihatpula, subheader]);


  const addPer = (judul, link) => {
      addLihatPula(id_km_pro, judul, link).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace(`/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`);
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(`/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add/lihatpula/open`);
          });
        }
      });
  };

  const handleChange = (e) => {
    setVal(e.target.value);
  };


  const handleSubmit =() => {
    getKnowledgebyJudul(val).then(({ data }) => {
      if(data.length){
        setLihatPula([]);
        data.map(dt => {
          return dt.statusKm.id_status_km === 9 ?
          setLihatPula(lihatpula => [...lihatpula, dt])
        : dt.statusKm.id_status_km === 10 ?
          setLihatPula(lihatpula => [...lihatpula, dt])
        : null
        })
      } else {
        setLihatPula([]);
      }
    })
    .catch(function(error) {
    })
    .then(function() {
    });
  }

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <InputGroup className="mb-3">
              <FormControl
                name="judul"
                aria-label="Default"
                placeholder="Masukkan Judul Pengetahuan"
                onChange={(e) => handleChange(e)}
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
          </div>
          <div className="col-lg-2 col-xl-2 mb-3">
            <Button type="submit" 
            onClick={handleSubmit} 
            variant="primary">
              Cari
            </Button>
          </div>
        </div>
        <div className="row">
        <Table responsive hover>
            <thead style={{ border: "1px solid #3699FF", textAlign: "center" }}>
              <tr>
                <th>No</th>
                <th style={{ textAlign: "left" }}>Judul</th>
                <th style={{ textAlign: "left" }}>Template</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody
              style={{ border: "1px solid #3699FF", textAlign: "center" }}
            >
              {lihatpula.map((data, index) => (
                <tr key={index} style={{ height: "40px" }}>
                  <td>{index + 1}</td>
                  <td style={{ textAlign: "left" }}>{data.judul}</td>
                  <td style={{ textAlign: "left" }}>{data.tipeKnowledge.template}</td>
                  <td>
                    <a
                      title="Tambah"
                      className="btn btn-icon btn-light btn-hover-success btn-sm mx-3"
                      onClick={() => addPer(data.judul, data.tipeKnowledge.template)}
                    >
                      <span className="svg-icon svg-icon-md svg-icon-success">
                        <SVG
                          src={toAbsoluteUrl(
                            "/media/svg/icons/Navigation/Plus.svg"
                          )}
                        />
                      </span>
                    </a>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </Modal.Body>
      <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}

export default LihatPulaModal;