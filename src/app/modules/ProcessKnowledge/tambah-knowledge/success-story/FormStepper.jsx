/* eslint-disable no-restricted-imports */
import React, { useEffect, useState } from "react";
import Select from "react-select";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepConnector from "@material-ui/core/StepConnector";
import Button from "@material-ui/core/Button";
import {
  Input,
  Textarea,
  Select as Sel,
  DatePickerField,
  Checkbox,
  Radio
} from "../../../../helpers";
import { Field } from "formik";
import CustomFileInput from "../../../../helpers/form/CustomFileInput";
import CircularProgress from "@material-ui/core/CircularProgress";
import { green } from "@material-ui/core/colors";
import PranalaLuarTable from "./pranala-luar/PranalaLuarTable";
import ReferensiTable from "./referensi/ReferensiTable";
import MateriPokok from "./materi-pokok/MateriPokok";

const useStyles = makeStyles(theme => ({
  root: {
    width: "90%"
  },
  button: {
    marginRight: theme.spacing(1)
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  connectorActive: {
    "& $connectorLine": {
      borderColor: theme.palette.secondary.main
    }
  },
  connectorCompleted: {
    "& $connectorLine": {
      borderColor: theme.palette.primary.main
    }
  },
  connectorDisabled: {
    "& $connectorLine": {
      borderColor: theme.palette.grey[100]
    }
  },
  connectorLine: {
    transition: theme.transitions.create("border-color")
  },
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12
  }
}));

function getSteps() {
  return [
    "Detil Isu Permasalahan",
    "Peraturan / Penegasan Terkait",
    "Usulan Penyusunan / Perubahan / Penggantian Peraturan / Penegasan",
    "4"
  ];
}

export default function FormStepper({
  btnRef,
  check,
  setShow,
  isEdit,
  per,
  handlePeraturan,
  handleDownload,
  loading,
  media
}) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();
  const [showPer, setShowPer] = useState(false);
  const [peraturan, setPeraturan] = useState([]);
  const [jenisPeraturan, setJenisPeraturan] = useState([]);
  const [jenisPajak, setJenisPajak] = useState([]);

  // const [isShow, setIsShow] = useState(false);

  function handleNext() {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  }

  function handleBack() {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  }

  const connector = (
    <StepConnector
      classes={{
        active: classes.connectorActive,
        completed: classes.connectorCompleted,
        disabled: classes.connectorDisabled,
        line: classes.connectorLine
      }}
    />
  );

  const simf = [
    { name: "Ya", value: "Ya" },
    { name: "Tidak", value: "Tidak" }
  ];

  useEffect(() => {}, []);
  useEffect(() => {
    if (per) {
      setShowPer(true);
    }
  }, [per]);
  const handleChangePeraturan = val => {
    handlePeraturan(val);
    setShowPer(true);
  };

  function getStepContent(step) {
    switch (step) {
      case 0:
        return (
          <>
            {/* Field Tipe Knowledge */}
            <div className="form-group row">
              <Field
                name="tipe_knowledge"
                component={Input}
                placeholder="Tipe Knowledge"
                label="Tipe Knowledge"
                disabled
                solid={"true"}
              />
            </div>
            {/* FIELD UPLOAD FILE */}
            {media !== "Dokumen" ? (
              <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Upload Media
                </label>
                <div className="col-lg-9 col-xl-6">
                  <Field
                    name="file"
                    component={CustomFileInput}
                    title="Select a file"
                    label="File"
                    style={{ display: "flex" }}
                  />
                </div>
              </div>
            ) : null}

            {/* FIELD PROSES BISNIS */}
            <div className="form-group row">
              <Sel name="proses_bisnis" label="Proses Bisnis">
                <option>Pilih Proses Bisnis</option>
                <option value="a">Business Process 1</option>
                <option value="b">Business Process 2</option>
              </Sel>
            </div>
            {/* FIELD BISNIS SEKTOR */}
            <div className="form-group row">
              <Sel name="binis_sektor" label="Bisnis Sektor">
                <option>Pilih Bisnis Sektor</option>
                <option value="a">Business Sektor 1</option>
                <option value="b">Business Sektor 2</option>
              </Sel>
            </div>
            {/* FIELD CASENAME */}
            <div className="form-group row">
              <Sel name="casename" label="Case Name">
                <option>Pilih Case Name</option>
                <option value="a">Case Name 1</option>
                <option value="b">Case Name 2</option>
              </Sel>
            </div>
            {/* FIELD SUBCASE */}
            <div className="form-group row">
              <Sel name="subcase" label="Sub Case">
                <option>Pilih Sub Case</option>
                <option value="a">Sub Case 1</option>
                <option value="b">Sub Case 2</option>
              </Sel>
            </div>
            {/* Field Judul */}
            <div className="form-group row">
              <Field
                name="judul"
                component={Textarea}
                placeholder="Judul"
                label="Judul"
              />
            </div>
            {/* Field Pendahuluan */}
            <div className="form-group row">
              <Field
                name="pendahuluan"
                component={Textarea}
                placeholder="Pendahuluan"
                label="Pendahuluan"
              />
            </div>
            {/* Field Daftar Isi */}
            <div className="form-group row">
              <Field
                name="daftar_isi"
                component={Textarea}
                placeholder="Daftar Isi"
                label="Daftar Isi"
              />
            </div>
            {/* Field Definisi */}
            <div className="form-group row">
              <Field
                name="definisi"
                component={Textarea}
                placeholder="Definisi"
                label="Definisi"
              />
            </div>
            {/* Field Tagging */}
            <div className="form-group row">
              <Field
                name="tagging"
                component={Textarea}
                placeholder="Tagging"
                label="Tagging"
              />
            </div>
          </>
        );
      case 1:
        return (
          <>
            {/* Field Materi Pokok */}
            {/* <div className="form-group row">
              <Field
                name="materi_pokok"
                component={Textarea}
                placeholder="Materi Pokok"
                label="Materi Pokok"
              />
            </div> */}
            <MateriPokok />
          </>
        );
      case 2:
        return (
          <>
            <PranalaLuarTable media={media} />
            <ReferensiTable media={media} />
          </>
        );
      case 3:
        return (
          <>
            <PranalaLuarTable media={media} />
            <ReferensiTable media={media} />
          </>
        );

      default:
        return "Unknown step";
    }
  }
  const saveForm = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  const reset = () => {
    setShow();
  };

  return (
    <div className={classes.root}>
      {/* <Stepper alternativeLabel activeStep={activeStep} connector={connector}>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper> */}
      <div>
        <div>
          {getStepContent(activeStep)}
          <div className="text-right">
            {isEdit ? (
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.button}
              >
                Kembali
              </Button>
            ) : (
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.button}
              >
                Kembali
              </Button>
            )
            // activeStep === 0 ? (
            //   <Button onClick={reset} className={classes.button}>
            //     Reset
            //   </Button>
            // ) : (
            //   <Button
            //     disabled={activeStep === 0}
            //     onClick={handleBack}
            //     className={classes.button}
            //   >
            //     Kembali
            //   </Button>
            // )
            }

            {activeStep === steps.length - 1 ? (
              <Button
                variant="contained"
                color="secondary"
                onClick={saveForm}
                disabled={check}
                className={classes.button}
              >
                {loading && (
                  <CircularProgress
                    size={24}
                    className={classes.buttonProgress}
                    color="secondary"
                  />
                )}
                {loading ? "Menyimpan. ." : "Simpan"}
              </Button>
            ) : (
              <Button
                variant="contained"
                color="secondary"
                onClick={handleNext}
                className={classes.button}
              >
                Selanjutnya
              </Button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
