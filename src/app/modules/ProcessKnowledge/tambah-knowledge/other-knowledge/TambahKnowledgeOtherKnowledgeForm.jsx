import React, { useEffect, useState } from "react";
import ReactPlayer from "react-player";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Input, Textarea, CustomAudio } from "../../../../helpers";

function TambahKnowledgeOtherKnowledgeForm({
  content,
  btnRef,
  saveForm,
  media,
  path,
}) {
  const [ValidateSchema, setValidateSchema] = useState();
  // const [probis, setProbis] = useState([]);
  // const [valProbis, setValProbis] = useState();
  // const [casename, setCasename] = useState([]);
  // const [valCasename, setValCasename] = useState();
  // const [subcase, setSubcase] = useState([]);
  // const [valSubcase, setValSubcase] = useState();
  // const [sektor, setSektor] = useState([]);
  // const [valSektor, setValSektor] = useState();
  const { STREAM_URL_VIDEO, STREAM_URL_AUDIO } = window.ENV;

  useEffect(() => {
    if (media === "dokumen") {
      setValidateSchema(
        Yup.object().shape({
          tipe_knowledge: Yup.string().required("Tipe Knowledge is required"),
          // id_probis: Yup.number().required("Proses Bisnis is required"),
          // id_sektor: Yup.number().required("Bisnis Sektor is required"),
          // id_csname: Yup.number().required("Case Name is required"),
          // id_subcase: Yup.number().required("Sub Case is required"),
          judul: Yup.string()
            .min(2, "Minimum 2 symbols")
            .required("Judul is required"),
          link_djpforum: Yup.string()
            .min(2, "Minimum 2 symbols")
            .required("Link DJP Forum is required"),
        })
      );
    } else {
      setValidateSchema(
        Yup.object().shape({
          tipe_knowledge: Yup.string().required("Tipe Knowledge is required"),
          // id_probis: Yup.number().required("Proses Bisnis is required"),
          // id_sektor: Yup.number().required("Bisnis Sektor is required"),
          // id_csname: Yup.number().required("Case Name is required"),
          // id_subcase: Yup.number().required("Sub Case is required"),
          judul: Yup.string()
            .min(2, "Minimum 2 symbols")
            .required("Judul is required"),
          link_djpforum: Yup.string()
            .min(2, "Minimum 2 symbols")
            .required("Link DJP Forum is required"),
          file: Yup.mixed()
            .required("A file is required")
            .test(
              "fileSize",
              "File too large",
              (value) => value && value.size <= FILE_SIZE
            )
            .test(
              "fileFormat",
              "Unsupported Format",
              (value) =>
                value && SUPPORTED_FORMATS.some((a) => value.type.includes(a))
            ),
        })
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [media]);

  const FILE_SIZE = 150000000;
  const SUPPORTED_FORMATS = ["audio/mpeg", "video/mp4", "video/mpeg"];

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ValidateSchema}
        onSubmit={(values) => {
          // console.log(values);
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          isValid,
          values,
          setFieldTouched,
          errors,
        }) => {
          const handleClick = () => {
            if (!values.file) {
              if (values.file_upload) {
                setFieldValue("file", {
                  file: values.file_upload,
                  size: 150000000,
                  type: ["audio/mpeg", "video/mp4", "video/mpeg"],
                });
              } else if (errors) {
                setFieldTouched("file", true);
              }
            }
          };
          return (
            <>
              <Form className="form form-label-right">
                <div className="form-group row">
                  <Field
                    name="tipe_knowledge"
                    component={Input}
                    placeholder="Tipe Knowledge"
                    label="Tipe Knowledge"
                    disabled
                    solid={"true"}
                  />
                </div>
                {/* FIELD UPLOAD FILE */}
                {media !== "dokumen" ? (
                  values.tipe_knowledge ? (
                    <div className="form-group row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Upload Media
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <Field
                          name="file"
                          component={CustomAudio}
                          title="Select a file"
                          label="File"
                          style={{ display: "flex" }}
                        />
                        {content.file_upload ? (
                          content.tipe_konten === "video" ? (
                            <ReactPlayer
                              className="react-player"
                              controls={true}
                              url={STREAM_URL_VIDEO + content.file_upload}
                            />
                          ) : (
                            <ReactPlayer
                              className="react-player"
                              controls={true}
                              width="480px"
                              height="40px"
                              url={STREAM_URL_AUDIO + content.file_upload}
                            />
                          )
                        ) : null}
                      </div>
                    </div>
                  ) : null
                ) : null}

                {/* Field Judul */}
                <div className="form-group row">
                  {path === "view" ? (
                    <Field
                      name="judul"
                      component={Textarea}
                      placeholder="Judul"
                      label="Judul"
                      disabled
                    />
                  ) : (
                    <Field
                      name="judul"
                      component={Textarea}
                      placeholder="Judul"
                      label="Judul"
                    />
                  )}
                </div>
                {/* Field Link DJP Forum */}
                <div className="form-group row">
                  {path === "view" ? (
                    <Field
                      name="link_djpforum"
                      component={Textarea}
                      placeholder="Link DJP Forum"
                      label="Link DJP Forum"
                      disabled
                    />
                  ) : (
                    <Field
                      name="link_djpforum"
                      component={Textarea}
                      placeholder="Link DJP Forum"
                      label="Link DJP Forum"
                    />
                  )}
                </div>
                {/* Field Catatan Tolak */}
                {content.catatan_tolak ? (
                  <div className="form-group row">
                    {path === "view" ? (
                      <Field
                        name="catatan_tolak"
                        component={Textarea}
                        placeholder="Catatan Tolak"
                        label="Catatan Tolak"
                        disabled
                      />
                    ) : (
                      <Field
                        name="catatan_tolak"
                        component={Textarea}
                        placeholder="Catatan Tolak"
                        label="Catatan Tolak"
                      />
                    )}
                  </div>
                ) : null}
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                  onClick={() => handleClick()}
                ></button>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default TambahKnowledgeOtherKnowledgeForm;
