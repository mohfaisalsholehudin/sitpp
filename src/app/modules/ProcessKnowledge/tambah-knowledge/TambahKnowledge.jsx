/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import TambahKnowledgeTable from "./TambahKnowledgeTable";

function TambahKnowledge() {
  const [tab, setTab] = useState("proses");

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Proses Knowledge"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          {/* <TambahKnowledgeTable /> */}
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("proses")}>
              <a
                className={`nav-link ${tab === "proses" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "proses").toString()}
              >
                Proses
              </a>
            </li>
            {/* {id && ( */}
            <>
              {" "}
              <li className="nav-item" onClick={() => setTab("tolak")}>
                <a
                  className={`nav-link ${tab === "tolak" && "active"}`}
                  data-toggle="tab"
                  role="button"
                  aria-selected={(tab === "tolak").toString()}
                >
                  Tolak
                </a>
              </li>
              <li className="nav-item" onClick={() => setTab("publish")}>
                <a
                  className={`nav-link ${tab === "publish" && "active"}`}
                  data-toggle="tab"
                  role="tab"
                  aria-selected={(tab === "publish").toString()}
                >
                  Publish
                </a>
              </li>
            </>
            {/* )} */}
          </ul>
          <div className="mt-5">
            {tab === "proses" && <TambahKnowledgeTable />}
            {tab === "tolak" && <TambahKnowledgeTable tab={"Tolak"} />}
            {tab === "publish" && <TambahKnowledgeTable tab={"Publish"} />}
          </div>
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default TambahKnowledge;
