import React, { useEffect, useState } from "react";
import {
  Modal,
  Table,
  FormControl,
  InputGroup,
  Button,
} from "react-bootstrap";
import SVG from "react-inlinesvg";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Textarea, Select as Sel } from "../../../../../../helpers";
import swal from "sweetalert";
import { toAbsoluteUrl } from "../../../../../../../_metronic/_helpers";
import { useHistory } from "react-router-dom";
import { useSubheader } from "../../../../../../../_metronic/layout";


function LihatPulaModal({ id, show, onHide}) {
  const history = useHistory();
  const [content, setContent] = useState();
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");

  useEffect(() => {
    let _title = id ? "Edit Lihat Pula" : "Tambah Lihat Pula";

    setTitle(_title);
    suhbeader.setTitle(_title);
  }, [id, suhbeader]);

  // const initialValues = {
  //   pokok_pengaturan: "",
  //   jenis: ""
  // };

  const validationSchema = Yup.object().shape({
    nama: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Nama is required"),
  });

  const savePengaturan = values => {
  };

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <InputGroup className="mb-3">
              <FormControl
                name="no_peraturan"
                aria-label="Default"
                placeholder="Masukkan Nama Peraturan"
                // onChange={(e) => handleChange(e)}
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
          </div>

          <div className="col-lg-2 col-xl-2 mb-3">
            <Button type="submit" 
            // onClick={handleSubmit} 
            variant="primary">
              Cari
            </Button>
          </div>
        </div>

        <div className="row">
          <Table responsive hover>
            <thead style={{ border: "1px solid #3699FF", textAlign: "center" }}>
              <tr>
                <th>No</th>
                <th style={{ textAlign: "left" }}>Nama</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody
              style={{ border: "1px solid #3699FF", textAlign: "center" }}
            >
              <tr style={{ height: "40px" }}>
                    <td>1</td>
                    <td style={{ textAlign: "left" }}>Per-1/PJ/2022</td>
                    <td>
                      <a
                        title="Tambah"
                        className="btn btn-icon btn-light btn-hover-success btn-sm mx-3"
                        // onClick={() => addPer(data.id_peraturan)}
                      >
                        <span className="svg-icon svg-icon-md svg-icon-success">
                          <SVG
                            src={toAbsoluteUrl(
                              "/media/svg/icons/Navigation/Plus.svg"
                            )}
                          />
                        </span>
                      </a>
                    </td>
                  </tr>
              {/* {perterkait.map((data, index) => (
                <tr key={index} style={{ height: "40px" }}>
                  <td>{index + 1}</td>
                  <td style={{ textAlign: "left" }}>{data.no_regulasi}</td>
                  <td style={{ textAlign: "left" }}>{data.perihal}</td>
                  <td>
                    <a
                      title="Tambah"
                      className="btn btn-icon btn-light btn-hover-success btn-sm mx-3"
                      onClick={() => addPer(data.id_peraturan)}
                    >
                      <span className="svg-icon svg-icon-md svg-icon-success">
                        <SVG
                          src={toAbsoluteUrl(
                            "/media/svg/icons/Navigation/Plus.svg"
                          )}
                        />
                      </span>
                    </a>
                  </td>
                </tr>
              ))} */}
            </tbody>
            {/* <tfoot style={{ border: '1px solid #3699FF', textAlign: 'center' }}>
                            <tr style={{ height: '40px' }}>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot> */}
          </Table>
        </div>
      </Modal.Body>
    </Modal>
  );
}

export default LihatPulaModal;
