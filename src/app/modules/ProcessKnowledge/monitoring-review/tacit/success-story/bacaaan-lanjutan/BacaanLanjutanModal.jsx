import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Textarea } from "../../../../../../helpers";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { useSubheader } from "../../../../../../../_metronic/layout";
import { getBacaanbyId, saveBacaan, updateBacaan } from "../../../../Api";


function BacaanLanjutanModal({ id_km_pro, step, jenis, id_bacaan, show, onHide }) {
  
  const history = useHistory();
  const [content, setContent] = useState();
  const subheader = useSubheader();

  const [title, setTitle] = useState("");

  const initialValues = {
    id_km_pro: id_km_pro,
    judul: "",
    link: ""
  };

  useEffect(() => {
    let _title = id_bacaan ? "Edit Bacaan Lanjutan" : "Tambah Bacaan Lanjutan";

    setTitle(_title);
    subheader.setTitle(_title);

    if (id_bacaan) {
      getBacaanbyId(id_bacaan).then(({ data }) => {
        setContent({
          id_bacaan: data.id_bacaan,
          judul: data.judul,
          link: data.link
        });
      });
    }
  }, [id_bacaan, subheader]);

  const validationSchema = Yup.object().shape({
    judul: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Judul is required"),
    link: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Link is required"),
  });

  const saveForm = values => {
    if (!id_bacaan) {
      saveBacaan(
        values.id_km_pro,
        values.judul,
        values.link
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Bacaan Lanjutan berhasil disimpan", "success").then(
            () => {
              history.push('/dashboard');
              history.replace(`/process-knowledge/review-pkp/${id_km_pro}/success-story/${jenis}/${step}/review`);
            }
          );
        } else {
          swal("Gagal", "Bacaan Lanjutan gagal disimpan", "error").then(() => {
            history.push('/dashboard');
            history.replace(`/process-knowledge/review-pkp/${id_km_pro}/success-story/${jenis}/${step}/review/bacaan-lanjutan/open`);
          });
        }
      }
      )
    } else {
      updateBacaan(
        values.id_bacaan,
        values.id_km_pro,
        values.judul,
        values.link
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Bacaan Lanjutan berhasil disimpan", "success").then(() => {
            history.push('/dashboard');
            history.replace(`/process-knowledge/review-pkp/${id_km_pro}/success-story/${jenis}/${step}/review`);
          });
        } else {
          swal("Gagal", "Bacaan Lanjutan gagal disimpan", "error").then(() => {
            history.push('/dashboard');
            history.replace(`/process-knowledge/review-pkp/${id_km_pro}/success-story/${jenis}/${step}/review/bacaan-lanjutan/open`);
          });
        }
      });
    }
  };

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <Formik
            enableReinitialize={true}
            initialValues={content || initialValues}
            validationSchema={validationSchema}
            onSubmit={values => {
              saveForm(values);
            }}
          >
            {({ handleSubmit }) => {
              return (
                <Form className="form form-label-right">
                  <div
                    className="form-group row"
                    style={{ marginBottom: "0px" }}
                  >
                    <div className="col-lg-9 col-xl-6">
                      <h5 className="mt-4" style={{ fontWeight: "600" }}>
                      Judul
                      </h5>
                    </div>
                  </div>
                  <div className="form-group row">
                    <Field
                      name="judul"
                      component={Textarea}
                      placeholder="Judul"
                      custom="custom"
                    />
                  </div>
                  <div
                    className="form-group row"
                    style={{ marginBottom: "0px" }}
                  >
                    <div className="col-lg-9 col-xl-6">
                      <h5 className="mt-6" style={{ fontWeight: "600" }}>
                      Link
                      </h5>
                    </div>
                  </div>
                  <div className="form-group row">
                    <Field
                      name="link"
                      component={Textarea}
                      placeholder="Link"
                      custom="custom"
                    />
                  </div>
                  <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Batal
                    </button>
                    {`  `}
                    <button
                      type="submit"
                      onSubmit={() => handleSubmit()}
                      className="btn btn-success ml-2"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="fas fa-check"></i>
                      Kirim
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </Modal.Body>
    </Modal>
  );
}

export default BacaanLanjutanModal;
