import React, { useEffect, useState } from "react";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import { Select as Sel } from "../../../../../../helpers";
import KategoriPengetahuanTable from "../kategori-pengetahuan/KategoriPengetahuanTable";
import Select from "react-select";
import { updateKnowledge, updateStatusPKP } from "../../../../Api";
import { useHistory } from "react-router-dom";

function KategoriPengetahuanForm({
  content,
  handleChangeEvaluation,
  setShow,
  isEdit,
  loading,
  jenis,
  btnRef,
  btnPublish,
  media,
  step,
  id_km_pro,
  showButton,
  hideButton,
  showForm,
  hideForm,
  lastPath
}) {
  const history = useHistory();
  const [isShow, setIsShow] = useState(false);
  const [isShowPengetahuan, setIsShowPengetahuan] = useState(false);
  const [table, setTable] = useState(false);
  const [isShowForm, setIsShowForm] = useState(true);
  const [valKategori, setValKategori] = useState([]);
  const [valLevel, setValLevel] = useState([]);
  
  console.log(content)
  // console.log(content.level_knowledge)
  const initialValues = {
    id_km_pro: id_km_pro,
    kategori: "",
    level_knowledge:""
  }

  const ValidateSchema = Yup.object().shape({
    kategori: Yup.string()
    .required("Kategori Pengetahuan is required"),
    level_knowledge: Yup.string()
    .required("Level Akses is required")
  });

  useEffect(() => {
    isShow ? setTable(true) : setTable(false);
    // isShowPengetahuan ? setButton(true) : setButton(false);
    isShowPengetahuan ? showButton() : hideButton();
    isShowForm ? showForm() : hideForm();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isShow, isShowPengetahuan, isShowForm]);

  const dataLevel = [
    {
      label: "1",
      value: "1"
    },
    {
      label: "2",
      value: "2"
    },
    {
      label: "3",
      value: "3"
    },
    {
      label: "4",
      value: "4"
    },
    { 
      label: "5",
      value: "5"
    }
  ];

  const dataPengetahuan = [
    {
      label: "Mudah",
      value: "mudah"
    },
    {
      label: "Sulit",
      value: "Sulit"
    }
  ];

  const change = () => {
    setTable(false);
    setIsShow(false);
  };

  const changePengetahuan = () => {
    setIsShowPengetahuan(false);
    hideButton();
  };

  const changeForm = () => {
    setIsShowForm(true)
  }

  const renderError = (message) => <p style={{ color: "red" }}>{message}</p>;

  const saveSimpan = (values) => {
    swal({
      title: "Setuju",
      text: "Apakah Anda yakin menyetujui usulan ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
    updateKnowledge(
      id_km_pro,
      "",
      content.catatan_ko,
      content.catatan_pkp,
      content.catatan_tolak,
      content.id_csname,
      "",
      content.id_probis,
      content.id_sektor,
      "",
      content.id_subcase,
      content.id_tipe_km,
      content.jenis,
      content.jml_view,
      content.judul,
      values.kategori,
      "",
      "",
      values.level_knowledge,
      content.media_upload,
      content.nama,
      "",
      "",
      "",
      content.nip_pkp,
      "",
      "",
      "",
      content.tipe_konten
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
                swal("Berhasil", "Draft Usulan Berhasil Disimpan", "success").then(() => {
                  history.push("/dashboard");
                  history.replace(`/process-knowledge/review-pkp`);
            })
        } else {
          swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(`/process-knowledge/review-pkp/${id_km_pro}/other-knowledge/${jenis}/5/review`);
          });
      }
    })
    }
      })
    }

  const savePublish = (values) => {
      if(values.kategori && values.level_knowledge){
      swal({
        title: "Setuju",
        text: "Apakah Anda Ingin Menyetujui Usulan Ini ?",
        icon: "warning",
        buttons: true
      }).then(ret => {
        if (ret == true) {
      updateKnowledge(
        id_km_pro,
        "",
        content.catatan_ko,
        content.catatan_pkp,
        content.catatan_tolak,
        content.id_csname,
        "",
        content.id_probis,
        content.id_sektor,
        "",
        content.id_subcase,
        content.id_tipe_km,
        content.jenis,
        content.jml_view,
        content.judul,
        values.kategori,
        "",
        "",
        values.level_knowledge,
        content.media_upload,
        content.nama,
        "",
        "",
        "",
        content.nip_pkp,
        "",
        "",
        "",
        content.tipe_konten
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          updateStatusPKP(content.id_km_pro, 2, 7, values.kategori, values.level_knowledge).then(({status}) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Usulan Berhasil Disetujui", "success").then(() => {
                  history.push("/dashboard");
                  history.replace(`/process-knowledge/review-pkp`);
            })
        } else {
          swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(`/process-knowledge/review-pkp/${id_km_pro}/other-knowledge/${jenis}/5/review`);
          });
        }
      });
      }
    })
  }
      })
    }
  }

  return (
    <>
      <>
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          validationSchema={ValidateSchema}
          onSubmit={values => {
            savePublish(values);
          }}
        >
          {({ handleSubmit, setFieldValue, isValid, values, errors, isSubmitting }) => {// const handleDownload =() => {
            //   window.open(DOWNLOAD_URL + values.file.file.slice(SLICE_ZIP));
            // }
            const handleChangeLevel = val => {
              // console.log(val.value);
              setFieldValue("level_knowledge", val.value);
              setValLevel(val.label);
              val.value === "1" ? setIsShow(true) : change();
            };

            const handleChangePengetahuan = val => {
              setFieldValue("kategori", val.value);
              setValKategori(val.label);
              val.value === "Sulit"
                ? setIsShowPengetahuan(true)
                : changePengetahuan();
            };

            const handleChangeForm = val => {
              val.value !== "Sulit"
              ? changeForm()
              : setIsShowForm(false)
            }
            return (
              <>
                <Form className="form form-label-right">
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Kategori Pengetahuan
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <Select
                        options={dataPengetahuan}
                        onChange={value => {handleChangePengetahuan(value); handleChangeForm(value)}}
                        value={dataPengetahuan.filter(data => data.label === valKategori)}
                      />
                      <ErrorMessage name="kategori" render={renderError} />
                    </div>
                  </div>
                  {/* FIELD Level Akses */}
                  { isShowForm ? (
                    <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Level Akses
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <Select
                        options={dataLevel}
                        onChange={value => handleChangeLevel(value)}
                        value={dataLevel.filter(data => data.label === valLevel)}
                      />
                      <ErrorMessage name="level_knowledge" render={renderError} />
                    </div>
                    <button
                      type="submit"
                      style={{ display: "none" }}
                      ref={btnPublish}
                      onSubmit={() => handleSubmit()}
                    ></button>
                    <button
                      type="button"
                      style={{ display: "none" }}
                      ref={btnRef}
                      onClick={() => saveSimpan(values)}
                    ></button>
                  </div>
                  ) : null}
                </Form>
              </>
            );
          }}
        </Formik>
      </>
      {table ? <KategoriPengetahuanTable mdeia={media} step={step} jenis={jenis} id_km_pro={id_km_pro} lastPath={lastPath}/> : null}
      {/* <div className="col-lg-12" style={{ textAlign: "center" }}>
        <button
          type="button"
          onClick={backAction}
          className="btn btn-light"
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
          }}
        >
          <i className="fa fa-arrow-left"></i>
          Kembali
        </button>
        {`  `}
        <button
          type="submit"
          className="btn btn-success ml-2"
          // onClick={setujuDialog}
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
          }}
          // disabled={disabled}
        >
          <i className="fas fa-check"></i>
          Simpan
        </button>
        {`  `}
        <button
          type="submit"
          className="btn btn-danger"
          // onClick={tolakDialog}
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
          }}
          // disabled={disabled}
        >
          <i className="flaticon2-cancel icon-nm"></i>
          Tolak
        </button>
        {`  `}
        <button
          type="submit"
          className="btn btn-info ml-2"
          // onClick={disposisiDialog}
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
          }}
          // disabled={disabled}
        >
          <i className="fas fa-share"></i>
          Disposisi
        </button>
      </div> */}
    </>
  );
}

export default KategoriPengetahuanForm;
