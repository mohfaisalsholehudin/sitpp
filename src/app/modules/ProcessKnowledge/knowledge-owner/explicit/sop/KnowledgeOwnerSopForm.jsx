import React, { useEffect, useState } from "react";
import ReactPlayer from 'react-player'
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import { Textarea, Select as Sel} from "../../../../../helpers";
import { getCasenamebyIdProbis, getSubcasebyIdCaseName, getProbisByStatus, getTipeKnowledgebyId, getSektorbyStatus } from "../../../Api";

function KnowledgeOwnerSopForm({
  content,
  saveForm,
  btnRef,
  media
}) {

  const [ValidateSchema, setValidateSchema] = useState();
  const { URL_DOWNLOAD, STREAM_URL_VIDEO, STREAM_URL_AUDIO } = window.ENV;
  const [tipe, setTipe] = useState([]);
  const [probis, setProbis] = useState([]);
  const [valProbis, setValProbis] = useState();
  const [sektor, setSektor] = useState([]);
  const [valSektor, setValSektor] = useState();
  const [caseName, setCaseName] = useState([]);
  const [valCasename, setValCasename] = useState();
  const [subCase, setSubCase] = useState([]);
  const [valSubcase, setValSubcase] = useState();

  useEffect(() => {
      setValidateSchema(
        Yup.object().shape({
          id_tipe_km: Yup.string().required("Tipe Knowledge is required"),
          id_probis: Yup.string().required("Proses Bisnis is required"),
          // id_sektor: Yup.string().required("Bisnis Sektor is required"),
          id_csname: Yup.string().required("Case Name is required"),
          judul: Yup.string()
            .min(2, "Minimum 2 symbols")
            .required("Judul is required"),
        })
      );
  }, [media]);

  useEffect(() => {
    if(content.id_tipe_km){
    getTipeKnowledgebyId(content.id_tipe_km).then(({data}) => {
        setTipe(data)
      })
    }
  }, [content]);
    
  useEffect(() => {
    getProbisByStatus(1).then(({ data }) => {
      data.map(data => {
        return setProbis(probis => [
          ...probis,
          {
            label: data.nama,
            value: data.id_probis
          }
        ]);
      });
      getSektorbyStatus(1).then(({ data }) => {
        data.map(data => {
          return setSektor(sektor => [
            ...sektor,
            {
              label: data.nama,
              value: data.id_sektor
            }
          ]);
        });
      });
    });
  }, []);

  useEffect(()=> {
    if(content.id_sektor){
      sektor.filter(data=>data.value === content.id_sektor)
      .map(data => {
        setValSektor(data.label);
      });
    } 
    if(content.id_probis){
      probis.filter(data=>data.value === content.id_probis)
      .map(data => {
        setValProbis(data.label);
      });
      getCasenamebyIdProbis(content.id_probis).then(({ data }) => {
        data.map(data => {
          return data.caseName.status === 1
            ? setCaseName(caseName => [
                ...caseName,
                {
                  label: data.caseName.nama,
                  value: data.caseName.id_case_name
                }
              ])
            : null;
        });
      });
      getSubcasebyIdCaseName(content.id_csname).then(({data})=> {
        data.map(data => {
          return data.subCase.status === 1
            ? setSubCase(subcase => [
                ...subcase,
                {
                  label: data.subCase.nama,
                  value: data.subCase.id_subcase
                }
              ])
            : null;
        });
      })
    } 
  },[sektor, content.id_sektor, content.id_probis, content.id_csname, probis])

  useEffect(()=> {
    if(content.id_csname){
      caseName.filter(data=>data.value === content.id_csname)
      .map(data => {
        setValCasename(data.label);
      });
    } else {
    }
  },[caseName, content.id_csname])

  useEffect(()=> {
    if(content.id_subcase){
      subCase.filter(data=>data.value === content.id_subcase)
      .map(data => {
        setValSubcase(data.label);
      });
    } else {
    }
  },[content.id_subcase, subCase])

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ValidateSchema}
        onSubmit={values => {
          saveForm(values);
        }}
      >
        {({ handleSubmit, setFieldValue }) => {
          const handleChangeProbis = val => {
            setFieldValue("id_probis", val.value);
            setValProbis(val.label);
            setCaseName([]);
            setSubCase([]);
            getCasenamebyIdProbis(val.value).then(({ data }) => {
              data.map(data => {
                return data.caseName.status === 1
                  ? setCaseName(casename => [
                      ...casename,
                      {
                        label: data.caseName.nama,
                        value: data.caseName.id_case_name
                      }
                    ])
                  : null;
              });
            });
          };
          const handleChangeCasename = val => {
            setFieldValue("id_csname", val.value);
            setValCasename(val.label);
            setSubCase([]);
            getSubcasebyIdCaseName(val.value).then(({data})=> {
              data.map(data => {
                return data.subCase.status === 1
                  ? setSubCase(subcase => [
                      ...subcase,
                      {
                        label: data.subCase.nama,
                        value: data.subCase.id_subcase
                      }
                    ])
                  : null;
              });
            })
          };
          const handleChangeSubcase = val => {
            setFieldValue("id_subcase", val.value);
            setValSubcase(val.label);
          };
          const handleChangeSektor = val => {
            setFieldValue("id_sektor", val.value);
            setValSektor(val.label);
          };
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD TIPE KNOWLEDGE */}
                <div className="form-group row">
                    <Sel 
                    name="id_tipe_km" 
                    label="Tipe Knowledge"
                    disabled
                    >
                        <option key ={tipe.id_tipe_km} value={tipe.id_tipe_km}>
                            {tipe.nama}
                          </option>
                      </Sel>
                </div>
            {/* FIELD UPLOAD FILE */}
            {content.tipe_konten !== "dokumen" ? (
                  <div className="form-group row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Upload Media
                      </label>
                      <div
                        className="col-lg-9 col-xl-6"
                        style={{ marginTop: "10px" }}
                      >
                      <a
                          href={URL_DOWNLOAD + content.media_upload}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          {content.media_upload}
                        </a>
                        {content.tipe_konten === "video" ? (
                          <ReactPlayer
                            className='react-player'
                            controls={true}
                            url={STREAM_URL_VIDEO+content.media_upload}
                          />
                        ) : 
                          content.tipe_konten === "audio" ? (
                          <ReactPlayer
                            className='react-player'
                            controls={true}
                            width="480px"
                            height="40px"
                            url={STREAM_URL_AUDIO+content.media_upload}
                          />
                          ) : null }
                      </div>
                    </div>
                ) : null}
            {/* Field Bisnis Sektor */}
            <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Bisnis Sektor
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={sektor}
                      onChange={value => handleChangeSektor(value)}
                      value={sektor.filter(data => data.label === valSektor)}
                    />
                  </div>
                </div>
                {/* Field Proses Bisnis */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Proses Bisnis
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={probis}
                      onChange={value => handleChangeProbis(value)}
                      value={probis.filter(data => data.label === valProbis)}
                    />
                  </div>
                </div>
                {/* Field Case Name */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Case Name
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={caseName}
                      onChange={value => handleChangeCasename(value)}
                      value={caseName.filter(
                        data => data.label === valCasename
                      )}
                    />
                  </div>
                </div>
                {/* FIELD SUBCASE */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Subcase
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={subCase}
                      onChange={value => handleChangeSubcase(value)}
                      value={subCase.filter(
                        data => data.label === valSubcase
                      )}
                    />
                  </div>
                </div>
            {/* Field Catatan PKP */}
            <div className="form-group row">
              <Field
                name="catatan_pkp"
                component={Textarea}
                placeholder="Catatan PKP"
                label="Catatan PKP"
                disabled
              />
            </div>
            {/* Field Judul */}
            <div className="form-group row">
              <Field
                name="judul"
                component={Textarea}
                placeholder="Judul"
                label="Judul"
              />
            </div>
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default KnowledgeOwnerSopForm;
