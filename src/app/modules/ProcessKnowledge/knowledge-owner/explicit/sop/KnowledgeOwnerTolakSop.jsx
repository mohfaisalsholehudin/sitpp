/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";
import { useSelector } from "react-redux";


/* Helper */
import { useSubheader } from "../../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import { getKnowledgebyId, tolakPKP, updateKnowledgeTolak } from "../../../Api";
import KnowledgeOwnerTolakForm from "./KnowledgeOwnerTolakForm";

function KnowledgeOwnerTolakSop({
  history,
  match: {
    params: { id_km_pro, jenis }
  }
}) {

  // Subheader
  const subheader = useSubheader();
  const [title, setTitle] = useState("");
  const [knowledge, setKnowledge] = useState([])
  const { user } = useSelector((state) => state.auth);


  useEffect(() => {
    let _title = "Catatan Tolak" ;

    setTitle(_title);
    subheader.setTitle(_title);

  }, [subheader]);

  useEffect(() => {
    getKnowledgebyId(id_km_pro).then(({data}) => {
      setKnowledge({
        id_km_pro: data.id_km_pro,
        id_tipe_km: data.id_tipe_km,
        id_ko: data.id_ko,
        id_sme: data.id_sme,
        id_probis: data.id_probis,
        id_sektor: data.id_sektor,
        id_csname: data.id_csname,
        id_subcase: data.id_subcase,
        })
    })
  }, [id_km_pro])

  const btnRef = useRef();
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  const backAction = () => {
    history.push(`/process-knowledge/review-ko/${id_km_pro}/sop/${jenis}/5/review`);
  };

  const saveForm = values => {
    swal({
      title: "Setuju",
      text: "Apakah Anda yakin menolak usulan ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
        updateKnowledgeTolak(
          knowledge.id_km_pro,
          values.catatan_tolak,
          knowledge.id_csname,
          knowledge.id_ko,
          knowledge.id_probis,
          knowledge.id_sektor,
          knowledge.id_sme,
          knowledge.id_subcase,
          knowledge.id_tipe_km,
          user.namaPegawai
    ).then(({ status }) => {
      if (status === 201 || status === 200) {
          tolakPKP(knowledge.id_km_pro, 12, values.catatan_tolak).then(({status}) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan berhasil ditolak", "success").then(() => {
                history.push("/dashboard");
                history.replace(`/process-knowledge/review-ko`);
          })
      } else {
        swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace(`/process-knowledge/review-ko/${id_km_pro}/sop/${jenis}/5/review`);
        });
      }
    });
    }
  })
}
    })
  }

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <KnowledgeOwnerTolakForm
              btnRef={btnRef}
              saveForm={saveForm}
              content={knowledge}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
        </div>
      </CardFooter>
    </Card>
  );
}

export default KnowledgeOwnerTolakSop;