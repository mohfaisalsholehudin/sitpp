import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls"; 
import KnowledgeOwnerTable from "./KnowledgeOwnerTable";

function KnowledgeOwner() {

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Review Knowledge Owner"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <KnowledgeOwnerTable />
        </CardBody>
      </Card>
    </>
  );
}

export default KnowledgeOwner;
