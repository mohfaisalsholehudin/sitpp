import React from "react";
import MateriPokok from "./materi-pokok/MateriPokok";

function KnowledgeOwnerSuccessStoryFormTwo({
  btnRef,
  saveFormTwo,
  draft,
  setDraft
}) {

  return (
    <>
      <MateriPokok
      draft={draft}
      setDraft={setDraft}
       />
      <button
        type="submit"
        style={{ display: "none" }}
        ref={btnRef}
        onClick={() => saveFormTwo()}
      ></button>
    </>
  );
}

export default KnowledgeOwnerSuccessStoryFormTwo;
