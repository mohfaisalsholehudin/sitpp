/* Library */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import swal from "sweetalert";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import {getReview, updateStatusPublish, updateWaktuPublish} from "../Api"


function ReviewPKPTable() {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const getCurrentDate = () => {
    const day = new Date();
    const YYYY = day.getFullYear();
      let MM = day.getMonth()+1;
      let DD = day.getDate();
      let HH = day.getHours() ;
      let mm = day.getMinutes()
      let ss = day.getSeconds();
      if(DD<10)
      {
          DD=`0${DD}`;
      }
      if(MM<10)
      {
          MM=`0${MM}`;
      }

      if(HH<10)
      {
          HH=`0${HH}`;
      }
      if(mm<10)
      {
          mm=`0${mm}`;
      }
      if(ss<10)
      {
          ss=`0${ss}`;
      }
    return YYYY + "-" + MM + "-" + DD + " " + HH + ":" + mm + ":" + ss
  }

  useEffect(()=> {
    getReview().then(({ data }) => {
      data.map(dt => {
        return dt.statusKm.id_status_km === 2 ?
        setContent(content => [...content, dt])
      : dt.statusKm.id_status_km === 3 ?
        setContent(content => [...content, dt])
      : dt.statusKm.id_status_km === 4 ?
        setContent(content => [...content, dt])
      : dt.statusKm.id_status_km === 5 ?
        setContent(content => [...content, dt])
      : dt.statusKm.id_status_km === 6 ?
        setContent(content => [...content, dt])
      : dt.statusKm.id_status_km === 7 ?
        setContent(content => [...content, dt])
      : dt.statusKm.id_status_km === 8 ?
        setContent(content => [...content, dt])
      : null
      })
      })
    },[])

    const review = (id_km_pro, jenis, template) => {
      switch (template) {
        case "Success Story":
            history.push(`/process-knowledge/review-pkp/${id_km_pro}/success-story/${jenis}/1/review`);
          break;
        case "Other Knowledge":
          history.push(`/process-knowledge/review-pkp/${id_km_pro}/other-knowledge/${jenis}/1/review`);
          break;
        case "SOP":
          history.push(`/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/1/review`);
          break;
        default:
          break;
      }
    };

  const apply = (id_km_pro) => {
    swal({
      title: "Apakah Anda Ingin Mengajukan Usulan Ini?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: false,
    }).then((willApply) => {
      if (willApply) {
        updateStatusPublish(id_km_pro).then(({ status }) => {
          if (status === 201 || status === 200) {
            updateWaktuPublish(
              id_km_pro,
              getCurrentDate()
            )
            swal("Berhasil", "Knowledge berhasil diterbitkan", "success").then(() => {
              history.push("/dashboard");
              history.replace("/process-knowledge/review-pkp");
            });
          } else {
            swal("Gagal", "Knowledge gagal diterbitkan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/process-knowledge/review-pkp");
            });
          }
        });
      }
    });
  };

  const detailKnowledge = (id_km_pro, jenis, template) => {
    switch (template) {
      case "Success Story":
        history.push(`/process-knowledge/review-pkp/${id_km_pro}/success-story/${jenis}/1/detail`);
        break;
      case "Other Knowledge":
        history.push(`/process-knowledge/review-pkp/${id_km_pro}/other-knowledge/${jenis}/1/detail`);
        break;
      case "SOP":
        history.push(`/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/1/detail`);
        break;
      default:
        break;
    }
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul",
      text: "Judul",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tipeKnowledge.nama",
      text: "Tipe Knowledge",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tipeKnowledge.jenis",
      text: "Jenis",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nama_kontri",
      text: "Kontributor",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "reviewer_pkp",
      text: "Reviewer",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "wkt_pkp",
      text: "Waktu Review",
      sort: true,
      formatter: columnFormatters.DateFormatterReviewPkp,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "statusKm.nama",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeAdded,
    },
    {
      dataField: "statusKm.id_status_km",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeAdded,
      hidden: true,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterProcessKnowledgePKP,
      formatExtraData: {
        reviewProposal: review,
        applyProposal: apply,
        detailProposal: detailKnowledge
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "statusKm.id_status_km",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "statusKm.id_status_km", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_km_pro"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default ReviewPKPTable;
