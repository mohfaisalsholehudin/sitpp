import React from "react";
import DocumentEditorKM from "../../../../../helpers/editor/DocumentEditorKM";

function ReviewPKPSuccessStoryFormTwo({
  btnRef,
  saveFormTwo,
  draft,
  setDraft,
  lastPath,
}) {
  return (
    <>
      <DocumentEditorKM
        content={draft}
        setDraft={setDraft}
        isReadOnly={lastPath === "review" ? false : true}
      />
      <button
        type="submit"
        style={{ display: "none" }}
        ref={btnRef}
        onClick={() => saveFormTwo()}
      ></button>
    </>
  );
}

export default ReviewPKPSuccessStoryFormTwo;
