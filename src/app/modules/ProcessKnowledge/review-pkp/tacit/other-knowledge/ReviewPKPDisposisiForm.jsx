import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Textarea, Select as Sel } from "../../../../../helpers";
import { getKownerbyCaseName } from "../../../Api";

function ReviewPKPDisposisiForm({ content, btnRef, saveForm }) {

  const [ValidateSchema, setValidateSchema] = useState();

  const initialValues = {
    id_km_pro: content.id_km_pro,
    id_ko: "",
    catatan_pkp: ""
  }

  const [knowledgeOwner, setKnowledgeOwner] = useState([])

  useEffect(() => {
    setValidateSchema(
      Yup.object().shape({
        id_ko: Yup.string().required("Knowledge Owner is required"),
        catatan_pkp: Yup.string().required("Catatan PKP is required"),
      })
      );
  }, []);

  useEffect(()=> {
    if(content.id_csname){
      getKownerbyCaseName(content.id_csname).then(({data}) => {
        setKnowledgeOwner(data)
      })
    }
  }, [content])
  
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={ValidateSchema}
        onSubmit={values => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
        }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD Knowledge Owner */}
                <div className="form-group row">
                    <Sel 
                    name="id_ko" 
                    label="Knowledge Owner"
                    >
                      <option value="">Pilih Knowledge Owner</option>
                        {knowledgeOwner.map((data, index) => (
                          <option key ={index} value={data.kowner.id_ko}>
                            {data.kowner.nm_es2}
                          </option>
                        ))}
                      </Sel>
                </div>
                {/* Field Catatan Disposisi */}
                <div className="form-group row">
                <Field
                    name="catatan_pkp"
                    component={Textarea}
                    placeholder="Catatan Disposisi"
                    label="Catatan Disposisi"
                />
                </div>
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ReviewPKPDisposisiForm;
