import React, { useEffect, useState, useRef } from "react";
import { Field, Formik, Form } from "formik";
import { useSubheader } from "../../../../_metronic/layout";
import Select from "react-select";
import DatePicker from "react-datepicker";
import { Textarea, Checkbox } from "../../../helpers";
import * as Yup from "yup";
import {
  getBisnisSektor,
  getBisnisSektorByStatus,
  getCaseNameByIdProbis,
  getProbisByStatus,
  getSubcaseByIdCasename,
  getTipeKnowledge,
  getTipeKnowledgeByStatus
} from "../../../references/Api";
import {
  DatePickerField,
  // DatePickerFieldRow,
  DatePickerFieldSplit
} from "../../../helpers";
import { DatePickerFieldRowEnd, DatePickerFieldRowStart } from "../../../helpers/form/DatePickerField";

function SearchKnowledgeForm({saveForm}) {
  const [tipeKm, setTipeKm] = useState([{"id_tipe_km" : 99, "nama": "Peraturan"}]);
  const [valTipeKm, setValTipeKm] = useState();
  const [probis, setProbis] = useState([]);
  const [valProbis, setValProbis] = useState();
  const [casename, setCasename] = useState([]);
  const [valCasename, setValCasename] = useState();
  const [subcase, setSubcase] = useState([]);
  const [valSubcase, setValSubcase] = useState();
  const [sektor, setSektor] = useState([]);
  const [valSektor, setValSektor] = useState();
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  // const [dateRange, setDateRange] = useState([null, null]);
  // const [startDate, endDate] = dateRange;
  const btnRef = useRef();

  const validationSchema = Yup.object().shape({
    // id_tipe_km: Yup.number().required("Tipe Knowledge is requried"),
    // key: Yup.string().required("Key Pencarian is requried")
    // tgl_knowledge: Yup.mixed()
    // .nullable(false)
    // .required("Tanggal Knowledge is required"),
    // jenis: Yup.string().required("Jenis is requried"),
    // idTipeKm:  Yup.array().min(1, "Tipe Knowledge is required").required("Tipe Knowledge is required"),
  });

  const initValues = {
    // id_tipe_km: "",
    // tgl_knowledge_start: "",
    // tgl_knowledge_end: "",
    // id_sektor: "",
    // id_probis: "",
    // id_csname: "",
    // id_subcase: "",
    // key: ""
  };
  useEffect(() => {
    // getTipeKnowledge().then(({ data }) => {
    //   data.map(data => {
    //     return setTipeKm(tipeKm => [
    //       ...tipeKm,
    //       {
    //         label: data.nama,
    //         value: data.id_tipe_km,
    //         jenis: data.jenis
    //       }
    //     ]);
    //   });
    // });
    getTipeKnowledgeByStatus().then(({data})=> {
      // setTipeKm(tipeKm => [...tipeKm, data])
      data.map(data => {
        return setTipeKm(tipeKm => [...tipeKm, data])
      })
    })
  }, []);
 
  const onChangeDate = dates => {
    console.log(dates);
    const [start, end] = dates;
    // console.log(end)
    // setStartDate(start);
    // setEndDate(end);
  };
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  useEffect(() => {
    getProbisByStatus(1).then(({ data }) => {
      data.map(data => {
        return setProbis(probis => [
          ...probis,
          {
            label: data.nama,
            value: data.id_probis
          }
        ]);
      });
      getBisnisSektorByStatus(1).then(({ data }) => {
        data.map(data => {
          return setSektor(sektor => [
            ...sektor,
            {
              label: data.nama,
              value: data.id_sektor
            }
          ]);
        });
      });
      // })
    });
  }, []);

  const changeStartDate = (val) => {
    setStartDate(val)
  }

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initValues}
        validationSchema={validationSchema}
        onSubmit={values => {
          // console.log(values);
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          handleClick,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          const handleChangeTipeKm = val => {
            setFieldValue("idTipeKm", val.value);
            setValTipeKm(val.label);
          };
          const handleChangeProbis = val => {
            setFieldValue("idProbis", val.value);
            setValProbis(val.label);
            setCasename([]);
            getCaseNameByIdProbis(val.value).then(({ data }) => {
              data.map(data => {
                return data.caseName.status === 1
                  ? setCasename(casename => [
                      ...casename,
                      {
                        label: data.caseName.nama,
                        value: data.caseName.id_case_name
                      }
                    ])
                  : null;
              });
            });
          };
          const handleChangeCasename = val => {
            setFieldValue("idCsname", val.value);
            setValCasename(val.label);
            setSubcase([]);
            getSubcaseByIdCasename(val.value).then(({ data }) => {
              data.map(data => {
                return data.subCase.status === 1
                  ? setSubcase(subcase => [
                      ...subcase,
                      {
                        label: data.subCase.nama,
                        value: data.subCase.id_subcase
                      }
                    ])
                  : null;
              });
            });
          };
          const handleChangeSubcase = val => {
            setFieldValue("idSubcase", val.value);
            setValSubcase(val.label);
          };
          const handleChangeSektor = val => {
            setFieldValue("idSektor", val.value);
            setValSektor(val.label);
          };

          // const handleChangeStartDate = () => {
          //   // changeStartDate(values.tgl_knowledge_start)
          //   // console.log(values.tgl_knowledge_start)
          //   console.log(values.tgl_knowledge_start)
          // }
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD TIPE KNOWLEDGE */}
                <div className="form-group row align-items-center">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Tipe Knowledge
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <div className="checkbox-inline">
                      {tipeKm.map((data, index) => (
                        <Field
                          component={Checkbox}
                          name="idTipeKm"
                          type="checkbox"
                          value={''+data.id_tipe_km}
                          content={data.nama}
                          // key={index}
                          // check={proposal.jns_pajak.split(',').includes(data.nm_jnspajak) }
                        />
                      ))}
                      {/* {tipeKm.map((data, index)=> (
                        <label>
                          <Field
                          type="checkbox"
                          name="idTipeKm"
                          value={''+data.id_tipe_km}
                          key={index}
                          />
                          {data.nama}
                        </label>
                      ))} */}
                    </div>
                  </div>
                </div>
                {/* <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Tipe Knowledge
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={tipeKm}
                      onChange={value => handleChangeTipeKm(value)}
                      value={tipeKm.filter(data => data.label === valTipeKm)}
                    />
                  </div>
                </div> */}
                {/* FIELD TANGGAL KNOWLEDGE */}
                <div className="form-group row">
                  <DatePickerFieldRowStart
                    name="startDate"
                    label="Tanggal Knowledge"
                    show="true"
                    selected={startDate}
                    // setstartdate={(val)=>changeStartDate(val)}
                    enddate={endDate}
                    onChange={setStartDate(values.tgl_knowledge_start)}
                    // onBlur={()=>handleChangeStartDate()}
                  />
                  <label className="col-form-label">s/d</label>
                  <DatePickerFieldRowEnd
                    name="endDate"
                    label="Tanggal Knowledge"
                    show="false"
                    selected={endDate}
                    startdate={startDate}
                    onChange={setEndDate(values.tgl_knowledge_end)}
                    // setenddate={setEndDate}
                  />
                </div>
                {/* Field Bisnis Sektor */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Mapping Knowledge
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={sektor}
                      onChange={value => handleChangeSektor(value)}
                      value={sektor.filter(data => data.label === valSektor)}
                    />
                  </div>
                </div>
                {/* Field Proses Bisnis */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    {""}
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={probis}
                      onChange={value => handleChangeProbis(value)}
                      value={probis.filter(data => data.label === valProbis)}
                    />
                  </div>
                </div>
                {/* Field Case Name */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                  {""}
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={casename}
                      onChange={value => handleChangeCasename(value)}
                      value={casename.filter(
                        data => data.label === valCasename
                      )}
                    />
                  </div>
                </div>
                {/* FIELD SUBCASE */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                  {""}
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={subcase}
                      onChange={value => handleChangeSubcase(value)}
                      value={subcase.filter(data => data.label === valSubcase)}
                    />
                  </div>
                </div>
                {/* FIELD KEY PENCARIAN */}
                <div className="form-group row">
                  <Field
                    name="keyword"
                    component={Textarea}
                    placeholder="Key Pencarian Berdasarkan Judul"
                    label="Key Pencarian Berdasarkan Judul"
                    withFeedbackLabel={false}
                    hide={'hide'}
                  />
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
      <>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="submit"
            className="btn btn-primary ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
            // disabled={disabled}
          >
            <i className="fas fa-search"></i>
            Cari
          </button>
        </div>
      </>
    </>
  );
}

export default SearchKnowledgeForm;
