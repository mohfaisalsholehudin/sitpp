import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls"; 
import FeedbackMain from "./FeebackMain";

function Feedback() {

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Feedback"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <FeedbackMain />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Feedback;
