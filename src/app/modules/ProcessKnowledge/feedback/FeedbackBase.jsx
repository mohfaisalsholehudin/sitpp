/* Library */
import React, { useEffect, useState } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../_metronic/_partials/controls";
import FeedbackDetailForm from "./FeedbackDetailForm";
import Swal from "sweetalert2";
import { getFeedbackbyId, tolakFeedback, setujuFeedback } from "../Api";

function FeedbackBase({
  history,
  match: {
    params: { id, status }
  }
}) {

  let thePath = document.URL;
  const initValues = {
    id_feedback: id,
    isi: "",
    nama_feedback: "",
    nip_feedback: "",
    judul: "",
    template: "",
    status: status
  };

  // Subheader
  const subheader = useSubheader();
  const [title, setTitle] = useState("");
  const [content, setContent] = useState([]);

  useEffect(() => {
    let _title = "Detail Feedback";
    setTitle(_title);
    getFeedbackbyId(id).then(({data}) => {
          setContent({
              id_feedback: data.id_feedback,
              isi: data.isi,
              nama_feedback: data.nama_feedback,
              nip_feedback: data.nip_feedback,
              judul: data.judul,
              template: data.template,
              status: data.status
          });
      });
    }, [id, subheader])
    
  
  const backAction = () => {
      history.push(
        `/process-knowledge/feedback`
      );
  };

  function tolakDialog() {
    Swal.fire({
      title: "Deactivate",
      input: "textarea",
      inputPlaceholder: "Masukkan catatan deactivate..",
      inputAttributes: {
        autocapitalize: "on",
      },
      text: "Apakah Anda yakin melakukan deactivate feedback ini ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#7cd1f9",
      cancelButtonColor: "#cacad3",
      confirmButtonText: "Deactivate",
      reverseButtons: true,
    }).then((data) => {
      if (data.value != null) {
        if (data.value == "") {
          swal("Gagal", "Catatan deactivate harus diisi", "error");
        } else {
          tolakFeedback(id, 0, data.value).then(({status}) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Feedback Berhasil Dilakukan Deactivate", "success").then(() => {
                history.push("/dashboard");
                history.replace(`/process-knowledge/feedback`);
          })
              } else {
                swal("Gagal", "Feedback Gagal Dilakukan Deactivate", "error").then(() => {
                  history.push("/dashboard");
                  history.replace("/process-knowledge/feedback");
                });
              }
            }
          );
        }
      }
    });
  }

  function setujuDialog() {
    Swal.fire({
      title: "Aktivasi",
      input: "textarea",
      inputPlaceholder: "Masukkan catatan aktivasi..",
      inputAttributes: {
        autocapitalize: "on",
      },
      text: "Apakah Anda yakin melakukan aktivasi feedback ini ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#7cd1f9",
      cancelButtonColor: "#cacad3",
      confirmButtonText: "Aktivasi",
      reverseButtons: true,
    }).then((data) => {
      if (data.value != null) {
        if (data.value == "") {
          swal("Gagal", "Catatan aktivasi harus diisi", "error");
        } else {
          setujuFeedback(id, 1, data.value).then(({status}) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Feedback Berhasil Dilakukan Aktivasi", "success").then(() => {
                history.push("/dashboard");
                history.replace(`/process-knowledge/feedback`);
          })
              } else {
                swal("Gagal", "Feedback Gagal Dilakukan Aktivasi", "error").then(() => {
                  history.push("/dashboard");
                  history.replace("/process-knowledge/feedback");
                });
              }
            }
          );
        }
      }
    });
  }

  function tombolProses() {
    if (content.status === 1) {
      return (
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          <button
            type="button"
            className="btn btn-danger ml-2"
            onClick={tolakDialog}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Deactivate
          </button>
        </div>
      );
    } else if (content.status === 0){
      return (
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          <button
            type="button"
            className="btn btn-success ml-2"
            onClick={setujuDialog}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fas fa-check"></i>
            Aktivasi
          </button>
        </div>
      );
    }
  }

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <FeedbackDetailForm
            content={content || initValues}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {tombolProses()}
      </CardFooter>
    </Card>
  );
}

export default FeedbackBase;