import React from "react";
import { Field, Formik, Form } from "formik";
import { Textarea } from "../../../helpers";

function FeedbackDetailForm({
  content
}) {

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
      >
        {({ values }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Isi Feedback */}
                <div className="form-group row">
                  <Field
                    name="isi"
                    component={Textarea}
                    placeholder="Isi Feedback"
                    label="Isi Feedback"
                    disabled
                  />
                </div>
                {/* Field Nama Pegawai */}
                <div className="form-group row">
                  <Field
                    name="nama_feedback"
                    component={Textarea}
                    placeholder="Pegawai"
                    label="Pegawai"
                    disabled
                  />
                </div>
                {/* Field NIP */}
                <div className="form-group row">
                  <Field
                    name="nip_feedback"
                    component={Textarea}
                    placeholder="NIP"
                    label="NIP"
                    disabled
                  />
                </div>
                <div className="form-group row">
                  Detail Knowledge:
                </div>
                {/* Field Judul */}
                <div className="form-group row">
                  <Field
                    name="judul"
                    component={Textarea}
                    placeholder="Judul"
                    label="Judul"
                    disabled
                  />
                </div>
                {/* Field Template */}
                <div className="form-group row">
                  <Field
                    name="template"
                    component={Textarea}
                    placeholder="Template"
                    label="Template"
                    disabled
                  />
                </div>
                <button
                  type="button"
                  style={{ display: "none" }}
                ></button>
                <button
                  type="button"
                  style={{ display: "none" }}
                ></button>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default FeedbackDetailForm;
