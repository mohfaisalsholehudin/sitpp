import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import { Textarea, Select as Sel } from "../../../helpers";

function ReviewPKPDisposisiForm({ content, btnRef, saveForm }) {

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    knowledge_owner: Yup.string().required("Knowledge Owner is requried"),
    catatan_disposisi: Yup.string().required("Catatan Disposisi is Required")
  });

  useEffect(() => {
    // getMasterJenis().then(({ data }) => {
    //   data.map(data => {
    //     return setMaster(master => [
    //       ...master,
    //       {
    //         label: data.nama,
    //         value: data.id
    //       }
    //     ]);
    //   });
    // });
  }, []);
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={values => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD Knowledge Owner */}
                <div className="form-group row">
                    <Sel name="knowledge_owner" label="Knowledge Owner">
                        <option>Pilih Knowledge Owner</option>
                        <option value="a">Seksi A</option>
                        <option value="b">Seksi B</option>
                        <option value="c">Seksi C</option>
                    </Sel>
                </div>
                {/* Field Catatan Disposisi */}
                <div className="form-group row">
                <Field
                    name="catatan_disposisi"
                    component={Textarea}
                    placeholder="Catatan Disposisi"
                    label="Catatan Disposisi"
                />
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ReviewPKPDisposisiForm;
