import React, { useEffect, useState, useRef } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
// import { DatePickerField, Input, Textarea, Select } from "../../helpers";
// import CustomFileInput from "../../helpers/CustomFileInput";
import FormStepper from "./FormStepper";
import CustomFileInput from "../../../../../helpers/form/CustomFileInput";
import { Input, Textarea, Select as Sel} from "../../../../../helpers";
import MateriPokok from "./materi-pokok/MateriPokok";

function ReviewPKPSopForm({
  content,
  handleChangeEvaluation,
  setShow,
  saveForm,
  isEdit,
  loading,
  media
}) {
  const [ValidateSchema, setValidateSchema] = useState();
  const {SLICE_URL} = window.ENV;

  useEffect(() => {
    if (media === "dokumen") {
      setValidateSchema(
        Yup.object().shape({
          tipe_knowledge: Yup.string().required("Tipe Knowledge is required"),
          proses_bisnis: Yup.string().required("Proses Bisnis is required"),
          binis_sektor: Yup.string().required("Bisnis Sektor is required"),
          casename: Yup.string().required("Case Name is required"),
          subcase: Yup.string().required("Sub Case is required"),
          judul: Yup.string()
            .min(2, "Minimum 2 symbols")
            .required("Judul is required"),
          // pendahuluan: Yup.string()
          //   .min(2, "Minimum 2 symbols")
          //   .required("Pendahuluan is required"),
          // daftar_isi: Yup.string()
          //   .min(2, "Minimum 2 symbols")
          //   .required("Daftar Isi is required"),
          // definisi: Yup.string()
          //   .min(2, "Minimum 2 symbols")
          //   .required("Definisi is required"),
          // tagging: Yup.string()
          //   .min(2, "Minimum 2 symbols")
          //   .required("Tagging is required"),
          // materi_pokok: Yup.string()
          //   .min(2, "Minimum 2 symbols")
          //   .required("Materi Pokok is required")
          // file: Yup.mixed()
          //   .required("A file is required")
          //   .test(
          //     "fileSize",
          //     "File too large",
          //     value => value && value.size <= FILE_SIZE
          //   )
          //   .test(
          //     "fileFormat",
          //     "Unsupported Format",
          //     value => value && SUPPORTED_FORMATS.includes(value.type)
          //   )
        })
      );
    } else {
      setValidateSchema(
        Yup.object().shape({
          tipe_knowledge: Yup.string().required("Tipe Knowledge is required"),
          proses_bisnis: Yup.string().required("Proses Bisnis is required"),
          binis_sektor: Yup.string().required("Bisnis Sektor is required"),
          casename: Yup.string().required("Case Name is required"),
          subcase: Yup.string().required("Sub Case is required"),
          judul: Yup.string()
            .min(2, "Minimum 2 symbols")
            .required("Judul is required"),
          // pendahuluan: Yup.string()
          //   .min(2, "Minimum 2 symbols")
          //   .required("Pendahuluan is required"),
          // daftar_isi: Yup.string()
          //   .min(2, "Minimum 2 symbols")
          //   .required("Daftar Isi is required"),
          // definisi: Yup.string()
          //   .min(2, "Minimum 2 symbols")
          //   .required("Definisi is required"),
          // tagging: Yup.string()
          //   .min(2, "Minimum 2 symbols")
          //   .required("Tagging is required"),
          // materi_pokok: Yup.string()
          //   .min(2, "Minimum 2 symbols")
          //   .required("Materi Pokok is required"),
          file: Yup.mixed()
            .required("A file is required")
            .test(
              "fileSize",
              "File too large",
              value => value && value.size <= FILE_SIZE
            )
            .test(
              "fileFormat",
              "Unsupported Format",
              value => value && SUPPORTED_FORMATS.includes(value.type)
            )
        })
      );
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [media]);

  const FILE_SIZE = 50000000;
  const SUPPORTED_FORMATS = [
    "application/x-rar-compressed",
    "application/octet-stream",
    "application/zip",
    "application/octet-stream",
    "application/x-zip-compressed",
    "multipart/x-zip",
    "application/vnd.rar"
  ];
  const btnRef = useRef();

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ValidateSchema}
        onSubmit={values => {
          // console.log(values);
          saveForm(values);
        }}
      >
        {({ handleSubmit, setFieldValue, isValid, values, isSubmitting }) => {
          // const handleDownload =() => {
          //   window.open(DOWNLOAD_URL + values.file.file.slice(SLICE_ZIP));
          // }
          return (
            <>
              <Form className="form form-label-right">
                {/* <FormStepper
                  btnRef={btnRef}
                  check={isSubmitting}
                  handleChangeEvaluation={handleChangeEvaluation}
                  setShow={setShow}
                  isEdit={isEdit}
                  loading={loading}
                  media={media}
                /> */}
                {/* FIELD TIPE KNOWLEDGE */}
                <div className="form-group row">
                    <Sel name="tipe_knowledge" label="Tipe Knowledge">
                        <option>Pilih Tipe Knowledge</option>
                        <option value="Success Story">Success Story</option>
                        <option value="SOP">SOP</option>
                        <option value="Lesson Learned">Other Knowledge</option>
                      </Sel>
                </div>
            {/* FIELD UPLOAD FILE */}
            {media !== "dokumen" ? (
              <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Upload Media
                </label>
                <div className="col-lg-9 col-xl-6"
                  style={{ marginTop: "10px" }}>
                    <a
                    href={values.file}
                    target="_blank"
                    rel="noopener noreferrer"
                    >
                      {values.file.slice(SLICE_URL)}
                    </a>
                  </div>
              </div>
            ) : null}
            {/* FIELD PROSES BISNIS */}
            <div className="form-group row">
              <Sel name="proses_bisnis" label="Proses Bisnis">
                <option>Pilih Proses Bisnis</option>
                <option value="a">Business Process 1</option>
                <option value="b">Business Process 2</option>
              </Sel>
            </div>
            {/* FIELD BISNIS SEKTOR */}
            <div className="form-group row">
              <Sel name="bisnis_sektor" label="Bisnis Sektor">
                <option>Pilih Bisnis Sektor</option>
                <option value="a">Business Sektor 1</option>
                <option value="b">Business Sektor 2</option>
              </Sel>
            </div>
            {/* FIELD CASENAME */}
            <div className="form-group row">
              <Sel name="casename" label="Case Name">
                <option>Pilih Case Name</option>
                <option value="a">Case Name 1</option>
                <option value="b">Case Name 2</option>
              </Sel>
            </div>
            {/* FIELD SUBCASE */}
            <div className="form-group row">
              <Sel name="subcase" label="Sub Case">
                <option>Pilih Sub Case</option>
                <option value="a">Sub Case 1</option>
                <option value="b">Sub Case 2</option>
              </Sel>
            </div>
            {/* Field Judul */}
            <div className="form-group row">
              <Field
                name="judul"
                component={Textarea}
                placeholder="Judul"
                label="Judul"
              />
            </div>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ReviewPKPSopForm;
