import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import { Select as Sel } from "../../../helpers";

function ReviewPKPEditForm({ content, btnRef, saveForm }) {
  const [master, setMaster] = useState([]);
  const [valMaster, setValMaster] = useState();

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    tipe_knowledge: Yup.string().required("Tipe Knowledge is requried"),
    tipe_konten: Yup.string().required("Tipe Konten is requried"),
  });

  useEffect(() => {
    // getMasterJenis().then(({ data }) => {
    //   data.map(data => {
    //     return setMaster(master => [
    //       ...master,
    //       {
    //         label: data.nama,
    //         value: data.id
    //       }
    //     ]);
    //   });
    // });
  }, []);
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={values => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD TIPE KNOWLEDGE */}
                <div className="form-group row">
                  <Sel name="tipe_knowledge" label="Tipe Knowledge" disabled>
                    <option>Pilih Tipe Knowledge</option>
                    <option value="Success Story">Success Story</option>
                    <option value="SOP">SOP</option>
                    <option value="Lesson Learned">Other Knowledge</option>
                  </Sel>
                </div>
                {/* FIELD TIPE KONTEN */}
                <div className="form-group row">
                  <Sel name="tipe_konten" label="Tipe Konten" disabled>
                    <option>Pilih Tipe Konten</option>
                    <option value="video">Video</option>
                    <option value="audio">Audio</option>
                    <option value="dokumen">Dokumen</option>
                  </Sel>
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ReviewPKPEditForm;
