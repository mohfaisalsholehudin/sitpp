/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import ReviewPKPSuccessStoryDisposisiForm from "./ReviewPKPSuccessStoryDisposisiForm";

function ReviewPKPSuccessStoryDisposisi({
  history,
  match: {
    params: { id, media, jenis}
  }
}) {
  const initValues = {
    tipe_knowledge: "Success Story",
    tipe_konten: "video"
  };

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();

  useEffect(() => {
    let _title = "Disposisi Pegawai" ;

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
      //   getDetilJenisById(id).then(({ data })=> {
      //     setContent(data)
      //   })
    }
  }, [id, suhbeader]);
  const btnRef = useRef();
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  const backAction = () => {
    history.push(`/process-knowledge/review-knowledge/success-story/${jenis}/${media}/5/review`);
  };

  const saveForm = values => {
        // acceptAction(penegasan.status);
        history.push(
          `/process-knowledge/review-knowledge/`
      );
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <ReviewPKPSuccessStoryDisposisiForm
              content={content || initValues}
              btnRef={btnRef}
              saveForm={saveForm}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <DetilJenisEditFooter backAction={backAction} btnRef={btnRef} /> */}
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
            // disabled={disabled}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
        </div>
      </CardFooter>
    </Card>
  );
}

export default ReviewPKPSuccessStoryDisposisi;