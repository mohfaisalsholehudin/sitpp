import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Field, Formik, Form } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import { Textarea, Select as Sel } from "../../../../../../helpers";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { useSubheader } from "../../../../../../../_metronic/layout";
import "../../../../../../helpers/DatePickerStyles.css";

function KategoriPengetahuanModal({ id, show, onHide, after, id_detil, id_lhr }) {
  const history = useHistory();
  const content = [
    { id: 3, tipe_konten: "video"}
  ];
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");

  const [master, setMaster] = useState([]);
  const [valMaster, setValMaster] = useState();

  useEffect(() => {
    let _title = id ? "Edit" : "Tambah";

    setTitle(_title);
    suhbeader.setTitle(_title);
  }, [id, suhbeader]);

  // const initialValues = {
  //   pokok_pengaturan: "",
  //   jenis: ""
  // };

  const validationSchema = Yup.object().shape({
    nama: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Nama File is required"),
  });

  const savePengaturan = values => {
  };

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <Formik
            enableReinitialize={true}
            initialValues={content}
            validationSchema={validationSchema}
            onSubmit={values => {
              // console.log(values);
              savePengaturan(values);
            }}
          >
            {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          const handleChangeMaster = val => {
            setFieldValue("id_jenis", val.value);
            setValMaster(val.label);
          };
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Eselon 2 */}
                <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                    Unit Eselon 2
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={master}
                      onChange={value => handleChangeMaster(value)}
                      value={master.filter(data => data.label === valMaster)}
                    />
                  </div>
                </div>
                 {/* Field Eselon 3 */}
                 <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                    Unit Eselon 3
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={master}
                      onChange={value => handleChangeMaster(value)}
                      value={master.filter(data => data.label === valMaster)}
                    />
                  </div>
                </div>
                 {/* Field Eselon 4 */}
                 <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                    Unit Eselon 4
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={master}
                      onChange={value => handleChangeMaster(value)}
                      value={master.filter(data => data.label === valMaster)}
                    />
                  </div>
                </div>
                {/* Field Jabatan */}
                <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                    Jabatan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={master}
                      onChange={value => handleChangeMaster(value)}
                      value={master.filter(data => data.label === valMaster)}
                    />
                  </div>
                </div>
                <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Batal
                    </button>
                    {`  `}
                    <button
                      type="submit"
                      onSubmit={() => handleSubmit()}
                      className="btn btn-success ml-2"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="fas fa-check"></i>
                      Kirim
                    </button>
                  </div>
              </Form>
            </>
          );
            }}
          </Formik>
        </>
      </Modal.Body>
    </Modal>
  );
}

export default KategoriPengetahuanModal;
