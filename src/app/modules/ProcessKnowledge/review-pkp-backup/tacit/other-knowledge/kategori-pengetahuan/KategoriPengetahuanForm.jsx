import React, { useEffect, useState } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { Select as Sel } from "../../../../../../helpers";
import KategoriPengetahuanTable from "../kategori-pengetahuan/KategoriPengetahuanTable";
import Select from "react-select";

function KategoriPengetahuanForm({
  content,
  handleChangeEvaluation,
  setShow,
  saveForm,
  isEdit,
  loading,
  media,
  step,
  showButton,
  hideButton,
  showForm,
  hideForm
}) {
  const [isShow, setIsShow] = useState(false);
  const [isShowPengetahuan, setIsShowPengetahuan] = useState(false);
  const [table, setTable] = useState(false);
  const [isShowForm, setIsShowForm] = useState(true);
  const ValidateSchema = Yup.object().shape({
    kategori_pengetahuan: Yup.string().required(
      "Kategori Pengetahuan is required"
    ),
    level_akses: Yup.string().required("Level Akses is required")
  });

  useEffect(() => {
    isShow ? setTable(true) : setTable(false);
    // isShowPengetahuan ? setButton(true) : setButton(false);
    isShowPengetahuan ? showButton() : hideButton();
    isShowForm ? showForm() : hideForm();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isShow, isShowPengetahuan, isShowForm]);

  const dataLevel = [
    {
      label: "1",
      value: "1"
    },
    {
      label: "2",
      value: "2"
    },
    {
      label: "3",
      value: "3"
    },
    {
      label: "4",
      value: "4"
    },
    { 
      label: "5",
      value: "5"
    }
  ];

  const dataPengetahuan = [
    {
      label: "Mudah",
      value: "mudah"
    },
    {
      label: "Sulit",
      value: "sulit"
    }
  ];

  const change = () => {
    setTable(false);
    setIsShow(false);
  };

  const changePengetahuan = () => {
    setIsShowPengetahuan(false);
    hideButton();
  };

  const changeForm = () => {
    setIsShowForm(true)
  }
  return (
    <>
      <>
        <Formik
          enableReinitialize={true}
          initialValues={content}
          validationSchema={ValidateSchema}
          onSubmit={values => {
            // console.log(values);
            saveForm(values);
          }}
        >
          {({ handleSubmit, setFieldValue, isValid, values, isSubmitting }) => {
            // const handleDownload =() => {
            //   window.open(DOWNLOAD_URL + values.file.file.slice(SLICE_ZIP));
            // }
            const handleChangeLevel = val => {
              // console.log(val.value);
              val.value === "1" ? setIsShow(true) : change();
            };

            const handleChangePengetahuan = val => {
              val.value === "sulit"
                ? setIsShowPengetahuan(true)
                : changePengetahuan();
            };

            const handleChangeForm = val => {
              val.value !== "sulit"
              ? changeForm()
              : setIsShowForm(false)
            }
            return (
              <>
                <Form className="form form-label-right">
                  {/* <FormStepper
                  btnRef={btnRef}
                  check={isSubmitting}
                  handleChangeEvaluation={handleChangeEvaluation}
                  setShow={setShow}
                  isEdit={isEdit}
                  loading={loading}
                  media={media}
                /> */}

                  {/* <Field
                name="kategori_pengetahuan"
                component={Input}
                placeholder="Kategori Pengetahuan"
                label="Kategori Pengetahuan"
                disabled
                solid={"true"}
              /> */}
                  {/* FIELD UPLOAD FILE */}
                  {/* {media !== "dokumen" ? (
              <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Upload Media
                </label>
                <div className="col-lg-9 col-xl-6">
                  <Field
                    name="file"
                    component={CustomFileInput}
                    title="Select a file"
                    label="File"
                    style={{ display: "flex" }}
                  />
                </div>
              </div>
            ) : null} */}

                  {/* FIELD Kategori Pengetahuan */}
                  {/* <div className="form-group row">
                    <Sel
                      name="kategori_pengetahuan"
                      label="Kategori Pengetahuan"
                    >
                      <option>Pilih Kategori Pengetahuan</option>
                      <option value="mudah">Mudah</option>
                      <option value="sedang">Sedang</option>
                      <option value="sulit">Sulit</option>
                    </Sel>
                  </div> */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Kategori Pengetahuan
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <Select
                        options={dataPengetahuan}
                        onChange={value => {handleChangePengetahuan(value); handleChangeForm(value)}}
                      />
                    </div>
                  </div>
                  {/* FIELD Level Akses */}
                  { isShowForm ? (
                    <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Level Akses
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <Select
                        options={dataLevel}
                        onChange={value => handleChangeLevel(value)}
                      />
                    </div>
                  </div>
                  ) : null}
                </Form>
              </>
            );
          }}
        </Formik>
      </>
      {table ? <KategoriPengetahuanTable mdeia={media} step={step} /> : null}
      {/* <div className="col-lg-12" style={{ textAlign: "center" }}>
        <button
          type="button"
          onClick={backAction}
          className="btn btn-light"
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
          }}
        >
          <i className="fa fa-arrow-left"></i>
          Kembali
        </button>
        {`  `}
        <button
          type="submit"
          className="btn btn-success ml-2"
          // onClick={setujuDialog}
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
          }}
          // disabled={disabled}
        >
          <i className="fas fa-check"></i>
          Simpan
        </button>
        {`  `}
        <button
          type="submit"
          className="btn btn-danger"
          // onClick={tolakDialog}
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
          }}
          // disabled={disabled}
        >
          <i className="flaticon2-cancel icon-nm"></i>
          Tolak
        </button>
        {`  `}
        <button
          type="submit"
          className="btn btn-info ml-2"
          // onClick={disposisiDialog}
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
          }}
          // disabled={disabled}
        >
          <i className="fas fa-share"></i>
          Disposisi
        </button>
      </div> */}
    </>
  );
}

export default KategoriPengetahuanForm;
