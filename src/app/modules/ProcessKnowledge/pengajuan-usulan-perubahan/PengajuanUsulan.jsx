/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import PengajuanUsulanTable from "./PengajuanUsulanTable";

function PengajuanUsulan() {
  const [tab, setTab] = useState("proses");

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Pengajuan Usulan Perubahan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          {/* <TambahKnowledgeTable /> */}
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("proses")}>
              <a
                className={`nav-link ${tab === "proses" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "proses").toString()}
              >
                Proses
              </a>
            </li>
            <>
              <li className="nav-item" onClick={() => setTab("selesai")}>
                <a
                  className={`nav-link ${tab === "selesai" && "active"}`}
                  data-toggle="tab"
                  role="tab"
                  aria-selected={(tab === "selesai").toString()}
                >
                  Selesai
                </a>
              </li>
            </>
            {/* )} */}
          </ul>
          <div className="mt-5">
            {tab === "proses" && <PengajuanUsulanTable />}
            {tab === "selesai" && <PengajuanUsulanTable tab={"selesai"} />}
          </div>
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default PengajuanUsulan;
