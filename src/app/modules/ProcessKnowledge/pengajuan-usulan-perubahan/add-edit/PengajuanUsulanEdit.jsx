import React, { useEffect, useState, useRef } from "react";
import { useSelector } from "react-redux";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
// import ProposalEditForm from "./ProposalEditForm";
// import ProposalEditFooter from "./ProposalEditFooter";
// import { getUsulanById, saveUsulan, updateUsulan } from "../../Api";
import swal from "sweetalert";
import PengajuanUsulanFooter from "./PengajuanUsulanFooter";
import PengajuanUsulanForm from "./PengajuanUsulanForm";
import {
  getUsulanPerubahanById,
  saveUsulanPerubahan,
  updateUsulanPerubahan,
} from "../../../../references/Api";
// import { uploadFileNew } from "../../../../references/Api";

function PengajuanUsulanEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const { user } = useSelector((state) => state.auth);

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [content, setContent] = useState();
  const [loading, setLoading] = useState(false);
  let thePath = document.URL;
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);

  const initValues = {
    judul_usulan: "",
    jenis_usulan: "",
    tipe: "",
    isi: "",
    nip_pengusul: user.nip9,
  };

  useEffect(() => {
    let _title = id
      ? lastPath === "view"
        ? "View Usulan Perubahan"
        : "Edit Usulan Perubahan"
      : "Tambah Usulan Perubahan";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
      getUsulanPerubahanById(id).then(({ data }) => {
        setContent(data);
      });
    }
  }, [id, lastPath, suhbeader]);
  const btnRef = useRef();
  const saveForm = (values) => {
    if (!id) {
      saveUsulanPerubahan(
        values.isi,
        values.jenis_usulan,
        values.judul_usulan,
        values.nip_pengusul,
        "Draft Pengajuan Template",
        values.tipe
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/process-knowledge/pengajuan");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/process-knowledge/pengajuan/add");
          });
        }
      });
    } else {
      updateUsulanPerubahan(
        id,
        values.isi,
        values.jenis_usulan,
        values.judul_usulan,
        values.nip_pengusul,
        "Draft Pengajuan Template",
        values.tipe
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/process-knowledge/pengajuan");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/process-knowledge/pengajuan/add");
          });
        }
      });
    }
    // if (!id) {
    //   enableLoading();
    //   const formData = new FormData();
    //   if (values.instansi) {
    //     formData.append("file", values.file);
    //     uploadFileNew(formData)
    //       .then(({ data }) =>
    //       {
    //         disableLoading();
    //         saveUsulan(
    //           values.alamat,
    //           "",
    //           data.message,
    //           values.instansi,
    //           values.jns_pajak.toString(),
    //           "",
    //           values.jns_usul,
    //           values.nip_perekam,
    //           values.nm_pic,
    //           values.no_pic,
    //           values.no_surat,
    //           values.perihal,
    //           values.tgl_surat,
    //           "",
    //           values.kd_kantor,
    //           values.kd_unit_org
    //         ).then(({ status }) => {
    //           if (status === 201 || status === 200) {
    //             swal("Berhasil", "Data berhasil disimpan", "success").then(
    //               () => {
    //                 history.push("/evaluation/proposal");
    //               }
    //             );
    //           } else {
    //             swal("Gagal", "Data gagal disimpan", "error").then(() => {
    //               history.push("/evaluation/proposal/new");
    //             });
    //           }
    //         })
    //   })
    //       .catch(() => window.alert("Oops Something went wrong !"));
    //   } else {
    //     formData.append("file", values.file);
    //     uploadFileNew(formData)
    //       .then(({ data }) =>
    //       {
    //         disableLoading();
    //         saveUsulan(
    //           values.alamat,
    //           "",
    //           data.message,
    //           "",
    //           values.jns_pajak.toString(),
    //           "",
    //           values.jns_usul,
    //           values.nip_perekam,
    //           values.nm_pic,
    //           values.no_pic,
    //           values.no_surat,
    //           values.perihal,
    //           values.tgl_surat,
    //           values.unit_kerja,
    //           values.kd_kantor,
    //           values.kd_unit_org
    //         ).then(({ status }) => {
    //           if (status === 201 || status === 200) {
    //             swal("Berhasil", "Data berhasil disimpan", "success").then(
    //               () => {
    //                 history.push("/evaluation/proposal");
    //               }
    //             );
    //           } else {
    //             swal("Gagal", "Data gagal disimpan", "error").then(() => {
    //               history.push("/evaluation/proposal/new");
    //             });
    //           }
    //         })
    //   })
    //       .catch(() => swal("Error", "Oops Something went wrong !", "error"));
    //   }
    // } else {
    //   if (values.file.name) {
    //     enableLoading();
    //     const formData = new FormData();
    //     formData.append("file", values.file);
    //     uploadFileNew(formData)
    //       .then(({ data }) =>
    //       {
    //         disableLoading();
    //         updateUsulan(
    //           values.id_usulan,
    //           values.alamat,
    //           "", //alasan_tolak
    //           data.message, //file_upload
    //           values.instansi,
    //           values.jns_pajak.toString(),
    //           "", //jns_Pengusul
    //           values.jns_usul,
    //           values.nip_perekam,  //nip_perekam
    //           values.nm_pic,
    //           values.no_pic,
    //           values.no_surat,
    //           values.perihal,
    //           values.tgl_surat,
    //           values.unit_kerja,
    //           values.kd_kantor,
    //           values.kd_unit_org
    //         ).then(({ status }) => {
    //           if (status === 201 || status === 200) {
    //             swal("Berhasil", "Data berhasil disimpan", "success").then(
    //               () => {
    //                 history.push("/evaluation/proposal");
    //               }
    //             );
    //           } else {
    //             swal("Gagal", "Data gagal disimpan", "error").then(() => {
    //               history.push("/evaluation/proposal/new");
    //             });
    //           }
    //         })
    //   })
    //       .catch(() => window.alert("Oops Something went wrong !"));
    //   } else {
    //     updateUsulan(
    //       values.id_usulan,
    //       values.alamat,
    //       "", //alasan_tolak
    //       values.file_upload,
    //       values.instansi,
    //       values.jns_pajak.toString(),
    //       "",  //jns_pengusul
    //       values.jns_usul,
    //       values.nip_perekam, //nip_perekam
    //       values.nm_pic,
    //       values.no_pic,
    //       values.no_surat,
    //       values.perihal,
    //       values.tgl_surat,
    //       values.unit_kerja,
    //       values.kd_kantor,
    //       values.kd_unit_org
    //     ).then(({ status }) => {
    //       if (status === 201 || status === 200) {
    //         swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
    //           history.push("/evaluation/proposal");
    //         });
    //       } else {
    //         swal("Gagal", "Data gagal disimpan", "error").then(() => {
    //           history.push("/evaluation/proposal/new");
    //         });
    //       }
    //     });
    //   }
    // }
  };
  const handleBack = () => {
    history.push(`/process-knowledge/pengajuan`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };

  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <PengajuanUsulanForm
            actionsLoading={actionsLoading}
            content={content || initValues}
            btnRef={btnRef}
            saveForm={saveForm}
            setDisabled={setDisabled}
            path={lastPath}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <PengajuanUsulanFooter
          backAction={handleBack}
          btnRef={btnRef}
          loading={loading}
          path={lastPath}
        />
      </CardFooter>
    </Card>
  );
}

export default PengajuanUsulanEdit;
