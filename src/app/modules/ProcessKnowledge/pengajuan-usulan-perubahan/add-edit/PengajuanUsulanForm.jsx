import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
// import "./styles.css";
import { Input, Textarea, Select as Sel } from "../../../../helpers";

function PengajuanUsulanForm({ content, btnRef, saveForm, path }) {
  const { user } = useSelector((state) => state.auth);

  // Validation schema
  const ValidationSchema = Yup.object().shape({
    judul_usulan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .required("Judul Usulan is required"),
    jenis_usulan: Yup.string().required("Jenis Usulan is required"),
    tipe: Yup.string().required("Tipe is required"),
    isi: Yup.string()
      .min(2, "Minimum 2 symbols")
      .trim("No Whitespace allowed")
      .required("Isi Usulan is required"),
  });

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ValidationSchema}
        onSubmit={(values) => {
          // console.log(values);
          saveForm(values);
        }}
      >
        {({ handleSubmit }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD JUDUL */}
                <div className="form-group row">
                  {path === "view" ? (
                    <Field
                      name="judul_usulan"
                      component={Input}
                      placeholder="Judul Usulan"
                      label="Judul Usulan"
                      disabled
                    />
                  ) : (
                    <Field
                      name="judul_usulan"
                      component={Input}
                      placeholder="Judul Usulan"
                      label="Judul Usulan"
                    />
                  )}
                </div>

                {/* FIELD JENIS USULAN */}
                <div className="form-group row">
                  {path === "view" ? (
                    <Sel
                      name="jenis_usulan"
                      label="Jenis Usulan"
                      // onBlur={() => handleChangeInstansi()}
                      disabled
                    >
                      <option>Pilih Jenis Usulan</option>
                      <option value="Template Knowledge">
                        Template Knowledge
                      </option>
                      <option value="Alur Persetujuan Knowledge">
                        Alur Persetujuan Knowledge
                      </option>
                      <option value="Taksonomi Knowledge">
                        Taksonomi Knowledge
                      </option>
                    </Sel>
                  ) : (
                    <Sel
                      name="jenis_usulan"
                      label="Jenis Usulan"
                      // onBlur={() => handleChangeInstansi()}
                    >
                      <option>Pilih Jenis Usulan</option>
                      <option value="Template Knowledge">
                        Template Knowledge
                      </option>
                      <option value="Alur Persetujuan Knowledge">
                        Alur Persetujuan Knowledge
                      </option>
                      <option value="Taksonomi Knowledge">
                        Taksonomi Knowledge
                      </option>
                    </Sel>
                  )}
                </div>
                {/* FIELD TIPE USULAN */}
                <div className="form-group row">
                  {path === "view" ? (
                    <Sel name="tipe" label="Tipe" disabled>
                      <option>Pilih Tipe Usulan</option>
                      <option value="Baru">Baru</option>
                      <option value="Perubahan">Perubahan</option>
                    </Sel>
                  ) : (
                    <Sel
                      name="tipe"
                      label="Tipe"
                      // onBlur={() => handleChangeInstansi()}
                    >
                      <option>Pilih Tipe Usulan</option>
                      <option value="Baru">Baru</option>
                      <option value="Perubahan">Perubahan</option>
                    </Sel>
                  )}
                </div>
                {/* FIELD ISI USULAN */}
                <div className="form-group row">
                  {path === "view" ? (
                    <Field
                      name="isi"
                      component={Textarea}
                      placeholder="Isi Usulan"
                      label="Isi Usulan"
                      disabled
                    />
                  ) : (
                    <Field
                      name="isi"
                      component={Textarea}
                      placeholder="Isi Usulan"
                      label="Isi Usulan"
                    />
                  )}
                </div>
                {path === "view" ? null : (
                  <button
                    type="submit"
                    style={{ display: "none" }}
                    ref={btnRef}
                    onSubmit={() => handleSubmit()}
                  ></button>
                )}
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default PengajuanUsulanForm;
