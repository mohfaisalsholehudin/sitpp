import React from 'react'
import MateriPokok from "./materi-pokok/MateriPokok";

function ReviewUpdateOtherKnowledgeFormTwo({
    btnRef, saveForm, draft, setDraft, path
  }) {
    return (
        <>
          <MateriPokok draft={draft} setDraft={setDraft} path={path} />
          <button
            type="submit"
            style={{ display: "none" }}
            ref={btnRef}
            onClick={() => saveForm()}
          ></button>
        </>
      );
}

export default ReviewUpdateOtherKnowledgeFormTwo