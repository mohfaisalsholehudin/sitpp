import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../_metronic/_partials/controls"; 
import ReviewUpdateTable from "./ReviewUpdateTable";

function ReviewUpdateKo() {

  return (
    <>
      <Card>
        <CardHeader
          title="Review Update Konten Knowledge Owner"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <ReviewUpdateTable />
        </CardBody>
        <CardFooter style={{ borderTop: "none" }}>
        </CardFooter>
      </Card>
    </>
  );
}

//#a6c8e6
export default ReviewUpdateKo;
