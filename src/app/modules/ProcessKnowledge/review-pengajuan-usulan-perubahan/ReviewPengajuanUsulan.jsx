/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import ReviewPengajuanUsulanTable from "./ReviewPengajuanUsulanTable";

function ReviewPengajuanUsulan() {

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Pengajuan Usulan Perubahan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <ReviewPengajuanUsulanTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default ReviewPengajuanUsulan;
