import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Textarea, Select as Sel } from "../../../../../helpers";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { useSubheader } from "../../../../../../_metronic/layout";
// import CustomFileInput from "../../../../helpers/form/CustomFileInput";
import { uploadFile } from "../../../../Evaluation/Api";
import { saveUploadTerkait, uploadKmi } from "../../../../../references/Api";
import CustomUploadInput from "../../../../../helpers/form/CustomUploadInput";
const { SLICE_UPLOAD } = window.ENV;


function UploadFileModal({ id, show, onHide, id_tipe_km, id_km_pro, step }) {
  const history = useHistory();
  const [content, setContent] = useState();
  const [loading, setLoading] = useState(false);
  const [isShow, setIsShow] = useState(true);
  const [isUpload, setIsUpload] = useState(false);
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");

  useEffect(() => {
    let _title = id ? "Edit File Lampiran" : "Tambah File Lampiran";

    setTitle(_title);
    suhbeader.setTitle(_title);
  }, [id, suhbeader]);

  const initialValues = {
    file: ""
  };

  const validationSchema = Yup.object().shape({
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });
  const FILE_SIZE = 15000000;
  const SUPPORTED_FORMATS = [
    // pdf
    "application/pdf",
    // xls
    "application/vnd.ms-excel",
    // xlsx
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    // doc
    "application/msword",
    // docx
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    // ppt
    "application/vnd.ms-powerpoint",
    // pptx
    "application/vnd.openxmlformats-officedocument.presentationml.presentation",
  ];

  const uploadAction = () => {
    setIsUpload(true);
    setIsShow(false);
  };
  // const handleCancel = () => {
  //   setIsUpload(false);
  //   setIsShow(true);
  // };
  const handleUpload = values => {
    enableLoading();
    const formData = new FormData();
    formData.append("file", values.file);
    uploadKmi(formData).then(({ data }) => {
      disableLoading();
      saveUploadTerkait(id_km_pro, data.message.slice(SLICE_UPLOAD)).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace(
              `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/${step}/detil`
            );
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push(
              `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/${step}/detil`
            );
          });
        }
      });
    }).catch(()=> {
      swal("Gagal", "Data gagal disimpan", "error").then(() => {
        history.push(
          `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/${step}/detil`
        );
      });
      disableLoading();
    })
  };
  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Tambah File Lampiran
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={values => {
              // console.log(values);
              handleUpload(values);
            }}
          >
            {({ handleSubmit, isSubmitting }) => {
              return (
                <Form className="form form-label-right">
                  <div
                    className="form-group row"
                    style={{ marginBottom: "0px" }}
                  ></div>
                  {/* FIELD UPLOAD FILE */}
                  <div className="form-group row">
                    {/* <label className="col-xl-3 col-lg-3 col-form-label">
                        Upload File
                      </label> */}
                    <div className="col-lg-12 col-xl-12">
                      <Field
                        name="file"
                        component={CustomUploadInput}
                        title="Select a file"
                        label="File"
                        style={{ display: "flex" }}
                      />
                    </div>
                  </div>
                  <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Batal
                    </button>
                    {`  `}

                    {loading ? (
                      <button
                        type="submit"
                        className="btn btn-success spinner spinner-white spinner-left ml-2"
                        onSubmit={() => handleSubmit()}
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                        }}
                        disabled={isSubmitting}
                      >
                        <span>Simpan</span>
                      </button>
                    ) : (
                      <button
                        type="submit"
                        onSubmit={() => handleSubmit()}
                        className="btn btn-success ml-2"
                        disabled={isSubmitting}
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                        }}
                      >
                        <i className="fas fa-save"></i>
                        Simpan
                      </button>
                    )}
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </Modal.Body>
    </Modal>
  );
}

export default UploadFileModal;
