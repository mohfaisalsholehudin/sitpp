import React,  { Suspense }  from "react";
import {Redirect, Switch } from "react-router-dom";
import {LayoutSplashScreen, ContentRoute } from "../../../_metronic/layout";
// import TambahKnowledgeSuccessStory from "./tambah-knowledge/success-story/TambahKnowledgeSuccessStory";
import TambahKnowledge from "./tambah-knowledge/TambahKnowledge";
import TambahKnowledgeEdit from "./tambah-knowledge/TambahKnowledgeEdit"
import ReviewPKP from "./review-pkp/ReviewPKP";
// import ReviewPKPDisposisi from "./review-pkp/ReviewPKPDisposisi";
import ReviewPKPDisposisiSop from "./review-pkp/explicit/sop/ReviewPKPDisposisiSop";
// import ReviewPKPEdit from "./review-pkp/tacit/ReviewPKPEdit";
// import ReviewPKPEdit from "./review-pkp/ReviewPKPEdit";
// import ReviewPKPTolak from "./review-pkp/ReviewPKPTolak";
import ReviewPKPTolakSop from "./review-pkp/explicit/sop/ReviewPKPTolakSop";
import ReviewPKPSuccessStoryTacit from "./review-pkp/tacit/success-story/ReviewPKPSuccessStoryTacit";
// import ReviewPKPSuccessStory from "./review-pkp/tacit/success-story/ReviewPKPSuccessStory";
// import KnowledgeOwner from "./knowledge-owner/KnowledgeOwner";
// import KnowledgeOwnerEdit from "./knowledge-owner/tacit/KnowledgeOwnerEdit";
// import KnowledgeOwnerSuccessStoryTacit from "./knowledge-owner/tacit/success-story/KnowledgeOwnerSuccessStoryTacit"
// import KnowledgeOwnerDisposisi from "./knowledge-owner/KnowledgeOwnerDisposisi";
// import KnowledgeOwnerTolak from "./knowledge-owner/KnowledgeOwnerTolak"
import ReviewPKPSopExplicit from "./review-pkp/explicit/sop/ReviewPKPSopExplicit";
// import TambahKnowledgeOtherKnowledge from "./tambah-knowledge/other-knowledge/TambahKnowledgeOtherKnowledge";
// import TambahKnowledgeSop from "./tambah-knowledge/sop/TambahKnowledgeSop";
import KnowledgeOwner from "./knowledge-owner/KnowledgeOwner";
// import KnowledgeOwnerEdit from "./knowledge-owner/KnowledgeOwnerEdit";
import KnowledgeOwnerSuccessStoryTacit from "./knowledge-owner/tacit/success-story/KnowledgeOwnerSuccessStoryTacit"
import KnowledgeOwnerSuccessStoryDisposisi from "./knowledge-owner/tacit/success-story/KnowledgeOwnerSuccessStoryDisposisi";
import KnowledgeOwnerSuccessStoryTolak from "./knowledge-owner/tacit/success-story/KnowledgeOwnerSuccessStoryTolak"
import ReviewPKPOtherKnowledgeTacit from "./review-pkp/tacit/other-knowledge/ReviewPKPOtherKnowledgeTacit"
import ReviewPKPTolakOtherKnowledge from "./review-pkp/tacit/other-knowledge/ReviewPKPTolakOtherKnowledge"
import ReviewPKPDisposisiOtherKnowledge from "./review-pkp/tacit/other-knowledge/ReviewPKPDisposisiOtherKnowledge"
import ReviewPKPSuccessStoryTolak from "./review-pkp/tacit/success-story/ReviewPKPSuccessStoryTolak";
import ReviewPKPSuccessStoryDisposisi from "./review-pkp/tacit/success-story/ReviewPKPSuccessStoryDisposisi";
import KnowledgeOwnerSopExplicit from "./knowledge-owner/explicit/sop/KnowledgeOwnerSopExplicit"
import KnowledgeOwnerTolakSop from "./knowledge-owner/explicit/sop/KnowledgeOwnerTolakSop"
import KnowledgeOwnerDisposisiSop from "./knowledge-owner/explicit/sop/KnowledgeOwnerDisposisiSop"
import KnowledgeOwnerOtherKnowledgeTacit from "./knowledge-owner/tacit/other-knowledge/KnowledgeOwnerOtherKnowledgeTacit"
import KnowledgeOwnerOtherKnowledgeDisposisi from "./knowledge-owner/tacit/other-knowledge/KnowledgeOwnerOtherKnowledgeDisposisi"
import KnowledgeOwnerOtherKnowledgeTolak from "./knowledge-owner/tacit/other-knowledge/KnowledgeOwnerOtherKnowledgeTolak"
import ReviewSME from "./review-sme/ReviewSME";
// import ReviewSMEEdit from "./review-sme/ReviewSMEEdit";
import ReviewSMESuccessStoryTacit from "./review-sme/tacit/success-story/ReviewSMESuccessStoryTacit"
import ReviewSMESuccessStoryTolak from "./review-sme/tacit/success-story/ReviewSMESuccessStoryTolak"
import ReviewSMEOtherKnowledgeTacit from "./review-sme/tacit/other-knowledge/ReviewSMEOtherKnowledgeTacit"
import ReviewSMETolakOtherKnowledge from "./review-sme/tacit/other-knowledge/ReviewSMETolakOtherKnowledge"
import ReviewSMESopExplicit from "./review-sme/explicit/sop/ReviewSMESopExplicit"
import ReviewSMETolakSop from "./review-sme/explicit/sop/ReviewSMETolakSop"
import TambahKnowledgeRoutes from "./tambah-knowledge/TambahKnowledgeRoutes";
import SearchKnowledge from "./search-knowledge/SearchKnowledge";

/* Begin Monitoring Knowledge */
import MonitoringKnowledge from "./monitoring-knowledge/MonitoringKnowledge";
import MonitoringKnowledgeSuccessStoryTacit from "./monitoring-knowledge/tacit/success-story/MonitoringKnowledgeSuccessStoryTacit";
import MonitoringKnowledgeOtherKnowledgeTacit from "./monitoring-knowledge/tacit/other-knowledge/MonitoringKnowledgeOtherKnowledgeTacit";
import MonitoringKnowledgeSopExplicit from "./monitoring-knowledge/explicit/sop/MonitoringKnowledgeSopExplicit";
/* End of Monitoring Knowledge */


/* Begin Review Update */
import ReviewUpdate from "./review-update-konten/ReviewUpdate";
import ReviewUpdateRoutes from "./review-update-konten/ReviewUpdateRoutes";
import ReviewUpdateSuccessStoryDisposisi from "./review-update-konten/success-story/ReviewUpdateSuccessStoryDisposisi";
import ReviewUpdateKo from "./review-update-konten-ko/ReviewUpdateKo";
import ReviewUpdateRoutesKo from "./review-update-konten-ko/ReviewUpdateRoutesKo";
import ReviewUpdateSuccessStoryDisposisiKo from "./review-update-konten-ko/success-story/ReviewUpdateSuccessStoryDisposisiKo";
/* End of Review Update */


/* Begin Feedback */
import Feedback from "./feedback/Feedback"
import FeedbackBase from "./feedback/FeedbackBase";
/* End of Feedback */

/* Begin Monitoring Review */
import MonitoringReview from "./monitoring-review/MonitoringReview";
import MonitoringReviewSuccessStoryTacit from "./monitoring-review/tacit/success-story/MonitoringReviewSuccessStoryTacit";
import MonitoringReviewOtherKnowledgeTacit from "./monitoring-review/tacit/other-knowledge/MonitoringReviewOtherKnowledgeTacit";
import MonitoringReviewSopExplicit from "./monitoring-review/explicit/sop/MonitoringReviewSopExplicit";
import ReviewUpdateSme from "./review-update-konten-sme/ReviewUpdateSme";
import ReviewUpdateRoutesSme from "./review-update-konten-sme/ReviewUpdateRoutesSme";
/* End of Monitoring Review */

/* Begin Rating */
import Rating from "./rating/Rating";
import RatingMain from "./rating/RatingMain"
import SearchKnowledgeRoutes from "./search-knowledge/SearchKnowledgeRoutes";
/* End of Rating */


/* Begin Mapping Peraturan */
import MappingPeraturan from "./mapping-peraturan/MappingPeraturan";
import MappingPeraturanEdit from "./mapping-peraturan/MappingPeraturanEdit";
/* End of Mapping Peraturan */


/* Begin Simple Knowledge */
import SimpleKnowledge from "./simple-knowledge/SimpleKnowledge";
import SimpleKnowledgeEdit from "./simple-knowledge/SimpleKnowledgeEdit";
import SimpleKnowledgeRoutes from "./simple-knowledge/SimpleKnowledgeRoutes";
import PengajuanUsulan from "./pengajuan-usulan-perubahan/PengajuanUsulan";
import PengajuanUsulanEdit from "./pengajuan-usulan-perubahan/add-edit/PengajuanUsulanEdit";
import ReviewPengajuanUsulan from "./review-pengajuan-usulan-perubahan/ReviewPengajuanUsulan";
import MatrixKnowledge from "./matrix-knowledge/MatrixKnowledge";
/* End of Simple Knowledge */
export default function ProcessKnowledge() {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
    <Switch>
         {
          /* Redirect from eCommerce root URL to /Jenis Pajak */
          <Redirect
            exact={true}
            from="/process-knowledge"
            to="/process-knowledge/add-knowledge"
          />
        }
        {/* Begin Tambah Knowledge */}
        {/* <ContentRoute path="/process-knowledge/add-knowledge/:id_tipe_km/:id_km_pro/:media/:step/add" component={TambahKnowledgeSop} />
        <ContentRoute path="/process-knowledge/add-knowledge/:id_tipe_km/:id_km_pro/:media/:step/add" component={TambahKnowledgeOtherKnowledge} /> */}
        {/* <ContentRoute path="/process-knowledge/add-knowledge/:id_tipe_km/:id_km_pro/:media/:step/add" component={TambahKnowledgeSuccessStory} /> */}
        {/* <ContentRoute path="/process-knowledge/add-knowledge/:id_tipe_km/:id_km_pro/:media/:step/add" component={TambahKnowledgeRoutes} /> */}
        <ContentRoute path="/process-knowledge/add-knowledge/:id_tipe_km/:id_km_pro/:step/add" component={TambahKnowledgeRoutes} />
        <ContentRoute path="/process-knowledge/add-knowledge/:id_tipe_km/:id_km_pro/:step/edit" component={TambahKnowledgeRoutes} />
        <ContentRoute path="/process-knowledge/add-knowledge/:id_tipe_km/:id_km_pro/:step/view" component={TambahKnowledgeRoutes} />
        <ContentRoute path="/process-knowledge/add-knowledge/add" component={TambahKnowledgeEdit} />
        <ContentRoute path="/process-knowledge/add-knowledge" component={TambahKnowledge} />
        {/* End of Tambah Knowledge */}

        {/* Begin Tambah Simple Knowledge */}
        <ContentRoute path="/process-knowledge/simple-knowledge/:id_tipe_km/:id_km_pro/:step/add" component={SimpleKnowledgeRoutes} />
        <ContentRoute path="/process-knowledge/simple-knowledge/:id_tipe_km/:id_km_pro/:step/edit" component={SimpleKnowledgeRoutes} />
        <ContentRoute path="/process-knowledge/simple-knowledge/:id_tipe_km/:id_km_pro/:step/view" component={SimpleKnowledgeRoutes} />
        <ContentRoute path="/process-knowledge/simple-knowledge/add" component={SimpleKnowledgeEdit} />
        <ContentRoute path="/process-knowledge/simple-knowledge" component={SimpleKnowledge} />
        {/* End of Tambah Simple Knowledge */}



         {/* Begin Review PKP */}
         <ContentRoute path="/process-knowledge/review-pkp/:id_km_pro/success-story/:jenis/:step/review" component={ReviewPKPSuccessStoryTacit} />
        <ContentRoute path="/process-knowledge/review-pkp/:id_km_pro/success-story/:jenis/:step/detail" component={ReviewPKPSuccessStoryTacit} />
        <ContentRoute path="/process-knowledge/review-pkp/:id_km_pro/sop/:jenis/:step/review" component={ReviewPKPSopExplicit} />
        <ContentRoute path="/process-knowledge/review-pkp/:id_km_pro/sop/:jenis/:step/detail" component={ReviewPKPSopExplicit} />
        <ContentRoute path="/process-knowledge/review-pkp/:id_km_pro/other-knowledge/:jenis/:step/review" component={ReviewPKPOtherKnowledgeTacit} />
        <ContentRoute path="/process-knowledge/review-pkp/:id_km_pro/other-knowledge/:jenis/:step/detail" component={ReviewPKPOtherKnowledgeTacit} />
        <ContentRoute path="/process-knowledge/review-pkp/:id_km_pro/success-story/:jenis/tolak" component={ReviewPKPSuccessStoryTolak} />
        <ContentRoute path="/process-knowledge/review-pkp/:id_km_pro/success-story/:jenis/disposisi" component={ReviewPKPSuccessStoryDisposisi} />
        <ContentRoute path="/process-knowledge/review-pkp/:id_km_pro/other-knowledge/:jenis/tolak" component={ReviewPKPTolakOtherKnowledge} />
        <ContentRoute path="/process-knowledge/review-pkp/:id_km_pro/other-knowledge/:jenis/disposisi" component={ReviewPKPDisposisiOtherKnowledge} />
        <ContentRoute path="/process-knowledge/review-pkp/:id_km_pro/sop/:jenis/tolak" component={ReviewPKPTolakSop} />
        <ContentRoute path="/process-knowledge/review-pkp/:id_km_pro/sop/:jenis/disposisi" component={ReviewPKPDisposisiSop} />
        <ContentRoute path="/process-knowledge/review-pkp" component={ReviewPKP} />
         {/* End of Review PKP */}

        {/* Begin Knowledge Owner */}
        <ContentRoute path="/process-knowledge/review-ko/:id_km_pro/success-story/:jenis/:step/review" component={KnowledgeOwnerSuccessStoryTacit} />
        <ContentRoute path="/process-knowledge/review-ko/:id_km_pro/sop/:jenis/:step/review" component={KnowledgeOwnerSopExplicit} />
        <ContentRoute path="/process-knowledge/review-ko/:id_km_pro/other-knowledge/:jenis/:step/review" component={KnowledgeOwnerOtherKnowledgeTacit} />
        <ContentRoute path="/process-knowledge/review-ko/:id_km_pro/success-story/:jenis/tolak" component={KnowledgeOwnerSuccessStoryTolak} />
        <ContentRoute path="/process-knowledge/review-ko/:id_km_pro/success-story/:jenis/disposisi" component={KnowledgeOwnerSuccessStoryDisposisi} />
        <ContentRoute path="/process-knowledge/review-ko/:id_km_pro/sop/:jenis/tolak" component={KnowledgeOwnerTolakSop} />
        <ContentRoute path="/process-knowledge/review-ko/:id_km_pro/sop/:jenis/disposisi" component={KnowledgeOwnerDisposisiSop} />
        <ContentRoute path="/process-knowledge/review-ko/:id_km_pro/other-knowledge/:jenis/tolak" component={KnowledgeOwnerOtherKnowledgeTolak} />
        <ContentRoute path="/process-knowledge/review-ko/:id_km_pro/other-knowledge/:jenis/disposisi" component={KnowledgeOwnerOtherKnowledgeDisposisi} />
        <ContentRoute path="/process-knowledge/review-ko" component={KnowledgeOwner} />
         {/* End of Knowledge Owner */}

        {/* Begin Review SME */}
        <ContentRoute path="/process-knowledge/review-sme/:id_km_pro/success-story/:jenis/:step/review" component={ReviewSMESuccessStoryTacit} />
        <ContentRoute path="/process-knowledge/review-sme/:id_km_pro/sop/:jenis/:step/review" component={ReviewSMESopExplicit} />
        <ContentRoute path="/process-knowledge/review-sme/:id_km_pro/other-knowledge/:jenis/:step/review" component={ReviewSMEOtherKnowledgeTacit} />
        <ContentRoute path="/process-knowledge/review-sme/:id_km_pro/success-story/:jenis/tolak" component={ReviewSMESuccessStoryTolak} />
        <ContentRoute path="/process-knowledge/review-sme/:id_km_pro/sop/:jenis/tolak" component={ReviewSMETolakSop} />
        <ContentRoute path="/process-knowledge/review-sme/:id_km_pro/other-knowledge/:jenis/tolak" component={ReviewSMETolakOtherKnowledge} />
        <ContentRoute path="/process-knowledge/review-sme" component={ReviewSME} />
        {/* End of Review SME */}

        {/* Begin Search Knowledge */}

        <ContentRoute path="/process-knowledge/search-knowledge/:id_km/:tipe_km/detil" component={SearchKnowledgeRoutes} />
        <ContentRoute path="/process-knowledge/search-knowledge" component={SearchKnowledge} />

        {/* End of Search Knowledge */}

        {/* Begin Review Update Knowledge */}
        <ContentRoute path="/process-knowledge/update-review/:id_tipe_km/:id_km_pro/disposisi/detil/" component={ReviewUpdateSuccessStoryDisposisi} />
        <ContentRoute path="/process-knowledge/update-review/:id_tipe_km/:id_km_pro/:step/view" component={ReviewUpdateRoutes} />
        <ContentRoute path="/process-knowledge/update-review/:id_tipe_km/:id_km_pro/:step/detil" component={ReviewUpdateRoutes} />
        <ContentRoute path="/process-knowledge/update-review" component={ReviewUpdate} />
        {/* End of Review Update Knowledge */}

        {/* Begin Review Update Knowledge KO */}
        <ContentRoute path="/process-knowledge/ko-update-review/:id_tipe_km/:id_km_pro/disposisi/detil/" component={ReviewUpdateSuccessStoryDisposisiKo} />
        <ContentRoute path="/process-knowledge/ko-update-review/:id_tipe_km/:id_km_pro/:step/view" component={ReviewUpdateRoutesKo} />
        <ContentRoute path="/process-knowledge/ko-update-review/:id_tipe_km/:id_km_pro/:step/detil" component={ReviewUpdateRoutesKo} />
        <ContentRoute path="/process-knowledge/ko-update-review" component={ReviewUpdateKo} />
        {/* End of Review Update Knowledge KO */}

        {/* Begin Review Update Knowledge SME */}
        <ContentRoute path="/process-knowledge/sme-update-review/:id_tipe_km/:id_km_pro/:step/detil" component={ReviewUpdateRoutesSme} />
        <ContentRoute path="/process-knowledge/sme-update-review" component={ReviewUpdateSme} />
        {/* End of Review Update Knowledge SME */}


        {/* Begin Monitoring Pengetahuan */}
        <ContentRoute path="/process-knowledge/monitoring-knowledge/:id_km_pro/success-story/:jenis/:step/detail" component={MonitoringKnowledgeSuccessStoryTacit} />
        <ContentRoute path="/process-knowledge/monitoring-knowledge/:id_km_pro/other-knowledge/:jenis/:step/detail" component={MonitoringKnowledgeOtherKnowledgeTacit} />
        <ContentRoute path="/process-knowledge/monitoring-knowledge/:id_km_pro/sop/:jenis/:step/detail" component={MonitoringKnowledgeSopExplicit} />
        <ContentRoute path="/process-knowledge/monitoring-knowledge" component={MonitoringKnowledge} />
        {/* End of Monitoring Pengetahuan */}

        {/* Begin Feedback */}
        <ContentRoute path="/process-knowledge/feedback/:id/edit" component={FeedbackBase} />
        <ContentRoute path="/process-knowledge/feedback" component={Feedback} />
        {/* End of Feedback */}

        {/* Begin Rating */}
        <ContentRoute path="/process-knowledge/rating/:id_km_pro/detail" component={RatingMain} />
        <ContentRoute path="/process-knowledge/rating" component={Rating} />
        {/* End of Rating */}

        {/* Begin Monitoring Review */}
        <ContentRoute path="/process-knowledge/monitoring-review/:id_km_pro/success-story/:jenis/:step/detail" component={MonitoringReviewSuccessStoryTacit} />
        <ContentRoute path="/process-knowledge/monitoring-review/:id_km_pro/other-knowledge/:jenis/:step/detail" component={MonitoringReviewOtherKnowledgeTacit} />
        <ContentRoute path="/process-knowledge/monitoring-review/:id_km_pro/sop/:jenis/:step/detail" component={MonitoringReviewSopExplicit} />
        <ContentRoute path="/process-knowledge/monitoring-review" component={MonitoringReview} />
        {/* End of Monitoring Review */}

        {/* Begin Mapping Regulasi */}
        <ContentRoute path="/process-knowledge/mapping/:id/:step/edit" component={MappingPeraturanEdit} />
        <ContentRoute path="/process-knowledge/mapping" component={MappingPeraturan} />
        {/* End of Mapping Regulasi */}

        {/* Begin Pengajuan Usulan */}
        <ContentRoute path="/process-knowledge/pengajuan/:id/view" component={PengajuanUsulanEdit} />
        <ContentRoute path="/process-knowledge/pengajuan/:id/edit" component={PengajuanUsulanEdit} />
        <ContentRoute path="/process-knowledge/pengajuan/add" component={PengajuanUsulanEdit} />
        <ContentRoute path="/process-knowledge/pengajuan" component={PengajuanUsulan} />
        {/* End of Pengajuan Usulan */}

        {/* Begin Review Pengajuan Usulan */}
        <ContentRoute path="/process-knowledge/review-pengajuan" component={ReviewPengajuanUsulan} />
        {/* End of Review Pengajuan Usulan */}

        {/* Begin Matrix Knowledge */}
        <ContentRoute path="/process-knowledge/matrix-knowledge" component={MatrixKnowledge} />
        {/* End of Matrix Knowledge */}



        




        

    </Switch>
  </Suspense>

  );
}
