import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
  PleaseWaitMessage,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import { useHistory } from "react-router-dom";

import {
  getPeraturanByJudul,
  getPeraturanByStatus,
} from "../../../references/Api";

import { Pagination } from "../../../helpers/pagination/Pagination";


function MappingPeraturanTable() {
  const history = useHistory();

  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(100);
  const [proposal, setProposal] = useState([]);
  const [data, setData] = useState([]);
  const [searchText, setSearchText] = useState("");


  const editPeraturan = (id) => history.push(`/process-knowledge/mapping/${id}/1/edit`)

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
    },
    {
      dataField: "no_regulasi",
      text: "No Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tgl_regulasi",
      text: "Tgl Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "perihal",
      text: "perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "id_peraturan",
      text: "ID Per",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      hidden: true
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterProcessKnowledgeMappingPeraturan,
      formatExtraData: {
        editPeraturan: editPeraturan
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "200px",
      },
    },
  ];

  const { SearchBar } = Search;

  useEffect(() => {
    // getRegulasiPagination(currentPage, sizePage).then(({ data }) => {
    //   setProposal(data.content);
    //   setData(data);
    // });

    getPeraturanByStatus(currentPage, sizePage, "Terima").then(({ data }) => {
      // console.log(data)
      setProposal(data.content);
      setData(data);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (searchText) {
      // console.log('ada isi')
      getPeraturanByJudul(currentPage, sizePage, searchText).then(
        ({ data }) => {
          console.log(data);
          setProposal(data.content);
          setData(data);
        }
      );
    } else {
      getPeraturanByStatus(currentPage, sizePage, "Terima").then(({ data }) => {
        setProposal(data.content);
        setData(data);
        // console.log(data.content)
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentPage, sizePage, searchText]);

  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_peraturan",
    pageNumber: currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "id_peraturan", order: "asc" }];
  const sizePerPageList = [
    { text: "100", value: 100 },
    { text: "150", value: 150 },
    { text: "200", value: 200 },
  ];
  const pagiOptions = {
    custom: true,
    // totalSize: proposal.length,
    totalSize: data.totalItems,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };
  const emptyDataMessage = () => {
    // return "No Data to Display";

    return (
      <div className="text-center">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  };

  const handleTableChange = (type, { sortField, sortOrder, data }) => {
    setProposal([]);
      let result;
      if (sortOrder === 'desc') {
        result = data.sort((a, b) => {
          if (a[sortField] > b[sortField]) {
            return 1;
          } else if (b[sortField] > a[sortField]) {
            return -1;
          }
          return 0;
        });
        setProposal(result)
      } else {
        result = data.sort((a, b) => {
          if (a[sortField] > b[sortField]) {
            return -1;
          } else if (b[sortField] > a[sortField]) {
            return 1;
          }
          return 0;
        });
        setProposal(result)

      }
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_peraturan"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {(props) => {
                    setSearchText(props.searchProps.searchText);
                    return (
                      <div>
                        <div className="row">
                          <div className="col-lg-6 col-xl-6 mb-3">
                            <SearchBar
                              {...props.searchProps}
                              style={{ width: "500px" }}
                            />
                            <br />
                          </div>
                          <div className="col-lg-6 col-xl-6 mb-3"></div>
                        </div>
                        <BootstrapTable
                          {...props.baseProps}
                          wrapperClasses="table-responsive"
                          bordered={false}
                          headerWrapperClasses="thead-light"
                          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                          defaultSorted={defaultSorted}
                          bootstrap4
                          remote
                          onTableChange={handleTableChange}
                          noDataIndication={emptyDataMessage}
                          {...paginationTableProps}
                        >
                          <PleaseWaitMessage entities={proposal} />
                        </BootstrapTable>
                        <Pagination
                          paginationProps={paginationProps}
                          isLoading={false}
                        />
                      </div>
                    );
                  }}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default MappingPeraturanTable;
