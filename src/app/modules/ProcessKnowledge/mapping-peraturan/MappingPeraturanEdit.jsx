import React, { useEffect, useState, useRef } from "react";
import { Modal, Spinner } from "react-bootstrap";
import { useSelector } from "react-redux";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../_metronic/layout";
import {
  getRegulasiById,
  getPeraturanTerkait,
  updateRegulasi,
  saveBodyRegulasi,
  getDokumenPer,
} from "../../../references/Api";
import swal from "sweetalert";
import { uploadFileNew } from "../../Evaluation/Api";
import MappingPeraturanEditForm from "./MappingPeraturanEditForm";
import MappingPeraturanTerkait from "./MappingPeraturanTerkait";
import MappingPeraturanDokumenLain from "./MappingPeraturanDokumenLain";
import BisnisSektorTable from "./bisnis-sektor/BisnisSektorTable";
import ProbisTable from "./probis/ProbisTable";
import CasenameTable from "./casename/CasenameTable";
import SubcaseTable from "./subcase/SubcaseTable";

const initValues = {
  alasan_tolak: "",
  body: "",
  file_upload: "",
  id_topik: "",
  jns_regulasi: "",
  kd_kantor: "",
  kd_unit_org: "",
  nip_pengusul: "",
  nip_pjbt_es3: "",
  nip_pjbt_es4: "",
  no_regulasi: "",
  perihal: "",
  status: "",
  tgl_regulasi: "",
};

function MappingPeraturanEdit({
  history,
  match: {
    params: { id, step },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const { role, user } = useSelector((state) => state.auth);
  const [actionsLoading] = useState(true);
  const [loading, setLoading] = useState(false);
  const [isDisabled, setIsDisabled] = useState();
  const [proposal, setProposal] = useState();
  const [dokLain, setDoklain] = useState([]);
  const [peraturanTerkait, setPeraturanTerkait] = useState([]);
  let thePath = document.URL;
  let nextStatus = "";
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);

  const [statNow, setstatNow] = useState("");

  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };
  useEffect(() => {
    if (lastPath == "proses") {
      let _title = "Proses Kajian";
      setTitle(_title);
      suhbeader.setTitle(_title);
    } else {
      let _title = "Edit Mapping Peraturan Perpajakan";
      setTitle(_title);
      suhbeader.setTitle(_title);
    }

    getRegulasiById(id).then(({ data }) => {
      setProposal({
        id_peraturan: data.id_peraturan,
        no_regulasi: data.no_regulasi,
        tgl_regulasi: data.tgl_regulasi,
        id_jnspajak: data.id_jnspajak,
        perihal: data.perihal,
        file_upload: data.file_upload,
        status: data.status,
        jns_regulasi: data.jns_regulasi,
        alasan_tolak: data.alasan_tolak ? data.alasan.tolak : "",
        id_topik: data.id_topik,
        body: data.body,
        nip_pengusul: data.nip_pengusul,
        nip_pjbt_es4: data.nip_pjbt_es4,
        nip_pjbt_es3: data.nip_pjbt_es3,
        wkt_usul: data.wkt_usul,
        wkt_teliti_es4: data.wkt_teliti_es4,
        wkt_teliti_es3: data.wkt_teliti_es3,

        wkt_create: data.wkt_create,
        wkt_update: data.wkt_update,
        kd_kantor: data.kd_kantor,
        kd_unit_org: data.kd_unit_org,
      });
      if (data.status === "Eselon 4") {
        setstatNow(2);
      } else if (data.status === "Eselon 3") {
        setstatNow(3);
      }
    });
  }, [id, lastPath, suhbeader]);

  useEffect(() => {
    getPeraturanTerkait(id).then(({ data }) => {
      data.map((dt) => {
        //setPeraturanTerkait(peraturanTerkait => [...peraturanTerkait,dt.kmperaturanTerkait]);

        setPeraturanTerkait((peraturanTerkait) => [
          ...peraturanTerkait,
          {
            id_per_terkait: dt.kmperaturanTerkait.id_per_terkait,
            id_peraturan: dt.kmregulasiPerpajakan.id_peraturan,
            no_regulasi: dt.kmregulasiPerpajakan.no_regulasi,
            tgl_regulasi: dt.kmregulasiPerpajakan.tgl_regulasi,
            perihal: dt.kmregulasiPerpajakan.perihal,
            status: dt.kmregulasiPerpajakan.status,
            file_upload: dt.kmregulasiPerpajakan.file_upload,
            jns_regulasi: dt.kmregulasiPerpajakan.jns_regulasi,
            id_topik: dt.kmregulasiPerpajakan.id_topik,
            body: dt.kmregulasiPerpajakan.body,
          },
        ]);
      });
    });
  }, [id]);

  useEffect(() => {
    getDokumenPer(id).then(({ data }) => {
      setDoklain(data);
    });
  }, [id]);

  const btnRef = useRef();

  const saveRegulasiTerkait = (values) => {
    if (values.file) {
      if (values.file.name) {
        enableLoading();
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFileNew(formData).then(({ data }) => {
          disableLoading();
          updateRegulasi(
            values.id_peraturan,
            values.alasan_tolak,
            values.body,
            data.message,
            values.id_topik,
            values.jns_regulasi,
            values.kd_kantor,
            values.kd_unit_org,
            values.nip_pengusul,
            values.nip_pjbt_es3,
            values.nip_pjbt_es4,
            values.no_regulasi,
            values.perihal,
            values.status,
            values.tgl_regulasi
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              saveBodyRegulasi(values.id_peraturan, values.body).then(
                ({ status }) => {
                  if (status === 201 || status === 200) {
                    switch (step) {
                      case "1":
                        history.push(`/process-knowledge/mapping/${id}/2/edit`);
                        break;
                      case "2":
                        history.push(`/process-knowledge/mapping`);
                        break;
                      default:
                        break;
                    }
                    // swal(
                    //   "Berhasil",
                    //   "Regulasi berhasil diubah",
                    //   "success"
                    // ).then(() => {
                    //   history.push("/process-knowledge/mapping");
                    // });
                  }
                }
              );
            } else {
              swal("Gagal", "Regulasi gagal diubah", "error").then(() => {
                history.push("/process-knowledge/mapping");
              });
            }
          });
        });
      } else {
        updateRegulasi(
          values.id_peraturan,
          values.alasan_tolak,
          values.body,
          values.file_upload,
          values.id_topik,
          values.jns_regulasi,
          values.kd_kantor,
          values.kd_unit_org,
          values.nip_pengusul,
          values.nip_pjbt_es3,
          values.nip_pjbt_es4,
          values.no_regulasi,
          values.perihal,
          values.status,
          values.tgl_regulasi
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            saveBodyRegulasi(values.id_peraturan, values.body).then(
              ({ status }) => {
                if (status === 201 || status === 200) {
                  switch (step) {
                    case "1":
                      history.push(`/process-knowledge/mapping/${id}/2/edit`);
                      break;
                    case "2":
                      history.push(`/process-knowledge/mapping`);
                      break;
                    default:
                      break;
                  }
                  // swal("Berhasil", "Regulasi berhasil diubah", "success").then(
                  //   () => {
                  //     history.push("/process-knowledge/mapping");
                  //   }
                  // );
                }
              }
            );
          } else {
            swal("Gagal", "Regulasi gagal diubah", "error").then(() => {
              history.push("/process-knowledge/mapping");
            });
          }
        });
      }
    } else {
      updateRegulasi(
        values.id_peraturan,
        values.alasan_tolak,
        values.body,
        "",
        values.id_topik,
        values.jns_regulasi,
        values.kd_kantor,
        values.kd_unit_org,
        values.nip_pengusul,
        values.nip_pjbt_es3,
        values.nip_pjbt_es4,
        values.no_regulasi,
        values.perihal,
        values.status,
        values.tgl_regulasi
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          saveBodyRegulasi(values.id_peraturan, values.body).then(
            ({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Regulasi berhasil diubah", "success").then(
                  () => {
                    history.push("/process-knowledge/mapping");
                  }
                );
              }
            }
          );
        } else {
          swal("Gagal", "Regulasi gagal diubah", "error").then(() => {
            history.push("/process-knowledge/mapping");
          });
        }
      });
    }
  };

  const backAction = () => {
    switch (step) {
      case "1":
        history.push(`/process-knowledge/mapping`);

        break;
      case "2":
        history.push(`/process-knowledge/mapping/${id}/1/edit`);

        break;

      default:
        break;
    }
    // }
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };

  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  const saveForm = (values) => {
    switch (step) {
      case "1":
        saveRegulasiTerkait(values);
        break;
      case "2":
        history.push(`/process-knowledge/mapping`);
        break;

      default:
        break;
    }
  };

  const checkStep = (val) => {
    switch (val) {
      case "1":
        return (
          <MappingPeraturanEditForm
            actionsLoading={actionsLoading}
            proposal={proposal || initValues}
            peraturanTerkait={peraturanTerkait}
            idPeraturan={id}
            btnRef={btnRef}
            saveRegulasiTerkait={saveForm}
            lastPath={lastPath}
          />
        );

      case "2":
        return (
          <>
            <MappingPeraturanTerkait
              peraturanTerkait={peraturanTerkait}
              idPeraturan={id}
              lastPath={lastPath}
              step={step}
            />
            <MappingPeraturanDokumenLain
              dokLain={dokLain}
              idPeraturan={id}
              lastPath={lastPath}
              step={step}
            />
            <BisnisSektorTable id_peraturan={id} step={step} />
            <ProbisTable id_peraturan={id} step={step} />
            <CasenameTable id_peraturan={id} step={step} />
            <SubcaseTable id_peraturan={id} step={step} />
          </>
        );

      default:
        break;
    }
  };
  return (
    <>
      <Card>
        {/* {actionsLoading && <ModalProgressBar />} */}
        <CardHeader
          title={title}
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <div className="mt-5">{checkStep(step)}</div>
          <br></br>
        </CardBody>
        <CardFooter style={{ borderTop: "none" }}>
          <>
            <div className="col-lg-12" style={{ textAlign: "right" }}>
              <button
                type="button"
                onClick={backAction}
                className="btn btn-light"
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                }}
              >
                <i className="fa fa-arrow-left"></i>
                Kembali
              </button>

              {step === "2" ? (
                <button
                  type="submit"
                  className="btn btn-success ml-2"
                  onClick={saveForm}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={disabled}
                >
                  <i className="fas fa-save"></i>
                  Simpan
                </button>
              ) : loading ? (
                <button
                  type="submit"
                  className="btn btn-success spinner spinner-white spinner-left ml-2"
                  onSubmit={saveButton}
                  disabled={true}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                >
                  <span>Selanjutnya</span>
                </button>
              ) : (
                <button
                  type="submit"
                  className="btn btn-success ml-2"
                  onClick={saveButton}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={disabled}
                >
                  Selanjutnya
                  <i
                    className="fa fa-arrow-right"
                    style={{ marginLeft: "0.5rem" }}
                  ></i>
                </button>
              )}
              {/* {loading ? (
              <button
                type="submit"
                className="btn btn-success spinner spinner-white spinner-left ml-2"
                onSubmit={saveForm}
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                }}
              >
                <span>Simpan</span>
              </button>
            ) : (
              <button
                type="submit"
                className="btn btn-success ml-2"
                onClick={saveForm}
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                }}
                // disabled={disabled}
              >
                <i className="fas fa-save"></i>
                Simpan
              </button>
            )} */}
            </div>
          </>
          {/* <RegulasiTerkaitFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></RegulasiTerkaitFooter> */}
        </CardFooter>
      </Card>
      <Modal
        show={loading}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Body style={{ textAlign: "center" }}>
          <h1>
            <Spinner
              animation="border"
              variant="primary"
              role="status"
              style={{ width: "50px", height: "50px" }}
            >
              <span className="sr-only">Loading...</span>
            </Spinner>
          </h1>
          <h1>Sedang Upload File. . .</h1>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default MappingPeraturanEdit;
