/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from "react";
import {
  Modal,
  Table,
  FormControl,
  InputGroup,
  Button,
} from "react-bootstrap";
import { addPerTerkaitById } from "../../../references/Api";
import axios from "axios";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import { useHistory } from "react-router-dom";
import swal from "sweetalert";

function MappingPeraturanTerkaitModal({ id, show, onHide, step }) {
  const [val, setVal] = useState();
  const [perterkait, setPerterkait] = useState([]);
  const history = useHistory();
  const { BACKEND_URL } = window.ENV;

  const addPer = (idPer) => {
    //alert(idPer)
    addPerTerkaitById(1, id, idPer).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
          history.push("/dashboard");
          history.replace(`/process-knowledge/mapping/${id}/${step}/edit`);
        });
      } else {
        swal("Gagal", "Data gagal disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace(`/process-knowledge/mapping/${id}/${step}/edit`);
        });
      }
    });

    //alert(id);
  };

  const handleChange = (e) => {
    setVal(e.target.value);
  };

  const handleSubmit = () => {
  //showPeraturan(val)
  if (val.length < 5) {
    swal({
      title: "Harap masukkan minimal 5 karakter!",
      text: "Klik OK untuk melanjutkan",
      icon: "info",
      closeOnClickOutside: false,
    }).then((willApply) => {
      if (willApply) {
        // history.push("/logout");
        setVal("");
      }
    });
  } else {
    axios
      .get(
        // `${BACKEND_URL}/api/kmregulasiperpajakan/terima/by_no?no_regulasi=${val}`
        `${BACKEND_URL}/api/kmregulasiperpajakan/terima/by_no?page=${1}&pencarian=${val}&size=${100}`
      )
      .then(function(response) {
        if (response.data) {
          setPerterkait(response.data.content);
        } else {
          setPerterkait([]);
        }
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      })
      .then(function() {
        // always executed
      });
  }
  };

  // console.log(perterkait);

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Cari Peraturan
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <InputGroup className="mb-3">
              <FormControl
                name="no_peraturan"
                aria-label="Default"
                placeholder="Masukkan Nomor Peraturan"
                onChange={(e) => handleChange(e)}
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
          </div>

          <div className="col-lg-2 col-xl-2 mb-3">
            <Button type="submit" onClick={handleSubmit} variant="primary">
              Cari
            </Button>
          </div>
        </div>

        <div className="row">
          <Table responsive hover>
            <thead style={{ border: "1px solid #3699FF", textAlign: "center" }}>
              <tr>
                <th>No</th>
                <th style={{ textAlign: "left" }}>No Peraturan</th>
                <th style={{ textAlign: "left" }}>Perihal</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody style={{ border: "1px solid #3699FF", textAlign: "center" }}>
              {perterkait.map((data, index) => (
                <tr key={index} style={{ height: "40px" }}>
                  <td>{index + 1}</td>
                  <td style={{ textAlign: "left" }}>{data.no_regulasi}</td>
                  <td style={{ textAlign: "left" }}>{data.perihal}</td>
                  <td>
                    <a
                      title="Tambah"
                      className="btn btn-icon btn-light btn-hover-success btn-sm mx-3"
                      onClick={() => addPer(data.id_peraturan)}
                    >
                      <span className="svg-icon svg-icon-md svg-icon-success">
                        <SVG
                          src={toAbsoluteUrl(
                            "/media/svg/icons/Navigation/Plus.svg"
                          )}
                        />
                      </span>
                    </a>
                  </td>
                </tr>
              ))}
            </tbody>
            {/* <tfoot style={{ border: '1px solid #3699FF', textAlign: 'center' }}>
                            <tr style={{ height: '40px' }}>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot> */}
          </Table>
        </div>
      </Modal.Body>
      <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            // onClick={backAction}
            onClick={onHide}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}

export default MappingPeraturanTerkaitModal;
