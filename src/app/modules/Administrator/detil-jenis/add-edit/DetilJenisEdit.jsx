/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import DetilJenisEditFooter from "./DetilJenisEditFooter";
import DetilJenisEditForm from "./DetilJenisEditForm";
import { saveDetilJenis, getDetilJenisById, updateDetilJenis } from "../../../Evaluation/Api";

function DetilJenisEdit({
  history,
  match: {
    params: { id }
  }
}) {
  const initValues = {
    id_jenis: "",
    nama: "",
    keterangan: ""
  };

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();

  useEffect(() => {
    let _title = id ? "Edit Detil Jenis" : "Tambah Detil Jenis";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
      getDetilJenisById(id).then(({ data })=> {
        setContent(data)
      })
    }
  }, [id, suhbeader]);
  const btnRef = useRef();

  const backAction = () => {
    history.push("/admin/detil-jenis");
  };
  const saveForm = values => {
    if (!id) {
      saveDetilJenis(values.id_jenis, values.nama, values.keterangan).then(
        ({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push("/admin/detil-jenis");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/admin/detil-jenis/add");
            });
          }
        }
      );
    }else{
      updateDetilJenis(id, values.id_jenis, values.nama, values.keterangan).then(
        ({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push("/admin/detil-jenis");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/admin/detil-jenis/add");
            });
          }
        }
      );
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <DetilJenisEditForm
            actionsLoading={actionsLoading}
            proposal={content || initValues}
            btnRef={btnRef}
            saveForm={saveForm}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <DetilJenisEditFooter backAction={backAction} btnRef={btnRef} />
      </CardFooter>
    </Card>
  );
}

export default DetilJenisEdit;
