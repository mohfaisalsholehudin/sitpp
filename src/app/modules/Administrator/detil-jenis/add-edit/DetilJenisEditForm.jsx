import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import "../../../../helpers/DatePickerStyles.css";
import { Input, Textarea } from "../../../../helpers";
import { getMasterJenis } from "../../../Evaluation/Api";

function DetilJenisEditForm({ proposal, btnRef, saveForm }) {
  const [master, setMaster] = useState([]);
  const [valMaster, setValMaster] = useState();

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    nama: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Nama Jenis is required"),
    keterangan: Yup.string().required("Keterangan is required"),
    id_jenis: Yup.number().required("Master Jenis is required")
  });

  useEffect(() => {
    getMasterJenis().then(({ data }) => {
      data.map(data => {
        return setMaster(master => [
          ...master,
          {
            label: data.nama,
            value: data.id
          }
        ]);
      });
    });
  }, []);

  useEffect(() => {
    if (proposal.id_jenis) {
      master
        .filter(data => data.value === proposal.id_jenis)
        .map(data => {
          setValMaster(data.label);
        });
    }
  }, [master, proposal.id_jenis]);

  // console.log(proposal)

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={ProposalEditSchema}
        onSubmit={values => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          const handleChangeMaster = val => {
            setFieldValue("id_jenis", val.value);
            setValMaster(val.label);
          };
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Master Jenis */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Master Jenis
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={master}
                      onChange={value => handleChangeMaster(value)}
                      value={master.filter(data => data.label === valMaster)}
                    />
                  </div>
                </div>
                {/* Field Jenis Pajak */}
                <div className="form-group row">
                  <Field
                    name="nama"
                    component={Input}
                    placeholder="Nama"
                    label="Nama"
                  />
                </div>
                {/* Field Keterangan */}
                <div className="form-group row">
                  <Field
                    name="keterangan"
                    component={Textarea}
                    placeholder="Keterangan"
                    label="Keterangan"
                  />
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default DetilJenisEditForm;
