import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import DetilJenisTable from "./DetilJenisTable";

function DetilJenis() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Detil Jenis"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <DetilJenisTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default DetilJenis;
