import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../../../helpers/column-formatters";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import { Pagination } from "../../../helpers/pagination/Pagination";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import { deletePengumuman, getPengumuman, saveTusiKm, updateStatusPengumuman } from "../../../references/Api";
import swal from "sweetalert";
import SVG from "react-inlinesvg";

function PengumumanTable({ id, status }) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);

  const add = () => history.push("/admin/pengumuman/add");
  const edit = (id) => history.push(`/admin/pengumuman/${id}/edit`);
  const editPublish = (id) => history.push(`/admin/pengumuman/${id}/publish`)
  const deleteAction = (id) => {
    swal({
      title: "Apakah Anda Yakin?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        deletePengumuman(id).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/admin/pengumuman");
            });
          } else {
            swal("Gagal", "Data gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace("/admin/pengumuman");
            });
          }
        });
      }
      // else {
      //   swal("Your imaginary file is safe!");
      // }
    });
  };
  const apply = (id_pengumuman) => {
    swal({
      title: "Apakah Anda Ingin Mengajukan Pengumuman Ini?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: false,
    }).then((willApply) => {
      if (willApply) {
        updateStatusPengumuman(id_pengumuman, "Publish Aktif").then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Pengumuman berhasil diajukan", "success").then(
              () => {
                history.push("/dashboard");
                history.replace("/admin/pengumuman");
              }
            );
          } else {
            swal("Gagal", "Pengumuman gagal diajukan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/admin/pengumuman");
            });
          }
        });
      }
    });
  };
  useEffect(() => {
    getPengumuman().then(({ data }) => {
      setContent(data);
    });
  }, []);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
    },
    {
      dataField: "judul",
      text: "Judul",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "target",
      text: "Target",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "updated_at",
      text: "Updated",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      hidden: true
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter:
        columnFormatters.StatusColumnFormatterAdminPengumuman,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterAdminPengumuman,
      formatExtraData: {
        editDialog: edit,
        editPublish: editPublish,
        deleteDialog: deleteAction,
        apply: apply
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "updated_at",
    pageNumber: currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "updated_at", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_pengumuman"
                  data={content}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={add}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default PengumumanTable;
