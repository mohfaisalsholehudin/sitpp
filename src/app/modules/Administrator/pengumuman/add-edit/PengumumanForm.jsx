import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
// import "./styles.css";
import { Input, Textarea, Select as Sel } from "../../../../helpers";

function PengumumanForm({ content, btnRef, nextRef, saveForm, nextForm, path, setTgt, tgt }) {
  const { user } = useSelector((state) => state.auth);
  const [valTipeTarget, setValTipeTarget] = useState();

  // Validation schema
  const ValidationSchema = Yup.object().shape({
    judul: Yup.string()
      .min(2, "Minimum 2 symbols")
      .required("Judul is required"),
    target: Yup.string().required("Target is required"),
    isi: Yup.string()
      .min(2, "Minimum 2 symbols")
      .trim("No Whitespace allowed")
      .required("Isi is required"),
  });

  const tipeTarget = [
    {
    label: "Individual",
    value: "Individual"
  },
  {
    label: "KO",
    value: "KO"
  },
  {
    label: "PKP",
    value: "PKP"
  },
  {
    label: "SME",
    value: "SME"
  },
  {
    label: "Semua Pegawai",
    value: "Semua Pegawai"
  },
]
  useEffect(() => {
    if (content.target) {
      tipeTarget
        .filter(data => data.value === content.target)
        .map(data => {
          setValTipeTarget(data.label);
        });
    } else {
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [content.target]);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ValidationSchema}
        onSubmit={(values) => {
          // console.log(values);
          saveForm(values);
        }}
      >
        {({ handleSubmit, setFieldValue, values }) => {
          // setTgt(values.target)
          const handleChangeTipeTarget = val => {
            setFieldValue("target", val.value);
            setValTipeTarget(val.label);
            setTgt(val.label)
          };
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD JUDUL */}
                <div className="form-group row">
                  {path === "view" ? (
                    <Field
                      name="judul"
                      component={Input}
                      placeholder="Judul"
                      label="Judul"
                      disabled
                    />
                  ) : (
                    <Field
                      name="judul"
                      component={Input}
                      placeholder="Judul"
                      label="Judul"
                    />
                  )}
                </div>
                {/* FIELD TARGET */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Pilih Target
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={tipeTarget}
                      onChange={value => handleChangeTipeTarget(value)}
                      value={tipeTarget.filter(data => data.label === valTipeTarget)}
                    />
                  </div>
                  {/* {path === "view" ? (
                    <Sel name="target" label="Target" disabled>
                      <option>Pilih Target</option>
                      <option value="Individual">Individual</option>
                      <option value="PKP">PKP</option>
                      <option value="KO">KO</option>
                      <option value="SME">SME</option>
                      <option value="Semua Pegawai">Semua Pegawai</option>
                    </Sel>
                  ) : (
                    <Sel
                      name="target"
                      label="Target"
                      // onBlur={() => handleChangeInstansi()}
                    >
                      <option>Pilih Target</option>
                      <option value="Individual">Individual</option>
                      <option value="PKP">PKP</option>
                      <option value="KO">KO</option>
                      <option value="SME">SME</option>
                      <option value="Semua Pegawai">Semua Pegawai</option>
                    </Sel>
                  )} */}
                </div>
                {/* FIELD ISI USULAN */}
                <div className="form-group row">
                  {path === "view" ? (
                    <Field
                      name="isi"
                      component={Textarea}
                      placeholder="Isi"
                      label="Isi"
                      disabled
                    />
                  ) : (
                    <Field
                      name="isi"
                      component={Textarea}
                      placeholder="Isi"
                      label="Isi"
                    />
                  )}
                </div>
                {path === "publish" ? (
                  <div className="form-group row">
                    <Sel name="status" label="Status">
                      {/* <option>Pilih Status</option> */}
                      <option value="Publish Aktif">Aktif</option>
                      <option value="Publish Non Aktif">Non Aktif</option>
                    </Sel>
                  </div>
                ) : null}

                {tgt === "Individual" ? 
                <button
                type="button"
                style={{ display: "none" }}
                ref={nextRef}
                onClick={() => nextForm(values)}
              ></button> : (
                  <button
                    type="submit"
                    style={{ display: "none" }}
                    ref={btnRef}
                    onSubmit={() => handleSubmit()}
                  ></button>
                )}
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default PengumumanForm;
