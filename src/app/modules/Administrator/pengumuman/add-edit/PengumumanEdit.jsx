import React, { useEffect, useState, useRef } from "react";
import { useSelector } from "react-redux";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import swal from "sweetalert";
// import PengajuanUsulanFooter from "./PengajuanUsulanFooter";
// import PengajuanUsulanForm from "./PengajuanUsulanForm";
import {
  getPengumumanById,
  getUsulanPerubahanById,
  savePengumuman,
  saveUsulanPerubahan,
  updatePengumuman,
  updateUsulanPerubahan,
} from "../../../../references/Api";
import PengumumanForm from "./PengumumanForm";
import PengumumanFooter from "./PengumumanFooter";

function PengumumanEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const { user } = useSelector((state) => state.auth);

  const [tgt, setTgt] = useState("")
  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [content, setContent] = useState();
  const [loading, setLoading] = useState(false);
  let thePath = document.URL;
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);

  const initValues = {
    judul: "",
    isi: "",
    target: "",
    status: "",
  };

  useEffect(() => {
    let _title = id ? "Edit Pengumuman / Pesan" : "Tambah Pengumuman / Pesan";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
      getPengumumanById(id).then(({ data }) => {
        setContent(data);
        if(data.target === 'Individual'){
          setTgt('Individual')
        }
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();
  const nextRef = useRef();
  const saveForm = (values) => {
    if (!id) {
      savePengumuman(values.isi, values.judul, values.target, "Draft").then(
        ({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push("/admin/pengumuman");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/admin/pengumuman/add");
            });
          }
        }
      );
    } else {
      updatePengumuman(
        id,
        values.isi,
        values.judul,
        values.target,
        lastPath === "publish"
          ? values.status === "Publish Aktif"
            ? "Publish Aktif"
            : "Publish Non Aktif"
          : "Draft"
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/admin/pengumuman");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/admin/pengumuman/add");
          });
        }
      });
    }
  };
  const handleBack = () => {
    history.push(`/admin/pengumuman`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };

  const nextForm = (values) => {
  // console.log(values)
  if (!id) {
    savePengumuman(values.isi, values.judul, values.target, "Draft").then(
      ({ status, data }) => {
        if (status === 201 || status === 200) {
          // swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push(`/admin/pengumuman/${data.id_pengumuman}/target`);
          // });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/admin/pengumuman/add");
          });
        }
      }
    );
  } else {
    updatePengumuman(
      id,
      values.isi,
      values.judul,
      values.target,
      lastPath === "publish"
        ? values.status === "Publish Aktif"
          ? "Publish Aktif"
          : "Publish Non Aktif"
        : "Draft"
    ).then(({ status, data }) => {
      if (status === 201 || status === 200) {
        // swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
          history.push(`/admin/pengumuman/${data.id_pengumuman}/target`);
        // });
      } else {
        swal("Gagal", "Data gagal disimpan", "error").then(() => {
          history.push("/admin/pengumuman/add");
        });
      }
    });
  }
  }

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <PengumumanForm
            actionsLoading={actionsLoading}
            content={content || initValues}
            btnRef={btnRef}
            nextRef={nextRef}
            saveForm={saveForm}
            nextForm={nextForm}
            setDisabled={setDisabled}
            path={lastPath}
            setTgt={setTgt}
            tgt={tgt}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <PengumumanFooter
          backAction={handleBack}
          btnRef={btnRef}
          nextRef={nextRef}
          loading={loading}
          path={lastPath}
          tgt={tgt}
        />
      </CardFooter>
    </Card>
  );
}

export default PengumumanEdit;
