import React, { useState, useEffect } from "react";
import { Route } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import { useHistory } from "react-router-dom";

//Helpers
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../../_metronic/_helpers";
import { Pagination } from "../../../../../helpers/pagination/Pagination";
import * as columnFormatters from "../../../../../helpers/column-formatters";
// import { getCosignUnit } from "../../../Evaluation/Api";
// import TindakLanjutDirektoratUnitOpen from "./TindakLanjutDirektoratUnitOpen";

function TargetFail({ id, failContent }) {
  const history = useHistory();
  // const [content, setContent] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);

  const openAction = (id_cosign) => {
    history.push(`/compose/process/detils/${id}/tindak-lanjut-direktorat/${id_cosign}/unit/open`)
  };


  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
    },
    {
      dataField: "nip",
      text: "NIP Pendek",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "message",
      text: "Alasan Gagal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    }
  ];

  const emptyDataMessage = () => {
    return "No Data";
  };
  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "nip",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "nip", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: failContent.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };

  return (
    <>
    <>
      <PaginationProvider pagination={paginationFactory(pagiOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <>
              <ToolkitProvider
                keyField="nip"
                data={failContent}
                columns={columns}
                search
              >
                {props => (
                  <div>
                    <div className="row">
                      <div className="col-lg-12 col-xl-12 mb-3 mt-3">
                        <h4> Daftar Pegawai Gagal Upload</h4>
                      </div>
                    </div>
                    <BootstrapTable
                      {...props.baseProps}
                      wrapperClasses="table-responsive"
                      bordered={false}
                      headerWrapperClasses="thead-light"
                      classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                      defaultSorted={defaultSorted}
                      bootstrap4
                      noDataIndication={emptyDataMessage}
                      {...paginationTableProps}
                    ></BootstrapTable>
                    <Pagination paginationProps={paginationProps} />
                  </div>
                )}
              </ToolkitProvider>
            </>
          );
        }}
      </PaginationProvider>
    </>
    {/* <Route path="/compose/process/detils/:id/tindak-lanjut-direktorat/:id_cosign/unit/open">
        {({ history, match }) => (
          <TindakLanjutDirektoratUnitOpen
            show={match != null}
            id={match && match.params.id}
            id_cosign={match && match.params.id_cosign}
            after={false}
            onHide={() => {
              history.push(
                `/compose/process/detils/${id}/tindak-lanjut-direktorat`
              );
            }}
            onRef={() => {
              history.push(
                `/compose/process/detils/${id}/tindak-lanjut-direktorat`

              );
            }}
          />
        )}
      </Route> */}
    </>
  );
}

export default TargetFail;
