import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import axios from "axios";
import swal from "sweetalert"
import * as Yup from "yup";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../../_metronic/_partials/controls";
import { uploadTarget } from "../../../../references/Api";
import CustomUploadInput from "../../../../helpers/form/CustomUploadInput";
import { useSelector } from "react-redux";
import TargetSuccess from "./upload/TargetSuccess";
import TargetFail from "./upload/TargetFail";
import CustomUploadPegawai from "../../../../helpers/form/CustomUploadPegawai";
const { BACKEND_URL } = window.ENV;

function PengumumanTargetUpload({
  history,
  match: {
    params: { id },
  },
}) {
  const [loading, setLoading] = useState(false);
  const { iamToken } = useSelector((state) => state.auth);
  const [isShow, setIsShow] = useState(false);
  const [successContent, setSuccessContent] = useState([]);
  const [failContent, setFailContent] = useState([]);

  const initialValues = {
    file: "",
  };

  const validationSchema = Yup.object().shape({
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        (value) => value && SUPPORTED_FORMATS.includes(value.type)
      ),
  });
  const FILE_SIZE = 15000000;
  const SUPPORTED_FORMATS = [
    // "application/vnd.ms-excel",
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  ];

  const uploadTarget = (formData) => {
    const config = {
      headers: {
        Authorization: iamToken,
        "Content-type": "multipart/form-data",
      },
    };
    return axios.post(
      `${BACKEND_URL}/api/pengumumantarget/uploadtarget`,
      formData,
      config
    );
  };

  const handleUpload = (values) => {
    enableLoading();
    const formData = new FormData();
    formData.append("upload", values.file);
    formData.append("idPengumuman", id);
    uploadTarget(formData).then(({ data }) => {
      disableLoading();
      setSuccessContent(data.data.tergetBerhasil);
      setFailContent(data.data.tergetgagal);
      setIsShow(true);
      //   updatePenyusunanKajianUnit(id, data.message).then(({ status }) => {
      //     if (status === 201 || status === 200) {
      //     //   swal("Berhasil", "Data berhasil disimpan", "success").then(
      //     //     () => {
      //     //       history.push("/dashboard");
      //     //       history.replace(
      //     //         `/compose/process/detils/${id}/tindak-lanjut-direktorat`
      //     //       );
      //     //     }
      //     //   );
      //     } else {
      //     //   swal("Gagal", "Data gagal disimpan", "error").then(() => {
      //     //     history.push(
      //     //       `/compose/process/detils/${id}/tindak-lanjut-direktorat`
      //     //     );
      //     //   });
      //     }
      //   });
    });
  };

  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  const handleBack = () => {
    history.push(`/admin/pengumuman/${id}/target`);
  };
  const handleSave = () => {
    swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
      history.push(`/admin/pengumuman/${id}/target`);
    });
  }
  return (
    <>
      <Card>
        <CardHeader
          title="Upload Target Pegawai"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <>
            <Formik
              enableReinitialize={true}
              initialValues={initialValues}
              validationSchema={validationSchema}
              onSubmit={(values) => {
                //   console.log(values);
                handleUpload(values);
                setIsShow(false);
              }}
            >
              {({ handleSubmit, isSubmitting }) => {
                return (
                  <Form className="form form-label-right">
                    <div
                      className="form-group row"
                      style={{ marginBottom: "0px" }}
                    ></div>
                    {/* FIELD UPLOAD FILE */}
                    <div className="form-group row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Upload File
                      </label>
                      <div className="col-lg-6 col-xl-6">
                        <Field
                          name="file"
                          component={CustomUploadPegawai}
                          title="Select a file"
                          label="File"
                          style={{ display: "flex" }}
                        />
                      </div>
                      <div className="col-lg-3 col-xl-3">
                        {loading ? (
                          <button
                            type="submit"
                            className="btn btn-primary spinner spinner-white spinner-left ml-2"
                            onSubmit={() => handleSubmit()}
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                            }}
                            disabled={isSubmitting}
                          >
                            <span>Upload</span>
                          </button>
                        ) : (
                          <button
                            type="submit"
                            onSubmit={() => handleSubmit()}
                            className="btn btn-primary ml-2"
                            // disabled={isSubmitting}
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                            }}
                          >
                            <i className="fas fa-upload"></i>
                            Upload
                          </button>
                        )}
                      </div>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </>
          {isShow ? (
            <>
              <TargetSuccess id={id} successContent={successContent} />
              <TargetFail id={id} failContent={failContent} />
            </>
          ) : null}
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              onClick={handleBack}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
            >
              <i className="fas fa-arrow-left"></i>
              Kembali
            </button>
            {`  `}
            {isShow ? (
              <button
                type="button"
                // onSubmit={() => handleSubmit()}
                onClick={handleSave}
                className="btn btn-success ml-2"
                // disabled={isSubmitting}
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                }}
              >
                <i className="fas fa-save"></i>
                Simpan
              </button>
            ) : null}

            {/* {loading ? (
                      <button
                        type="submit"
                        className="btn btn-success spinner spinner-white spinner-left ml-2"
                        onSubmit={() => handleSubmit()}
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                        }}
                        disabled={isSubmitting}
                      >
                        <span>Kirim</span>
                      </button>
                    ) : (
                      <button
                        type="submit"
                        onSubmit={() => handleSubmit()}
                        className="btn btn-success ml-2"
                        disabled={isSubmitting}
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                        }}
                      >
                        <i className="fas fa-check"></i>
                        Kirim
                      </button>
                    )} */}
          </div>
        </CardBody>
      </Card>
    </>
  );
}

export default PengumumanTargetUpload;
