import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import "../../../../helpers/DatePickerStyles.css";
import { Input, Textarea } from "../../../../helpers";
import axios from "axios";
import { useSelector } from "react-redux";
import swal from "sweetalert";

const { BACKEND_URL } = window.ENV;

function SubjectEditForm({ content, btnRef, saveForm }) {
  const [master, setMaster] = useState([]);
  const [valMaster, setValMaster] = useState();
  const { iamToken } = useSelector(state => state.auth);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    nama: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Nama is required"),
    nip: Yup.number().required("NIP is required"),
    unit: Yup.string().required("Unit is required"),
    jabatan: Yup.string().required("Jabatan is required")
  });

  useEffect(() => {
    // getMasterJenis().then(({ data }) => {
    //   data.map(data => {
    //     return setMaster(master => [
    //       ...master,
    //       {
    //         label: data.nama,
    //         value: data.id
    //       }
    //     ]);
    //   });
    // });
  }, []);
  const getNip = nip => {
    return axios.post(`${BACKEND_URL}/api/iam/getbynip`, nip, {
      headers: { Authorization: iamToken, "Content-Type": "application/json" }
    });
  };
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={values => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          const searchNip = () => {
            setFieldValue("nama", "Loading...");
            setFieldValue("unit", "Loading...");
            setFieldValue("jabatan", "Loading...");
            getNip(values.nip).then(({ data }) => {
              if (data.length > 0) {
                setFieldValue("nama", data[0].nama);
                setFieldValue("unit", data[0].jabatanPegawais[0].unit.nama);
                setFieldValue(
                  "jabatan",
                  data[0].jabatanPegawais[0].jabatan.nama
                );
              } else {
                swal("Gagal", "Data Tidak Tersedia", "error");
                setFieldValue("nama", "");
                setFieldValue("unit", "");
                setFieldValue("jabatan", "");
              }
            });
          };
          return (
            <>
              <Form className="form form-label-right">
                {/* Field NIP */}
                <div className="form-group row">
                  <Field
                    name="nip"
                    component={Input}
                    placeholder="NIP"
                    label="NIP"
                    maxLength={9}
                    withFeedbackLabel={false}
                  />
                  <button
                    type="button"
                    onClick={searchNip}
                    className="btn btn-primary"
                  >
                    <i className="fas fa-search"></i>
                    Cari
                  </button>
                </div>
                {/* Field Nama */}
                <div className="form-group row">
                  <Field
                    name="nama"
                    component={Input}
                    placeholder="Nama"
                    label="Nama"
                    disabled
                  />
                </div>
                {/* Field Unit */}
                <div className="form-group row">
                  <Field
                    name="unit"
                    component={Input}
                    placeholder="Unit"
                    label="Unit"
                    disabled
                  />
                </div>
                {/* Field Jabatan */}
                <div className="form-group row">
                  <Field
                    name="jabatan"
                    component={Input}
                    placeholder="Jabatan"
                    label="Jabatan"
                    disabled
                  />
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default SubjectEditForm;
