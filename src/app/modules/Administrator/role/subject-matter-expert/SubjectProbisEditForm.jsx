import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import "../../../../helpers/DatePickerStyles.css";
import { Input, Textarea } from "../../../../helpers";
import {
  getBisnisSektor,
  getCaseName,
  getCaseNameByIdProbis,
  getProbis,
  getProbisByStatus
} from "../../../../references/Api";

function SubjectProbisEditForm({ content, btnRef, saveForm }) {
  const [probis, setProbis] = useState([]);
  const [valProbis, setValProbis] = useState();
  const [casename, setCasename] = useState([]);
  const [valCasename, setValCasename] = useState();
  const [sektor, setSektor] = useState([]);
  const [valSektor, setValSektor] = useState();

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    probis: Yup.number().required("Probis is required"),
    casename: Yup.number().required("Casename is required"),
    // sektor: Yup.number().required("Sektor is required")
  });
  useEffect(() => {
    getProbisByStatus(1).then(({ data }) => {
      data.map(data => {
        return setProbis(probis => [
          ...probis,
          {
            label: data.nama,
            value: data.id_probis
          }
        ]);
      });
      // getBisnisSektor().then(({ data }) => {
      //   data.map(data => {
      //     return setSektor(sektor => [
      //       ...sektor,
      //       {
      //         label: data.nama,
      //         value: data.id_sektor
      //       }
      //     ]);
      //   });
      // });
      // })
    });
  }, []);
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={values => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          const handleChangeProbis = val => {
            setFieldValue("probis", val.value);
            setValProbis(val.label);
            setCasename([]);
            getCaseNameByIdProbis(val.value).then(({ data }) => {
              data.map(data => {
                return data.caseName.status === 1 ?
                 setCasename(casename => [
                  ...casename,
                  {
                    label: data.caseName.nama,
                    value: data.caseName.id_case_name
                  }
                ]) : null
              });
            });
          };
          const handleChangeCasename = val => {
            setFieldValue("casename", val.value);
            setValCasename(val.label);
          };
          // const handleChangeSektor = val => {
          //   setFieldValue("sektor", val.value);
          //   setValSektor(val.label);
          // };
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Bisnis Sektor */}
                {/* <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Bisnis Sektor
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={sektor}
                      onChange={value => handleChangeSektor(value)}
                      value={sektor.filter(data => data.label === valSektor)}
                    />
                  </div>
                </div> */}
                {/* Field Proses Bisnis */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Proses Bisnis
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={probis}
                      onChange={value => handleChangeProbis(value)}
                      value={probis.filter(data => data.label === valProbis)}
                    />
                  </div>
                </div>
                {/* Field Case Name */}
                {/* {casename ? ( */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Case Name
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={casename}
                      onChange={value => handleChangeCasename(value)}
                      value={casename.filter(
                        data => data.label === valCasename
                      )}
                    />
                  </div>
                </div>
                {/* ) : null} */}

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default SubjectProbisEditForm;
