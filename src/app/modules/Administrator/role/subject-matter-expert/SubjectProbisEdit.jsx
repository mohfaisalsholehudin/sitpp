/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { ActionsColumnFormatterAdminRoleSubjectMatterExpert } from "../../../../helpers/column-formatters";
import { getSmeByIdSme, saveSmeDetil } from "../../../../references/Api";
import SubjectProbisEditForm from "./SubjectProbisEditForm";
import KnowledgeOwnerProbisEditForm from "./SubjectProbisEditForm";

function SubjectProbisEdit({
  history,
  match: {
    params: { id, id_sme_detil }
  }
}) {
  const initValues = {
    probis: "",
    sektor: "",
    casename: "",
  };

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();

  useEffect(() => {
    let _title = id_sme_detil
      ? "Edit Mapping"
      : "Tambah Mapping";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id_sme_detil) {
        // getSmeByIdSme(id).then(({ data })=> {
        //   setContent(data)
        // })
    }
  }, [id_sme_detil, suhbeader]);
  const btnRef = useRef();
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  const backAction = () => {
    history.push(`/admin/role/subject-matter-expert/${id}/edit`);
  };
  const saveForm = values => {
    if (!id_sme_detil) {
      saveSmeDetil(values.casename, id).then(
        ({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/admin/role/subject-matter-expert/${id}/edit`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/admin/role/subject-matter-expert/${id}/edit`);
            });
          }
        }
      );
    } else {
      //   updateDetilJenis(id, values.id_jenis, values.nama, values.keterangan).then(
      //     ({ status }) => {
      //       if (status === 201 || status === 200) {
      //         swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
      //           history.push("/admin/detil-jenis");
      //         });
      //       } else {
      //         swal("Gagal", "Data gagal disimpan", "error").then(() => {
      //           history.push("/admin/detil-jenis/add");
      //         });
      //       }
      //     }
      //   );
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <SubjectProbisEditForm
              content={content || initValues}
              btnRef={btnRef}
              saveForm={saveForm}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <DetilJenisEditFooter backAction={backAction} btnRef={btnRef} /> */}
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
            // disabled={disabled}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
        </div>
      </CardFooter>
    </Card>
  );
}

export default SubjectProbisEdit;
