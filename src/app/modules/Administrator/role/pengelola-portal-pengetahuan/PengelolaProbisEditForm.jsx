import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import "../../../../helpers/DatePickerStyles.css";
import { Input, Textarea } from "../../../../helpers";

function PengelolaProbisEditForm({ content, btnRef, saveForm }) {
  const [master, setMaster] = useState([]);
  const [valMaster, setValMaster] = useState();

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    nama: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Nama is required"),
    nip: Yup.string().required("NIP is required"),
    unit: Yup.number().required("Unit is required"),
    jabatan: Yup.number().required("Jabatan is required")
  });

  useEffect(() => {
    // getMasterJenis().then(({ data }) => {
    //   data.map(data => {
    //     return setMaster(master => [
    //       ...master,
    //       {
    //         label: data.nama,
    //         value: data.id
    //       }
    //     ]);
    //   });
    // });
  }, []);
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={values => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          const handleChangeMaster = val => {
            setFieldValue("id_jenis", val.value);
            setValMaster(val.label);
          };
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Proses Bisnis */}
                <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                    Proses Bisnis
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={master}
                      onChange={value => handleChangeMaster(value)}
                      value={master.filter(data => data.label === valMaster)}
                    />
                  </div>
                </div>
                 {/* Field Case Name */}
                 <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                    Case Name
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={master}
                      onChange={value => handleChangeMaster(value)}
                      value={master.filter(data => data.label === valMaster)}
                    />
                  </div>
                </div>
                 {/* Field Bisnis Sektor */}
                 <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                    Bisnis Sektor
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={master}
                      onChange={value => handleChangeMaster(value)}
                      value={master.filter(data => data.label === valMaster)}
                    />
                  </div>
                </div>
                

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default PengelolaProbisEditForm;
