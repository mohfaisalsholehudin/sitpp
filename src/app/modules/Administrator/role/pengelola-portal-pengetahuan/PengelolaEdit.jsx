/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { getPengPortalById, savePengPortal, updatePengPortal } from "../../../../references/Api";
import PengelolaEditForm from "./PengelolaEditForm";

function PengelolaEdit({
  history,
  match: {
    params: { id }
  }
}) {
  const initValues = {
    eselon2: "",
    eselon3: "",
    eselon4: "",
  };

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();

  useEffect(() => {
    let _title = id
      ? "Edit Role Pengelola Portal KMS"
      : "Tambah Role Pengelola Portal KMS";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
      getPengPortalById(id).then(({ data }) => {
        setContent({
          nm_unit_es2: data.nm_unit_es2,
          nm_unit_es3: data.nm_unit_es3,
          nm_unit_es4: data.nm_unit_es4,
          kd_unit_es2: data.kd_unit_es2,
          kd_unit_es3: data.kd_unit_es3,
          kd_unit_es4: data.kd_unit_es4
        });
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  const backAction = () => {
    history.push("/admin/role/pengelola-portal-pengetahuan");
  };
  const saveForm = values => {
    if (!id) {
      savePengPortal(
        values.nm_unit_es2,
        values.nm_unit_es3,
        values.nm_unit_es4,
        values.kd_unit_es2,
        values.kd_unit_es3,
        values.kd_unit_es4
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/admin/role/pengelola-portal-pengetahuan");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/admin/role/pengelola-portal-pengetahuan/add");
          });
        }
      });
    } else {
      updatePengPortal(
        id,
        values.nm_unit_es2,
        values.nm_unit_es3,
        values.nm_unit_es4,
        values.kd_unit_es2,
        values.kd_unit_es3,
        values.kd_unit_es4
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/admin/role/pengelola-portal-pengetahuan");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/admin/role/pengelola-portal-pengetahuan/add");
          });
        }
      });
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <PengelolaEditForm
              content={content || initValues}
              btnRef={btnRef}
              saveForm={saveForm}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <DetilJenisEditFooter backAction={backAction} btnRef={btnRef} /> */}
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
            // disabled={disabled}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
        </div>
      </CardFooter>
    </Card>
  );
}

export default PengelolaEdit;
