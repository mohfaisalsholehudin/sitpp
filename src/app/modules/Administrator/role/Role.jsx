import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import RoleTable from "./RoleTable";

function Role() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Role Knowledge Management"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <RoleTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Role;
