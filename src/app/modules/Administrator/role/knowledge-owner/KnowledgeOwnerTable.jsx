/* Library */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../../../helpers/column-formatters";
import { getKowner } from "../../../../references/Api";

/* Component */
// import ProposalOpen from "./ProposalOpen";
// import ProposalReject from "./ProposalReject";

/* Utility */

function KnowledgeOwnerTable() {
  const history = useHistory();
  const [content, setContent] = useState([]);
// const content = [
//     {id: 1, jabatan: "Kepala Seksi", nama_unit: "Seksi A", unit: "Direktorat TIK", tgl_input:"11/01/2022"},
//     {id: 2, jabatan: "Kepala Seksi", nama_unit: "Seksi B", unit: "Direktorat TPB", tgl_input:"23/02/2022"},
//     {id: 3, jabatan: "Kasubdit", nama_unit: "Seksi C", unit: "Direktorat P2Humas", tgl_input:"13/01/2022"},
// ]

  const add = () => history.push("/admin/role/knowledge-owner/add");
  //   const edit = (id) => history.push(`/admin/detil-jenis/${id}/edit`);
  const edit = id => history.push(`/admin/role/knowledge-owner/${id}/edit`);
  const deleteAction = id => {
    //     swal({
    //       title: "Apakah Anda Yakin?",
    //       text: "Klik OK untuk melanjutkan",
    //       icon: "warning",
    //       buttons: true,
    //       dangerMode: true
    //     }).then(willDelete => {
    //       if (willDelete) {
    //         deleteDetilJenis(id).then(({ status }) => {
    //           if (status === 200) {
    //             swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
    //               history.push("/dashboard");
    //               history.replace("/admin/detil-jenis");
    //             });
    //           } else {
    //             swal("Gagal", "Data gagal disimpan", "error").then(() => {
    //               history.push("/dashboard");
    //               history.replace("/admin/detil-jenis");
    //             });
    //           }
    //         });
    //       }
    //       // else {
    //       //   swal("Your imaginary file is safe!");
    //       // }
    //     });
  };

    useEffect(()=> {
      getKowner().then(({ data }) => {
        setContent(data)
      })
    },[])

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    // {
    //   dataField: "nm_jabatan",
    //   text: "Jabatan",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses
    // },
    {
      dataField: "nm_es4",
      text: "Nama Unit",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nm_es2",
      text: "Unit",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    // {
    //   dataField: "nm_jabatan",
    //   text: "Jabatan",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses
    // },
    {
      dataField: "created",
      text: "Tgl Input",
      formatter: columnFormatters.DateFormatterKowner,
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterAdminRoleKnowledgeOwner,
      formatExtraData: {
        openEditDialog: edit,
        openDeleteDialog: deleteAction
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_ko",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "id_ko", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_ko"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right"
                            }}
                            onClick={add}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default KnowledgeOwnerTable;
