/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import {
  getKownerById,
  saveKowner,
  updateKowner
} from "../../../../references/Api";
import KnowledgeOwnerEditForm from "./KnowledgeOwnerEditForm";
import KnowledgeOwnerProbisTable from "./KnowledgeOwnerProbisTable";

function KnowledgeOwnerEdit({
  history,
  match: {
    params: { id }
  }
}) {
  const initValues = {
    nm_es2: "",
    nm_es3: "",
    nm_es4: "",
    kd_unit_es2: "",
    kd_unit_es3: "",
    kd_unit_es4: ""
  };

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();

  useEffect(() => {
    let _title = id
      ? "Edit Anggota Knowledge Owner"
      : "Tambah Anggota Knowledge Owner";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
      getKownerById(id).then(({ data }) => {
        setContent({
          nm_es2: data.nm_es2,
          nm_es3: data.nm_es3,
          nm_es4: data.nm_es4,
          kd_unit_es2: data.kd_unit_es2,
          kd_unit_es3: data.kd_unit_es3,
          kd_unit_es4: data.kd_unit_es4
        });
      });
    }
  }, [id, suhbeader]);

  const btnRef = useRef();
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  const backAction = () => {
    history.push("/admin/role/knowledge-owner");
  };
  const saveForm = values => {
    if (!id) {
      saveKowner(
        values.nm_es2,
        values.nm_es3,
        values.nm_es4,
        values.kd_unit_es2,
        values.kd_unit_es3,
        values.kd_unit_es4,
        // values.kd_jabatan,
        // values.nm_jabatan
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/admin/role/knowledge-owner");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/admin/role/knowledge-owner/add");
          });
        }
      });
    } else {
      updateKowner(
        id,
        values.nm_es2,
        values.nm_es3,
        values.nm_es4,
        values.kd_unit_es2,
        values.kd_unit_es3,
        values.kd_unit_es4,
        // values.kd_jabatan,
        // values.nm_jabatan
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/admin/role/knowledge-owner");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/admin/role/knowledge-owner/add");
          });
        }
      });
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <KnowledgeOwnerEditForm
              content={content || initValues}
              btnRef={btnRef}
              saveForm={saveForm}
            />
          </div>
          {/* {id_lhr ? <DetilLhrTablePokok id={id} id_lhr={id_lhr} /> : null} */}
          {id ? <KnowledgeOwnerProbisTable id={id} /> : null}
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <DetilJenisEditFooter backAction={backAction} btnRef={btnRef} /> */}
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
            // disabled={disabled}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
        </div>
      </CardFooter>
    </Card>
  );
}

export default KnowledgeOwnerEdit;
