import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import { Route, useHistory } from "react-router-dom";

//Helpers
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../_metronic/_helpers";
import { Pagination } from "../../../../helpers/pagination/Pagination";
import * as columnFormatters from "../../../../helpers/column-formatters";
import PenjaminProbisTableOpen from "./PenjaminProbisTableOpen";
import { deletePkpDetil, getPkpDetilByIdPkp } from "../../../../references/Api";

function PenjaminProbisTable({ id_pkp }) {
  const history = useHistory();
  const [content, setContent] = useState([]);

  useEffect(() => {
    getPkpDetilByIdPkp(id_pkp).then(({ data }) => {
      setContent(data);
    });
  }, [id_pkp]);

  const add = () => {
    history.push(
      `/admin/role/penjamin-kualitas-pengetahuan/${id_pkp}/edit/probis/add`
    );
  };
  const edit = id_lhr_detil => {
    // history.push(`/compose/proposal/process/detils/${id}/lhr/${id_lhr}/edit/${id_lhr_detil}/pokok`);
  };
  const deleteAction = id => {
    swal({
      title: "Apakah Anda Yakin?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        deletePkpDetil(id).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace(`/admin/role/penjamin-kualitas-pengetahuan/${id_pkp}/edit`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace(`/admin/role/penjamin-kualitas-pengetahuan/${id_pkp}/edit`);
            });
          }
        });
      }
      // else {
      //   swal("Your imaginary file is safe!");
      // }
    });
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "probis.nama",
      text: "Nama Probis",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterAdminRolePenjaminKualitasPengetahuanProbis,
      formatExtraData: {
        openEditDialog: edit,
        openDeleteDialog: deleteAction,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const emptyDataMessage = () => {
    return <div className="text-center">No Data to Display</div>;
  };
  const { SearchBar } = Search;
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "probis.nama",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "probis.nama", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="pkpDetil.id_pkp_detil"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-12 col-xl-12 mb-3">
                          <h4> Daftar Mapping</h4>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div
                          className="col-lg-6 col-xl-6 mb-3"
                          style={{ textAlign: "right" }}
                        >
                          <button
                            type="button"
                            className="btn btn-primary ml-3"
                            style={{
                              float: "right"
                            }}
                            onClick={add}
                          >
                            <i className="fa fa-plus"></i>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      {/*Route to Open Details*/}
      <Route path="/admin/role/penjamin-kualitas-pengetahuan/:id_pkp/edit/probis/add">
        {({ history, match }) => (
          <PenjaminProbisTableOpen
            show={match != null}
            id={match && match.params.id}
            id_pkp={id_pkp}
            after={false}
            onHide={() => {
              history.push(
                `/admin/role/penjamin-kualitas-pengetahuan/${id_pkp}/edit`
              );
            }}
            onRef={() => {
              history.push(
                `/admin/role/penjamin-kualitas-pengetahuan/${id_pkp}/edit`
              );
            }}
          />
        )}
      </Route>
    </>
  );
}

export default PenjaminProbisTable;
