import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Textarea, Select as Sel } from "../../../../helpers";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { getProbis, savePkpDetil } from "../../../../references/Api";
import Select from "react-select";

function PenjaminProbisTableOpen({
  id,
  show,
  onHide,
  after,
  id_pkp,
  id_lhr
}) {
  const history = useHistory();
  const [content, setContent] = useState();
  const [probis, setProbis] = useState([]);
  const [valProbis, setValProbis] = useState();

  useEffect(() => {
    getProbis().then(({ data }) => {
      data.map(data => {
        return setProbis(probis => [
          ...probis,
          {
            label: data.nama,
            value: data.id_probis
          }
        ]);
      });
    });
  }, []);

  const initialValues = {
    probis: "",
  };

  const validationSchema = Yup.object().shape({
    probis: Yup.number().required("Probis is required")
  });

  const savePengaturan = values => {
    if (!id) {
        savePkpDetil(id_pkp, values.probis,).then(
          ({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push('/dashboard');
                history.replace(`/admin/role/penjamin-kualitas-pengetahuan/${id_pkp}/edit`)
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push('/dashboard');
                history.replace(`/admin/role/penjamin-kualitas-pengetahuan/${id_pkp}/edit`)
              });
            }
          }
        );
    } else {
        // updatePkp(id, values.nip, values.nama, values.unit, values.jabatan).then(
        //   ({ status }) => {
        //     if (status === 201 || status === 200) {
        //       swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
        //         history.push("/admin/role/penjamin-kualitas-pengetahuan");
        //       });
        //     } else {
        //       swal("Gagal", "Data gagal disimpan", "error").then(() => {
        //         history.push("/admin/role/penjamin-kualitas-pengetahuan/add");
        //       });
        //     }
        //   }
        // );
    }
  };

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Tambah Probis
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <Formik
            enableReinitialize={true}
            initialValues={content || initialValues}
            validationSchema={validationSchema}
            onSubmit={values => {
              // console.log(values);
              savePengaturan(values);
            }}
          >
            {({ handleSubmit, setFieldValue }) => {
              const handleChangeProbis = val => {
                setFieldValue("probis", val.value);
                setValProbis(val.label);
              };
              return (
                <Form className="form form-label-right">
                  <div
                    className="form-group row"
                    style={{ marginBottom: "0px" }}
                  >
                  </div>
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Proses Bisnis
                    </label>
                    <div className="col-lg-12 col-xl-12">
                      <Select
                        options={probis}
                        onChange={value => handleChangeProbis(value)}
                        value={probis.filter(data => data.label === valProbis)}
                      />
                    </div>
                  </div>
                  <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Batal
                    </button>
                    {`  `}
                    <button
                      type="submit"
                      onSubmit={() => handleSubmit()}
                      className="btn btn-success ml-2"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="fas fa-check"></i>
                      Kirim
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </Modal.Body>
    </Modal>
  );
}

export default PenjaminProbisTableOpen;
