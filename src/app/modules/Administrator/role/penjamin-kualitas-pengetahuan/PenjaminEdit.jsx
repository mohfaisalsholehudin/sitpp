/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { getPkpById, savePkp, updatePkp } from "../../../../references/Api";
import PenjaminEditForm from "./PenjaminEditForm";
import PenjaminProbisTable from "./PenjaminProbisTable";

function PenjaminEdit({
  history,
  match: {
    params: { id }
  }
}) {
  const initValues = {
    nama: "",
    nip: "",
    unit: "",
    jabatan: ""
  };

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();

  useEffect(() => {
    let _title = id
      ? "Edit Anggota Penjamin Kualitas Pengetahuan"
      : "Tambah Anggota Penjamin Kualitas Pengetahuan";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
        getPkpById(id).then(({ data })=> {
          setContent({
            nip: data.nip,
            nama: data.nama,
            unit: data.unit, 
            jabatan: data.seksi
          })
        })
    }
  }, [id, suhbeader]);
  const btnRef = useRef();
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  const backAction = () => {
    history.push("/admin/role/penjamin-kualitas-pengetahuan");
  };
  const saveForm = values => {
    if (!id) {
      // console.log(values)
        savePkp(values.nip, values.nama, values.unit, values.jabatan).then(
          ({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push("/admin/role/penjamin-kualitas-pengetahuan");
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push("/admin/role/penjamin-kualitas-pengetahuan/add");
              });
            }
          }
        );
    } else {
        updatePkp(id, values.nip, values.nama, values.unit, values.jabatan).then(
          ({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push("/admin/role/penjamin-kualitas-pengetahuan");
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push("/admin/role/penjamin-kualitas-pengetahuan/add");
              });
            }
          }
        );
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <PenjaminEditForm
              content={content || initValues}
              btnRef={btnRef}
              saveForm={saveForm}
            />
          </div>
          {/* {id_lhr ? <DetilLhrTablePokok id={id} id_lhr={id_lhr} /> : null} */}
          {id ? <PenjaminProbisTable id_pkp={id} /> : null}
          {/* <PenjaminProbisTable /> */}
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <DetilJenisEditFooter backAction={backAction} btnRef={btnRef} /> */}
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
            // disabled={disabled}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
        </div>
      </CardFooter>
    </Card>
  );
}

export default PenjaminEdit;
