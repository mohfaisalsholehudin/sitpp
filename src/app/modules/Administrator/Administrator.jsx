import React,  { Suspense }  from "react";
import {Redirect, Switch } from "react-router-dom";
import {LayoutSplashScreen, ContentRoute } from "../../../_metronic/layout";
import DetilJenisEdit from "./detil-jenis/add-edit/DetilJenisEdit";
import DetilJenis from "./detil-jenis/DetilJenis";
import JenisPajakEdit from "./jenis-pajak/add-edit/JenisPajakEdit";
import JenisPajak from "./jenis-pajak/JenisPajak";
import JenisPeraturanEdit from "./jenis-peraturan/add-edit/JenisPeraturanEdit";
import JenisPeraturan from "./jenis-peraturan/JenisPeraturan";
import MasterJenisEdit from "./master-jenis/add-edit/MasterJenisEdit";
import MasterJenis from "./master-jenis/MasterJenis";
import Role from "./role/Role";
import Penjamin from "./role/penjamin-kualitas-pengetahuan/Penjamin"
import PenjaminEdit from "./role/penjamin-kualitas-pengetahuan/PenjaminEdit";
import KnowledgeOwner from "./role/knowledge-owner/KnowledgeOwner";
import KnowledgeOwnerEdit from "./role/knowledge-owner/KnowledgeOwnerEdit";
import KnowledgeOwnerProbisEdit from "./role/knowledge-owner/KnowledgeOwnerProbisEdit";
import Subject from "./role/subject-matter-expert/Subject";
import SubjectEdit from "./role/subject-matter-expert/SubjectEdit";
import SubjectProbisEdit from "./role/subject-matter-expert/SubjectProbisEdit";
import Pengelola from "./role/pengelola-portal-pengetahuan/Pengelola";
import PengelolaEdit from "./role/pengelola-portal-pengetahuan/PengelolaEdit";
import PengelolaProbisEdit from "./role/pengelola-portal-pengetahuan/PengelolaProbisEdit";
import Probis from "./setting/probis/Probis";
import ProbisEdit from "./setting/probis/ProbisEdit";
import CaseName from "./setting/probis/case-name/CaseName";
import CaseNameEdit from "./setting/probis/case-name/CaseNameEdit";
import Subcase from "./setting/probis/sub-case/Subcase";
import SubcaseEdit from "./setting/probis/sub-case/SubcaseEdit";
import SektorBisnis from "./setting/sektor-bisnis/SektorBisnis";
import SektorBisnisEdit from "./setting/sektor-bisnis/SektorBisnisEdit";
import TipeKnowledge from "./setting/tipe-knowledge/TipeKnowledge";
import TipeKnowledgeEdit from "./setting/tipe-knowledge/TipeKnowledgeEdit";
import Tusi from "./setting/tugas-fungsi/Tusi";
import TusiEdit from "./setting/tugas-fungsi/TusiEdit";
import Pengumuman from "./pengumuman/Pengumuman";
import PengumumanEdit from "./pengumuman/add-edit/PengumumanEdit";
import PengumumanTarget from "./pengumuman/target/PengumumanTarget";
import PengumumanTargetEdit from "./pengumuman/target/PengumumanTargetEdit";
import PengumumanTargetUpload from "./pengumuman/target/PengumumanTargetUpload";


export default function Administrator() {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
    <Switch>
         {
          /* Redirect from eCommerce root URL to /Jenis Pajak */
          <Redirect
            exact={true}
            from="/admin"
            to="/admin/jenis-pajak"
          />
        }

      <ContentRoute path="/admin/jenis-pajak/:id/edit" component={JenisPajakEdit} />
      <ContentRoute path="/admin/jenis-pajak/add" component={JenisPajakEdit} />

     
      <ContentRoute path="/admin/jenis-pajak" component={JenisPajak} />
      <ContentRoute path="/admin/peraturan-jenis/:id/edit" component={JenisPeraturanEdit} />
      <ContentRoute path="/admin/peraturan-jenis/add" component={JenisPeraturanEdit} />
      <ContentRoute path="/admin/peraturan-jenis" component={JenisPeraturan} />

      <ContentRoute
        path="/admin/master-jenis/add"
        component={MasterJenisEdit}
      />
      <ContentRoute path="/admin/master-jenis" component={MasterJenis} />


      <ContentRoute path="/admin/detil-jenis/:id/edit" component={DetilJenisEdit} />
      <ContentRoute path="/admin/detil-jenis/add" component={DetilJenisEdit} />
      <ContentRoute path="/admin/detil-jenis" component={DetilJenis} />



      {/* Begin Penambahan Role */}
        {/* Begin Penambahan Role - Pengelola Portal Pengetahuan*/}
        <ContentRoute path="/admin/role/pengelola-portal-pengetahuan/probis/add" component={PengelolaProbisEdit} />
        <ContentRoute path="/admin/role/pengelola-portal-pengetahuan/:id/edit" component={PengelolaEdit} />
        <ContentRoute path="/admin/role/pengelola-portal-pengetahuan/add" component={PengelolaEdit} />
        <ContentRoute path="/admin/role/pengelola-portal-pengetahuan" component={Pengelola} />
        {/* End of Penambahan Role - Pengelola Portal Pengetahuan*/}

        {/* Begin Penambahan Role - Subject Matter Expert*/}
        <ContentRoute path="/admin/role/subject-matter-expert/probis/:id/add" component={SubjectProbisEdit} />
        <ContentRoute path="/admin/role/subject-matter-expert/:id/edit" component={SubjectEdit} />
        <ContentRoute path="/admin/role/subject-matter-expert/add" component={SubjectEdit} />
        <ContentRoute path="/admin/role/subject-matter-expert" component={Subject} />
        {/* End of Penambahan Role - Subject Matter Expert*/}

        {/* Begin Penambahan Role - Knowledge Owner*/}
        <ContentRoute path="/admin/role/knowledge-owner/probis/:id/add" component={KnowledgeOwnerProbisEdit} />
        <ContentRoute path="/admin/role/knowledge-owner/:id/edit" component={KnowledgeOwnerEdit} />
        <ContentRoute path="/admin/role/knowledge-owner/add" component={KnowledgeOwnerEdit} />
        <ContentRoute path="/admin/role/knowledge-owner" component={KnowledgeOwner} />
        {/* End of Penambahan Role - Knowledge Owner*/}

        {/* Begin Penambahan Role - Penjamin Kualitas Pengetahuan*/}
        <ContentRoute path="/admin/role/penjamin-kualitas-pengetahuan/:id/edit" component={PenjaminEdit} />
        <ContentRoute path="/admin/role/penjamin-kualitas-pengetahuan/add" component={PenjaminEdit} />
        <ContentRoute path="/admin/role/penjamin-kualitas-pengetahuan" component={Penjamin} />
        {/* End of Penambahan Role - Penjamin Kualitas Pengetahuan*/}
      <ContentRoute path="/admin/role" component={Role} />
      {/* End of Penambahan Role */}

      {/* Begin Setting Knowledge */}
        {/* Begin Setting Knowledge - Bisnis Proses*/}
        <ContentRoute path="/admin/setting/probis/:id_probis/case-name/:id_casename/subcase/:id_subcase/edit" component={SubcaseEdit} />
        <ContentRoute path="/admin/setting/probis/:id_probis/case-name/:id_casename/subcase/add" component={SubcaseEdit} />
        <ContentRoute path="/admin/setting/probis/:id_probis/case-name/:id_casename/subcase" component={Subcase} />
        <ContentRoute path="/admin/setting/probis/:id_probis/case-name/:id_casename/edit" component={CaseNameEdit} />
        <ContentRoute path="/admin/setting/probis/:id_probis/case-name/add" component={CaseNameEdit} />
        <ContentRoute path="/admin/setting/probis/:id_probis/case-name" component={CaseName} />
        <ContentRoute path="/admin/setting/probis/:id_probis/edit" component={ProbisEdit} />
        <ContentRoute path="/admin/setting/probis/add" component={ProbisEdit} />
        <ContentRoute path="/admin/setting/probis" component={Probis} />
        {/* End of Setting Knowledge - Bisnis Proses*/}

        {/* Begin Setting Knowledge - Bisnis Sektor*/}
        <ContentRoute path="/admin/setting/sektor/:id_sektor/edit" component={SektorBisnisEdit} />
        <ContentRoute path="/admin/setting/sektor/add" component={SektorBisnisEdit} />
        <ContentRoute path="/admin/setting/sektor" component={SektorBisnis} />
        {/* End of Setting Knowledge - Bisnis Sektor*/}

         {/* Begin Setting Knowledge - Tipe Knowledge*/}
         <ContentRoute path="/admin/setting/tipe-knowledge/:id/edit" component={TipeKnowledgeEdit} />
         <ContentRoute path="/admin/setting/tipe-knowledge/add" component={TipeKnowledgeEdit} />
         <ContentRoute path="/admin/setting/tipe-knowledge" component={TipeKnowledge} />
         {/* End of Setting Knowledge - Tipe Knowledge*/}

         {/* Begin Setting Knowledge - Tugas dan Fungsi*/}
         <ContentRoute path="/admin/setting/tusi/:id/edit" component={TusiEdit} />
         <ContentRoute path="/admin/setting/tusi/add" component={TusiEdit} />
         <ContentRoute path="/admin/setting/tusi" component={Tusi} />
         {/* End of Setting Knowledge - Tugas dan Fungsi*/}

         {/* Begin Setting Knowledge - Pengumuman*/}
         <ContentRoute path="/admin/pengumuman/:id/target/upload" component={PengumumanTargetUpload} />
         <ContentRoute path="/admin/pengumuman/:id/target/add" component={PengumumanTargetEdit} />
         <ContentRoute path="/admin/pengumuman/:id/target" component={PengumumanTarget} />
         <ContentRoute path="/admin/pengumuman/:id/publish" component={PengumumanEdit} />
         <ContentRoute path="/admin/pengumuman/:id/edit" component={PengumumanEdit} />
         <ContentRoute path="/admin/pengumuman/add" component={PengumumanEdit} />
         <ContentRoute path="/admin/pengumuman" component={Pengumuman} />
         {/* End of Setting Knowledge - Pengumuman*/}



      {/* End of Setting Knowledge */}



    </Switch>
  </Suspense>

  );
}
