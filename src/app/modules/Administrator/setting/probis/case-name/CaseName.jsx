import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import { getCaseNameById, getProbisById } from "../../../../../references/Api";
import CaseNameTable from "./CaseNameTable";

function CaseName({
  history,
  match: {
    params: { id_probis }
  }
}) {
  const [detil, setDetil] = useState([]);

  useEffect(() => {
    getProbisById(id_probis).then(({ data }) => {
      setDetil(data);
    });
  }, [id_probis]);

  const handleBack = () => {
    history.push(`/admin/setting/probis`)
  }
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Case Name"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <>
            <div className="row">
              <div className="col-xl-12 col-lg-12 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Bisnis Proses
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${detil.nama}`}
                      {/* : Business Process 1 */}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-12 col-lg-12 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Keterangan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {/* {`: ${DateFormat(detil.tgl_penyusunan)}`} */}
                      {`: ${detil.keterangan}`}
                      {/* : Keterangan Business Process 1 */}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </>
          <CaseNameTable id_probis={id_probis} />
        </CardBody>
        <CardFooter style={{ borderTop: "none" }}>
          <>
          <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={handleBack}
            className="btn btn-light-success"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          
        </div>
          </>
      </CardFooter>
      </Card>
    </>
  );
}

export default CaseName;
