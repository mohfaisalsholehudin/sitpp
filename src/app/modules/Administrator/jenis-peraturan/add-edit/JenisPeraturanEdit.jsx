/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";

/* Utility */
import {
  getJenisPeraturanById,
  saveJenisPeraturan,
  updateJenisPeraturan
} from "../../../Evaluation/Api";
import JenisPeraturanEditForm from "./JenisPeraturanEditForm";
import JenisPeraturanEditFooter from "./JenisPeraturanEditFooter";

function JenisPeraturanEdit({
  history,
  match: {
    params: { id }
  }
}) {
  const initValues = {
    nm_jnsperaturan: "",
    status: "",
    aplikasi:""
  };

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();

  useEffect(() => {
    let _title = id ? "Edit Jenis Peraturan" : "Tambah Jenis Peraturan";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
      getJenisPeraturanById(id).then(({ data }) => {
        setContent(data);
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();

  const saveForm = values => {
    if (!id) {
      saveJenisPeraturan(
        values.nm_jnsperaturan,
        values.status, 
        values.aplikasi
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/admin/peraturan-jenis");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/admin/peraturan-jenis/add");
          });
        }
      });
    } else {
      updateJenisPeraturan(
        id,
        values.nm_jnsperaturan,
        values.status, 
        values.aplikasi
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/admin/peraturan-jenis");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/admin/peraturan-jenis/add");
          });
        }
      });
    }
  };
  const backAction = () => {
    history.push("/admin/peraturan-jenis");
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <JenisPeraturanEditForm
            actionsLoading={actionsLoading}
            proposal={content || initValues}
            btnRef={btnRef}
            saveForm={saveForm}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <JenisPeraturanEditFooter backAction={backAction} btnRef={btnRef} />
      </CardFooter>
    </Card>
  );
}

export default JenisPeraturanEdit;
