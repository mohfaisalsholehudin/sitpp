import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import JenisPajakTable from "./JenisPajakTable";

function JenisPajak() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Jenis Pajak"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <JenisPajakTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default JenisPajak;
