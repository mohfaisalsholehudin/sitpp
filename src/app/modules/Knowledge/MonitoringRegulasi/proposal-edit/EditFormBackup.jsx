import React, { forwardRef, useEffect } from "react";
import { useFormik, useField, Field} from "formik";
import * as Yup from "yup";
import { Input, Select } from "../../../../_metronic/_partials/controls";
import DatePicker from "react-datepicker";
import "./styles.css";
// import CustomFileInput from "./CustomFileInput";

function ProposalEditForm({ proposal, btnRef, saveProposal }) {
  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    noSurat: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No Surat is required"),
    tglSurat: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),
    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Perihal is required"),
    jenisPajak: Yup.string().required("Jenis Pajak is required"),
    jenisKantor: Yup.string().required("Jenis Kantor is required"),
    instansi: Yup.string().required("Instansi is required"),
    alamat: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Alamat is required"),
    noTelp: Yup.number().required("No Telp PIC is required"),
    namaPic: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Nama PIC is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileFormat",
        "Unsupported Format",
        (value) => value && SUPPORTED_FORMATS.includes(value.type)
      ),
  });

  // const FILE_SIZE = 2000000000;
  const SUPPORTED_FORMATS = [
    "application/pdf"
  ];

  const getInputClasses = (fieldname) => {
    if (formik.touched[fieldname] && formik.errors[fieldname]) {
      return "is-invalid";
    }

    if (formik.touched[fieldname] && !formik.errors[fieldname]) {
      return "is-valid";
    }

    return "";
  };
  const formik = useFormik({
    initialValues: proposal,
    validationSchema: ProposalEditSchema,
    onSubmit: (values, { setStatus, setSubmitting }) => {
      saveProposal(values, setStatus, setSubmitting);
    },
    onReset: (values, { resetForm }) => {
      resetForm();
    },
    onChange: (values)=> {
      console.log(values);
    }
  });

  const CustomDate = forwardRef(({ value, onClick, onChange }, ref) => (
    <div>
      <div className="input-group input-group-lg ">
        <div className="input-group-prepend">
          <span className="input-group-text">
            <i className="far fa-calendar-alt"></i>
          </span>
        </div>
        <input
          type="text"
          placeholder="Pilih Tanggal"
          className={`form-control form-control-lg  ${getInputClasses(
            "tglSurat"
          )}`}
          name="tglSurat"
          {...formik.getFieldProps("tglSurat")}
          onClick={onClick}
          onChange={onChange}
          ref={ref}
          value={value}
        />
      </div>
      {formik.touched.tglSurat && formik.errors.tglSurat ? (
        <div className="invalid-feedback" style={{ display: "block" }}>
          {formik.errors.tglSurat}
        </div>
      ) : null}
    </div>
  ));

  const checkOfficeType = (val) => {
    switch (val) {
      case "1":
        return (
          <div className="form-group row">
            <label className="col-xl-3 col-lg-3 col-form-label">Instansi</label>
            <div className="col-lg-9 col-xl-6">
              <div>
                <input
                  type="text"
                  className={`form-control form-control-lg  ${getInputClasses(
                    "instansi"
                  )}`}
                  name="instansi"
                  placeholder="Instansi"
                  {...formik.getFieldProps("instansi")}
                />
                {formik.touched.instansi && formik.errors.instansi ? (
                  <div className="invalid-feedback">
                    {formik.errors.instansi}
                  </div>
                ) : null}
              </div>
            </div>
          </div>
        );
        break;
      case "2":
        return (
          <div className="form-group row">
            <label className="col-xl-3 col-lg-3 col-form-label">
              Unit Kerja
            </label>
            <div className="col-lg-9 col-xl-6">
              <select
                className="form-control form-control-lg "
                name="instansi"
                {...formik.getFieldProps("instansi")}
              >
                <option> Pilih Unit Kerja</option>
                <option value="1">KPP Pratama A</option>
                <option value="2">Kanwil B</option>
              </select>
            </div>
          </div>
        );
        break;
      default:
        return "";
        break;
    }
  };

 
    const handleChangeFile = (e) => {
      // e.preventDefault();
      console.log(e)
      console.log('masuk')
      // let reader = new FileReader();
      // let file = e.target.files[0];
      // if (file) {
      //   console.log(file)
      //   // reader.onloadend = () => {
      //   //   this.setState({
      //   //     file: file,
      //   //     imagePreviewUrl: reader.result
      //   //   });
      //   // };
      //   // reader.readAsDataURL(file);
      //   // this.props.setFieldValue(this.props.field.name, file);
      // }
    }

  // console.log(formik.getFieldProps('file'))
  console.log(formik)

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className="form">
        <div className="form-group row">
          <label className="col-xl-3 col-lg-3 col-form-label">No Surat</label>
          <div className="col-lg-9 col-xl-6">
            <div>
              <input
                type="text"
                className={`form-control form-control-lg  ${getInputClasses(
                  "noSurat"
                )}`}
                name="noSurat"
                placeholder="No Surat"
                {...formik.getFieldProps("noSurat")}
              />
              {formik.touched.noSurat && formik.errors.noSurat ? (
                <div className="invalid-feedback">{formik.errors.noSurat}</div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="form-group row">
          <label className="col-xl-3 col-lg-3 col-form-label">
            Tanggal Surat
          </label>
          <div className="col-lg-9 col-xl-6">
            <DatePicker
              selected={
                (formik.values.tglSurat && new Date(formik.values.tglSurat)) ||
                null
              }
              onChange={(val) => {
                formik.setFieldValue("tglSurat", val);
              }}
              customInput={<CustomDate />}
              dateFormat="dd MMMM yyyy"
            />
          </div>
        </div>
        <div className="form-group row">
          <label className="col-xl-3 col-lg-3 col-form-label">Perihal</label>
          <div className="col-lg-9 col-xl-6">
            <div>
              <textarea
                type="text"
                className={`form-control form-control-lg  ${getInputClasses(
                  "perihal"
                )}`}
                name="perihal"
                placeholder="Perihal"
                {...formik.getFieldProps("perihal")}
              />
              {formik.touched.perihal && formik.errors.perihal ? (
                <div className="invalid-feedback">{formik.errors.perihal}</div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="form-group row">
          <label className="col-xl-3 col-lg-3 col-form-label">
            Jenis Pajak
          </label>
          <div className="col-lg-9 col-xl-6">
            <select
              className="form-control form-control-lg "
              name="jenisPajak"
              {...formik.getFieldProps("jenisPajak")}
            >
              <option>Pilih Jenis Pajak</option>
              <option value="1">PPN</option>
              <option value="2">PPH 21</option>
              <option value="3">PPH 23</option>
              <option value="4">PPH 25</option>
            </select>
          </div>
        </div>
        <div className="form-group row">
          <label className="col-xl-3 col-lg-3 col-form-label">Jenis</label>
          <div className="col-lg-9 col-xl-6">
            <select
              className="form-control form-control-lg "
              name="jenisKantor"
              {...formik.getFieldProps("jenisKantor")}
            >
              <option>Pilih Jenis</option>
              <option value="1">Asosiasi/KL</option>
              <option value="2">Unit DJP</option>
            </select>
          </div>
        </div>
        <div className="form-group row">
          <label className="col-xl-3 col-lg-3 col-form-label">
            Upload File
          </label>
          <div className="col-lg-9 col-xl-6">
            <div>
              <input
                type="file"
                className={` ${getInputClasses("file")}`}
                name="file"
                // errorMessage={formik.errors["file"] ? formik.errors["file"] : undefined}
                setFieldValue={formik.setFieldValue}

                // onChange={formik.handleChange}
                // component={CustomFileInput}
                // {...formik.getFieldProps("file")}
              />
              {formik.touched.file && formik.errors.file ? (
                <div className="invalid-feedback">{formik.errors.file}</div>
              ) : null}
            </div>
          </div>
        </div>

        {/* HIDE START HERE */}

        {formik.values.jenisKantor !== undefined ? (
          <>
            {" "}
            {checkOfficeType(formik.values.jenisKantor)}
            <div className="form-group row">
              <label className="col-xl-3 col-lg-3 col-form-label">Alamat</label>
              <div className="col-lg-9 col-xl-6">
                <div>
                  <textarea
                    type="text"
                    className={`form-control form-control-lg  ${getInputClasses(
                      "alamat"
                    )}`}
                    name="alamat"
                    placeholder="Alamat"
                    {...formik.getFieldProps("alamat")}
                  />
                  {formik.touched.alamat && formik.errors.alamat ? (
                    <div className="invalid-feedback">
                      {formik.errors.alamat}
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
            <div className="form-group row">
              <label className="col-xl-3 col-lg-3 col-form-label">
                No Telp PIC
              </label>
              <div className="col-lg-9 col-xl-6">
                <div>
                  <div className="input-group input-group-lg ">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fa fa-phone"></i>
                      </span>
                    </div>
                    <input
                      type="text"
                      placeholder="No Telp PIC"
                      className={`form-control form-control-lg  ${getInputClasses(
                        "noTelp"
                      )}`}
                      name="noTelp"
                      {...formik.getFieldProps("noTelp")}
                    />
                  </div>
                  {formik.touched.noTelp && formik.errors.noTelp ? (
                    <div
                      className="invalid-feedback"
                      style={{ display: "block" }}
                    >
                      {formik.errors.noTelp}
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
            <div className="form-group row">
              <label className="col-xl-3 col-lg-3 col-form-label">
                Nama PIC / Penandatangan Surat
              </label>
              <div className="col-lg-9 col-xl-6">
                <div>
                  <input
                    type="text"
                    className={`form-control form-control-lg  ${getInputClasses(
                      "namaPic"
                    )}`}
                    name="namaPic"
                    placeholder="Nama PIC"
                    {...formik.getFieldProps("namaPic")}
                  />
                  {formik.touched.namaPic && formik.errors.namaPic ? (
                    <div className="invalid-feedback">
                      {formik.errors.namaPic}
                    </div>
                  ) : null}
                </div>
              </div>
            </div>{" "}
          </>
        ) : null}

        {/* END HIDE */}
      </div>
    </form>
  );
}

export default ProposalEditForm;
