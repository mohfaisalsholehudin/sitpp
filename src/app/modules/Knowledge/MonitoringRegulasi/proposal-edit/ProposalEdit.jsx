import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import { useSelector } from "react-redux";
import ProposalEditForm from "./ProposalEditForm";
import ProposalEditFooter from "./ProposalEditFooter";
import {
  updateStatusRegulasi,
  getRegulasiById,
  uploadFile,
  saveRegulasi,
  updateRegulasi,
} from "../../Api";
import swal from "sweetalert";

function RegulasiEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();
  const { user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();
  const [isDisabled, setIsDisabled] = useState();
  const [proposal, setProposal] = useState();

  const initValues = {
    alasan_tolak: "",
    body: "",
    file_upload: "",
    id_topik: "",
    jns_regulasi: "",
    kd_kantor: user.kantorLegacyKode,
    kd_unit_org: user.unitEs4LegacyKode,
    nip_pengusul: user.nip9,
    nip_pjbt_es3: "",
    nip_pjbt_es4: "",
    no_regulasi: "",
    perihal: "",
    status: "",
    tgl_regulasi: "",
  };

  const getCurrentDate = () => {
    const date = new Date();
    return date;
  };

  const handleChangeProposalRegulasi = (val) => {
    getRegulasiById(val.value).then(({ data }) => {
      setContent({
        id_peraturan: data.id_peraturan,
        no_regulasi: data.no_regulasi,
        tgl_regulasi: getCurrentDate(),
        id_jnspajak: data.id_jnspajak,
        perihal: data.perihal,
        file_upload: data.file_upload,
        status: data.status,
        jns_regulasi: data.jns_regulasi,
        alasan_tolak: data.alasan_tolak,
        id_topik: data.id_topik,
        body: data.body,
        nip_pengusul: data.user.nip9,
        nip_pjbt_es4: data.nip_pjbt_es4,
        nip_pjbt_es3: data.nip_pjbt_es3,
        wkt_usul: data.wkt_usul,
        wkt_teliti_es4: data.wkt_teliti_es4,
        wkt_teliti_es3: data.wkt_teliti_es3,

        wkt_create: data.wkt_create,
        wkt_update: data.wkt_update,
        kd_kantor: data.kd_kantor,
        kd_unit_org: data.kd_unit_org,
      });
    });
  };

  useEffect(() => {
    let _title = id ? "Edit Regulasi Perpajakan" : "Tambah Regulasi Perpajakan";

    setTitle(_title);
    suhbeader.setTitle(_title);

    if (id) {
      getRegulasiById(id).then(({ data }) => {
        setProposal({
          id_peraturan: data.id_peraturan,
          no_regulasi: data.no_regulasi,
          tgl_regulasi: data.tgl_regulasi,
          id_jnspajak: data.id_jnspajak,
          perihal: data.perihal,
          file_upload: data.file_upload,
          status: data.status,
          jns_regulasi: data.jns_regulasi,
          alasan_tolak: data.alasan_tolak,
          id_topik: data.id_topik,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
          kd_kantor: data.kd_kantor,
          kd_unit_org: data.kd_unit_org,
        });
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();

  const saveProposal = (values) => {
    if (!id) {
      const formData = new FormData();

      formData.append("file", values.file);
      //console.log(formData);

      uploadFile(formData)
        .then(({ data }) =>
          saveRegulasi(
            values.alasan_tolak,
            values.body,
            data.message,
            values.id_topik,
            values.jns_regulasi,
            values.kd_kantor,
            values.kd_unit_org,
            values.nip_pengusul,
            values.nip_pjbt_es3,
            values.nip_pjbt_es4,
            values.no_regulasi,
            values.perihal,
            values.status,
            values.tgl_regulasi
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push("/knowledge/regulasi/monitoring");
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push("/knowledge/regulasi/monitoring/new");
              });
            }
          })
        )
        .catch(() => window.alert("Oops Something went wrong !"));
    } else {
      //console.log(values.status)
      // ketika status tolak dan melakukan edit , otomatis status jadi draft lagi

      if (values.file) {
        //console.log(values.file.name);
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData)
          .then(({ data }) =>
            updateRegulasi(
              values.id_peraturan,
              values.alasan_tolak,
              values.body,
              data.message,
              values.id_topik,
              values.jns_regulasi,
              values.kd_kantor,
              values.kd_unit_org,
              values.nip_pengusul,
              values.nip_pjbt_es3,
              values.nip_pjbt_es4,
              values.no_regulasi,
              values.perihal,
              values.status,
              values.tgl_regulasi
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push("/knowledge/regulasi/monitoring");
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push("/knowledge/regulasi/monitoring/new");
                });
              }
            })
          )
          .catch(() => window.alert("Oops Something went wrong !"));
      } else {
        updateRegulasi(
          values.id_peraturan,
          values.alasan_tolak,
          values.body,
          values.file_upload,
          values.id_topik,
          values.jns_regulasi,
          values.kd_kantor,
          values.kd_unit_org,
          values.nip_pengusul,
          values.nip_pjbt_es3,
          values.nip_pjbt_es4,
          values.no_regulasi,
          values.perihal,
          values.status,
          values.tgl_regulasi
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push("/knowledge/regulasi/monitoring");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/knowledge/regulasi/monitoring/new");
            });
          }
        });
      }

      if (values.status == "Tolak") {
        updateStatusRegulasi(id, 5, 1, user.nip9).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Peraturan Berubah Menjadi Draft", "success").then(
              () => {
                history.push("/dashboard");
                history.replace("/knowledge/regulasi/monitoring");
              }
            );
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/regulasi/monitoring");
            });
          }
        });
      }
    }
  };

  const backToProposalList = () => {
    history.push(`/knowledge/regulasi/monitoring`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <ProposalEditForm
            actionsLoading={actionsLoading}
            proposal={proposal || initValues}
            btnRef={btnRef}
            saveProposal={saveProposal}
            handleChangeProposalRegulasi={handleChangeProposalRegulasi}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <ProposalEditFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></ProposalEditFooter>
      </CardFooter>
    </Card>
  );
}

export default RegulasiEdit;
