// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function KodefikasiMonitoringActionColumnFormatter(
  cellContent,
  row,
  rowIndex,
  {
    openEditDialog,
    openDeleteDialog,
    showHistory,
    applyProposal,
    showHistoryTolak,
    showReject,
    detailKodefikasi,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            <a
              title="Status Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Eselon 3":
        return (
          <>
            <a
              title="Status Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
          </>
        );

      case "Terima":
        return (
          <>
            <a
              title="Status Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
            <> </>
            <a
              title="Detail Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKodefikasi(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      //   case "Draft":
      //     return (
      //       <>
      //         <a
      //           title="Edit Proposal"
      //           className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      //           onClick={() => openEditDialog(row.id_kodefikasi)}
      //         >
      //           <span className="svg-icon svg-icon-md svg-icon-primary">
      //             <SVG
      //               src={toAbsoluteUrl(
      //                 "/media/svg/icons/Communication/Write.svg"
      //               )}
      //             />
      //           </span>
      //         </a>
      //         <> </>

      //         <a
      //           title="Hapus Proposal"
      //           className="btn btn-icon btn-light btn-hover-danger btn-sm"
      //           onClick={() => openDeleteDialog(row.id_kodefikasi)}
      //         >
      //           <span className="svg-icon svg-icon-md svg-icon-danger">
      //             <SVG
      //               src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
      //             />
      //           </span>
      //         </a>
      //         <> </>
      //         <a
      //           title="Ajukan Proposal"
      //           className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      //           onClick={() => applyProposal(row.id_kodefikasi)}
      //         >
      //           <span className="svg-icon svg-icon-md svg-icon-success">
      //             <SVG
      //               src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
      //             />
      //           </span>
      //         </a>
      //       </>
      //     );

      case "Tolak":
        return (
          <>
            <a
              title="Alasan Penolakan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistoryTolak(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fa-solid fa-circle-info"></i>
              </span>
            </a>
            <> </>
            {/* <a
              title="Edit Proposal"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a> */}
            <> </>
          </>
        );

      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

// export function ActionsColumnFormatterEvaResearch(
//   cellContent,
//   row,
//   rowIndex,
//   { openEditDialog, openDeleteDialog, showProposal }
// ) {
//   return (
//     <>
//       <a
//         title="Open Proposal"
//         className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
//         onClick={() => showProposal(row.id_kodefikasi)}
//       >
//         <span className="svg-icon svg-icon-md svg-icon-dark">
//           <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} />
//         </span>
//       </a>
//     </>
//   );
// }

// export function ActionsColumnFormatterEvaValidate(
//   cellContent,
//   row,
//   rowIndex,
//   { openAddDetil, openDeleteDialog, showProposal, showReject, applyValidation }
// ) {
//   const checkStatus = (status) => {
//     switch (status) {
//       case "Eselon 4":
//         return (
//           <>
//             <a
//               title="Status Kodefikasi"
//               className="btn btn-icon btn-light btn-hover-dark btn-sm mx-3"
//               onClick={() => showProposal(row.id_validator)}
//             >
//               <span className="svg-icon svg-icon-md svg-icon-dark">
//                 <SVG
//                   src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
//                 />
//               </span>
//             </a>
//           </>
//         );
//       case "Eselon 3":
//         return (
//           <>
//             <a
//               title="Status Kodefikasi"
//               className="btn btn-icon btn-light btn-hover-dark btn-sm mx-3"
//               onClick={() => showProposal(row.id_validator)}
//             >
//               <span className="svg-icon svg-icon-md svg-icon-dark">
//                 <SVG
//                   src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
//                 />
//               </span>
//             </a>
//           </>
//         );

//       case "Tolak":
//         return (
//           <>
//             <a
//               title="Show Reject"
//               className="btn btn-icon btn-light btn-hover-dark btn-sm mx-3"
//               onClick={() => showReject(row.id_validator)}
//             >
//               <span className="svg-icon svg-icon-md svg-icon-dark">
//                 <SVG
//                   src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
//                 />
//               </span>
//             </a>
//             <> </>
//             <a
//               title="Edit Detil"
//               className="btn btn-icon btn-light btn-hover-primary btn-sm"
//               onClick={() => openAddDetil(row.id_validator)}
//             >
//               <span className="svg-icon svg-icon-md svg-icon-primary">
//                 <SVG
//                   src={toAbsoluteUrl(
//                     "/media/svg/icons/Communication/Write.svg"
//                   )}
//                 />
//               </span>
//             </a>
//             {/* <> </>
//             <a
//               title="Ajukan Validasi"
//               className="btn btn-icon btn-light btn-hover-success btn-sm mx-3"
//               onClick={() => applyValidation(row.id_validator)}
//             >
//               <span className="svg-icon svg-icon-md svg-icon-success">
//                 <SVG
//                   src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
//                 />
//               </span>
//             </a> */}
//           </>
//         );

//       case null:
//         return (
//           <>
//             <a
//               title="Tambah Detil"
//               className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
//               onClick={() => openAddDetil(row.id_validator)}
//             >
//               <span className="svg-icon svg-icon-md svg-icon-primary">
//                 <SVG
//                   src={toAbsoluteUrl(
//                     "/media/svg/icons/Communication/Write.svg"
//                   )}
//                 />
//               </span>
//             </a>
//           </>
//         );

//       case "Draft":
//         return (
//           <>
//             <a
//               title="Edit Detil"
//               className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
//               onClick={() => openAddDetil(row.id_validator)}
//             >
//               <span className="svg-icon svg-icon-md svg-icon-primary">
//                 <SVG
//                   src={toAbsoluteUrl(
//                     "/media/svg/icons/Communication/Write.svg"
//                   )}
//                 />
//               </span>
//             </a>
//             <> </>
//             <a
//               title="Ajukan Validasi"
//               className="btn btn-icon btn-light btn-hover-success btn-sm"
//               onClick={() => applyValidation(row.id_validator)}
//             >
//               <span className="svg-icon svg-icon-md svg-icon-success">
//                 <SVG
//                   src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
//                 />
//               </span>
//             </a>
//           </>
//         );

//       default:
//         break;
//     }
//   };

//   return <>{checkStatus(row.status)}</>;
// }

// export function ActionsColumnFormatterEvaRevalidate(
//   cellContent,
//   row,
//   rowIndex,
//   { openEditDialog, openDeleteDialog, showProposal }
// ) {
//   return (
//     <>
//       <a
//         title="Open Proposal"
//         className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
//         onClick={() => showProposal(row.id_validator)}
//       >
//         <span className="svg-icon svg-icon-md svg-icon-dark">
//           <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} />
//         </span>
//       </a>
//     </>
//   );
// }
