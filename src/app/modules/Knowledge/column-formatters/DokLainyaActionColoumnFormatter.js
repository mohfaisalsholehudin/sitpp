import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function DokLainyaActionColoumnFormatter(
  cellContent,
  row,
  rowIndex,
  { deleteDialog }
) {
  return (
    <>
      <a
        title="Hapus Peraturan"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => deleteDialog(row.id_km_dok_lainnya)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}
