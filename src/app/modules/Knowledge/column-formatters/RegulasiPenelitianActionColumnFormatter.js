// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function RegulasiPenelitianActionColumnFormatter(
  cellContent,
  row,
  rowIndex,
  {
    prosesRegulasi,
    detailRegulasi,
    editRegulasi,
    deleteDialog,
    showHistory,
    applyRegulasi,
    openAddKodTerkait,
    // openPeraturan,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}

            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailRegulasi(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-info text-info"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editRegulasi(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Peraturan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyRegulasi(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <i className="fas fa-check text-success"></i>
              </span>
            </a>

            {/* <a
              title="Isi Materi"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
              //onClick={() => openAddPerTerkait(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Files/File.svg")} />
              </span>
            </a>

            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddKodTerkait(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Plus.svg")}
                />
              </span>
            </a> */}

            <> </>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            {/* <a
              title="Show Reject"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReject(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
               <i className="fas fa-info text-info"></i>
              </span>
            </a>
            <> </> */}
            <a
              title="Edit Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editRegulasi(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
          </>
        );

        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function RegulasiPenelitianActionColumnFormatterEs4(
  cellContent,
  row,
  rowIndex,
  {
    prosesRegulasi,
    detailRegulasi,
    editRegulasi,
    deleteDialog,
    showHistory,
    applyRegulasi,
    openAddKodTerkait,
    // openPeraturan,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}

            {/* <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailRegulasi(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-info text-info"></i>
              </span>
            </a> */}

            {/* Proses Terima Tolak */}
            <a
              title="Proses Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openPeraturan(row.id_peraturan)}
              onClick={() => prosesRegulasi(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editRegulasi(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Peraturan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyRegulasi(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <i className="fas fa-check text-success"></i>
              </span>
            </a>

            {/* <a
              title="Isi Materi"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
              //onClick={() => openAddPerTerkait(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Files/File.svg")} />
              </span>
            </a>

            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddKodTerkait(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Plus.svg")}
                />
              </span>
            </a> */}

            <> </>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            {/* <a
              title="Show Reject"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReject(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
               <i className="fas fa-info text-info"></i>
              </span>
            </a>
            <> </> */}
            <a
              title="Edit Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editRegulasi(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
          </>
        );

        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function RegulasiPenelitianActionColumnFormatterEs3(
  cellContent,
  row,
  rowIndex,
  {
    prosesRegulasi,
    detailRegulasi,
    editRegulasi,
    deleteDialog,
    showHistory,
    applyRegulasi,
    openAddKodTerkait,
    // openPeraturan,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}

            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailRegulasi(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-info text-info"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Proses Terima Tolak */}
            <a
              title="Proses Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openPeraturan(row.id_peraturan)}
              onClick={() => prosesRegulasi(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editRegulasi(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Peraturan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyRegulasi(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <i className="fas fa-check text-success"></i>
              </span>
            </a>

            {/* <a
              title="Isi Materi"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
              //onClick={() => openAddPerTerkait(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Files/File.svg")} />
              </span>
            </a>

            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddKodTerkait(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Plus.svg")}
                />
              </span>
            </a> */}

            <> </>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            <a
              title="Edit Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editRegulasi(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
          </>
        );

        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}
