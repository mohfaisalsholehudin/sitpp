// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function PutusanProposalActionColumnFormatter(
  cellContent,
  row,
  rowIndex,
  { rejectPutusan, openEditDialog, openDeleteDialog, showHistory, applyProposal, detailProposal, openAddPerTerkait, addBody }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Putusan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")} /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}

            <a
              title="Detail Putusan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailProposal(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Eselon 3":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Putusan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")} /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}

            <a
              title="Detail Putusan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailProposal(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

      case "Draft":
        return (
          <>
            <a
              title="Edit Putusan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Putusan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => openDeleteDialog(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Putusan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>

            <a
              title="Isi Materi"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
              onClick={() => addBody(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Files/File.svg"
                  )}
                /> */}
                <i className="fas fa-file-alt text-primary"></i>
              </span>
            </a>


            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddPerTerkait(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Navigation/Plus.svg"
                  )}
                /> */}
                <i className="fas fa-plus text-primary"></i>
              </span>
            </a>
            <> </>
          </>
        );

      case "Tolak":
        return (
          <>
            <a
              title="Alasan Penolakan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => rejectPutusan(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Code/Info-circle.svg")}
                /> */}
                <i className="fas fa-info text-info"></i>
              </span>
            </a>
            <a
              title="Edit Putusan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <a
              title="Hapus Putusan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => openDeleteDialog(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>
          </>
        );

      default:
        break;
    }
  };

  return <>{checkStatus(row.putusanPengadilan.status)}</>;
}