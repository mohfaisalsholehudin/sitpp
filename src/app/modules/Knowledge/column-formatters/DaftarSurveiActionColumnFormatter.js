// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function DaftarSurveiActionColumnFormatter(
  cellContent,
  row,
  rowIndex,
  { detailSurvei, showHistory }
) {

  const checkStatus = (status) => {
    switch (status) {

      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Survei"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.kmSurvei.id_lapsurvey)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")} /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
          </>
        );
        break;
      
        case "Eselon 3":
          return (
            <>
              {/* Show history proses Pengajuan */}
              <a
                title="Status Survei"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => showHistory(row.kmSurvei.id_lapsurvey)}
              >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")} /> */}
                  <i className="fas fa-stopwatch text-dark"></i>
                </span>
              </a>
            </>
          );
          break;

      case "Terima":
        return (
          <>
            <a
              title="Status Survei"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.kmSurvei.id_lapsurvey)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}
            <a
              title="Detail Survei"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailSurvei(row.kmSurvei.id_lapsurvey)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.kmSurvei.status)}</>;
}
