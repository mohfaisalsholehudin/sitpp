import React, { useEffect, useState, useRef, useCallback } from "react";
import axios from "axios";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../../_metronic/_partials/controls";
import {
  getDraft,
  saveBodyKodefikasi,
  getKodefikasiById,
  uploadFile,
} from "../../Api";
import EmailEditor from "react-email-editor";
import { RPerDirjen } from "../../../../references/template_draft/RPerDirjen";
import { RUU } from "../../../../references/template_draft/RUU";
import { RPP } from "../../../../references/template_draft/RPP";
import { RPMK } from "../../../../references/template_draft/RPMK";
import CustomFileInput from "../../../../helpers/form/CustomFileInput";
import CustomLampiranInput from "../../../../helpers/form/CustomLampiranInput";

import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";
// import "./decoupled.css";

function Body({
  history,
  match: {
    params: { id },
  },
}) {
  const [draft, setDraft] = useState("");
  const [body, setBody] = useState("");
  const [propbody, setPropbody] = useState("");
  const [show, setShow] = useState(false);
  const [isEditorLoaded, setIsEditorLoaded] = useState(false);
  const [isComponentMounted, setIsComponentMounted] = useState(false);
  const [content, setContent] = useState({
    file: "",
    lampiran: "",
  });
  // const [draftRuu, setDraftRuu] = useState([]);
  // const [draftRpp, setDraftRpp] = useState([]);
  // const [draftRpmk, setDraftRpmk] = useState([]);
  // const [draftRper, setDraftRper] = useState([]);

  const validationSchema = Yup.object().shape({
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        (value) => value && SUPPORTED_FORMATS.includes(value.type)
      ),
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  const emailEditorRef = useRef(null);

  useEffect(() => {
    getKodefikasiById(id).then(({ data }) => {
      if (data.body) {
        setPropbody(JSON.parse(data.body));
      }

      // data.map((dt) => {
      //   //setPeraturanTerkait(peraturanTerkait => [...peraturanTerkait,dt.kmperaturanTerkait]);

      // });
    });
  }, [id]);

  console.log(propbody);
  const onReady = () => {
    // editor is ready
    setShow(true);
  };

  const tes = (id, body) => {
    // editor is ready
    let x = JSON.stringify(body);

    setBody(x);
  };

  // console.log(body);
  const saveBody = (values) => {
    console.log(id + "__" + values);
    saveBodyKodefikasi(id, values).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Materi berhasil disimpan", "success").then(() => {
          history.push("/knowledge/kodefikasi/usulan");
        });
      } else {
        swal("Gagal", "Materi gagal disimpan", "error").then(() => {
          history.push("/knowledge/kodefikasi/usulan/new");
        });
      }
    });
  };
  const handleBack = () => {
    history.push(`/knowledge/kodefikasi/usulan/${id}/process`);
  };

  return (
    <Card>
      <CardHeader
        title="Buat Draft Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="col-lg-12 mb-4" style={{ textAlign: "right" }}>
            {/* <button
              type="button"
              className="btn btn-light-primary ml-2"
              onClick={onLoad}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
            >
              <i className="fa fa-redo"></i>
              Reload
            </button> */}
          </div>
          <div className="row">
            <div className="document-editor">
              <div className="document-editor__toolbar"></div>
              <div className="document-editor__editable-container">
                <CKEditor
                  onReady={(editor) => {
                    console.log("Editor is ready to use!", editor);
                    window.editor = editor;

                    // Add these two lines to properly position the toolbar
                    const toolbarContainer = document.querySelector(
                      ".document-editor__toolbar"
                    );
                    toolbarContainer.appendChild(
                      editor.ui.view.toolbar.element
                    );
                  }}
                  config={{
                    simpleUpload: {
                      // The URL that the images are uploaded to.
                      uploadUrl: "http://localhost:8080/uploadckeditor",

                      // Enable the XMLHttpRequest.withCredentials property.
                      withCredentials: false,

                      // Headers sent along with the XMLHttpRequest to the upload server.
                      headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": "true",
                      },
                    },
                  }}
                  //ICAAAAAAAAAAAAAAAAAAAALLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                  //INI BUAT GET HTML HASIL NYA editor.getData() SEPERTI DIBAWAH INI DITAMPILIN KE LOG
                  onChange={(event, editor) => tes(1, editor.getData())}
                  //onChange={ ( event, editor ) => console.log( { event, editor } ) }
                  editor={Editor}
                  //ICAAAAAAAAAAAAAAALLLLLLLLLLLLLLLLLLLLLLLL
                  //INI BUAT NAMBAHIN TEMPLATE NYA DI BAWAH INI PAKAI HTML
                  data={propbody}
                />
              </div>
            </div>
          </div>
          <div className="mt-4">
            <Formik
              enableReinitialize={true}
              initialValues={content}
              validationSchema={validationSchema}
              onSubmit={(values) => {
                //saveDraft(values);
                //console.log(body);
              }}
            >
              {({ handleSubmit }) => {
                return (
                  <Form className="form form-label-right">
                    {show ? (
                      <>
                        <div className="form-group row">
                          <label className="col-xl-3 col-lg-3 col-form-label">
                            Upload File
                          </label>
                          <div className="col-lg-9 col-xl-6">
                            <Field
                              name="file"
                              component={CustomFileInput}
                              title="Select a file"
                              label="File"
                              style={{ display: "flex" }}
                            />
                          </div>
                        </div>
                      </>
                    ) : null}
                    {/* <div className="form-group row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Upload Lampiran
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <Field
                          name="lampiran"
                          component={CustomLampiranInput}
                          title="Select a file"
                          label="Lampiran"
                          style={{ display: "flex" }}
                        />
                      </div>
                    </div> */}

                    <div className="col-lg-12" style={{ textAlign: "right" }}>
                      <button
                        type="button"
                        onClick={handleBack}
                        className="btn btn-light"
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                        }}
                      >
                        <i className="fa fa-arrow-left"></i>
                        Kembali
                      </button>
                      {`  `}
                      <button
                        type="submit"
                        className="btn btn-success ml-2"
                        onClick={() => saveBody(body)}
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                        }}
                      >
                        <i className="fas fa-save"></i>
                        Simpan
                      </button>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </>
      </CardBody>
    </Card>
  );
}

export default Body;
