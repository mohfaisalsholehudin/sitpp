import axios from "axios";
const {BACKEND_URL} = window.ENV;

//API KMS

export function getPeraturan() {
  return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/`);
}

export function getPeraturanById(id) {
  return axios.get(`/kmregulasiperpajakan/${id}`);
}


export function uploadFile(formData) {
  const config = {
    headers: {
      "Content-type": "multipart/form-data",
    },
  };
  return axios.post("/upload", formData, config);
}


export function updatePeraturan(
  id,
  file_upload,
  id_topik,
  no_regulasi,
  perihal,
  status,
  tgl_regulasi,
  jns_regulasi,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  body,
  alasan_tolak
) {
  return axios.put(`/kmregulasiperpajakan/${id}`, {
    file_upload,
    id_topik,
    no_regulasi,
    perihal,
    status,
    tgl_regulasi,
    jns_regulasi,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    body,
    alasan_tolak,
  });
}

export function getKajianNonTerima() {
  return axios.get("/kmhslkajian/nonterima");
}

export function getKajianById(id) {
  return axios.get(`/kmhslkajian/${id}`);
}

export function deleteKajianById(id) {
  return axios.delete(`/kmhslkajian/${id}`);
}

export function updateStatusKajian(id, status, alasan = null) {
  if (alasan) {
    return axios.put(`/kmhslkajian/updatekmhasilkajianstatus/${id}/${status}`, {
      alasan_tolak: alasan,
    });
  } else {
    return axios.put(
      `/kmhslkajian/updatekmhasilkajianstatus/${id}/${status}`,
      {}
    );
  }
}

export function saveKajian(
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  perihal,
  status,
  tgl_kajian,
  unit_kajian
) {
  return axios.post("/kmhslkajian/", {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    perihal,
    status,
    tgl_kajian,
    unit_kajian,
  });
}

export function updateKajian(
    id,
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    perihal,
    status,
    tgl_kajian,
    unit_kajian
) {
  return axios.put(`/kmhslkajian/${id}`, {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    perihal,
    status,
    tgl_kajian,
    unit_kajian,
  });
}

export function getJenisKajian() {
  return axios.get("/detiljenis/byidjenis/2");
}

export function getDitPembuat() {
  return axios.get("/detiljenis/byidjenis/8");
}

export function getDokumenKajian(id) {
  return axios.get(`/kmdoklainnya/kajian/${id}`);
}

export function getPeraturanTerkaitKajian(id) {
  return axios.get(`/kmperaturanterkait/kajian/${id}`);
}

export function saveDokLainKajian(file_upload, id_kajian, keterangan) {
  return axios.post(`/kmdoklainnya/3`, {
    file_upload,
    id_kajian,
    keterangan,
  });
}

export function getKajianTerima() {
  return axios.get("/kmhslkajian/terima");
}




// putusan


export function getPutusanNonTerima() {
  return axios.get("/kmputusanpengadilan/nonterima");
}

export function getPutusanById(id) {
  return axios.get(`/kmputusanpengadilan/${id}`);
}

export function deletePutusanById(id) {
  return axios.delete(`/kmputusanpengadilan/${id}`);
}

export function updateStatusPutusan(id, status, alasan = null) {
  if (alasan) {
    return axios.put(`/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${status}`, {
      alasan_tolak: alasan,
    });
  } else {
    return axios.put(
      `/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${status}`,
      {}
    );
  }
}

export function savePutusan(
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_putusan,
  perihal,
  status,
  tgl_putusan
) {
  return axios.post("/kmputusanpengadilan/", {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_putusan,
    perihal,
    status,
    tgl_putusan
  });
}

export function updatePutusan(
    id,
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_putusan,
    perihal,
    status,
    tgl_putusan
) {
  return axios.put(`/kmputusanpengadilan/${id}`, {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_putusan,
    perihal,
    status,
    tgl_putusan
  });
}

export function getJenisPengadilan() {
  return axios.get("/detiljenis/byidjenis/3");
}


export function getDokumenPutusan(id) {
  return axios.get(`/kmdoklainnya/putusan/${id}`);
}

export function getPeraturanTerkaitPutusan(id) {
  return axios.get(`/kmperaturanterkait/putusan/${id}`);
}


export function getPutusanTerima() {
  return axios.get("/kmputusanpengadilan/terima");
}



/// API WAHYU

export function getPenegasanNonTerima() {
  return axios.get("/kmsuratpenegasan/");
}

export function getPenegasanTerima() {
  return axios.get("/kmsuratpenegasan/terima");
}

export function geturatPenegasanByNoSuratpenegasan(no_suratpenegasan){
  return axios.get(`/kmsuratpenegasan/byno_suratpenegasan/${no_suratpenegasan}`);
}
export function getPenegasanById(id) {
  return axios.get(`/kmsuratpenegasan/${id}`);
}

export function updateStatusPenegasan(id, status, alasan = null) {
  if (alasan) {
    return axios.put(`/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${status}`, {
      alasan_tolak: alasan
    });
  } else {
    return axios.put(`/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${status}`, {});
  }
}

export function savePenegasan(
  
  alasan_tolak ,
  body ,
  file_upload ,
  nip_pengusul ,
  nip_pjbt_es3 ,
  nip_pjbt_es4 ,
  no_suratpenegasan ,
  perihal ,
  status ,
  tgl_suratpenegasan
  
) {

  return axios.post("/kmsuratpenegasan/", {
    alasan_tolak ,
    body ,
    file_upload ,
    nip_pengusul ,
    nip_pjbt_es3 ,
    nip_pjbt_es4 ,
    no_suratpenegasan ,
    perihal ,
    status ,
    tgl_suratpenegasan
  });
}

export function updatePenegasan(
  id,
  alasan_tolak ,
  body ,
  file_upload ,
  nip_pengusul ,
  nip_pjbt_es3 ,
  nip_pjbt_es4 ,
  no_suratpenegasan ,
  perihal ,
  status ,
  tgl_suratpenegasan
  
) {

  return axios.put(`/kmsuratpenegasan/${id}`, {
    alasan_tolak,
    body ,
    file_upload ,
    nip_pengusul ,
    nip_pjbt_es3 ,
    nip_pjbt_es4 ,
    no_suratpenegasan ,
    perihal ,
    status ,
    tgl_suratpenegasan
  });
}

export function deleteSuratPenegasanById(id) {
  return axios.delete(`/kmsuratpenegasan/${id}`);
}

export function getRekomendasiNonTerima() {
  return axios.get("/kmrekomendasi/nonterima/");
}

export function getRekomendasiTerima() {
  return axios.get("/kmrekomendasi/terima/");
}

export function getRekomendasiById(id) {
  return axios.get(`/kmrekomendasi/${id}`);
}

export function updateStatusRekomendasi(id, status, alasan = null) {
  if (alasan) {
    return axios.put(`/kmrekomendasi/updatekmrekomendasistatus/${id}/${status}`, {
      alasan_tolak: alasan
    });
  } else {
    return axios.put(`/kmrekomendasi/updatekmrekomendasistatus/${id}/${status}`, {});
  }
}

export function saveRekomendasi(
  
  alasan_tolak ,
  body ,
  file_upload ,
  id_detiljns ,
  nip_pengusul ,
  nip_pjbt_es3 ,
  nip_pjbt_es4 ,
  no_hsl_pemeriksaan ,
  perihal ,
  status ,
  tgl_pemeriksaan,
  thn_periksa 
  
) {

  return axios.post("/kmrekomendasi/", {
  alasan_tolak ,
  body ,
  file_upload ,
  id_detiljns ,
  nip_pengusul ,
  nip_pjbt_es3 ,
  nip_pjbt_es4 ,
  no_hsl_pemeriksaan ,
  perihal ,
  status ,
  tgl_pemeriksaan,
  thn_periksa 
  });
}

export function getJenisPemeriksaan(){
  return axios.get("/detiljenis/byidjenis/4");
}

export function updateRekomendasi(
  id,
  alasan_tolak ,
  body ,
  file_upload ,
  id_detiljns ,
  nip_pengusul ,
  nip_pjbt_es3 ,
  nip_pjbt_es4 ,
  no_hsl_pemeriksaan ,
  perihal ,
  status ,
  tgl_pemeriksaan,
  thn_periksa
) {

  return axios.put(`/kmrekomendasi/${id}`, {
  alasan_tolak ,
  body ,
  file_upload ,
  id_detiljns ,
  nip_pengusul ,
  nip_pjbt_es3 ,
  nip_pjbt_es4 ,
  no_hsl_pemeriksaan ,
  perihal ,
  status ,
  tgl_pemeriksaan,
  thn_periksa 
  });
}

export function deleteRekomendasiById(id) {
  return axios.delete(`/kmrekomendasi/${id}`);
}

// API KMKODEFIKASI
export function getKodefikasi() {
  return axios.get("/kmkodefikasi/nonterima");
}

export function getKodefikasiTerima() {
  return axios.get("/kmkodefikasi/terima");
}
export function getKodefikasiById(id) {
  return axios.get(`/kmkodefikasi/${id}`);
}

export function deleteKodefikasiById(id) {
  return axios.delete(`/kmkodefikasi/${id}`);
}
export function updateStatusKodefikasi(id, status, alasan = null) {
  if (alasan) {
    return axios.put(`/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`, {
      alasan_tolak: alasan,
    });
  } else {
    return axios.put(
      `/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`,
      {}
    );
  }
}

export function deleteStatusById(id) {
  return axios.delete(`/kmkodefikasi/${id}`);
}

export function getJenisPajakById(id) {
  return axios.get(`/jnspajak/${id}`);
}

export function getJenisPajak() {
  return axios.get("/jnspajak/");
}
export function saveKodefikasi(
  alasan_tolak,
  body,
  file_upload,
  id_jnspajak,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_nd,
  perihal,
  status,
  tgl_nd
) {
  return axios.post("/kmkodefikasi/", {
    alasan_tolak,
    body,
    file_upload,
    id_jnspajak,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_nd,
    perihal,
    status,
    tgl_nd,
  });
}

export function updateKodefikasi(
  id,
  alasan_tolak,
  body,
  file_upload,
  id_jnspajak,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_nd,
  perihal,
  status,
  tgl_nd
) {
  return axios.put(`/kmkodefikasi/${id}`, {
    alasan_tolak,
    body,
    file_upload,
    id_jnspajak,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_nd,
    perihal,
    status,
    tgl_nd,
  });
}

export function kmupdatebody(id, body){
  return axios.put(`/kmregulasiperpajakan/updatebody/${id}`,{body})
}


// Begin Api Fajrul

export function getRegulasiNonTerimaEs4(kdKantor, kdUnitOrg) {
  return axios.get(
    `/api/kmregulasiperpajakan/nonterima/es4/${kdKantor}/${kdUnitOrg}`
  );
}

export function getRegulasiNonTerimaEs3(kdKantor, kdUnitOrg) {
  return axios.get(
    `/api/kmregulasiperpajakan/nonterima/es3/${kdKantor}/${kdUnitOrg}`
  );
}

export function getRegulasiByKantor(kdKantor) {
  return axios.get(`/api/kmregulasiperpajakan/search/?kdKantor=${kdKantor}`);
}

export function getRegulasiNonTerimaByNip(nip) {
  return axios.get(`/api/kmregulasiperpajakan/nonterima/nip/${nip}`);
}

export function deleteRegulasi(id) {
  return axios.delete(`/api/kmregulasiperpajakan/${id}`);
}

export function updateStatusRegulasi(id, statusNow, statusNext, nip, alasan) {
  if (statusNext === 2) {
    return axios.put(
      `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );

  } else if (statusNext === 3) {
    return axios.put(
      `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
      { nip_pjbt_es4: nip }
    );
  } else if (statusNext === 5) {
    if (statusNow === 2) {
      return axios.put(
        `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
        { nip_pjbt_es4: nip, alasan_tolak: alasan }
      );
    } else if (statusNow === 3) {
      return axios.put(
        `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
        { nip_pjbt_es3: nip, alasan_tolak: alasan }
      );
    }
  } else if (statusNext === 6) {
    return axios.put(
      `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
      { nip_pjbt_es3: nip }
    );
  } else if (statusNext === 1) {
    return axios.put(
      `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );
  }
}

export function getRegulasiById(id) {
  return axios.get(`/api/kmregulasiperpajakan/${id}`);
}

export function getPeraturanTerima() {
  return axios.get("/api/kmregulasiperpajakan/terima");
}


export function getJenisPeraturan() {
  return axios.get("/api/jnsperaturan/");
}

export function getTopik() {
  return axios.get("/api/detiljenis/byidjenis/1");
}

export function getRegulasiSearchEselon4(kdKantor, kdUnitOrgEs4, status) {
  return axios.get(
    `/api/kmregulasiperpajakan/search?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&status=${status}`
  );
}

export function getRegulasiSearchEselon3(kdKantor, kdUnitOrgEs3, status) {
  return axios.get(
    `/api/kmregulasiperpajakan/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrgEs3}&status=${status}`
  );
}

export function saveRegulasi(
  alasan_tolak,
  body,
  file_upload,
  id_topik,
  jns_regulasi,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_regulasi,
  perihal,
  status,
  tgl_regulasi
) {
  return axios.post("/api/kmregulasiperpajakan/", {
    alasan_tolak,
    body,
    file_upload,
    id_topik,
    jns_regulasi,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_regulasi,
    perihal,
    status,
    tgl_regulasi,
  });
}

export function updateRegulasi(
  id,
  alasan_tolak,
  body,
  file_upload,
  id_topik,
  jns_regulasi,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_regulasi,
  perihal,
  status,
  tgl_regulasi
) {
  return axios.put(`/api/kmregulasiperpajakan/${id}`, {
    file_upload,
    alasan_tolak,
    body,
    file_upload,
    id_topik,
    jns_regulasi,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_regulasi,
    perihal,
    status,
    tgl_regulasi,
  });
}

export function updateStatusPeraturan(id, status, alasan = null) {
  if (alasan) {
    return axios.put(
      `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${status}`,
      {
        alasan_tolak: alasan,
      }
    );
  } else {
    return axios.put(
      `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${status}`,
      {}
    );
  }
}
export function savePeraturan(
  file_upload,
  id_topik,
  no_regulasi,
  perihal,
  status,
  tgl_regulasi,
  jns_regulasi,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  body,
  alasan_tolak
) {
  return axios.post("/api/kmregulasiperpajakan/", {
    file_upload,
    id_topik,
    no_regulasi,
    perihal,
    status,
    tgl_regulasi,
    jns_regulasi,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    body,
    alasan_tolak,
  });
}
export function deletePeraturanById(id) {
  return axios.delete(`/api/kmregulasiperpajakan/${id}`);
}

export function getPeraturanTerkait(id) {
  return axios.get(`/api/kmperaturanterkait/regulasi/${id}`);
}

export function deletePeraturanTerkait(id) {
  return axios.delete(`/api/kmperaturanterkait/${id}`);
}

export function getPeraturanByNo() {
  return axios.get(`/api/kmregulasiperpajakan/terima/by_no?no_regulasi=`);
}

export function addPerTerkaitById(refKm, id, idPer) {
  if (refKm == 1) {
    return axios.post(`/api/kmperaturanterkait/${refKm}`, {
      id_peraturan: id,
      id_peraturan_terkait: idPer,
    });
  } else if (refKm == 3) {
    return axios.post(`/api/kmperaturanterkait/${refKm}`, {
      id_kajian: id,
      id_peraturan_terkait: idPer,
    });
  } else if (refKm == 4) {
    return axios.post(`/api/kmperaturanterkait/${refKm}`, {
      id_putusan: id,
      id_peraturan_terkait: idPer,
    });
  }
}

export function getHistoryStatusKM(idRefKm, idKm) {
  return axios.get(`/api/logstatuskm/${idRefKm}/${idKm}`);
}

export function getTopikById(id) {
  return axios.get(`/api/reftopik/${id}`);
}

export function getJnsPeraturanById(id) {
  return axios.get(`/api/jnsperaturan/${id}`);
}

export function getDokumenPer(id) {
  return axios.get(`/api/kmdoklainnya/per/${id}`);
}

export function saveDokLainPeraturan(file_upload, id_per, keterangan) {
  return axios.post(`/api/kmdoklainnya/1`, {
    file_upload,
    id_per,
    keterangan,
  });
}

export function deleteDokLain(id) {
  return axios.delete(`/api/kmdoklainnya/${id}`);
}

export function updateBody(id, body) {
  return axios.put(`/api/kmregulasiperpajakan/updatebody/${id}`, {
    body: body,
  });
}



//End of Api Fajrul

