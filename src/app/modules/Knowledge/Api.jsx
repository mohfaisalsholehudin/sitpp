// import axios from "axios";
// const {BACKEND_URL} = window.ENV;

// export function getPeraturan() {
//   return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/`);
// }

// export function getPeraturanById(id) {
//   return axios.get(`/kmregulasiperpajakan/${id}`);
// }

// export function uploadFile(formData) {
//   const config = {
//     headers: {
//       "Content-type": "multipart/form-data",
//     },
//   };
//   return axios.post("/upload", formData, config);
// }

// export function updatePeraturan(
//   id,
//   file_upload,
//   id_topik,
//   no_regulasi,
//   perihal,
//   status,
//   tgl_regulasi,
//   jns_regulasi,
//   nip_pengusul,
//   nip_pjbt_es3,
//   nip_pjbt_es4,
//   body,
//   alasan_tolak
// ) {
//   return axios.put(`/kmregulasiperpajakan/${id}`, {
//     file_upload,
//     id_topik,
//     no_regulasi,
//     perihal,
//     status,
//     tgl_regulasi,
//     jns_regulasi,
//     nip_pengusul,
//     nip_pjbt_es3,
//     nip_pjbt_es4,
//     body,
//     alasan_tolak,
//   });
// }

// export function getKajianNonTerima() {
//   return axios.get("/kmhslkajian/nonterima");
// }

// export function getKajianById(id) {
//   return axios.get(`/kmhslkajian/${id}`);
// }

// export function deleteKajianById(id) {
//   return axios.delete(`/kmhslkajian/${id}`);
// }

// export function updateStatusKajian(id, status, alasan = null) {
//   if (alasan) {
//     return axios.put(`/kmhslkajian/updatekmhasilkajianstatus/${id}/${status}`, {
//       alasan_tolak: alasan,
//     });
//   } else {
//     return axios.put(
//       `/kmhslkajian/updatekmhasilkajianstatus/${id}/${status}`,
//       {}
//     );
//   }
// }

// export function saveKajian(
//   alasan_tolak,
//   body,
//   file_upload,
//   id_detiljns,
//   nip_pengusul,
//   nip_pjbt_es3,
//   nip_pjbt_es4,
//   perihal,
//   status,
//   tgl_kajian,
//   unit_kajian
// ) {
//   return axios.post("/kmhslkajian/", {
//     alasan_tolak,
//     body,
//     file_upload,
//     id_detiljns,
//     nip_pengusul,
//     nip_pjbt_es3,
//     nip_pjbt_es4,
//     perihal,
//     status,
//     tgl_kajian,
//     unit_kajian,
//   });
// }

// export function updateKajian(
//     id,
//     alasan_tolak,
//     body,
//     file_upload,
//     id_detiljns,
//     nip_pengusul,
//     nip_pjbt_es3,
//     nip_pjbt_es4,
//     perihal,
//     status,
//     tgl_kajian,
//     unit_kajian
// ) {
//   return axios.put(`/kmhslkajian/${id}`, {
//     alasan_tolak,
//     body,
//     file_upload,
//     id_detiljns,
//     nip_pengusul,
//     nip_pjbt_es3,
//     nip_pjbt_es4,
//     perihal,
//     status,
//     tgl_kajian,
//     unit_kajian,
//   });
// }

// export function getJenisKajian() {
//   return axios.get("/detiljenis/byidjenis/2");
// }

// export function getDitPembuat() {
//   return axios.get("/detiljenis/byidjenis/8");
// }

// export function getDokumenKajian(id) {
//   return axios.get(`/kmdoklainnya/kajian/${id}`);
// }

// export function getPeraturanTerkaitKajian(id) {
//   return axios.get(`/kmperaturanterkait/kajian/${id}`);
// }

// export function saveDokLainKajian(file_upload, id_kajian, keterangan) {
//   return axios.post(`/kmdoklainnya/3`, {
//     file_upload,
//     id_kajian,
//     keterangan,
//   });
// }

// export function getKajianTerima() {
//   return axios.get("/kmhslkajian/terima");
// }

// // putusan

// export function getPutusanNonTerima() {
//   return axios.get("/kmputusanpengadilan/nonterima");
// }

// export function getPutusanById(id) {
//   return axios.get(`/kmputusanpengadilan/${id}`);
// }

// export function deletePutusanById(id) {
//   return axios.delete(`/kmputusanpengadilan/${id}`);
// }

// export function updateStatusPutusan(id, status, alasan = null) {
//   if (alasan) {
//     return axios.put(`/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${status}`, {
//       alasan_tolak: alasan,
//     });
//   } else {
//     return axios.put(
//       `/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${status}`,
//       {}
//     );
//   }
// }

// export function savePutusan(
//   alasan_tolak,
//   body,
//   file_upload,
//   id_detiljns,
//   nip_pengusul,
//   nip_pjbt_es3,
//   nip_pjbt_es4,
//   no_putusan,
//   perihal,
//   status,
//   tgl_putusan
// ) {
//   return axios.post("/kmputusanpengadilan/", {
//     alasan_tolak,
//     body,
//     file_upload,
//     id_detiljns,
//     nip_pengusul,
//     nip_pjbt_es3,
//     nip_pjbt_es4,
//     no_putusan,
//     perihal,
//     status,
//     tgl_putusan
//   });
// }

// export function updatePutusan(
//     id,
//     alasan_tolak,
//     body,
//     file_upload,
//     id_detiljns,
//     nip_pengusul,
//     nip_pjbt_es3,
//     nip_pjbt_es4,
//     no_putusan,
//     perihal,
//     status,
//     tgl_putusan
// ) {
//   return axios.put(`/kmputusanpengadilan/${id}`, {
//     alasan_tolak,
//     body,
//     file_upload,
//     id_detiljns,
//     nip_pengusul,
//     nip_pjbt_es3,
//     nip_pjbt_es4,
//     no_putusan,
//     perihal,
//     status,
//     tgl_putusan
//   });
// }

// export function getJenisPengadilan() {
//   return axios.get("/detiljenis/byidjenis/3");
// }

// export function getDokumenPutusan(id) {
//   return axios.get(`/kmdoklainnya/putusan/${id}`);
// }

// export function getPeraturanTerkaitPutusan(id) {
//   return axios.get(`/kmperaturanterkait/putusan/${id}`);
// }

// export function getPutusanTerima() {
//   return axios.get("/kmputusanpengadilan/terima");
// }

// /// API WAHYU

// export function getPenegasanNonTerima() {
//   return axios.get("/kmsuratpenegasan/");
// }

// export function getPenegasanTerima() {
//   return axios.get("/kmsuratpenegasan/terima");
// }

// export function geturatPenegasanByNoSuratpenegasan(no_suratpenegasan){
//   return axios.get(`/kmsuratpenegasan/byno_suratpenegasan/${no_suratpenegasan}`);
// }
// export function getPenegasanById(id) {
//   return axios.get(`/kmsuratpenegasan/${id}`);
// }

// export function updateStatusPenegasan(id, status, alasan = null) {
//   if (alasan) {
//     return axios.put(`/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${status}`, {
//       alasan_tolak: alasan
//     });
//   } else {
//     return axios.put(`/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${status}`, {});
//   }
// }

// export function savePenegasan(

//   alasan_tolak ,
//   body ,
//   file_upload ,
//   nip_pengusul ,
//   nip_pjbt_es3 ,
//   nip_pjbt_es4 ,
//   no_suratpenegasan ,
//   perihal ,
//   status ,
//   tgl_suratpenegasan

// ) {

//   return axios.post("/kmsuratpenegasan/", {
//     alasan_tolak ,
//     body ,
//     file_upload ,
//     nip_pengusul ,
//     nip_pjbt_es3 ,
//     nip_pjbt_es4 ,
//     no_suratpenegasan ,
//     perihal ,
//     status ,
//     tgl_suratpenegasan
//   });
// }

// export function updatePenegasan(
//   id,
//   alasan_tolak ,
//   body ,
//   file_upload ,
//   nip_pengusul ,
//   nip_pjbt_es3 ,
//   nip_pjbt_es4 ,
//   no_suratpenegasan ,
//   perihal ,
//   status ,
//   tgl_suratpenegasan

// ) {

//   return axios.put(`/kmsuratpenegasan/${id}`, {
//     alasan_tolak,
//     body ,
//     file_upload ,
//     nip_pengusul ,
//     nip_pjbt_es3 ,
//     nip_pjbt_es4 ,
//     no_suratpenegasan ,
//     perihal ,
//     status ,
//     tgl_suratpenegasan
//   });
// }

// export function deleteSuratPenegasanById(id) {
//   return axios.delete(`/kmsuratpenegasan/${id}`);
// }

// export function getRekomendasiNonTerima() {
//   return axios.get("/kmrekomendasi/nonterima/");
// }

// export function getRekomendasiTerima() {
//   return axios.get("/kmrekomendasi/terima/");
// }

// export function getRekomendasiById(id) {
//   return axios.get(`/kmrekomendasi/${id}`);
// }

// export function updateStatusRekomendasi(id, status, alasan = null) {
//   if (alasan) {
//     return axios.put(`/kmrekomendasi/updatekmrekomendasistatus/${id}/${status}`, {
//       alasan_tolak: alasan
//     });
//   } else {
//     return axios.put(`/kmrekomendasi/updatekmrekomendasistatus/${id}/${status}`, {});
//   }
// }

// export function saveRekomendasi(

//   alasan_tolak ,
//   body ,
//   file_upload ,
//   id_detiljns ,
//   nip_pengusul ,
//   nip_pjbt_es3 ,
//   nip_pjbt_es4 ,
//   no_hsl_pemeriksaan ,
//   perihal ,
//   status ,
//   tgl_pemeriksaan,
//   thn_periksa

// ) {

//   return axios.post("/kmrekomendasi/", {
//   alasan_tolak ,
//   body ,
//   file_upload ,
//   id_detiljns ,
//   nip_pengusul ,
//   nip_pjbt_es3 ,
//   nip_pjbt_es4 ,
//   no_hsl_pemeriksaan ,
//   perihal ,
//   status ,
//   tgl_pemeriksaan,
//   thn_periksa
//   });
// }

// export function getJenisPemeriksaan(){
//   return axios.get("/detiljenis/byidjenis/4");
// }

// export function updateRekomendasi(
//   id,
//   alasan_tolak ,
//   body ,
//   file_upload ,
//   id_detiljns ,
//   nip_pengusul ,
//   nip_pjbt_es3 ,
//   nip_pjbt_es4 ,
//   no_hsl_pemeriksaan ,
//   perihal ,
//   status ,
//   tgl_pemeriksaan,
//   thn_periksa
// ) {

//   return axios.put(`/kmrekomendasi/${id}`, {
//   alasan_tolak ,
//   body ,
//   file_upload ,
//   id_detiljns ,
//   nip_pengusul ,
//   nip_pjbt_es3 ,
//   nip_pjbt_es4 ,
//   no_hsl_pemeriksaan ,
//   perihal ,
//   status ,
//   tgl_pemeriksaan,
//   thn_periksa
//   });
// }

// export function deleteRekomendasiById(id) {
//   return axios.delete(`/kmrekomendasi/${id}`);
// }

// // API KMKODEFIKASI
// export function getKodefikasi() {
//   return axios.get("/kmkodefikasi/nonterima");
// }

// export function getKodefikasiTerima() {
//   return axios.get("/kmkodefikasi/terima");
// }
// export function getKodefikasiById(id) {
//   return axios.get(`/kmkodefikasi/${id}`);
// }

// export function deleteKodefikasiById(id) {
//   return axios.delete(`/kmkodefikasi/${id}`);
// }
// export function updateStatusKodefikasi(id, status, alasan = null) {
//   if (alasan) {
//     return axios.put(`/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`, {
//       alasan_tolak: alasan,
//     });
//   } else {
//     return axios.put(
//       `/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`,
//       {}
//     );
//   }
// }

// export function deleteStatusById(id) {
//   return axios.delete(`/kmkodefikasi/${id}`);
// }

// export function getJenisPajakById(id) {
//   return axios.get(`/jnspajak/${id}`);
// }

// export function getJenisPajak() {
//   return axios.get("/jnspajak/");
// }
// export function saveKodefikasi(
//   alasan_tolak,
//   body,
//   file_upload,
//   id_jnspajak,
//   nip_pengusul,
//   nip_pjbt_es3,
//   nip_pjbt_es4,
//   no_nd,
//   perihal,
//   status,
//   tgl_nd
// ) {
//   return axios.post("/kmkodefikasi/", {
//     alasan_tolak,
//     body,
//     file_upload,
//     id_jnspajak,
//     nip_pengusul,
//     nip_pjbt_es3,
//     nip_pjbt_es4,
//     no_nd,
//     perihal,
//     status,
//     tgl_nd,
//   });
// }

// export function updateKodefikasi(
//   id,
//   alasan_tolak,
//   body,
//   file_upload,
//   id_jnspajak,
//   nip_pengusul,
//   nip_pjbt_es3,
//   nip_pjbt_es4,
//   no_nd,
//   perihal,
//   status,
//   tgl_nd
// ) {
//   return axios.put(`/kmkodefikasi/${id}`, {
//     alasan_tolak,
//     body,
//     file_upload,
//     id_jnspajak,
//     nip_pengusul,
//     nip_pjbt_es3,
//     nip_pjbt_es4,
//     no_nd,
//     perihal,
//     status,
//     tgl_nd,
//   });
// }

// export function kmupdatebody(id, body){
//   return axios.put(`/kmregulasiperpajakan/updatebody/${id}`,{body})
// }

// // Begin Api Fajrul

// export function getRegulasiNonTerimaEs4(kdKantor, kdUnitOrg) {
//   return axios.get(
//     `/api/kmregulasiperpajakan/nonterima/es4/${kdKantor}/${kdUnitOrg}`
//   );
// }

// export function getRegulasiNonTerimaEs3(kdKantor, kdUnitOrg) {
//   return axios.get(
//     `/api/kmregulasiperpajakan/nonterima/es3/${kdKantor}/${kdUnitOrg}`
//   );
// }

// export function getRegulasiByKantor(kdKantor) {
//   return axios.get(`/api/kmregulasiperpajakan/search/?kdKantor=${kdKantor}`);
// }

// export function getRegulasiNonTerimaByNip(nip) {
//   return axios.get(`/api/kmregulasiperpajakan/nonterima/nip/${nip}`);
// }

// export function deleteRegulasi(id) {
//   return axios.delete(`/api/kmregulasiperpajakan/${id}`);
// }

// export function updateStatusRegulasi(id, statusNow, statusNext, nip, alasan) {
//   if (statusNext === 2) {
//     return axios.put(
//       `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
//       { nip_pengusul: nip }
//     );

//   } else if (statusNext === 3) {
//     return axios.put(
//       `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
//       { nip_pjbt_es4: nip }
//     );
//   } else if (statusNext === 5) {
//     if (statusNow === 2) {
//       return axios.put(
//         `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
//         { nip_pjbt_es4: nip, alasan_tolak: alasan }
//       );
//     } else if (statusNow === 3) {
//       return axios.put(
//         `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
//         { nip_pjbt_es3: nip, alasan_tolak: alasan }
//       );
//     }
//   } else if (statusNext === 6) {
//     return axios.put(
//       `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
//       { nip_pjbt_es3: nip }
//     );
//   } else if (statusNext === 1) {
//     return axios.put(
//       `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
//       { nip_pengusul: nip }
//     );
//   }
// }

// export function getRegulasiById(id) {
//   return axios.get(`/api/kmregulasiperpajakan/${id}`);
// }

// export function getPeraturanTerima() {
//   return axios.get("/api/kmregulasiperpajakan/terima");
// }

// export function getJenisPeraturan() {
//   return axios.get("/api/jnsperaturan/");
// }

// export function getTopik() {
//   return axios.get("/api/detiljenis/byidjenis/1");
// }

// export function getRegulasiSearchEselon4(kdKantor, kdUnitOrgEs4, status) {
//   return axios.get(
//     `/api/kmregulasiperpajakan/search?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&status=${status}`
//   );
// }

// export function getRegulasiSearchEselon3(kdKantor, kdUnitOrgEs3, status) {
//   return axios.get(
//     `/api/kmregulasiperpajakan/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrgEs3}&status=${status}`
//   );
// }

// export function saveRegulasi(
//   alasan_tolak,
//   body,
//   file_upload,
//   id_topik,
//   jns_regulasi,
//   kd_kantor,
//   kd_unit_org,
//   nip_pengusul,
//   nip_pjbt_es3,
//   nip_pjbt_es4,
//   no_regulasi,
//   perihal,
//   status,
//   tgl_regulasi
// ) {
//   return axios.post("/api/kmregulasiperpajakan/", {
//     alasan_tolak,
//     body,
//     file_upload,
//     id_topik,
//     jns_regulasi,
//     kd_kantor,
//     kd_unit_org,
//     nip_pengusul,
//     nip_pjbt_es3,
//     nip_pjbt_es4,
//     no_regulasi,
//     perihal,
//     status,
//     tgl_regulasi,
//   });
// }

// export function updateRegulasi(
//   id,
//   alasan_tolak,
//   body,
//   file_upload,
//   id_topik,
//   jns_regulasi,
//   kd_kantor,
//   kd_unit_org,
//   nip_pengusul,
//   nip_pjbt_es3,
//   nip_pjbt_es4,
//   no_regulasi,
//   perihal,
//   status,
//   tgl_regulasi
// ) {
//   return axios.put(`/api/kmregulasiperpajakan/${id}`, {
//     file_upload,
//     alasan_tolak,
//     body,
//     file_upload,
//     id_topik,
//     jns_regulasi,
//     kd_kantor,
//     kd_unit_org,
//     nip_pengusul,
//     nip_pjbt_es3,
//     nip_pjbt_es4,
//     no_regulasi,
//     perihal,
//     status,
//     tgl_regulasi,
//   });
// }

// export function updateStatusPeraturan(id, status, alasan = null) {
//   if (alasan) {
//     return axios.put(
//       `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${status}`,
//       {
//         alasan_tolak: alasan,
//       }
//     );
//   } else {
//     return axios.put(
//       `/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${status}`,
//       {}
//     );
//   }
// }
// export function savePeraturan(
//   file_upload,
//   id_topik,
//   no_regulasi,
//   perihal,
//   status,
//   tgl_regulasi,
//   jns_regulasi,
//   nip_pengusul,
//   nip_pjbt_es3,
//   nip_pjbt_es4,
//   body,
//   alasan_tolak
// ) {
//   return axios.post("/api/kmregulasiperpajakan/", {
//     file_upload,
//     id_topik,
//     no_regulasi,
//     perihal,
//     status,
//     tgl_regulasi,
//     jns_regulasi,
//     nip_pengusul,
//     nip_pjbt_es3,
//     nip_pjbt_es4,
//     body,
//     alasan_tolak,
//   });
// }
// export function deletePeraturanById(id) {
//   return axios.delete(`/api/kmregulasiperpajakan/${id}`);
// }

// export function getPeraturanTerkait(id) {
//   return axios.get(`/api/kmperaturanterkait/regulasi/${id}`);
// }

// export function deletePeraturanTerkait(id) {
//   return axios.delete(`/api/kmperaturanterkait/${id}`);
// }

// export function getPeraturanByNo() {
//   return axios.get(`/api/kmregulasiperpajakan/terima/by_no?no_regulasi=`);
// }

// export function addPerTerkaitById(refKm, id, idPer) {
//   if (refKm == 1) {
//     return axios.post(`/api/kmperaturanterkait/${refKm}`, {
//       id_peraturan: id,
//       id_peraturan_terkait: idPer,
//     });
//   } else if (refKm == 3) {
//     return axios.post(`/api/kmperaturanterkait/${refKm}`, {
//       id_kajian: id,
//       id_peraturan_terkait: idPer,
//     });
//   } else if (refKm == 4) {
//     return axios.post(`/api/kmperaturanterkait/${refKm}`, {
//       id_putusan: id,
//       id_peraturan_terkait: idPer,
//     });
//   }
// }

// export function getHistoryStatusKM(idRefKm, idKm) {
//   return axios.get(`/api/logstatuskm/${idRefKm}/${idKm}`);
// }

// export function getTopikById(id) {
//   return axios.get(`/api/reftopik/${id}`);
// }

// export function getJnsPeraturanById(id) {
//   return axios.get(`/api/jnsperaturan/${id}`);
// }

// export function getDokumenPer(id) {
//   return axios.get(`/api/kmdoklainnya/per/${id}`);
// }

// export function saveDokLainPeraturan(file_upload, id_per, keterangan) {
//   return axios.post(`/api/kmdoklainnya/1`, {
//     file_upload,
//     id_per,
//     keterangan,
//   });
// }

// export function deleteDokLain(id) {
//   return axios.delete(`/api/kmdoklainnya/${id}`);
// }

// export function updateBody(id, body) {
//   return axios.put(`/api/kmregulasiperpajakan/updatebody/${id}`, {
//     body: body,
//   });
// }

// //End of Api Fajrul

import axios from "axios";
const { BACKEND_URL } = window.ENV;

export function uploadFile(formData) {
  const config = {
    headers: {
      "Content-type": "multipart/form-data",
    },
  };
  return axios.post(`${BACKEND_URL}/api/upload`, formData, config);
}

export function uploadFileNew(formData) {
  const config = {
    headers: {
      "Content-type": "multipart/form-data"
    }
  };
  return axios.post(`${BACKEND_URL}/api/newupload`, formData, config);
}

export function getRegulasiAll(){
  return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/`)
}
export function getRegulasiPagination(page=1, size=30){
  return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/?page=${page}&size=${size}`)
}

export function getRegulasiNonTerima(page=1, size=30, text){
  if(text){
    return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/nonterima/?page=${page}&search=${text}&size=${size}`)
  } else {
    return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/nonterima/?page=${page}&size=${size}`)
  }
}

export function getRegulasiMonitoringKantor(page=1, size=30, kantor, text){
  if(text){
    return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/monitoringkantor/?kd_kantor=${kantor}&page=${page}&search=${text}&size=${size}`)
  } else {
    return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/monitoringkantor/?kd_kantor=${kantor}&page=${page}&size=${size}`)
  }
}

export function getPeraturanByStatus(page=1, size=30, status){
  return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/all/status?page=${page}&size=${size}&status=${status}`)
}

export function getPeraturanByJudul(page=1, size=30, pencarian){
  return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/terima/by_no?page=${page}&pencarian=${pencarian}&size=${size}`)
}

export function getRegulasiNonTerimaEs4(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmregulasiperpajakan/nonterima/es4/${kdKantor}/${kdUnitOrg}`
  );
}

export function getRegulasiNonTerimaEs3(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmregulasiperpajakan/nonterima/es3/${kdKantor}/${kdUnitOrg}`
  );
}

export function getRegulasiByKantor(kdKantor) {
  return axios.get(
    `${BACKEND_URL}/api/kmregulasiperpajakan/search/?kdKantor=${kdKantor}`
  );
}

export function getRegulasiNonTerimaByNip(nip) {
  return axios.get(
    `${BACKEND_URL}/api/kmregulasiperpajakan/nonterima/nip/${nip}`
  );
}

export function deleteRegulasi(id) {
  return axios.delete(`${BACKEND_URL}/api/kmregulasiperpajakan/${id}`);
}

export function updateStatusRegulasi(id, statusNow, statusNext, nip, alasan) {
  if (statusNext == 2) {
    return axios.put(
      `${BACKEND_URL}/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );

    // return axios.put(`${BACKEND_URL}/api/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`, {
    //   alasan_tolak: alasan,

    // });
  } else if (statusNext == 3) {
    return axios.put(
      `${BACKEND_URL}/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
      { nip_pjbt_es4: nip }
    );
  } else if (statusNext == 5) {
    if (statusNow == 2) {
      return axios.put(
        `${BACKEND_URL}/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
        { nip_pjbt_es4: nip, alasan_tolak: alasan }
      );
    } else if (statusNow == 3) {
      return axios.put(
        `${BACKEND_URL}/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
        { nip_pjbt_es3: nip, alasan_tolak: alasan }
      );
    }
  } else if (statusNext == 6) {
    return axios.put(
      `${BACKEND_URL}/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
      { nip_pjbt_es3: nip }
    );
  } else if (statusNext == 1) {
    return axios.put(
      `${BACKEND_URL}/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );
  }
}

export function getRegulasiById(id) {
  return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/${id}`);
}

export function getPeraturanTerima() {
  return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/terima`);
}


export function getPeraturanById(id) {
  return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/${id}`);
}

export function getJenisPeraturan() {
  return axios.get(`${BACKEND_URL}/api/jnsperaturan/`);
}

export function getTopik() {
  //return axios.get(`{BACKEND_URL}/api/reftopik/`);
  return axios.get(`${BACKEND_URL}/api/detiljenis/byidjenis/1`);
}

export function getRegulasiSearchEselon4(kdKantor, kdUnitOrgEs4, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmregulasiperpajakan/search?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&status=${status}`
  );
}

export function getRegulasiSearchEselon3(kdKantor, kdUnitOrgEs3, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmregulasiperpajakan/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrgEs3}&status=${status}`
  );
}

export function saveRegulasi(
  alasan_tolak,
  body,
  file_upload,
  id_topik,
  jns_regulasi,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_regulasi,
  perihal,
  status,
  tgl_regulasi
) {
  return axios.post(`${BACKEND_URL}/api/kmregulasiperpajakan/`, {
    alasan_tolak,
    body,
    file_upload,
    id_topik,
    jns_regulasi,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_regulasi,
    perihal,
    status,
    tgl_regulasi,
  });
}

export function updateRegulasi(
  id,
  alasan_tolak,
  body,
  file_upload,
  id_topik,
  jns_regulasi,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_regulasi,
  perihal,
  status,
  tgl_regulasi
) {
  return axios.put(`${BACKEND_URL}/api/kmregulasiperpajakan/${id}`, {
    file_upload,
    alasan_tolak,
    body,
    file_upload,
    id_topik,
    jns_regulasi,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_regulasi,
    perihal,
    status,
    tgl_regulasi,
  });
}
export function updateStatusPeraturan(id, status, alasan = null) {
  if (alasan) {
    return axios.put(
      `${BACKEND_URL}/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${status}`,
      {
        alasan_tolak: alasan,
      }
    );
  } else {
    return axios.put(
      `${BACKEND_URL}/api/kmregulasiperpajakan/updatekmregulasistatus/${id}/${status}`,
      {}
    );
  }
}
export function savePeraturan(
  file_upload,
  id_topik,
  no_regulasi,
  perihal,
  status,
  tgl_regulasi,
  jns_regulasi,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  body,
  alasan_tolak,
  kd_kantor,
  kd_unit_org
) {
  return axios.post(`${BACKEND_URL}/api/kmregulasiperpajakan/`, {
    file_upload,
    id_topik,
    no_regulasi,
    perihal,
    status,
    tgl_regulasi,
    jns_regulasi,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    body,
    alasan_tolak,
    kd_kantor,
    kd_unit_org
  });
}

export function deletePeraturanById(id) {
  return axios.delete(`${BACKEND_URL}/api/kmregulasiperpajakan/${id}`);
}

export function getPeraturanTerkait(id) {
  return axios.get(`${BACKEND_URL}/api/kmperaturanterkait/regulasi/${id}`);
}

export function deletePeraturanTerkait(id) {
  return axios.delete(`${BACKEND_URL}/api/kmperaturanterkait/${id}`);
}

export function getPeraturanByNo() {
  return axios.get(
    `${BACKEND_URL}/api/kmregulasiperpajakan/terima/by_no?no_regulasi=`
  );
}

export function addPerTerkaitById(refKm, id, idPer) {
  if (refKm == 1) {
    return axios.post(`${BACKEND_URL}/api/kmperaturanterkait/${refKm}`, {
      id_peraturan: id,
      id_peraturan_terkait: idPer,
    });
  } else if (refKm == 3) {
    return axios.post(`${BACKEND_URL}/api/kmperaturanterkait/${refKm}`, {
      id_kajian: id,
      id_peraturan_terkait: idPer,
    });
  } else if (refKm == 4) {
    return axios.post(`${BACKEND_URL}/api/kmperaturanterkait/${refKm}`, {
      id_putusan: id,
      id_peraturan_terkait: idPer,
    });
  }
}

export function getHistoryStatusKM(idRefKm, idKm) {
  return axios.get(`${BACKEND_URL}/api/logstatuskm/${idRefKm}/${idKm}`);
}

export function getTopikById(id) {
  return axios.get(`${BACKEND_URL}/api/reftopik/${id}`);
}

export function getJnsPeraturanById(id) {
  return axios.get(`${BACKEND_URL}/api/jnsperaturan/${id}`);
}

export function getDokumenPer(id) {
  return axios.get(`${BACKEND_URL}/api/kmdoklainnya/per/${id}`);
}

export function saveDokLainPeraturan(file_upload, id_per, keterangan) {
  return axios.post(`${BACKEND_URL}/api/kmdoklainnya/1`, {
    file_upload,
    id_per,
    keterangan,
  });
}

export function deleteDokLain(id) {
  return axios.delete(`${BACKEND_URL}/api/kmdoklainnya/${id}`);
}

export function updateBody(id, body) {
  return axios.put(`${BACKEND_URL}/api/kmregulasiperpajakan/updatebody/${id}`, {
    body: body,
  });
}

//KAJIANNN PROPOSALLLLLLL

export function getKajianNonTerimaEs4(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmhslkajian/nonterima/es4/${kdKantor}/${kdUnitOrg}`
  );
}

export function getKajianNonTerimaEs3(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmhslkajian/nonterima/es3/${kdKantor}/${kdUnitOrg}`
  );
}

export function getKajianSearchEselon4(kdKantor, kdUnitOrgEs4, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmhslkajian/search?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&status=${status}`
  );
}

export function getKajianSearchEselon3(kdKantor, kdUnitOrgEs3, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmhslkajian/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrgEs3}&status=${status}`
  );
}

export function updateStatusKajian(
  id,
  statusNow,
  statusNext,
  nip,
  alasan = null
) {
  if (statusNext == 2) {
    return axios.put(
      `${BACKEND_URL}/api/kmhslkajian/updatekmhasilkajianstatus/${id}/${statusNext}`,
      {
        nip_pengusul: nip,
      }
    );

    // return axios.put(`kmhslkajian/updatekmhasilkajianstatus/${id}/${status}`, {
    //   alasan_tolak: alasan,

    // });
  } else if (statusNext == 3) {
    return axios.put(
      `${BACKEND_URL}/api/kmhslkajian/updatekmhasilkajianstatus/${id}/${statusNext}`,
      {
        nip_pjbt_es4: nip,
      }
    );
  } else if (statusNext == 5) {
    if (statusNow == 2) {
      return axios.put(
        `${BACKEND_URL}/api/kmhslkajian/updatekmhasilkajianstatus/${id}/${statusNext}`,
        {
          nip_pjbt_es4: nip,
          alasan_tolak: alasan,
        }
      );
    } else if (statusNow == 3) {
      return axios.put(
        `${BACKEND_URL}/api/kmhslkajian/updatekmhasilkajianstatus/${id}/${statusNext}`,
        {
          nip_pjbt_es3: nip,
          alasan_tolak: alasan,
        }
      );
    }
  } else if (statusNext == 6) {
    return axios.put(
      `${BACKEND_URL}/api/kmhslkajian/updatekmhasilkajianstatus/${id}/${statusNext}`,
      {
        nip_pjbt_es3: nip,
      }
    );
  } else if (statusNext == 1) {
    return axios.put(
      `${BACKEND_URL}/api/kmhslkajian/updatekmhasilkajianstatus/${id}/${statusNext}`,
      {
        nip_pengusul: nip,
      }
    );
  }
}

export function getKajianNonTerima() {
  return axios.get(`${BACKEND_URL}/api/kmhslkajian/nonterima`);
}

export function getKajianNonTerimaByNip(nip) {
  return axios.get(`${BACKEND_URL}/api/kmhslkajian/nonterima/nip/${nip}`);
}

export function getKajianById(id) {
  return axios.get(`${BACKEND_URL}/api/kmhslkajian/${id}`);
}

export function deleteKajianById(id) {
  return axios.delete(`${BACKEND_URL}/api/kmhslkajian/${id}`);
}

// export function updateStatusKajian(id, status, alasan = null) {
//   if (alasan) {
//     return axios.put(`${BACKEND_URL}/api/kmhslkajian/updatekmhasilkajianstatus/${id}/${status}`, {
//       alasan_tolak: alasan,
//     });
//   } else {
//     return axios.put(
//       `${BACKEND_URL}/api/kmhslkajian/updatekmhasilkajianstatus/${id}/${status}`,
//       {}
//     );
//   }
// }

export function saveKajian(
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  perihal,
  status,
  tgl_kajian,
  unit_kajian
) {
  return axios.post(`${BACKEND_URL}/api/kmhslkajian/`, {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    perihal,
    status,
    tgl_kajian,
    unit_kajian,
  });
}

export function updateKajian(
  id_kajian,
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  perihal,
  status,
  tgl_kajian,
  unit_kajian
) {
  return axios.put(`${BACKEND_URL}/api/kmhslkajian/${id_kajian}`, {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    perihal,
    status,
    tgl_kajian,
    unit_kajian,
  });
}

export function getKajianByKantor(kdKantor) {
  return axios.get(
    `${BACKEND_URL}/api/kmhslkajian/search/?kdKantor=${kdKantor}`
  );
}

export function getJenisKajian() {
  return axios.get(`${BACKEND_URL}/api/detiljenis/byidjenis/2`);
}

export function getDitPembuat() {
  return axios.get(`${BACKEND_URL}/api/detiljenis/byidjenis/8`);
}

export function getDokumenKajian(id) {
  return axios.get(`${BACKEND_URL}/api/kmdoklainnya/kajian/${id}`);
}

export function getPeraturanTerkaitKajian(id) {
  return axios.get(`${BACKEND_URL}/api/kmperaturanterkait/kajian/${id}`);
}

export function saveDokLainKajian(file_upload, id_kajian, keterangan) {
  return axios.post(`${BACKEND_URL}/api/kmdoklainnya/3`, {
    file_upload,
    id_kajian,
    keterangan,
  });
}

export function getKajianTerima() {
  return axios.get(`${BACKEND_URL}/api/kmhslkajian/terima`);
}

export function applyKodefikasi(id) {
  return axios.put(
    `${BACKEND_URL}/api/kmkodefikasi/updatekmkodefikasistatus/${id}/2`,
    {}
  );
}

export function getKodefikasiByKantor(kdKantor) {
  return axios.get(
    `${BACKEND_URL}/api/kmkodefikasi/search/?kdKantor=${kdKantor}`
  );
}

export function getKodefikasiProposal(nip, kdKantor, kdUnitOrgEs4, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmkodefikasi/search/?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&nip=${nip}&status=${status}`
  );
}

export function getKodefikasiSearchDraft(nip, kdKantor, kdUnitOrgEs4, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmkodefikasi/search/?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&nip=${nip}&status=${status}`
  );
}

export function getKodefikasiSearchEselon4(kdKantor, kdUnitOrgEs4, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmkodefikasi/search?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&status=${status}`
  );
}

export function getKodefikasiSearchEselon3(kdKantor, kdUnitOrgEs3, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmkodefikasi/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrgEs3}&status=${status}`
  );
}

export function getKodefikasi() {
  return axios.get(`${BACKEND_URL}/api/kmkodefikasi/nonterima`);
}

export function getKodefikasiNonTerimaEs4(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmkodefikasi/nonterima/es4/${kdKantor}/${kdUnitOrg}`
  );
}

export function getKodefikasiNonTerimaEs3(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmkodefikasi/nonterima/es3/${kdKantor}/${kdUnitOrg}`
  );
}
export function getKodefikasiNonTerimaByKantor(kdKantor) {
  return axios.get(
    `${BACKEND_URL}/api/kmkodefikasi/nonterima/kantor/${kdKantor}`
  );
}

export function getKodefikasiNonTerimaByNip(nip) {
  return axios.get(`${BACKEND_URL}/api/kmkodefikasi/nonterima/nip/${nip}`);
}

export function getKodefikasiTerima() {
  return axios.get(`${BACKEND_URL}/api/kmkodefikasi/terima`);
}

export function getKodefikasiTerimaEs3(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmkodefikasi/terima/es3/${kdKantor}/${kdUnitOrg}`
  );
}

export function getKodefikasiTerimaEs4(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmkodefikasi/terima/es4/${kdKantor}/${kdUnitOrg}`
  );
}

export function getKodefikasiTerimaByKantor(kdKantor) {
  return axios.get(`${BACKEND_URL}/api/kmkodefikasi/terima/kantor/${kdKantor}`);
}

export function getKodefikasiTerimaByNip(nip) {
  return axios.get(`${BACKEND_URL}/api/kmkodefikasi/terima/nip/${nip}`);
}

// export function getKodefikasiKonseptorNip(KdKantor) {
//   return axios.get(
//     `${BACKEND_URL}/apikmkodefikasi/search?kdKantor={$kdKantor}&kdUnitOrgEs3=3321123&nip=815100735&status=Draft`
//   );
// }

export function getKodefikasiById(id) {
  return axios.get(`${BACKEND_URL}/api/kmkodefikasi/${id}`);
}

export function deleteKodefikasi(id_kodefikasi) {
  return axios.delete(`${BACKEND_URL}/api/kmkodefikasi/${id_kodefikasi}`);
}
export function updateStatusKodefikasi(
  id,
  statusNow,
  statusNext,
  nip,
  alasan = null
) {
  if (statusNext == 2) {
    return axios.put(
      `${BACKEND_URL}/api/kmkodefikasi/updatekmkodefikasistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );

    // return axios.put(`${BACKEND_URL}/api/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`, {
    //   alasan_tolak: alasan,

    // });
  } else if (statusNext == 3) {
    return axios.put(
      `${BACKEND_URL}/api/kmkodefikasi/updatekmkodefikasistatus/${id}/${statusNext}`,
      { nip_pjbt_es4: nip }
    );
  } else if (statusNext == 5) {
    if (statusNow == 2) {
      return axios.put(
        `${BACKEND_URL}/api/kmkodefikasi/updatekmkodefikasistatus/${id}/${statusNext}`,
        { nip_pjbt_es4: nip, alasan_tolak: alasan }
      );
    } else if (statusNow == 3) {
      return axios.put(
        `${BACKEND_URL}/api/kmkodefikasi/updatekmkodefikasistatus/${id}/${statusNext}`,
        { nip_pjbt_es3: nip, alasan_tolak: alasan }
      );
    }
  } else if (statusNext == 6) {
    return axios.put(
      `${BACKEND_URL}/api/kmkodefikasi/updatekmkodefikasistatus/${id}/${statusNext}`,
      { nip_pjbt_es3: nip }
    );
  } else if (statusNext == 1) {
    return axios.put(
      `${BACKEND_URL}/api/kmkodefikasi/updatekmkodefikasistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );
  }
}

export function deleteStatusById(id) {
  return axios.delete(`${BACKEND_URL}/api/kmkodefikasi/${id}`);
}

export function getJenisPajakById(id) {
  return axios.get(`${BACKEND_URL}/api/jnspajak/${id}`);
}

export function getJenisPajak() {
  return axios.get(`${BACKEND_URL}/api/jnspajak/`);
}
export function saveKodefikasi(
  alasan_tolak,
  body,
  file_upload,
  id_jnspajak,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_nd,
  perihal,
  status,
  tgl_nd
) {
  return axios.post(`${BACKEND_URL}/api/kmkodefikasi/`, {
    alasan_tolak,
    body,
    file_upload,
    id_jnspajak,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_nd,
    perihal,
    status,
    tgl_nd,
  });
}

export function updateKodefikasi(
  id,
  alasan_tolak,
  body,
  file_upload,
  id_jnspajak,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_nd,
  perihal,
  status,
  tgl_nd
) {
  return axios.put(`${BACKEND_URL}/api/kmkodefikasi/${id}`, {
    alasan_tolak,
    body,
    file_upload,
    id_jnspajak,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_nd,
    perihal,
    status,
    tgl_nd,
  });
}

export function saveDraftComposing(
  body_draft,
  jns_draft,
  id_penyusunan,
  fileupload,
  filelampiranr
) {
  return axios.post(`${BACKEND_URL}/api/draftperaturan/`, {
    body_draft,
    jns_draft,
    id_penyusunan,
    fileupload,
    filelampiranr,
  });
}

export function updateDraftComposing(
  body_draft,
  id_draft,
  fileupload,
  filelampiranr,
  keterangan = 1
) {
  return axios.put(
    `${BACKEND_URL}/api/draftperaturan/${id_draft}/${keterangan}`,
    {
      body_draft,
      fileupload,
      filelampiranr,
    }
  );
}

export function getDraft(id_draft) {
  return axios.get(`${BACKEND_URL}/api/draftperaturan/${id_draft}`);
}

export function saveBodyRegulasi(id, body) {
  return axios.put(`${BACKEND_URL}/api/kmregulasiperpajakan/updatebody/${id}`, {
    body: body,
  });
}

export function saveBodyKajian(id, body) {
  return axios.put(`${BACKEND_URL}/api/kmhslkajian/updatebody/${id}`, {
    body: body,
  });
}

export function saveBodyKodefikasi(id, body) {
  return axios.put(`${BACKEND_URL}/api/kmkodefikasi/updatebody/${id}`, {
    body: body,
  });
}

// /${BACKEND_URL}/apiFGD

export function saveBodyFgd(id, body) {
  return axios.put(`${BACKEND_URL}/api/kmfgd/updatebody/${id}`, {
    body: body,
  });
}

export function updateStatusFgd(id, statusNow, statusNext, nip, alasan = null) {
  if (statusNext == 2) {
    return axios.put(
      `${BACKEND_URL}/api/kmfgd/updatekmfgdstatus/${id}/${statusNext}`,
      {
        nip_pengusul: nip,
      }
    );

    // return axios.put(`${BACKEND_URL}/api/kmfgd/updatekmfgdstatus/${id}/${status}`, {
    //   alasan_tolak: alasan,

    // });
  } else if (statusNext == 3) {
    return axios.put(
      `${BACKEND_URL}/api/kmfgd/updatekmfgdstatus/${id}/${statusNext}`,
      {
        nip_pjbt_es4: nip,
      }
    );
  } else if (statusNext == 5) {
    if (statusNow == 2) {
      return axios.put(
        `${BACKEND_URL}/api/kmfgd/updatekmfgdstatus/${id}/${statusNext}`,
        {
          nip_pjbt_es4: nip,
          alasan_tolak: alasan,
        }
      );
    } else if (statusNow == 3) {
      return axios.put(
        `${BACKEND_URL}/api/kmfgd/updatekmfgdstatus/${id}/${statusNext}`,
        {
          nip_pjbt_es3: nip,
          alasan_tolak: alasan,
        }
      );
    }
  } else if (statusNext == 6) {
    return axios.put(
      `${BACKEND_URL}/api/kmfgd/updatekmfgdstatus/${id}/${statusNext}`,
      {
        nip_pjbt_es3: nip,
      }
    );
  } else if (statusNext == 1) {
    return axios.put(
      `${BACKEND_URL}/api/kmfgd/updatekmfgdstatus/${id}/${statusNext}`,
      {
        nip_pengusul: nip,
      }
    );
  }
}

export function getFgdByKantor(kdKantor) {
  return axios.get(`${BACKEND_URL}/api/kmfgd/search/?kdKantor=${kdKantor}`);
}

export function getFgdNonTerimaEs4(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmfgd/nonterima/es4/${kdKantor}/${kdUnitOrg}`
  );
}

export function getFgdNonTerimaEs3(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmfgd/nonterima/es3/${kdKantor}/${kdUnitOrg}`
  );
}
export function getFgdNonTerimaByNip(nip) {
  return axios.get(`${BACKEND_URL}/api/kmfgd/nonterima/nip/${nip}`);
}

export function getFgdTerima() {
  return axios.get(`${BACKEND_URL}/api/kmfgd/terima`);
}
export function getFgdById(id) {
  return axios.get(`${BACKEND_URL}/api/kmfgd/${id}`);
}

export function deleteFgd(id) {
  return axios.delete(`${BACKEND_URL}/api/kmfgd/${id}`);
}

export function saveFgd(
  alasan_tolak,
  body,
  file_upload,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_nd,
  perihal,
  status,
  tgl_nd,
  deskripsi
) {
  return axios.post(`${BACKEND_URL}/api/kmfgd/`, {
    alasan_tolak,
    body,
    file_upload,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_nd,
    perihal,
    status,
    tgl_nd,
    deskripsi
  });
}

export function updateFgd(
  id,
  alasan_tolak,
  body,
  file_upload,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_nd,
  perihal,
  status,
  tgl_nd,
  deskripsi
) {
  return axios.put(`${BACKEND_URL}/api/kmfgd/${id}`, {
    alasan_tolak,
    body,
    file_upload,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_nd,
    perihal,
    status,
    tgl_nd,
    deskripsi
  });
}

export function getPutusanNonTerima() {
  return axios.get(`${BACKEND_URL}/api/kmputusanpengadilan/nonterima`);
}

export function getPutusanNonTerimaByNip(nip) {
  return axios.get(
    `${BACKEND_URL}/api/kmputusanpengadilan/nonterima/nip/${nip}`
  );
}

export function getPutusanNonTerimaByKantor(kdKantor) {
  return axios.get(
    `${BACKEND_URL}/api/kmputusanpengadilan/nonterima/kantor/${kdKantor}`
  );
}

export function getPutusanNonTerimaByEs4(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmputusanpengadilan/nonterima/es4/${kdKantor}/${kdUnitOrg}`
  );
}

export function getPutusanNonTerimaByEs3(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmputusanpengadilan/nonterima/es3/${kdKantor}/${kdUnitOrg}`
  );
}

export function getPutusanSearchEselon3(kdKantor, kdUnitOrg, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmputusanpengadilan/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrg}&status=${status}`
  );
}

export function getPutusanSearchKantor(kdKantor) {
  return axios.get(
    `${BACKEND_URL}/api/kmputusanpengadilan/search?kdKantor=${kdKantor}`
  );
}

export function getPutusanById(id) {
  return axios.get(`${BACKEND_URL}/api/kmputusanpengadilan/${id}`);
}

export function deletePutusanById(id) {
  return axios.delete(`${BACKEND_URL}/api/kmputusanpengadilan/${id}`);
}

export function updateStatusPutusan(
  id,
  statusNow,
  statusNext,
  nip,
  alasan = null
) {
  if (statusNext == 2) {
    return axios.put(
      `${BACKEND_URL}/api/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );

    // return axios.put(`${BACKEND_URL}/api/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`, {
    //   alasan_tolak: alasan,

    // });
  } else if (statusNext == 3) {
    return axios.put(
      `${BACKEND_URL}/api/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${statusNext}`,
      { nip_pjbt_es4: nip }
    );
  } else if (statusNext == 5) {
    if (statusNow == 2) {
      return axios.put(
        `${BACKEND_URL}/api/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${statusNext}`,
        { nip_pjbt_es4: nip, alasan_tolak: alasan }
      );
    } else if (statusNow == 3) {
      return axios.put(
        `${BACKEND_URL}/api/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${statusNext}`,
        { nip_pjbt_es3: nip, alasan_tolak: alasan }
      );
    }
  } else if (statusNext == 6) {
    return axios.put(
      `${BACKEND_URL}/api/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${statusNext}`,
      { nip_pjbt_es3: nip }
    );
  } else if (statusNext == 1) {
    return axios.put(
      `${BACKEND_URL}/api/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );
  }
}

export function savePutusan(
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_putusan,
  perihal,
  status,
  tgl_putusan
) {
  return axios.post(`${BACKEND_URL}/api/kmputusanpengadilan/`, {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_putusan,
    perihal,
    status,
    tgl_putusan,
  });
}

export function updatePutusan(
  id,
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_putusan,
  perihal,
  status,
  tgl_putusan
) {
  return axios.put(`${BACKEND_URL}/api/kmputusanpengadilan/${id}`, {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_putusan,
    perihal,
    status,
    tgl_putusan,
  });
}

export function saveBodyPutusan(id, body) {
  return axios.put(`${BACKEND_URL}/api/kmputusanpengadilan/updatebody/${id}`, {
    body: body,
  });
}

export function getJenisPengadilan() {
  return axios.get(`${BACKEND_URL}/api/detiljenis/byidjenis/3`);
}

export function getDokumenPutusan(id) {
  return axios.get(`${BACKEND_URL}/api/kmdoklainnya/putusan/${id}`);
}

export function getPeraturanTerkaitPutusan(id) {
  return axios.get(`${BACKEND_URL}/api/kmperaturanterkait/putusan/${id}`);
}

export function getPutusanTerima() {
  return axios.get(`${BACKEND_URL}/api/kmputusanpengadilan/terima`);
}

export function getPutusanTerimaByNip(nip) {
  return axios.get(`${BACKEND_URL}/api/kmputusanpengadilan/terima/nip/${nip}`);
}

export function getPutusanTerimaByKantor(kdKantor) {
  return axios.get(
    `${BACKEND_URL}/api/kmputusanpengadilan/terima/kantor/${kdKantor}`
  );
}

export function getPutusanTerimaByEs4(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmputusanpengadilan/terima/es4/${kdKantor}/${kdUnitOrg}`
  );
}

export function getPutusanTerimaByEs3(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmputusanpengadilan/terima/es3/${kdKantor}/${kdUnitOrg}`
  );
}

export function getPenegasanSearchEselon4(kdKantor, kdUnitOrgEs4, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmsuratpenegasan/search?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&status=${status}`
  );
}

export function getPenegasanSearchEselon3(kdKantor, kdUnitOrgEs3, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmsuratpenegasan/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrgEs3}&status=${status}`
  );
}

export function getPenegasanSearchKantor(kdKantor) {
  return axios.get(
    `${BACKEND_URL}/api/kmsuratpenegasan/search?kdKantor=${kdKantor}`
  );
}

export function getPenegasanSearchTolak(nip) {
  return axios.get(
    `${BACKEND_URL}/api/kmsuratpenegasan/search?nip=${nip}&status=Tolak`
  );
}

export function getPenegasanByKantor(kdKantor) {
  return axios.get(
    `${BACKEND_URL}/api/kmsuratpenegasan/all/kantor/${kdKantor}`
  );
}

export function getPenegasanNonTerima() {
  return axios.get(`${BACKEND_URL}/api/kmsuratpenegasan/nonterima`);
}

export function getPenegasanNonTerimaByNip(nip) {
  return axios.get(`${BACKEND_URL}/api/kmsuratpenegasan/nonterima/nip/${nip}`);
}

export function getPenegasanNonTerimaByKantor(kdKantor) {
  return axios.get(
    `${BACKEND_URL}/api/kmsuratpenegasan/nonterima/kantor/${kdKantor}`
  );
}

export function getPenegasanNonTerimaByEs4(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmsuratpenegasan/nonterima/es4/${kdKantor}/${kdUnitOrg}`
  );
}

export function getPenegasanNonTerimaByEs3(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmsuratpenegasan/nonterima/es3/${kdKantor}/${kdUnitOrg}`
  );
}

export function getPenegasanTerima() {
  return axios.get(`${BACKEND_URL}/api/kmsuratpenegasan/terima`);
}

export function getPenegasanTerimaByNip(nip) {
  return axios.get(`${BACKEND_URL}/api/kmsuratpenegasan/terima/nip/${nip}`);
}

export function getPenegasanTerimaByKantor(kdKantor) {
  return axios.get(
    `${BACKEND_URL}/api/kmsuratpenegasan/terima/kantor/${kdKantor}`
  );
}

export function getPenegasanTerimaByEs4(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmsuratpenegasan/terima/es4/${kdKantor}/${kdUnitOrg}`
  );
}

export function getPenegasanTerimaByEs3(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmsuratpenegasan/terima/es3/${kdKantor}/${kdUnitOrg}`
  );
}

export function geturatPenegasanByNoSuratpenegasan(no_suratpenegasan) {
  return axios.get(
    `${BACKEND_URL}/api/kmsuratpenegasan/byno_suratpenegasan/${no_suratpenegasan}`
  );
}
export function getPenegasanById(id) {
  return axios.get(`${BACKEND_URL}/api/kmsuratpenegasan/${id}`);
}

export function updateStatusPenegasan(
  id,
  statusNow,
  statusNext,
  nip,
  alasan = null
) {
  if (statusNext == 2) {
    return axios.put(
      `${BACKEND_URL}/api/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );

    // return axios.put(`${BACKEND_URL}/api/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`, {
    //   alasan_tolak: alasan,

    // });
  } else if (statusNext == 3) {
    return axios.put(
      `${BACKEND_URL}/api/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${statusNext}`,
      { nip_pjbt_es4: nip }
    );
  } else if (statusNext == 5) {
    if (statusNow == 2) {
      return axios.put(
        `${BACKEND_URL}/api/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${statusNext}`,
        { nip_pjbt_es4: nip, alasan_tolak: alasan }
      );
    } else if (statusNow == 3) {
      return axios.put(
        `${BACKEND_URL}/api/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${statusNext}`,
        { nip_pjbt_es3: nip, alasan_tolak: alasan }
      );
    }
  } else if (statusNext == 6) {
    return axios.put(
      `${BACKEND_URL}/api/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${statusNext}`,
      { nip_pjbt_es3: nip }
    );
  } else if (statusNext == 1) {
    return axios.put(
      `${BACKEND_URL}/api/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );
  }
}

export function savePenegasan(
  alasan_tolak,
  body,
  file_upload,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_suratpenegasan,
  perihal,
  status,
  tgl_suratpenegasan,
  jns_pajak
) {
  return axios.post(`${BACKEND_URL}/api/kmsuratpenegasan/`, {
    alasan_tolak,
    body,
    file_upload,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_suratpenegasan,
    perihal,
    status,
    tgl_suratpenegasan,
    jns_pajak
  });
}

export function updatePenegasan(
  id,
  alasan_tolak,
  body,
  file_upload,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_suratpenegasan,
  perihal,
  status,
  tgl_suratpenegasan,
  jns_pajak
) {
  return axios.put(`${BACKEND_URL}/api/kmsuratpenegasan/${id}`, {
    alasan_tolak,
    body,
    file_upload,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_suratpenegasan,
    perihal,
    status,
    tgl_suratpenegasan,
    jns_pajak
  });
}

export function saveBodyPenegasan(id, body) {
  return axios.put(`${BACKEND_URL}/api/kmsuratpenegasan/updatebody/${id}`, {
    body: body,
  });
}

export function deleteSuratPenegasanById(id) {
  return axios.delete(`${BACKEND_URL}/api/kmsuratpenegasan/${id}`);
}

export function getRekomendasiSearchEselon4(kdKantor, kdUnitOrgEs4, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmrekomendasi/search?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&status=${status}`
  );
}

export function getRekomendasiSearchEselon3(kdKantor, kdUnitOrgEs3, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmrekomendasi/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrgEs3}&status=${status}`
  );
}

export function getRekomendasiNonTerima() {
  return axios.get(`${BACKEND_URL}/api/kmrekomendasi/nonterima/`);
}

export function getRekomendasiNonTerimaByNip(nip) {
  return axios.get(`${BACKEND_URL}/api/kmrekomendasi/nonterima/nip/${nip}`);
}

export function getRekomendasiNonTerimaEs4(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmrekomendasi/nonterima/es4/${kdKantor}/${kdUnitOrg}`
  );
}

export function getRekomendasiTerima() {
  return axios.get(`${BACKEND_URL}/api/kmrekomendasi/terima/`);
}

export function getRekomendasiSearchKantor(kdKantor) {
  return axios.get(
    `${BACKEND_URL}/api/kmrekomendasi/search?kdKantor=${kdKantor}`
  );
}

export function getRekomendasiById(id) {
  return axios.get(`${BACKEND_URL}/api/kmrekomendasi/${id}`);
}

export function updateStatusRekomendasi(
  id,
  statusNow,
  statusNext,
  nip,
  alasan = null
) {
  if (statusNext == 2) {
    return axios.put(
      `${BACKEND_URL}/api/kmrekomendasi/updatekmrekomendasistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );

    // return axios.put(`${BACKEND_URL}/api/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`, {
    //   alasan_tolak: alasan,

    // });
  } else if (statusNext == 3) {
    return axios.put(
      `${BACKEND_URL}/api/kmrekomendasi/updatekmrekomendasistatus/${id}/${statusNext}`,
      { nip_pjbt_es4: nip }
    );
  } else if (statusNext == 5) {
    if (statusNow == 2) {
      return axios.put(
        `${BACKEND_URL}/api/kmrekomendasi/updatekmrekomendasistatus/${id}/${statusNext}`,
        { nip_pjbt_es4: nip, alasan_tolak: alasan }
      );
    } else if (statusNow == 3) {
      return axios.put(
        `${BACKEND_URL}/api/kmrekomendasi/updatekmrekomendasistatus/${id}/${statusNext}`,
        { nip_pjbt_es3: nip, alasan_tolak: alasan }
      );
    }
  } else if (statusNext == 6) {
    return axios.put(
      `${BACKEND_URL}/api/kmrekomendasi/updatekmrekomendasistatus/${id}/${statusNext}`,
      { nip_pjbt_es3: nip }
    );
  } else if (statusNext == 1) {
    return axios.put(
      `${BACKEND_URL}/api/kmrekomendasi/updatekmrekomendasistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );
  }
}

export function saveRekomendasi(
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_hsl_pemeriksaan,
  perihal,
  status,
  tgl_pemeriksaan,
  thn_periksa
) {
  return axios.post(`${BACKEND_URL}/api/kmrekomendasi/`, {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_hsl_pemeriksaan,
    perihal,
    status,
    tgl_pemeriksaan,
    thn_periksa,
  });
}

export function getJenisPemeriksaan() {
  return axios.get(`${BACKEND_URL}/api/detiljenis/byidjenis/4`);
}

export function updateRekomendasi(
  id,
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_hsl_pemeriksaan,
  perihal,
  status,
  tgl_pemeriksaan,
  thn_periksa
) {
  return axios.put(`${BACKEND_URL}/api/kmrekomendasi/${id}`, {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_hsl_pemeriksaan,
    perihal,
    status,
    tgl_pemeriksaan,
    thn_periksa,
  });
}

export function saveBodyRekomendasi(id, body) {
  return axios.put(`${BACKEND_URL}/api/kmrekomendasi/updatebody/${id}`, {
    body: body,
  });
}

export function deleteRekomendasiById(id) {
  return axios.delete(`${BACKEND_URL}/api/kmrekomendasi/${id}`);
}

export function getAudiensiSearchEselon4(kdKantor, kdUnitOrgEs4, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmaudiensi/search?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&status=${status}`
  );
}

export function getAudiensiSearchEselon3(kdKantor, kdUnitOrgEs3, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmaudiensi/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrgEs3}&status=${status}`
  );
}

export function getAudiensiNonTerima() {
  return axios.get(`${BACKEND_URL}/api/kmaudiensi/nonterima`);
}

export function getAudiensiByKantor(kdKantor) {
  return axios.get(`${BACKEND_URL}/api/kmaudiensi/search?kdKantor=${kdKantor}`);
}

export function getAudiensiNonTerimaEs4(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmaudiensi/nonterima/es4/${kdKantor}/${kdUnitOrg}`
  );
}

export function getAudiensiNonTerimaByNip(nip) {
  return axios.get(`${BACKEND_URL}/api/kmaudiensi/nonterima/nip/${nip}`);
}

export function getAudiensiTerima() {
  return axios.get(`${BACKEND_URL}/api/kmaudiensi/terima`);
}

export function getJenisAudiensi() {
  return axios.get(`${BACKEND_URL}/api/detiljenis/byidjenis/9`);
}

export function getAudiensiById(id) {
  return axios.get(`${BACKEND_URL}/api/kmaudiensi/${id}`);
}

export function updateStatusAudiensi(
  id,
  statusNow,
  statusNext,
  nip,
  alasan = null
) {
  if (statusNext == 2) {
    return axios.put(
      `${BACKEND_URL}/api/kmaudiensi/updatekmaudiensistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );

    // return axios.put(`${BACKEND_URL}/api/kmaudiensi/updatekmaudiensistatus/${id}/${status}`, {
    //   alasan_tolak: alasan,

    // });
  } else if (statusNext == 3) {
    return axios.put(
      `${BACKEND_URL}/api/kmaudiensi/updatekmaudiensistatus/${id}/${statusNext}`,
      { nip_pjbt_es4: nip }
    );
  } else if (statusNext == 5) {
    if (statusNow == 2) {
      return axios.put(
        `${BACKEND_URL}/api/kmaudiensi/updatekmaudiensistatus/${id}/${statusNext}`,
        { nip_pjbt_es4: nip, alasan_tolak: alasan }
      );
    } else if (statusNow == 3) {
      return axios.put(
        `${BACKEND_URL}/api/kmaudiensi/updatekmaudiensistatus/${id}/${statusNext}`,
        { nip_pjbt_es3: nip, alasan_tolak: alasan }
      );
    }
  } else if (statusNext == 6) {
    return axios.put(
      `${BACKEND_URL}/api/kmaudiensi/updatekmaudiensistatus/${id}/${statusNext}`,
      { nip_pjbt_es3: nip }
    );
  } else if (statusNext == 1) {
    return axios.put(
      `${BACKEND_URL}/api/kmaudiensi/updatekmaudiensistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );
  }
}

export function deleteAudiensiById(id) {
  return axios.delete(`${BACKEND_URL}/api/kmaudiensi/${id}`);
}

export function saveAudiensi(
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_nd,
  perihal,
  status,
  tgl_nd,
  deskripsi,
  jns_pajak
) {
  return axios.post(`${BACKEND_URL}/api/kmaudiensi/`, {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_nd,
    perihal,
    status,
    tgl_nd,
    deskripsi,
    jns_pajak
  });
}

export function updateAudiensi(
  id,
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_nd,
  perihal,
  status,
  tgl_nd,
  deskripsi,
  jns_pajak
) {
  return axios.put(`${BACKEND_URL}/api/kmaudiensi/${id}`, {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_nd,
    perihal,
    status,
    tgl_nd,
    deskripsi,
    jns_pajak
  });
}

export function saveBodyAudiensi(id, body) {
  return axios.put(`${BACKEND_URL}/api/kmaudiensi/updatebody/${id}`, {
    body: body,
  });
}

export function getSurveiSearchEselon4(kdKantor, kdUnitOrgEs4, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmsurvei/search?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&status=${status}`
  );
}

export function getSurveiSearchEselon3(kdKantor, kdUnitOrgEs3, status) {
  return axios.get(
    `${BACKEND_URL}/api/kmsurvei/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrgEs3}&status=${status}`
  );
}

export function getSurveiSearchEKantor(kdKantor) {
  return axios.get(`${BACKEND_URL}/api/kmsurvei/search?kdKantor=${kdKantor}`);
}

export function getSurveiNonTerima() {
  return axios.get(`${BACKEND_URL}/api/kmsurvei/nonterima`);
}

export function getSurveiNonTerimaEs4(kdKantor, kdUnitOrg) {
  return axios.get(
    `${BACKEND_URL}/api/kmsurvei/nonterima/es4/${kdKantor}/${kdUnitOrg}`
  );
}

export function getSurveiNonTerimaByNip(nip) {
  return axios.get(`${BACKEND_URL}/api/kmsurvei/nonterima/nip/${nip}`);
}

export function getSurveiByKantor(kdKantor) {
  return axios.get(`${BACKEND_URL}/api/kmsurvei/search?kdKantor=${kdKantor}`);
}

export function getSurveiTerima() {
  return axios.get(`${BACKEND_URL}/api/kmsurvei/terima`);
}

export function getJenisSurvei() {
  return axios.get(`${BACKEND_URL}/api/detiljenis/byidjenis/6`);
}

export function getSurveiById(id) {
  return axios.get(`${BACKEND_URL}/api/kmsurvei/${id}`);
}

export function updateStatusSurvei(
  id,
  statusNow,
  statusNext,
  nip,
  alasan = null
) {
  if (statusNext == 2) {
    return axios.put(
      `${BACKEND_URL}/api/kmsurvei/updatekmsurveistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );

    // return axios.put(`${BACKEND_URL}/api/kmsurvei/updatekmsurveistatus/${id}/${status}`, {
    //   alasan_tolak: alasan,

    // });
  } else if (statusNext == 3) {
    return axios.put(
      `${BACKEND_URL}/api/kmsurvei/updatekmsurveistatus/${id}/${statusNext}`,
      { nip_pjbt_es4: nip }
    );
  } else if (statusNext == 5) {
    if (statusNow == 2) {
      return axios.put(
        `${BACKEND_URL}/api/kmsurvei/updatekmsurveistatus/${id}/${statusNext}`,
        { nip_pjbt_es4: nip, alasan_tolak: alasan }
      );
    } else if (statusNow == 3) {
      return axios.put(
        `${BACKEND_URL}/api/kmsurvei/updatekmsurveistatus/${id}/${statusNext}`,
        { nip_pjbt_es3: nip, alasan_tolak: alasan }
      );
    }
  } else if (statusNext == 6) {
    return axios.put(
      `${BACKEND_URL}/api/kmsurvei/updatekmsurveistatus/${id}/${statusNext}`,
      { nip_pjbt_es3: nip }
    );
  } else if (statusNext == 1) {
    return axios.put(
      `${BACKEND_URL}/api/kmsurvei/updatekmsurveistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );
  }
}

export function deleteSurveiById(id) {
  return axios.delete(`${BACKEND_URL}/api/kmsurvei/${id}`);
}

export function saveSurvei(
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_nd,
  perihal,
  status,
  tgl_nd
) {
  return axios.post(`${BACKEND_URL}/api/kmsurvei/`, {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_nd,
    perihal,
    status,
    tgl_nd,
  });
}

export function updateSurvei(
  id,
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_nd,
  perihal,
  status,
  tgl_nd
) {
  return axios.put(`${BACKEND_URL}/api/kmsurvei/${id}`, {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_nd,
    perihal,
    status,
    tgl_nd,
  });
}

export function saveBodySurvei(id, body) {
  return axios.put(`${BACKEND_URL}/api/kmsurvei/updatebody/${id}`, {
    body: body,
  });
}

export function saveBodyDashboard(id, body) {
  return axios.put(`${BACKEND_URL}/dashboard/updateDashboard/${id}`, {
    body: body,
  });
}

export function getDashboardById(id) {
  return axios.get(`${BACKEND_URL}/dashboard/${id}`);
}
