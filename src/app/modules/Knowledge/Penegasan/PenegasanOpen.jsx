import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { getPenegasanById, updateStatusPenegasan } from "../Api";
import PenegasanReject from "./PenegasanReject";
import { DateFormat } from "../helpers/DateFormat";


function PenegasanOpen({ id, show, onHide }) {

  const history = useHistory();
  const [data, setData] = useState([]);
  const [isReject, setIsReject] = useState(false);
  const [isShow, setIsShow] = useState(true);
  let nextStatus = '';

  useEffect(() => {
    if (id) {
      getPenegasanById(id).then(({ data }) => {
        setData(data)
      })
    }
  }, [id])

  const acceptAction = (statusNow) => {

    if (statusNow == 'Eselon 4') {
      nextStatus = 3;

    } else if (statusNow == 'Eselon 3') {
      nextStatus = 6;

    }

    updateStatusPenegasan(id, nextStatus).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Data berhasil disimpan", "success").then(
          () => {
            history.push("/dashboard");
            history.replace("/knowledge/penegasan");
          }
        );
      } else {
        swal("Gagal", "Data gagal disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/penegasan");
        });
      }
    })
  }

  const handleReject = () => {
    setIsReject(true);
    setIsShow(false);
  }

  const handleCancel = () => {
    setIsReject(false);
    setIsShow(true);
  }

  const reject = (val) => {
    updateStatusPenegasan(data.id_suratpenegasan, 5, val.alasan_penolakan)
      .then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(
            () => {
              history.push("/dashboard");
              history.replace("/knowledge/penegasan");
            }
          );
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace("/knowledge/penegasan");
          });
        }
      })
  }

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detil Surat Penegasan
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Nomor Surat Penegasan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
              {`: ${data.no_suratpenegasan}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Tgl Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
              {`: ${DateFormat(data.tgl_suratpenegasan)}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Perihal</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">{`: ${data.perihal}`}</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">File</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">{`: `}<a href={data.file_upload} target="_blank">download</a></span>
          </div>
        </div>
        {isReject ?
          <PenegasanReject
            handleCancel={handleCancel}
            reject={reject}
          /> : ""}

      </Modal.Body>
      {isShow ? <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={handleReject}
            className="btn btn-danger"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tolak
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={() => acceptAction(data.status)}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fas fa-check"></i>
            Setuju
          </button>
        </div>
      </Modal.Footer> : ""}

    </Modal>
  );
}

export default PenegasanOpen;