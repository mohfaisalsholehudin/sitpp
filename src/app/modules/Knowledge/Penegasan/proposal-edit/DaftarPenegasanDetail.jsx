import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../_metronic/layout";
import DaftarPenegasanDetailForm from "./DaftarPenegasanDetailForm";
import DaftarPenegasanDetailFooter from "./DaftarPenegasanDetailFooter";
import { getPenegasanById } from "../../Api";

const initValues = {
  file_upload: "",
  body: "",
  nip_pengusul: "",
  status: "",
  nip_pjbt_es3: "",
  nip_pjbt_es4: "",
  no_suratpenegasan: "",
  perihal: "",
  tgl_suratpenegasan: "",
  alasan_tolak: "",
};

function DaftarPenegasanDetail({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [penegasanProses, setPenegasanProses] = useState();
  const [PenegasanProsesDetail, setPenegasanDetail] = useState([]);
  let thePath = document.URL;
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);

  useEffect(() => {
      let _title = "Detail Surat Penegasan";
      setTitle(_title);
      suhbeader.setTitle(_title);

      getPenegasanById(id).then(({ data }) => {
        setPenegasanProses({
            id_suratpenegasan: data.id_suratpenegasan,
            no_suratpenegasan: data.no_suratpenegasan,
            tgl_suratpenegasan: data.tgl_suratpenegasan,
            perihal: data.perihal,
            file_upload: data.file_upload,
            status: data.status,
            alasan_tolak: data.alasan_tolak,
            body: data.body,
            nip_pengusul: data.nip_pengusul,
            nip_pjbt_es4: data.nip_pjbt_es4,
            nip_pjbt_es3: data.nip_pjbt_es3,
            wkt_usul: data.wkt_usul,
            wkt_teliti_es4: data.wkt_teliti_es4,
            wkt_teliti_es3: data.wkt_teliti_es3,
            wkt_create: data.wkt_create,
            wkt_update: data.wkt_update
        });
      });
    }, [id, suhbeader]);

  const btnRef = useRef();

  const backToDaftarPenegasanList = (values) => {
    history.push(`/knowledge/penegasan/monitoring`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <DaftarPenegasanDetailForm
            actionsLoading={actionsLoading}
            proposal={penegasanProses || initValues}
            PenegasanDetail={PenegasanProsesDetail}
            idPenegasan={id}
            btnRef={btnRef}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <DaftarPenegasanDetailFooter
          backAction={backToDaftarPenegasanList}
          btnRef={btnRef}
        ></DaftarPenegasanDetailFooter>
      </CardFooter>
    </Card>
  );
}

export default DaftarPenegasanDetail;
