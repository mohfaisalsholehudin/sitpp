import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";
import { Pagination } from "../pagination/Pagination";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import swal from "sweetalert";
import SVG from "react-inlinesvg";
import {
  getPenegasanById,
  deleteSuratPenegasanById,
  updateStatusPenegasan,
  getPenegasanNonTerimaByNip,
  getPenegasanSearchEselon3,
  getPenegasanNonTerimaByEs4,
} from "../Api";
import PenegasanHistory from "./PenegasanHistory";
import { useSelector } from "react-redux";
import PenegasanReject from "./PenegasanReject";

function PenegasanTable() {
  const { role, user } = useSelector((state) => state.auth);
  console.log(role);
  console.log(user);
  const konseptor = role.includes("ROLE_PERATURAN_KONSEPTOR");
  const es4 = role.includes("ROLE_PERATURAN_PENELITI_LVL1");
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const admin = role.includes("ROLE_ADMIN_PERATURAN");
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_suratpenegasan",
    pageNumber: 1,
    pageSize: 50,
  };
  const history = useHistory();

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/penegasan/penelitian/${id}/history`);

  const prosesPenegasan = (id) =>
    history.push(`/knowledge/penegasan/penelitian/${id}/proses`);

  const detailPenegasan = (id) =>
    history.push(`/knowledge/penegasan/penelitian/${id}/detail`);

  const rejectPenegasan = (id) =>
    history.push(`/knowledge/penegasan/penelitian/${id}/reject`);

  const [penegasan, setPenegasan] = useState([]);

  useEffect(() => {
    if (es4) {
      getPenegasanNonTerimaByEs4(
        user.kantorLegacyKode,
        user.unitEs4LegacyKode
      ).then(({ data }) => {
        data.map((data) => {
          return data.status === "Eselon 4"
            ? setPenegasan((penegasan) => [...penegasan, data])
            : data.status === "Eselon 3"
            ? setPenegasan((penegasan) => [...penegasan, data])
            : data.status === "Tolak"
            ? setPenegasan((penegasan) => [...penegasan, data])
            : null;
        });
      });
    }
  }, [es4, user]);

  useEffect(() => {
    if (es3) {
      getPenegasanSearchEselon3(
        user.kantorLegacyKode,
        user.unitEs4LegacyKode,
        "Eselon%203"
      ).then(({ data }) => {
        data.map((dt) => {
          setPenegasan((penegasan) => [...penegasan, dt]);
        });
      });
    }
  }, [es3, user]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "no_suratpenegasan",
      text: "No Surat Penegasan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tgl_suratpenegasan",
      text: "Tgl Surat Penegasan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "perihal",
      text: "Perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        user.jabatan === "Pelaksana"
          ? columnFormatters.PenegasanActionColumnFormatterPelaksana
          : user.jabatan === "Kepala Seksi"
          ? columnFormatters.PenegasanActionColumnFormatterEselon4
          : columnFormatters.PenegasanActionColumnFormatterEselon3,
      formatExtraData: {
        showHistory: showHistoryPengajuan,
        detailPenegasan: detailPenegasan,
        prosesPenegasan: prosesPenegasan,
        rejectPenegasan: rejectPenegasan,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const { SearchBar } = Search;

  const defaultSorted = [{ dataField: "no_perencanaan", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: penegasan.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_suratpenegasan"
                  data={penegasan}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/penegasan/penelitian/:id/reject">
        {({ history, match }) => (
          <PenegasanReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/penegasan/penelitian");
            }}
            onRef={() => {
              history.push("/knowledge/penegasan/penelitian");
            }}
          />
        )}
      </Route>
      <Route path="/knowledge/penegasan/penelitian/:id/history">
        {({ history, match }) => (
          <PenegasanHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/penegasan/penelitian");
            }}
            onRef={() => {
              history.push("/knowledge/penegasan/penelitian");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default PenegasanTable;
