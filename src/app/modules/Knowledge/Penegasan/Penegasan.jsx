import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import PenegasanTable from "./PenegasanTable";

function Penegasan() {
  return (
    <>
      <Card>
        <CardHeader
          title="Penelitian Surat Penegasan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <PenegasanTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Penegasan;
