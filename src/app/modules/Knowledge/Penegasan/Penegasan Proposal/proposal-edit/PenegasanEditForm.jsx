import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
  Checkbox,
} from "../proposal-edit/helpers";
import CustomFileInput from "./helpers/CustomFileInput";
import { getJenisPajak } from "../../../../Evaluation/Api";

function PenegasanEditForm({
  proposal,
  loading,
  btnRef,
  saveProposal,
  backAction,
}) {
  const { user } = useSelector((state) => state.auth);
  const [jenisPajak, setJenisPajak] = useState([]);
  //const validProductValues = products.map(({ _id }) => _id);

  // Validation schema
  const PenegasanEditSchema = Yup.object().shape({
    no_suratpenegasan: Yup.string()
      .min(2, "Minimum 2 karakter")
      .max(50, "Maximum 50 karakter")
      .required("No Surat Penegasan is required"),
    tgl_suratpenegasan: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),
    perihal: Yup.string()
      .min(2, "Minimum 2 karakter")
      .required("Perihal is required"),
    jns_pajak:  Yup.array().min(1, "Jenis Pajak is required").required("Jenis Pajak is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        (value) => value && SUPPORTED_FORMATS.includes(value.type)
      ),
  });

  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(()=> {
    getJenisPajak().then(({ data }) => {
      data.map(data => {
        return data.status === 'AKTIF' ? setJenisPajak(jenisPajak => [...jenisPajak, data]) : null;
      })
    });
  },[])

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={PenegasanEditSchema}
        onSubmit={(values) => {
          //console.log(values);
          saveProposal(values);
        }}
      >
        {({ handleSubmit, setFieldValue, isSubmitting, values }) => {
          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          };

          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    name="no_suratpenegasan"
                    component={Input}
                    placeholder="Nomor Surat Penegasan"
                    label="Nomor Surat Penegasan"
                    // onClick={()=>handleChangeNip()}
                  />
                </div>
                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_suratpenegasan"
                    label="Tanggal Surat"
                  />
                </div>
                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    // onClick={()=>handleChangeKdKantor()}
                  />
                </div>

                 {/* FIELD JENIS PAJAK */}
                 <div className="form-group row align-items-center">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Jenis Pajak
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <div className="checkbox-inline">
                      {jenisPajak.map((data, index) => (
                        <Field
                          component={Checkbox}
                          name="jns_pajak"
                          type="checkbox"
                          value={data.nm_jnspajak}
                          content={data.nm_jnspajak}
                          key={index}
                          // check={proposal.jns_pajak.split(',').includes(data.nm_jnspajak) }
                        />
                      ))}
                    </div>
                  </div>
                </div>

                {/* FIELD UPLOAD FILE */}
                {values.no_suratpenegasan ? (
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Upload File
                    </label>
                    <div className="col-lg-8 col-xl-6">
                      <Field
                        name="file"
                        component={CustomFileInput}
                        title="Select a file"
                        label="File"
                        // setFieldValue={setFieldValue}
                        // // errorMessage={errors["file"] ? errors["file"] : undefined}
                        // // touched={touched["file"]}
                        style={{ display: "flex" }}
                        // onBlur={handleBlur}
                      />
                    </div>
                  </div>
                ) : null}

                {/* Ganti Footer */}
                <div className="col-lg-12" style={{ textAlign: "right" }}>
                  <button
                    type="button"
                    onClick={backAction}
                    className="btn btn-light"
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                    }}
                  >
                    <i className="fa fa-arrow-left"></i>
                    Kembali
                  </button>
                  {`  `}

                  {loading ? (
                    <button
                      type="submit"
                      className="btn btn-success spinner spinner-white spinner-left ml-2"
                      onSubmit={() => handleSubmit()}
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                      disabled={isSubmitting}
                    >
                      <span>Simpan</span>
                    </button>
                  ) : (
                    <button
                      type="submit"
                      className="btn btn-success ml-2"
                      onSubmit={() => handleSubmit()}
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                      disabled={isSubmitting}
                    >
                      <i className="fas fa-save"></i>
                      <span>Simpan</span>
                    </button>
                  )}
                </div>
                {/* <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>  */}
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default PenegasanEditForm;
