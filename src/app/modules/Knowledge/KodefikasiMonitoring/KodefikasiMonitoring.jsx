import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import KodefikasiMonitoringTable from "./KodefikasiMonitoringTable";

function KodefikasiMonitoring() {
  return (
    <>
      <Card>
        <CardHeader
          title="Monitoring Kodefikasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          {/* <ul className="nav nav-tabs nav-line-tabs nav-line-tabs-2x mb-5 fs-6">
            <li className="nav-item">
              <a
                className="nav-link active"
                data-bs-toggle="tab"
                href="#kt_tab_pane_4"
              >
                Kantor
              </a>
            </li>
          </ul> */}
          <KodefikasiMonitoringTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default KodefikasiMonitoring;
