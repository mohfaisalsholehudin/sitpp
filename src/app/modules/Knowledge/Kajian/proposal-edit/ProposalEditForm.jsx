import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
  Checkbox,
} from "../proposal-edit/helpers";
import CustomFileInput from "./helpers/CustomFileInput";
import { getJenisKajian, getDitPembuat } from "../../Api";

function ProposalEditForm({
  proposal,
  btnRef,
  saveProposal,
  loading,
  backAction,
}) {
  const [jenisKajian, setJenisKajian] = useState([]);
  const [direktoratPembuat, setDitPembuat] = useState([]);
  const { user } = useSelector((state) => state.auth);

  //const validProductValues = products.map(({ _id }) => _id);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    tgl_kajian: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),

    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      .required("Perihal is required"),

    unit_kajian: Yup.string().required("Unit Kajian is required"),
    id_detiljns: Yup.string().required("Jenis Kajian is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        (value) => value && SUPPORTED_FORMATS.includes(value.type)
      ),
  });

  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    getJenisKajian().then(({ data }) => {
      setJenisKajian(data);
    });

    getDitPembuat().then(({ data }) => {
      setDitPembuat(data);
    });
  }, []);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          //console.log(values);
          saveProposal(values);
        }}
      >
        {({ handleSubmit, setFieldValue, isSubmitting, values }) => {
          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          };

          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField name="tgl_kajian" label="Tanggal Kajian" />
                </div>
                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="id_detiljns" label="Jenis Kajian">
                    <option value="">Pilih Jenis Kajian</option>
                    {jenisKajian.map((data, index) => (
                      <option key={index} value={data.id}>
                        {data.nama}
                      </option>
                    ))}
                  </Sel>
                </div>

                <div className="form-group row">
                  <Sel name="unit_kajian" label="Direktorat Pembuat">
                    <option value="">Pilih Direktorat Pembuat</option>
                    {direktoratPembuat.map((data, index) => (
                      <option key={index} value={data.id}>
                        {data.nama}
                      </option>
                    ))}
                  </Sel>
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                  />
                </div>

                {/* FIELD UPLOAD FILE */}
                {values.tgl_kajian ? (
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Upload File
                    </label>
                    <div className="col-lg-8 col-xl-6">
                      <Field
                        name="file"
                        component={CustomFileInput}
                        title="Select a file"
                        label="File"
                        style={{ display: "flex" }}
                      />
                    </div>
                  </div>
                ) : null}
                <div className="col-lg-12" style={{ textAlign: "right" }}>
                  <button
                    type="button"
                    onClick={backAction}
                    className="btn btn-light"
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                    }}
                  >
                    <i className="fa fa-arrow-left"></i>
                    Kembali
                  </button>
                  {`  `}

                  {loading ? (
                    <button
                      type="submit"
                      className="btn btn-success spinner spinner-white spinner-left ml-2"
                      onSubmit={() => handleSubmit()}
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                      disabled={isSubmitting}
                    >
                      <span>Simpan</span>
                    </button>
                  ) : (
                    <button
                      type="submit"
                      className="btn btn-success ml-2"
                      onSubmit={() => handleSubmit()}
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                      disabled={isSubmitting}
                    >
                      <i className="fas fa-save"></i>
                      <span>Simpan</span>
                    </button>
                  )}
                </div>
                {/* <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button> */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ProposalEditForm;
