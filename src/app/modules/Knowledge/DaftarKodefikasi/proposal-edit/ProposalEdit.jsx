import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";
// import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import ProposalEditForm from "./ProposalEditForm";
import ProposalEditFooter from "./ProposalEditFooter";
import {
  // getKodefikasi,
  getKodefikasiById,
  saveKodefikasi,
  updateKodefikasi,
  uploadFile,
} from "../../Api";
import swal from "sweetalert";

//12 data yang dikirim, yang belum dimasukkan 'nip_perekam', 'jns_usul' dan yang tidak dimasukkan disini adalah 'alasan_tolak'.
const initValues = {
  file_upload: "",
  body: "",
  id_jnspajak: "",
  nip_pengusul: "",
  status: "",
  nip_pjbt_es3: "",
  nip_pjbt_es4: "",
  no_nd: "",
  perihal: "",
  tgl_nd: "",
  alasan_tolak: "",
};

function KodefikasiEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [proposal, setProposal] = useState();

  useEffect(() => {
    let _title = id ? "Edit Kodefikasi" : "Tambah Kodefikasi";

    setTitle(_title);
    suhbeader.setTitle(_title);

    if (id) {
      getKodefikasiById(id).then(({ data }) => {
        setProposal({
          id_kodefikasi: data.id_kodefikasi,
          no_nd: data.no_nd,
          tgl_nd: data.tgl_nd,
          id_jnspajak: data.id_jnspajak,
          perihal: data.perihal,
          file_upload: data.file_upload,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
        });
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();

  const saveProposal = (values) => {
    if (!id) {
      const formData = new FormData();

      formData.append("file", values.file);

      //console.log(values)

      uploadFile(formData)
        .then(({ data }) =>
          saveKodefikasi(
            "",
            "",
            data.message,
            values.id_jnspajak,
            "",
            "",
            "",
            values.no_nd,
            values.perihal,
            "",
            values.tgl_nd
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push("/knowledge/kodefikasi");
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push("/knowledge/kodefikasi/new");
              });
            }
          })
        )
        .catch(() => window.alert("Oops Something went wrong !"));
    } else {
      if (values.file) {
        //console.log(values.file.name);
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData)
          .then(({ data }) =>
            updateKodefikasi(
              values.id_kodefikasi,
              "",
              "",
              data.message,
              values.id_jnspajak,
              "",
              "",
              "",
              values.no_nd,
              values.perihal,
              "",
              values.tgl_nd
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push("/knowledge/kodefikasi");
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push("/knowledge/kodefikasi/new");
                });
              }
            })
          )
          .catch(() => window.alert("Oops Something went wrong !"));
      } else {
        updateKodefikasi(
          values.id_kodefikasi,
          "",
          "",

          values.id_jnspajak,
          "",
          "",
          "",
          values.no_nd,
          values.perihal,
          "",
          values.tgl_nd
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push("/knowledge/kodefikasi");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/knowledge/kodefikasi/new");
            });
          }
        });
      }
    }
  };

  const backToProposalList = () => {
    history.push(`/knowledge/kodefikasi`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <ProposalEditForm
            actionsLoading={actionsLoading}
            proposal={proposal || initValues}
            btnRef={btnRef}
            saveProposal={saveProposal}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <ProposalEditFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></ProposalEditFooter>
      </CardFooter>
    </Card>
  );
}

export default KodefikasiEdit;

//   }, [id, suhbeader]);
//   const btnRef = useRef();
//   const saveProposal = (values) => {
//     if (!id) {
//       const formData = new FormData();
//     } else {
//     }
//   };
//   const backToProposalList = () => {
//     history.push(`/knowledge/kodefikasi`);
//   };

//   const setDisabled = (val) => {
//     setIsDisabled(val);
//   };
//   return (
//     <Card>
//       {/* {actionsLoading && <ModalProgressBar />} */}
//       <CardHeader
//         title={title}
//         style={{ backgroundColor: "#FFC91B" }}
//       ></CardHeader>
//       <CardBody>
//         <div className="mt-5">
//           <ProposalEditForm
//             actionsLoading={actionsLoading}
//             proposal={proposal || initValues}
//             btnRef={btnRef}
//             saveProposal={saveProposal}
//             setDisabled={setDisabled}
//           />
//         </div>
//       </CardBody>
//       <CardFooter style={{ borderTop: "none" }}>
//         <ProposalEditFooter
//           backAction={backToProposalList}
//           btnRef={btnRef}
//         ></ProposalEditFooter>
//       </CardFooter>
//     </Card>
//   );
// }

// export default KajianEdit;
