import { CollectionsBookmark } from "@material-ui/icons";
import React, { useEffect, useState } from "react";
import { Modal, Table } from "react-bootstrap";
//import { getLogUsulan, getUsulanById } from "../Api";
import { getKajianById, getHistoryStatusKM } from "../Api";
import { DateFormat } from "../helpers/DateFormat";
function KajianHistory({ id, show, onHide }) {
  const [proposal, setProposal] = useState([]);
  const [log, setLog] = useState([]);

  useEffect(() => {
    if (id) {
      getKajianById(id).then(({ data }) => {
        setProposal(data);
      });

      //refkm kajian = 3
      getHistoryStatusKM(3, id).then(({ data }) => {
        setLog(data);
      });
    }
  }, [id]);
  const checkName = (name) => {
    switch (name) {
      case "Draft":
        return "Input Usulan";
      case "Eselon 4":
        return "Penelitian Eselon 4";
      case "Eselon 3":
        return "Penelitian Eselon 3";
      case "Terima":
        return "Permohonan Diterima";
      case "Tolak":
        return "Permohonan Ditolak";
      default:
        break;
    }
  };
  const showStatus = () => {
    return log.map((data, index) => {
      const date = new Date(data.wkt_proses);
      const tanggal = String(date.getDate()).padStart(2, "0");
      var bulan = String(date.getMonth() + 1).padStart(2, "0");
      const tahun = date.getFullYear();

      const jam = (date.getHours() < 10 ? "0" : "") + date.getHours();
      const menit = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();

      const tampilTanggal =
        tanggal + "-" + bulan + "-" + tahun + " - " + jam + ":" + menit;

      return (
        <tr key={index} style={{ borderBottom: "1px solid lightgrey" }}>
          <td key={data.id_log}>{index + 1}</td>
          <td key={data.status} style={{ textAlign: "left" }}>
            {checkName(data.status)}
          </td>

          <td key={data.wkt_proses} style={{ textAlign: "left" }}>
            {tampilTanggal}
          </td>
        </tr>
      );
    });
  };

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Status Pengajuan
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Tgl Kajian</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${DateFormat(proposal.tgl_kajian)}`}
              {/* {`: ${proposal.tgl_surat}`} */}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Perihal</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${proposal.perihal}`}
            </span>
          </div>
        </div>
        <div className="row">
          <Table responsive hover>
            <thead
              style={{
                border: "1px solid #3699FF",
                textAlign: "center",
                backgroundColor: "#3699FF",
              }}
            >
              <tr>
                <th>NO</th>
                <th style={{ textAlign: "left" }}>STATUS</th>
                <th style={{ textAlign: "left" }}>TANGGAL</th>
              </tr>
            </thead>
            <tbody
              style={{ border: "1px solid lightgrey", textAlign: "center" }}
            >
              {showStatus()}
            </tbody>
            <tfoot
              style={{
                border: "1px solid #3699FF",
                textAlign: "center",
                backgroundColor: "#3699FF",
              }}
            >
              <tr style={{ height: "40px" }}>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tfoot>
          </Table>
        </div>
      </Modal.Body>
      <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            //onClick={backAction}
            onClick={onHide}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}

export default KajianHistory;
