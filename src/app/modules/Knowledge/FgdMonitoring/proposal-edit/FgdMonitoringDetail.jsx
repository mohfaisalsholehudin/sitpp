import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import FgdMonitoringDetailForm from "./FgdMonitoringDetailForm";
import FgdMonitoringDetailFooter from "./FgdMonitoringDetailFooter";
import { getFgdById, getFgd, updateStatusFgd } from "../../Api";
import swal from "sweetalert";
import { useSelector } from "react-redux";

const initValues = {
  alasan_tolak: "",
  body: "",
  file_upload: "",
  kd_kantor: "",
  kd_unit_org: "",
  nip_pengusul: "",
  nip_pjbt_es3: "",
  nip_pjbt_es4: "",
  no_nd: "",
  perihal: "",
  status: "",
  tgl_nd: "",
};

function FgdMonitoringDetail({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();
  const { role, user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [proposal, setProposal] = useState();
  const [FgdDetail, setFgdDetail] = useState([]);
  let thePath = document.URL;
  let nextStatus = "";
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);

  const [statNow, setstatNow] = useState("");
  //let statNow = "";
  // if (proposal.status == "Eselon 4") {
  //   statNow = 2;
  // } else if (proposal.status == "Eselon 3") {
  //   statNow = 3;
  // }

  //console.log(statNow);

  const acceptAction = (statusNow) => {
    if (statusNow == "Eselon 4") {
      nextStatus = 3;
    } else if (statusNow == "Eselon 3") {
      nextStatus = 6;
    }
    //alert(id + " " + statNow + " " + nextStatus + " " + user.nip9);

    updateStatusFgd(id, statNow, nextStatus, user.nip9).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Data berhasil diteruskan ke atasan", "success").then(
          () => {
            history.push("/dashboard");
            history.replace("/knowledge/fgd/monitoring");
          }
        );
      } else {
        swal("Gagal", "Data gagal diteruskan ke atasan", "error").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/fgd/monitoring");
        });
      }
    });
  };

  function tolakDialog(idPer, statNow) {
    swal("Masukan Alasan Tolak:", {
      title: "Tolak",
      text: "Apakah Anda yakin menolak data ini ?",
      icon: "warning",
      buttons: true,
      content: "input",
      content: {
        element: "input",
        attributes: {
          placeholder: "masukan alasan",
          type: "textarea",
        },
      },
    }).then((value) => {
      if (value != null) {
        updateStatusFgd(idPer, statNow, 5, user.nip9, value).then(
          ({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan berhasil ditolak", "success").then(
                () => {
                  history.push("/dashboard");
                  history.replace("/knowledge/fgd/monitoring");
                }
              );
            } else {
              swal("Gagal", "Penolakan usulan gagal", "error").then(() => {
                history.push("/dashboard");
                history.replace("/knowledge/fgd/monitoring");
              });
            }
          }
        );
      }
    });
  }

  function tombolProses() {
    if (lastPath == "proses") {
      return (
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={() => tolakDialog(id, statNow)}
            className="btn btn-danger"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tolak
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={() => setujuDialog(id)}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <i className="fas fa-check"></i>
            Setuju
          </button>
        </div>
      );
    } else {
      return "";
    }
  }

  const setujuDialog = (id) => {
    swal({
      title: "Setuju",
      text: "Apakah Anda yakin menyetujui usulan ini ?",
      icon: "warning",
      buttons: true,
      //successMode : true
      //dangerMode: true,
    }).then((ret) => {
      if (ret == true) {
        acceptAction(proposal.status);
      }
    });
  };

  useEffect(() => {
    if (lastPath == "proses") {
      let _title = "Proses Fgd";
      setTitle(_title);
      suhbeader.setTitle(_title);
    } else {
      let _title = "Detail Fgd";
      setTitle(_title);
      suhbeader.setTitle(_title);
    }

    getFgdById(id).then(({ data }) => {
      setProposal({
        id_fgd: data.id_fgd,
        no_nd: data.no_nd,
        tgl_nd: data.tgl_nd,
        perihal: data.perihal,
        file_upload: data.file_upload,
        alasan_tolak: data.alasan_tolak,
        body: data.body,
        nip_pengusul: data.nip_pengusul,
        nip_pjbt_es4: data.nip_pjbt_es4,
        nip_pjbt_es3: data.nip_pjbt_es3,
        wkt_usul: data.wkt_usul,
        wkt_teliti_es4: data.wkt_teliti_es4,
        wkt_teliti_es3: data.wkt_teliti_es3,
        status: data.status,
        wkt_create: data.wkt_create,
        wkt_update: data.wkt_update,
        kd_unit_org: data.kd_unit_org,
        kd_kantor: data.kd_kantor,
      });

      if (data.status == "Eselon 4") {
        setstatNow(2);
      } else if (data.status == "Eselon 3") {
        setstatNow(3);
      }
    });
  }, [id, suhbeader]);

  const btnRef = useRef();

  const saveFgdDetail = (values) => {};

  //console.log(peraturanTerkait)

  const backToProposalList = () => {
    history.push(`/knowledge/fgd/monitoring`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <FgdMonitoringDetailForm
            actionsLoading={actionsLoading}
            proposal={proposal || initValues}
            FgdDetail={FgdDetail}
            idFgd={id}
            btnRef={btnRef}
            saveFgdDetail={saveFgdDetail}
            setDisabled={setDisabled}
          />
        </div>
        <br></br>
        {tombolProses()}
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <FgdMonitoringDetailFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></FgdMonitoringDetailFooter>
      </CardFooter>
    </Card>
  );
}

export default FgdMonitoringDetail;
