import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import FgdMonitoringTable from "./FgdMonitoringTable";

function FgdMonitoring() {
  return (
    <>
      <Card>
        <CardHeader
          title="Monitoring FGD"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          {/* <ul className="nav nav-tabs nav-line-tabs nav-line-tabs-2x mb-5 fs-6">
            <li className="nav-item">
              <a
                className="nav-link active"
                data-bs-toggle="tab"
                href="#kt_tab_pane_4"
              >
                Kantor
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                data-bs-toggle="tab"
                href="#kt_tab_pane_5"
              >
                Link 2
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                data-bs-toggle="tab"
                href="#kt_tab_pane_6"
              >
                Link 3
              </a>
            </li>
          </ul>
          <div className="tab-content" id="myTabContent" />
          <div
            className="tab-pane fade show active"
            id="kt_tab_pane_4"
            role="tabpanel"
          ></div>
          <div
            className="tab-pane fade"
            id="kt_tab_pane_5"
            role="tabpanel"
          ></div> */}
          <FgdMonitoringTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default FgdMonitoring;
