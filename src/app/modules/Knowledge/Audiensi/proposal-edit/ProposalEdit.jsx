import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";
// import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import AudiensiEditForm from "./AudiensiEditForm";
import AudiensiEditFooter from "./AudiensiEditFooter";
import {
  uploadFile,
  getAudiensiById,
  saveAudiensi,
  updateAudiensi,
  updateStatusAudiensi,
} from "../../Api";
import swal from "sweetalert";

function AudiensiEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [audiensi, setAudiensi] = useState();
  const [loading, setLoading] = useState(false);
  const [content, setContent] = useState();

  //12 data yang dikirim, yang belum dimasukkan 'nip_perekam', 'jns_usul' dan yang tidak dimasukkan disini adalah 'alasan_tolak'.
  const { user } = useSelector((state) => state.auth);
  const initValues = {
    alasan_tolak: "" ,
    body: "" ,
    file_upload: "" ,
    id_detiljns: "" ,
    kd_kantor: user.kantorLegacyKode,
    kd_unit_org: user.unitLegacyKode,
    nip_pengusul: user.nip9,
    nip_pjbt_es3: "" ,
    nip_pjbt_es4: "" ,
    no_nd: "" ,
    perihal: "" ,
    status: "" ,
    tgl_nd: "",
};

  const getCurrentDate = () => {
    const date = new Date();
    return date;
  };

  const handleChangeProposalAudiensi = (val) => {
    getAudiensiById(val.value).then(({ data }) => {
      setContent({
        id_audiensi: data.id_audiensi,
          no_nd: data.no_nd,
          tgl_nd: getCurrentDate(),
          id_detiljns: data.id_detiljns,
          perihal: data.perihal,
          file_upload: data.file_upload,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          status: data.status,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
          kd_kantor: data.kd_kantor,
          kd_unit_org: data.kd_unit_org
      });
    });
  };

  useEffect(() => {
    let _title = id ? "Edit Audiensi" : "Tambah Audiensi";

    setTitle(_title);
    suhbeader.setTitle(_title);

    if (id) {
      getAudiensiById(id).then(({ data }) => {
        setAudiensi({
          id_audiensi: data.id_audiensi,
          no_nd: data.no_nd,
          tgl_nd: data.tgl_nd,
          id_detiljns: data.id_detiljns,
          perihal: data.perihal,
          file_upload: data.file_upload,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          status: data.status,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
          kd_kantor: data.kd_kantor,
          kd_unit_org: data.kd_unit_org
        });
      });
    }
  }, [id, suhbeader]);

  const btnRef = useRef();

  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  const saveSurat = (values) => {
    if (!id) {
      if (values.file.name) {
        enableLoading();
        const formData = new FormData();

        formData.append("file", values.file);
        //console.log(formData);

        uploadFile(formData)
          .then(({ data }) => {
            disableLoading();
            saveAudiensi(
              values.alasan_tolak,
              values.body,
              data.message,
              values.id_detiljns,
              values.kd_kantor,
              values.kd_unit_org,
              values.nip_pengusul,
              values.nip_pjbt_es3,
              values.nip_pjbt_es4,
              values.no_nd,
              values.perihal,
              values.status,
              values.tgl_nd
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Usulan berhasil disimpan", "success").then(
                  () => {
                    history.push("/knowledge/audiensi/usulan");
                  }
                );
              } else {
                swal("Gagal", "Usulan gagal disimpan", "error").then(() => {
                  history.push("/knowledge/audiensi/usulan/new");
                });
              }
            });
          })
          .catch(() => window.alert("Oops Something went wrong !"));
      } else {
        saveAudiensi(
              values.alasan_tolak,
              values.body,
              values.file_upload,
              values.id_detiljns,
              values.kd_kantor,
              values.kd_unit_org,
              values.nip_pengusul,
              values.nip_pjbt_es3,
              values.nip_pjbt_es4,
              values.no_nd,
              values.perihal,
              values.status,
              values.tgl_nd
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan berhasil disimpan", "success").then(() => {
              history.push("/knowledge/audiensi/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal disimpan", "error").then(() => {
              history.push("/knowledge/audiensi/usulan/new");
            });
          }
        });
      }
    } else {
      //console.log(values.status)
      // ketika status tolak dan melakukan edit , otomatis status jadi draft lagi

      if (values.status === "Tolak") {
        if (values.file) {
          enableLoading();
          const formData = new FormData();
          formData.append("file", values.file);
          uploadFile(formData).then(({ data }) => {
            disableLoading();
            updateAudiensi(
              values.alasan_tolak,
              values.body,
              data.message,
              values.id_detiljns,
              values.kd_kantor,
              values.kd_unit_org,
              values.nip_pengusul,
              values.nip_pjbt_es3,
              values.nip_pjbt_es4,
              values.no_nd,
              values.perihal,
              values.status,
              values.tgl_nd
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                updateStatus();
              } else {
                swal("Gagal", "Usulan gagal dirubah", "error").then(() => {
                  history.push("/dashboard");
                  history.replace("/knowledge/audiensi/usulan");
                });
              }
            });
          });
        } else {
          updateAudiensi(
              values.alasan_tolak,
              values.body,
              values.file_upload,
              values.id_detiljns,
              values.kd_kantor,
              values.kd_unit_org,
              values.nip_pengusul,
              values.nip_pjbt_es3,
              values.nip_pjbt_es4,
              values.no_nd,
              values.perihal,
              values.status,
              values.tgl_nd
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              updateStatus();
            } else {
              swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                history.push("/dashboard");
                history.replace("/knowledge/audiensi/usulan");
              });
            }
          });
        }
      } else {
        if (values.file) {
          enableLoading();
          //console.log(values.file.name);
          const formData = new FormData();
          formData.append("file", values.file);
          uploadFile(formData)
            .then(({ data }) => {
              disableLoading();
              updateAudiensi(
                values.alasan_tolak,
                values.body,
                data.message,
                values.id_detiljns,
                values.kd_kantor,
                values.kd_unit_org,
                values.nip_pengusul,
                values.nip_pjbt_es3,
                values.nip_pjbt_es4,
                values.no_nd,
                values.perihal,
                values.status,
                values.tgl_nd
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Usulan berhasil diubah", "success").then(
                    () => {
                      history.push("/knowledge/audiensi/usulan");
                    }
                  );
                } else {
                  swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                    history.push("/knowledge/audiensi/usulan/new");
                  });
                }
              });
            })
            .catch(() => window.alert("Oops Something went wrong !"));
        } else {
          updateAudiensi(
              values.alasan_tolak,
              values.body,
              values.file_upload,
              values.id_detiljns,
              values.kd_kantor,
              values.kd_unit_org,
              values.nip_pengusul,
              values.nip_pjbt_es3,
              values.nip_pjbt_es4,
              values.no_nd,
              values.perihal,
              values.status,
              values.tgl_nd
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan berhasil diubah", "success").then(() => {
                history.push("/knowledge/audiensi/usulan");
              });
            } else {
              swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                history.push("/knowledge/audiensi/usulan/new");
              });
            }
          });
        }
      }
    }
  };

  const updateStatus = () => {
    updateStatusAudiensi(id, 5, 1, user.nip9).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Usulan Berubah Menjadi Draft", "success").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/audiensi/usulan");
        });
      } else {
        swal("Gagal", "Usulan gagal diubah", "error").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/audiensi/usulan");
        });
      }
    });
  };

  const backToAudiensiList = () => {
    history.push(`/knowledge/audiensi`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };

  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <AudiensiEditForm
            actionsLoading={actionsLoading}
            proposal={audiensi || initValues}
            btnRef={btnRef}
            saveProposal={saveSurat}
            loading={loading}
            handleChangeProposalAudiensi={handleChangeProposalAudiensi}
            setDisabled={setDisabled}
            backAction={backToAudiensiList}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <AudiensiEditFooter
          backAction={backToAudiensiList}
          btnRef={btnRef}
        ></AudiensiEditFooter> */}
      </CardFooter>
    </Card>
  );
}

export default AudiensiEdit;