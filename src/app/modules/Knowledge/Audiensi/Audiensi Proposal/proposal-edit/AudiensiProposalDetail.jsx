import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../../_metronic/layout";
import AudiensiDetailForm from "./AudiensiDetailForm";
import AudiensiDetailFooter from "./AudiensiDetailFooter";
import {
  getAudiensiById,
} from "../../../Api";
import { useSelector } from "react-redux";

const initValues = {
  alasan_tolak: "" ,
  body: "" ,
  file_upload: "" ,
  id_detiljns: "" ,
  kd_kantor: "" ,
  kd_unit_org: "" ,
  nip_pengusul: "" ,
  nip_pjbt_es3: "" ,
  nip_pjbt_es4: "" ,
  no_nd: "" ,
  perihal: "" ,
  status: "" ,
  tgl_nd: "",
};

function AudiensiProposalDetail({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const { role, user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [audiensi, setAudiensi] = useState();
  const [AudiensiDetail, setAudiensiDetail] = useState([]);
  let thePath = document.URL;
  let nextStatus = "";
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);
  const [statNow, setstatNow] = useState("");

  useEffect(() => {
    
      let _title = "Detail Audiensi";
      setTitle(_title);
      suhbeader.setTitle(_title);

    getAudiensiById(id).then(({ data }) => {
      setAudiensi({
        id_audiensi: data.id_audiensi,
        no_nd: data.no_nd,
        tgl_nd: data.tgl_nd,
        id_detiljns: data.id_detiljns,
        perihal: data.perihal,
        file_upload: data.file_upload,
        alasan_tolak: data.alasan_tolak,
        body: data.body,
        nip_pengusul: data.nip_pengusul,
        nip_pjbt_es4: data.nip_pjbt_es4,
        nip_pjbt_es3: data.nip_pjbt_es3,
        wkt_usul: data.wkt_usul,
        wkt_teliti_es4: data.wkt_teliti_es4,
        wkt_teliti_es3: data.wkt_teliti_es3,
        status: data.status,
        wkt_create: data.wkt_create,
        wkt_update: data.wkt_update
      });
    });
  }, [id, suhbeader]);

  const btnRef = useRef();

  const saveAudiensiDetail = (values) => {};

  const backToAudiensiList = (values) => {
    history.push(`/knowledge/audiensi/usulan`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <AudiensiDetailForm
            actionsLoading={actionsLoading}
            proposal={audiensi || initValues}
            AudiensiDetail={AudiensiDetail}
            idAudiensi={id}
            btnRef={btnRef}
            saveAudiensiDetail={saveAudiensiDetail}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <AudiensiDetailFooter
          backAction={backToAudiensiList}
          btnRef={btnRef}
        ></AudiensiDetailFooter>
      </CardFooter>
    </Card>
  );
}

export default AudiensiProposalDetail;
