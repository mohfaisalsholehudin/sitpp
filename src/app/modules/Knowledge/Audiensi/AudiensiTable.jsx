import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";
import { Pagination } from "../pagination/Pagination";
import {
  getAudiensiNonTerimaByNip,
  getAudiensiSearchEselon3,
  getAudiensiNonTerimaEs4,
} from "../Api";
import AudiensiHistory from "./AudiensiHistory";
import { useSelector } from "react-redux";

function AudiensiTable() {
  const { role, user } = useSelector((state) => state.auth);
  const konseptor = role.includes("ROLE_PERATURAN_KONSEPTOR");
  const es4 = role.includes("ROLE_PERATURAN_PENELITI_LVL1");
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const admin = role.includes("ROLE_ADMIN_PERATURAN");
  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "wkt_update",
    pageNumber: 1,
    pageSize: 50,
  };
  const history = useHistory();

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/audiensi/penelitian/${id}/history`);

  const prosesAudiensi = (id) =>
    history.push(`/knowledge/audiensi/penelitian/${id}/proses`);

  const detailAudiensi = (id) =>
    history.push(`/knowledge/audiensi/penelitian/${id}/detail`);

  const [audiensi, setAudiensi] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmAudiensi.no_nd",
      text: "No ND Hasil Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmAudiensi.tgl_nd",
      text: "Tgl ND Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmAudiensi.perihal",
      text: "Perihal Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "jenisAudiensi.nama",
      text: "Jenis Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmAudiensi.status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.AudiensiStatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        user.jabatan === "Pelaksana"
          ? columnFormatters.AudiensiActionColumnFormatterPelaksana
          : user.jabatan === "Kepala Seksi"
          ? columnFormatters.AudiensiActionColumnFormatterEselon4
          : columnFormatters.AudiensiActionColumnFormatterEselon3,
      formatExtraData: {
        showHistory: showHistoryPengajuan,
        detailAudiensi: detailAudiensi,
        prosesAudiensi: prosesAudiensi,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const { SearchBar } = Search;

  useEffect(() => {
    if (es4) {
      getAudiensiNonTerimaEs4(user.kantorLegacyKode, user.unitEs4LegacyKode).then(
        ({ data }) => {
          data.map((data) => {
            return data.kmAudiensi.status === "Eselon 4"
              ? setAudiensi((audiensi) => [...audiensi, data])
              : data.kmAudiensi.status === "Eselon 3"
              ? setAudiensi((audiensi) => [...audiensi, data])
              : data.kmAudiensi.status === "Tolak"
              ? setAudiensi((audiensi) => [...audiensi, data])
              : null;
          });
        }
      );
    }
  }, [es4, user]);

  useEffect(() => {
    if (es3) {
      getAudiensiSearchEselon3(
        user.kantorLegacyKode,
        user.unitEs4LegacyKode,
        "Eselon%203"
      ).then(({ data }) => {
        data.map((dt) => {
          setAudiensi((audiensi) => [...audiensi, dt]);
        });
      });
    }
  }, [es3, user]);

  const defaultSorted = [{ dataField: "wkt_update", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);
  const pagiOptions = {
    custom: true,
    totalSize: audiensi.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="kmAudiensi.id_audiensi"
                  data={audiensi}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/audiensi/penelitian/:id/history">
        {({ history, match }) => (
          <AudiensiHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/audiensi/penelitian");
            }}
            onRef={() => {
              history.push("/knowledge/audiensi/penelitian");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default AudiensiTable;
