import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import SurveiTable from "./SurveiTable";

function Survei() {
  return (
    <>
      <Card>
        <CardHeader
          title="Penelitian Survei"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <SurveiTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Survei;
