import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import DaftarSurveiTable from "./DaftarSurveiTable";

function DaftarSurvei() {
  return (
    <>
      <Card>
        <CardHeader
          title="Monitoring Survei"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <DaftarSurveiTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default DaftarSurvei;
