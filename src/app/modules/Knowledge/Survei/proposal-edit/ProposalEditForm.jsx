import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
  Checkbox,
} from "./helpers";
import CustomFileInput from "./helpers/CustomFileInput";
// import { getJenisPajak } from "../../Api";
// import { unitKerja } from "../../../../references/UnitKerja";
import { getJenisSurvei } from "../../Api";

function ProposalEditForm({ proposal, btnRef, saveProposal }) {
  const [jenis, setJenis] = useState([]);
  const { user } = useSelector((state) => state.auth);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    no_nd: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No Surat is required"),
    tgl_nd: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),
    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Perihal is required"),
    id_detiljns: Yup.string()
      .min(1, "Jenis Pajak is required")
      .required("Jenis Pajak is required"),
    file: Yup.mixed()
      .test(
        "fileSize",
        "File too large",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        (value) => value && SUPPORTED_FORMATS.includes(value.type)
      ),
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    getJenisSurvei().then(({ data }) => {
      setJenis(data);
    });
  }, []);

  // console.log(setSurvei);
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          //console.log(values);
          saveProposal(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid,
        }) => {
          
          return (
            <>
              <Form className="form form-label-right">

                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    name="no_nd"
                    component={Input}
                    placeholder="Nomor ND Survei"
                    label="Nomor ND Survei"
                    // onClick={()=>handleChangeNip()}
                  />
                </div>

                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField name="tgl_nd" label="Tanggal ND" />
                </div>

                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="id_detiljns" label="Jenis Survei">
                  <option value="">Pilih Jenis Survei</option>
                    {jenis.map((data, index) => (
                      <option key={index} value={data.id}>{data.nama}</option>
                    ))}
                  </Sel>
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal Survei"
                    label="Perihal Survei"
                  />
                </div>

                {/* FIELD UPLOAD FILE */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Upload File
                  </label>
                  <div className="col-lg-8 col-xl-6">
                    <Field
                      name="file"
                      component={CustomFileInput}
                      title="Select a file"
                      label="File"
                      // setFieldValue={setFieldValue}
                      // errorMessage={errors["file"] ? errors["file"] : undefined}
                      // touched={touched["file"]}
                      style={{ display: "flex" }}
                      // onBlur={handleBlur}
                    />
                  </div>
                </div>
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ProposalEditForm;
