import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../_metronic/layout";
import DaftarSurveiDetailForm from "./DaftarSurveiDetailForm";
import DaftarSurveiDetailFooter from "./DaftarSurveiDetailFooter";
import { getSurveiById } from "../../Api";

const initValues = {
  alasan_tolak: "",
  body: "",
  file_upload: "",
  id_detiljns: "",
  kd_kantor: "",
  kd_unit_org: "",
  nip_pengusul: "",
  nip_pjbt_es3: "",
  nip_pjbt_es4: "",
  no_nd: "",
  perihal: "",
  status: "",
  tgl_nd: ""
};

function DaftarSurveiDetail({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [surveiterima, setSurveiTerima] = useState();
  const [SurveiTerimaDetail, setSurveiTerimaDetail] = useState([]);
  let thePath = document.URL;
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);

  useEffect(() => {
      let _title = "Detail Survei";
      setTitle(_title);
      suhbeader.setTitle(_title);

      getSurveiById(id).then(({ data }) => {
        setSurveiTerima({
        id_lapsurvey: data.id_lapsurvey,
        no_nd: data.no_nd,
        tgl_nd: data.tgl_nd,
        id_detiljns: data.id_detiljns,
        perihal: data.perihal,
        file_upload: data.file_upload,
        alasan_tolak: data.alasan_tolak,
        body: data.body,
        nip_pengusul: data.nip_pengusul,
        nip_pjbt_es4: data.nip_pjbt_es4,
        nip_pjbt_es3: data.nip_pjbt_es3,
        wkt_usul: data.wkt_usul,
        wkt_teliti_es4: data.wkt_teliti_es4,
        wkt_teliti_es3: data.wkt_teliti_es3,
        status: data.status,
        wkt_create: data.wkt_create,
        wkt_update: data.wkt_update,
        kd_kantor: data.kd_kantor,
        kd_unit_org: data.kd_unit_org
        });
      });
    }, [id, suhbeader]);

  const btnRef = useRef();

  const backToDaftarSurveiList = (values) => {
    history.push(`/knowledge/survei/monitoring`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <DaftarSurveiDetailForm
            actionsLoading={actionsLoading}
            proposal={surveiterima || initValues}
            SurveiTerimaDetail={SurveiTerimaDetail}
            idSurveiTerima={id}
            btnRef={btnRef}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <DaftarSurveiDetailFooter
          backAction={backToDaftarSurveiList}
          btnRef={btnRef}
        ></DaftarSurveiDetailFooter>
      </CardFooter>
    </Card>
  );
}

export default DaftarSurveiDetail;
