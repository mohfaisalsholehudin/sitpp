import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../../_metronic/layout";
import ProposalEditForm from "./ProposalEditForm";
import ProposalEditFooter from "./ProposalEditFooter";
import {
  uploadFile,
  getSurveiById,
  saveSurvei,
  updateSurvei,
  updateStatusSurvei,
} from "../../../Api";
import swal from "sweetalert";
import { useSelector } from "react-redux";

function SurveiEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const { user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [loading, setLoading] = useState(false);
  const [isDisabled, setIsDisabled] = useState();
  const [survei, setSurvei] = useState();
  const [content, setContent] = useState();

  const initValues = {
    alasan_tolak: "",
    body: "",
    file_upload: "",
    id_detiljns: "",
    kd_kantor: user.kantorLegacyKode,
    kd_unit_org: user.unitEs4LegacyKode,
    nip_pengusul: user.nip9,
    nip_pjbt_es3: "",
    nip_pjbt_es4: "",
    no_nd: "",
    perihal: "",
    status: "",
    tgl_nd: "",
  };

  const getCurrentDate = () => {
    const date = new Date();
    return date;
  };

  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  const handleChangeProposalSurvei = (val) => {
    getSurveiById(val.value).then(({ data }) => {
      setContent({
        id_lapsurvey: data.id_lapsurvey,
        no_nd: data.no_nd,
        tgl_nd: getCurrentDate(),
        id_detiljns: data.id_detiljns,
        perihal: data.perihal,
        file_upload: data.file_upload,
        alasan_tolak: data.alasan_tolak,
        body: data.body,
        nip_pengusul: data.nip_pengusul,
        nip_pjbt_es4: data.nip_pjbt_es4,
        nip_pjbt_es3: data.nip_pjbt_es3,
        wkt_usul: data.wkt_usul,
        wkt_teliti_es4: data.wkt_teliti_es4,
        wkt_teliti_es3: data.wkt_teliti_es3,
        status: data.status,
        wkt_create: data.wkt_create,
        wkt_update: data.wkt_update,
        kd_kantor: data.kd_kantor,
        kd_unit_org: data.kd_unit_org,
      });
    });
  };

  useEffect(() => {
    let _title = id ? "Edit Survei" : "Tambah Survei";

    setTitle(_title);
    suhbeader.setTitle(_title);

    if (id) {
      getSurveiById(id).then(({ data }) => {
        setSurvei({
          id_lapsurvey: data.id_lapsurvey,
          no_nd: data.no_nd,
          tgl_nd: data.tgl_nd,
          id_detiljns: data.id_detiljns,
          perihal: data.perihal,
          file_upload: data.file_upload,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          status: data.status,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
          kd_kantor: data.kd_kantor,
          kd_unit_org: data.kd_unit_org,
        });
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();

  const saveSurat = (values) => {
    if (!id) {
      if (values.file.name) {
        enableLoading();
        const formData = new FormData();

        formData.append("file", values.file);
        //console.log(formData);

        uploadFile(formData)
          .then(({ data }) => {
            disableLoading();
            saveSurvei(
              values.alasan_tolak,
              values.body,
              data.message,
              values.id_detiljns,
              values.kd_kantor,
              values.kd_unit_org,
              values.nip_pengusul,
              values.nip_pjbt_es3,
              values.nip_pjbt_es4,
              values.no_nd,
              values.perihal,
              values.status,
              values.tgl_nd
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Usulan berhasil dibuat", "success").then(
                  () => {
                    history.push("/knowledge/survei/usulan");
                  }
                );
              } else {
                swal("Gagal", "Usulan gagal dibuat", "error").then(() => {
                  history.push("/knowledge/survei/usulan/new");
                });
              }
            });
          })
          .catch(() => window.alert("Oops Something went wrong !"));
      } else {
        saveSurvei(
          values.alasan_tolak,
          values.body,
          values.file_upload,
          values.id_detiljns,
          values.kd_kantor,
          values.kd_unit_org,
          values.nip_pengusul,
          values.nip_pjbt_es3,
          values.nip_pjbt_es4,
          values.no_nd,
          values.perihal,
          values.status,
          values.tgl_nd
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan berhasil disimpan", "success").then(() => {
              history.push("/knowledge/survei/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal disimpan", "error").then(() => {
              history.push("/knowledge/survei/usulan/new");
            });
          }
        });
      }
    } else {
      //console.log(values.status)
      // ketika status tolak dan melakukan edit , otomatis status jadi draft lagi

      if (values.status === "Tolak") {
        if (values.file.name) {
          enableLoading();
          const formData = new FormData();
          formData.append("file", values.file);
          uploadFile(formData).then(({ data }) => {
            disableLoading();
            updateSurvei(
              values.id_lapsurvey,
              values.alasan_tolak,
              values.body,
              data.message,
              values.id_detiljns,
              values.kd_kantor,
              values.kd_unit_org,
              values.nip_pengusul,
              values.nip_pjbt_es3,
              values.nip_pjbt_es4,
              values.no_nd,
              values.perihal,
              values.status,
              values.tgl_nd
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                updateStatus();
              } else {
                swal("Gagal", "Usulan gagal dirubah", "error").then(() => {
                  history.push("/dashboard");
                  history.replace("/knowledge/survei/usulan");
                });
              }
            });
          });
        } else {
          updateSurvei(
            values.id_lapsurvey,
            values.alasan_tolak,
            values.body,
            values.file_upload,
            values.id_detiljns,
            values.kd_kantor,
            values.kd_unit_org,
            values.nip_pengusul,
            values.nip_pjbt_es3,
            values.nip_pjbt_es4,
            values.no_nd,
            values.perihal,
            values.status,
            values.tgl_nd
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              updateStatus();
            } else {
              swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                history.push("/dashboard");
                history.replace("/knowledge/survei/usulan");
              });
            }
          });
        }
      } else {
        if (values.file.name) {
          enableLoading();
          //console.log(values.file.name);
          const formData = new FormData();
          formData.append("file", values.file);
          uploadFile(formData)
            .then(({ data }) => {
              disableLoading();
              updateSurvei(
                values.id_lapsurvey,
                values.alasan_tolak,
                values.body,
                data.message,
                values.id_detiljns,
                values.kd_kantor,
                values.kd_unit_org,
                values.nip_pengusul,
                values.nip_pjbt_es3,
                values.nip_pjbt_es4,
                values.no_nd,
                values.perihal,
                values.status,
                values.tgl_nd
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Usulan berhasil diubah", "success").then(
                    () => {
                      history.push("/knowledge/survei/usulan");
                    }
                  );
                } else {
                  swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                    history.push("/knowledge/survei/usulan/new");
                  });
                }
              });
            })
            .catch(() => window.alert("Oops Something went wrong !"));
        } else {
          updateSurvei(
            values.id_lapsurvey,
            values.alasan_tolak,
            values.body,
            values.file_upload,
            values.id_detiljns,
            values.kd_kantor,
            values.kd_unit_org,
            values.nip_pengusul,
            values.nip_pjbt_es3,
            values.nip_pjbt_es4,
            values.no_nd,
            values.perihal,
            values.status,
            values.tgl_nd
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan berhasil diubah", "success").then(() => {
                history.push("/knowledge/survei/usulan");
              });
            } else {
              swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                history.push("/knowledge/survei/usulan/new");
              });
            }
          });
        }
      }
    }
  };

  const updateStatus = () => {
    updateStatusSurvei(id, 5, 1, user.nip9).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Usulan Berubah Menjadi Draft", "success").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/survei/usulan");
        });
      } else {
        swal("Gagal", "Usulan gagal diubah", "error").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/survei/usulan");
        });
      }
    });
  };

  const backToSurveiList = () => {
    history.push(`/knowledge/survei/usulan`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <ProposalEditForm
            actionsLoading={actionsLoading}
            proposal={survei || initValues}
            btnRef={btnRef}
            saveProposal={saveSurat}
            loading={loading}
            handleChangeProposalSurvei={handleChangeProposalSurvei}
            setDisabled={setDisabled}
            backAction={backToSurveiList}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <ProposalEditFooter
          backAction={backToSurveiList}
          btnRef={btnRef}
        ></ProposalEditFooter> */}
      </CardFooter>
    </Card>
  );
}

export default SurveiEdit;
