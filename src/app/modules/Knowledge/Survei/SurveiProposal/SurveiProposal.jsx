import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../../_metronic/_partials/controls";
import SurveiProposalTable from "./SurveiProposalTable";

function SurveiProposal() {
  return (
    <>
      <Card>
        <CardHeader
          title="Usulan Survei"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <SurveiProposalTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default SurveiProposal;
