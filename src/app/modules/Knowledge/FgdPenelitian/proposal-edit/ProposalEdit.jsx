import React, { useEffect, useState, useRef } from "react";
import { useSelector } from "react-redux";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../_metronic/layout";
import ProposalEditForm from "./ProposalEditForm";
import ProposalEditFooter from "./ProposalEditFooter";
import {
  getFgdById,
  saveFgd,
  updateFgd,
  updateStatusFgd,
  uploadFile,
} from "../../Api";
import swal from "sweetalert";

function FgdEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();
  const { user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [content, setContent] = useState();
  const [proposal, setProposal] = useState();

  const initValues = {
    alasan_tolak: "",
    body: "",
    file_upload: "",
    kd_kantor: user.kantorLegacyKode,
    kd_unit_org: user.unitEs4LegacyKode,
    nip_pengusul: user.nip9,
    nip_pjbt_es3: "",
    nip_pjbt_es4: "",
    no_nd: "",
    perihal: "",
    status: "",
    tgl_nd: "",
  };

  const getCurrentDate = () => {
    const date = new Date();
    return date;
  };

  const handleChangeProposalFgd = (val) => {
    getFgdById(val.value).then(({ data }) => {
      setContent({
        id_fgd: data.id_fgd,
        no_nd: data.no_nd,
        tgl_nd: getCurrentDate(),
        perihal: data.perihal,
        file_upload: data.file_upload,
        alasan_tolak: data.alasan_tolak,
        body: data.body,
        nip_pengusul: data.user.nip9,
        nip_pjbt_es4: data.nip_pjbt_es4,
        nip_pjbt_es3: data.nip_pjbt_es3,
        wkt_usul: data.wkt_usul,
        wkt_teliti_es4: data.wkt_teliti_es4,
        wkt_teliti_es3: data.wkt_teliti_es3,
        status: data.status,
        wkt_create: data.wkt_create,
        wkt_update: data.wkt_update,
        kd_unit_org: data.kd_unit_org,
        kd_kantor: data.kd_kantor,
      });
    });
  };

  useEffect(() => {
    let _title = id ? "Edit Fgd" : "Tambah Fgd";

    setTitle(_title);
    suhbeader.setTitle(_title);

    if (id) {
      getFgdById(id).then(({ data }) => {
        setProposal({
          id_fgd: data.id_fgd,
          no_nd: data.no_nd,
          tgl_nd: data.tgl_nd,
          perihal: data.perihal,
          file_upload: data.file_upload,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          status: data.status,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
          kd_unit_org: data.kd_unit_org,
          kd_kantor: data.kd_kantor,
        });
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();

  const saveProposal = (values) => {
    if (!id) {
      const formData = new FormData();

      formData.append("file", values.file);

      //console.log(values)

      uploadFile(formData)
        .then(({ data }) =>
          saveFgd(
            values.alasan_tolak,
            values.body,
            data.message,
            values.kd_kantor,
            values.kd_unit_org,
            values.nip_pengusul,
            values.nip_pjbt_es3,
            values.nip_pjbt_es4,
            values.no_nd,
            values.perihal,
            values.status,
            values.tgl_nd
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan berhasil disimpan", "success").then(
                () => {
                  history.push("/knowledge/fgd/penelitian");
                }
              );
            } else {
              swal("Gagal", "Usulan gagal disimpan", "error").then(() => {
                history.push("/knowledge/fgd/penelitian/add");
              });
            }
          })
        )
        .catch(() => window.alert("Oops Something went wrong !"));
    } else {
      if (values.file) {
        //console.log(values.file.name);
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData)
          .then(({ data }) =>
            updateFgd(
              values.id_fgd,
              values.alasan_tolak,
              values.body,
              data.message,

              values.kd_kantor,
              values.kd_unit_org,
              values.nip_pengusul,
              values.nip_pjbt_es3,
              values.nip_pjbt_es4,
              values.no_nd,
              values.perihal,
              values.status,
              values.tgl_nd
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Usulan berhasil diubah", "success").then(
                  () => {
                    history.push("/knowledge/fgd/penelitian");
                  }
                );
              } else {
                swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                  history.push("/knowledge/fgd/penelitian/add");
                });
              }
            })
          )
          .catch(() => window.alert("Oops Something went wrong !"));
      } else {
        updateFgd(
          values.id_fgd,
          values.alasan_tolak,
          values.body,
          values.file_upload,

          values.kd_kantor,
          values.kd_unit_org,
          values.nip_pengusul,
          values.nip_pjbt_es3,
          values.nip_pjbt_es4,
          values.no_nd,
          values.perihal,
          values.status,
          values.tgl_nd
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan berhasil diubah", "success").then(() => {
              history.push("/knowledge/fgd/penelitian");
            });
          } else {
            swal("Gagal", "Usulan gagal diubah", "error").then(() => {
              history.push("/knowledge/fgd/penelitian/add");
            });
          }
        });
      }
      if (values.status == "Tolak") {
        updateFgd(
          values.id_fgd,
          values.alasan_tolak,
          values.body,
          values.file_upload,

          values.kd_kantor,
          values.kd_unit_org,
          values.nip_pengusul,
          values.nip_pjbt_es3,
          values.nip_pjbt_es4,
          values.no_nd,
          values.perihal,
          values.status,
          values.tgl_nd
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan Berubah Menjadi Draft", "success").then(
              () => {
                history.push("/dashboard");
                history.replace("/knowledge/fgd/penelitian");
              }
            );
          } else {
            swal("Gagal", "Usulan gagal diubah", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/fgd/penelitian");
            });
          }
        });
        updateStatusFgd(id, 5, 1, user.nip9).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan Berubah Menjadi Draft", "success").then(
              () => {
                history.push("/dashboard");
                history.replace("/knowledge/fgd/penelitian");
              }
            );
          } else {
            swal("Gagal", "Usulan gagal diubah", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/fgd/penelitian");
            });
          }
        });
      }
    }
  };

  const backToProposalList = () => {
    history.push(`/knowledge/fgd/penelitian`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <ProposalEditForm
            actionsLoading={actionsLoading}
            proposal={proposal || initValues}
            btnRef={btnRef}
            saveProposal={saveProposal}
            handleChangeProposalFgd={handleChangeProposalFgd}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <ProposalEditFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></ProposalEditFooter>
      </CardFooter>
    </Card>
  );
}

export default FgdEdit;

//   }, [id, suhbeader]);
//   const btnRef = useRef();
//   const saveProposal = (values) => {
//     if (!id) {
//       const formData = new FormData();
//     } else {
//     }
//   };
//   const backToProposalList = () => {
//     history.push(`/knowledge/fgd`);
//   };

//   const setDisabled = (val) => {
//     setIsDisabled(val);
//   };
//   return (
//     <Card>
//       {/* {actionsLoading && <ModalProgressBar />} */}
//       <CardHeader
//         title={title}
//         style={{ backgroundColor: "#FFC91B" }}
//       ></CardHeader>
//       <CardBody>
//         <div className="mt-5">
//           <ProposalEditForm
//             actionsLoading={actionsLoading}
//             proposal={proposal || initValues}
//             btnRef={btnRef}
//             saveProposal={saveProposal}
//             setDisabled={setDisabled}
//           />
//         </div>
//       </CardBody>
//       <CardFooter style={{ borderTop: "none" }}>
//         <ProposalEditFooter
//           backAction={backToProposalList}
//           btnRef={btnRef}
//         ></ProposalEditFooter>
//       </CardFooter>
//     </Card>
//   );
// }

// export default KajianEdit;
