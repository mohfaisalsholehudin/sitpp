import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import FgdPenelitianTable from "./FgdPenelitianTable";

function FgdPenelitian() {
  return (
    <>
      <Card>
        <CardHeader
          title="Penelitian FGD"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <FgdPenelitianTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default FgdPenelitian;
