import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import RegulasiTable from "./RegulasiTable";

function RegulasiDone() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Regulasi Perpajakan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <RegulasiTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default RegulasiDone;
