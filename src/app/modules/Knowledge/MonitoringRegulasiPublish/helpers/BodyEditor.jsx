// import React, { useEffect, useState, useRef, useCallback } from "react";
// import axios from "axios";
// import { Field, Formik, Form } from "formik";
// import * as Yup from "yup";
// import swal from "sweetalert";
// import {
//   Card,
//   CardBody,
//   CardHeader,
// } from "../../../../../_metronic/_partials/controls";
// import {
//   getDraft,
//   saveDraftComposing,
//   updateDraftComposing,
//   uploadFile,
// } from "../../Api";
// import EmailEditor from "react-email-editor";
// import { RPerDirjen } from "../../../../references/template_draft/RPerDirjen";
// import { RUU } from "../../../../references/template_draft/RUU";
// import { RPP } from "../../../../references/template_draft/RPP";
// import { RPMK } from "../../../../references/template_draft/RPMK";
// import CustomFileInput from "../../../../helpers/form/CustomFileInput";
// import CustomLampiranInput from "../../../../helpers/form/CustomLampiranInput";

// import { CKEditor } from "@ckeditor/ckeditor5-react";
// import Editor from "ckeditor5-custom-build/build/ckeditor";
// import "./decoupled.css";
// class BodyEditor extends Component {
//   constructor(props) {
//     super(props);

//     this.modules = {
//       toolbar: [
//         [{ font: [] }],
//         [{ size: ["small", false, "large", "huge"] }],
//         ["bold", "italic", "underline"],
//         [{ list: "ordered" }, { list: "bullet" }],
//         [{ align: [] }],
//         [{ color: [] }, { background: [] }],
//         ["clean"],
//       ],
//     };

//     this.formats = [
//       "font",
//       "size",
//       "bold",
//       "italic",
//       "underline",
//       "list",
//       "bullet",
//       "align",
//       "color",
//       "background",
//     ];

//     this.state = {
//       comments: "",
//     };

//     this.rteChange = this.rteChange.bind(this);
//   }

//   rteChange = (content, delta, source, editor) => {
//     console.log(editor.getHTML()); // rich text
//     console.log(editor.getText()); // plain text
//     console.log(editor.getLength()); // number of characters
//   };

//   render() {
//     return (
//       <div>
//         <ReactQuill
//           theme="bubble"
//           readOnly={true}
//           modules={this.modules}
//           formats={this.formats}
//           onChange={this.rteChange}
//           value={this.state.comments || ASU}
//         />
//       </div>
//     );
//   }
// }
// export default BodyEditor;
