import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";
import { Pagination } from "../pagination/Pagination";
import { getRekomendasiSearchKantor } from "../Api";
import RekomendasiHistory from "./RekomendasiHistory";
import { useSelector } from "react-redux";

function DaftarRekomendasiTable() {
  const { role, user } = useSelector((state) => state.auth);
  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "wkt_update",
    pageNumber: 1,
    pageSize: 50,
  };
  const history = useHistory();

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/rekomendasi/monitoring/${id}/history`);

  const detailRekomendasi = (id) =>
    history.push(`/knowledge/rekomendasi/monitoring/${id}/detail`);

  const [rekomendasi, setRekomendasi] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "km_rekomendasi.no_hsl_pemeriksaan",
      text: "No Pemeriksaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "km_rekomendasi.tgl_pemeriksaan",
      text: "Tgl Pemeriksaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "km_rekomendasi.perihal",
      text: "perihal Pemeriksaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "jns_pemeriksaan.nama",
      text: "Jenis Pemeriksaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "km_rekomendasi.status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.RekomendasiStatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.DaftarRekomendasiActionColumnFormatter,
      formatExtraData: {
        showHistory: showHistoryPengajuan,
        detailRekomendasi: detailRekomendasi,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const { SearchBar } = Search;

  useEffect(() => {
    getRekomendasiSearchKantor(user.kantorLegacyKode).then(({ data }) => {
      data.map((data) => {
        return data.km_rekomendasi.status === "Eselon 4"
          ? setRekomendasi((rekomendasi) => [...rekomendasi, data])
          : data.km_rekomendasi.status === "Eselon 3"
          ? setRekomendasi((rekomendasi) => [...rekomendasi, data])
          : data.km_rekomendasi.status === "Terima"
          ? setRekomendasi((rekomendasi) => [...rekomendasi, data])
          : null;
      });
    });
  }, []);

  const defaultSorted = [{ dataField: "wkt_update", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: rekomendasi.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="km_rekomendasi.id_rekomendasi"
                  data={rekomendasi}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3"></div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/rekomendasi/monitoring/:id/history">
        {({ history, match }) => (
          <RekomendasiHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/rekomendasi/monitoring");
            }}
            onRef={() => {
              history.push("/knowledge/rekomendasi/monitoring");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default DaftarRekomendasiTable;
