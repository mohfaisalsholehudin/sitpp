import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import RekomendasiTable from "./RekomendasiTable";

function Rekomendasi() {
  return (
    <>
      <Card>
        <CardHeader
          title="Penelitian Rekomendasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <RekomendasiTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Rekomendasi;
