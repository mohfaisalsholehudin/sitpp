import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import DaftarRekomendasiTable from "./DaftarRekomendasiTable";

function DaftarRekomendasi() {
  return (
    <>
      <Card>
        <CardHeader
          title="Monitoring Rekomendasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <DaftarRekomendasiTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default DaftarRekomendasi;
