import React from "react";

function ProposalEditFooter({ backAction, btnRef}) {

  const saveForm = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };


  return (
    <>
      <div className="col-lg-12" style={{ textAlign: "right" }}>
        <button
          type="button"
          onClick={backAction}
          className="btn btn-light"
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
          }}
        >
          <i className="fa fa-arrow-left"></i>
          Kembali
        </button>
      
        <button
          type="submit"
          className="btn btn-success ml-2"
          onClick={saveForm}
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
          }}
        >
          <i className="fas fa-save"></i>
          Simpan
        </button>
      </div>
    </>
  );
}

export default ProposalEditFooter;
