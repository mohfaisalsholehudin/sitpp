import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
  Checkbox,
} from "./helpers";
import CustomFileInput from "./helpers/CustomFileInput";
import { getJenisPemeriksaan } from "../../../Api";

function RekomendasiEditForm({
  proposal,
  loading,
  btnRef,
  saveProposal,
  backAction,
}) {
  const [jenis, setJenis] = useState([]);
  const { user } = useSelector((state) => state.auth);

  // Validation schema
  const RekomendasiEditSchema = Yup.object().shape({
    no_hsl_pemeriksaan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No Surat is required"),
    tgl_pemeriksaan: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),
    id_detiljns: Yup.string()
      .min(1, "Jenis Pemeriksaan is required")
      .required("Jenis Pemeriksaan is required"),
    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      .required("Perihal is required"),
    thn_periksa: Yup.string()
    .required("Tahun Pemeriksaan is required")
    .matches(/^[0-9]+$/, "Harus diisi dengan angka")
    .min(4, 'Minimum 4 symbols')
    .max(4, 'Maximum 4 symbols'),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        (value) => value && SUPPORTED_FORMATS.includes(value.type)
      ),
  });

  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    getJenisPemeriksaan().then(({ data }) => {
      setJenis(data);
    });
  }, []);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={RekomendasiEditSchema}
        onSubmit={(values) => {
          saveProposal(values);
          // console.log(values)
        }}
      >
        {({ handleSubmit, setFieldValue, isSubmitting, values }) => {
          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          };

          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    name="no_hsl_pemeriksaan"
                    component={Input}
                    placeholder="Nomor Hasil Pemeriksaan"
                    label="Nomor Hasil Pemeriksaan"
                    // onClick={()=>handleChangeNip()}
                  />
                </div>

                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_pemeriksaan"
                    label="Tanggal Hasil Pemeriksaan"
                  />
                </div>

                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="id_detiljns" label="Jenis Pemeriksaan">
                    <option value="">Jenis Pemeriksaan</option>
                    {jenis.map((data, index) => (
                      <option key={index} value={data.id}>
                        {data.nama}
                      </option>
                    ))}
                  </Sel>
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    // onClick={()=>handleChangeKdKantor()}
                  />
                </div>

                {/* FIELD tahun pemeriksaan */}
                <div className="form-group row">
                  <Field
                    name="thn_periksa"
                    component={Input}
                    placeholder="Tahun Pemeriksaan"
                    label="Tahun Pemeriksaan"
                  />
                </div>

                {/* FIELD UPLOAD FILE */}
                {values.no_hsl_pemeriksaan ? (
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Upload File
                    </label>
                    <div className="col-lg-8 col-xl-6">
                      <Field
                        name="file"
                        component={CustomFileInput}
                        title="Select a file"
                        label="File"
                        style={{ display: "flex" }}
                      />
                    </div>
                  </div>
                ) : null}
                {/* Ganti Footer */}
                <div className="col-lg-12" style={{ textAlign: "right" }}>
                  <button
                    type="button"
                    onClick={backAction}
                    className="btn btn-light"
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                    }}
                  >
                    <i className="fa fa-arrow-left"></i>
                    Kembali
                  </button>
                  {`  `}

                  {loading ? (
                    <button
                      type="submit"
                      className="btn btn-success spinner spinner-white spinner-left ml-2"
                      onSubmit={() => handleSubmit()}
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                      disabled={isSubmitting}
                    >
                      <span>Simpan</span>
                    </button>
                  ) : (
                    <button
                      type="submit"
                      className="btn btn-success ml-2"
                      onSubmit={() => handleSubmit()}
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                      disabled={isSubmitting}
                    >
                      <i className="fas fa-save"></i>
                      <span>Simpan</span>
                    </button>
                  )}
                </div>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default RekomendasiEditForm;
