import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import DokumentasiTable from "./DokumentasiTable";

function Dokumentasi() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Dokumentasi Perencanaan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <DokumentasiTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Dokumentasi;
