import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";
import { Pagination } from "../pagination/Pagination";
import { getPutusanSearchKantor } from "../Api";
import PutusanHistory from "./PutusanHistory";
import { useSelector } from "react-redux";

function DaftarPutusanTable() {
  const { role, user } = useSelector((state) => state.auth);
  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "wkt_update",
    pageNumber: 1,
    pageSize: 50,
  };
  const history = useHistory();

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/putusan/monitoring/${id}/history`);

  const detailPutusan = (id) =>
    history.push(`/knowledge/putusan/monitoring/${id}/detail`);

  const [putusan, setPutusan] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "putusanPengadilan.no_putusan",
      text: "No Putusan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "putusanPengadilan.tgl_putusan",
      text: "Tgl Putusan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "bentukPengadilan.nama",
      text: "Bentuk Pengadilan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "putusanPengadilan.perihal",
      text: "Perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "putusanPengadilan.status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.PutusanStatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.DaftarPutusanColumnActionFormatter,
      formatExtraData: {
        showHistory: showHistoryPengajuan,
        detailPutusan: detailPutusan,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const { SearchBar } = Search;

  useEffect(() => {
    getPutusanSearchKantor(user.kantorLegacyKode).then(({ data }) => {
      data.map((data) => {
        return data.putusanPengadilan.status === "Eselon 4"
          ? setPutusan((putusan) => [...putusan, data])
          : data.putusanPengadilan.status === "Eselon 3"
          ? setPutusan((putusan) => [...putusan, data])
          : data.putusanPengadilan.status === "Terima"
          ? setPutusan((putusan) => [...putusan, data])
          : null;
      });
    });
  }, []);

  const defaultSorted = [{ dataField: "wkt_update", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: putusan.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="putusanPengadilan.id_putpengadilan"
                  data={putusan}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3"></div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/putusan/monitoring/:id/history">
        {({ history, match }) => (
          <PutusanHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/putusan/monitoring");
            }}
            onRef={() => {
              history.push("/knowledge/putusan/monitoring");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default DaftarPutusanTable;
