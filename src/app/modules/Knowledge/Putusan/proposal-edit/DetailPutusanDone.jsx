import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import RegulasiDetailForm from "./RegulasiDetailForm";
import RegulasiTerkaitFooter from "./RegulasiTerkaitFooter";
import { getPutusanById, getPeraturanTerkaitPutusan, updateStatusPutusan } from "../../Api";
import swal from "sweetalert";

const initValues = {
    id_putpengadilan: "",
    no_putusan: "",
    id_detiljns: "",
    perihal: "",
    file_upload: "",
    alasan_tolak: "",
    body: "",
    nip_pengusul: "",
    nip_pjbt_es4: "",
    nip_pjbt_es3: "",
    status: "",
  };

function DetailPutusanDone({
   history,
   match: {
    params: { id }
    }
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [proposal, setProposal] = useState();
  const [peraturanTerkait, setPeraturanTerkait] = useState([]);
  let thePath = document.URL;
  let nextStatus = '';
  const lastPath = thePath.substring(thePath.lastIndexOf('/') + 1);

  useEffect(() => {

      let _title = "Detail Putusan";
      setTitle(_title);
      suhbeader.setTitle(_title);

    getPutusanById(id).then(({ data }) => {

      setProposal({
        id_putusan: data.id_putusan,
        no_putusan: data.no_putusan,
        tgl_putusan: data.tgl_putusan,
        id_detiljns: data.id_detiljns,
        unit_putusan: data.unit_putusan,
        perihal: data.perihal,
        file_upload: data.file_upload,
        alasan_tolak: data.alasan_tolak,
        body: data.body,
        status: data.status,
        nip_pengusul: data.nip_pengusul,
        nip_pjbt_es4: data.nip_pjbt_es4,
        nip_pjbt_es3: data.nip_pjbt_es3,
        wkt_usul: data.wkt_usul,
        wkt_teliti_es4: data.wkt_teliti_es4,
        wkt_teliti_es3: data.wkt_teliti_es3,
        wkt_create: data.wkt_create,
        wkt_update: data.wkt_update,
      });

    });

  }, [id, suhbeader]);


  useEffect(() => {

    getPeraturanTerkaitPutusan(id).then(({ data }) => {
      data.map((dt) => {
        //setPeraturanTerkait(peraturanTerkait => [...peraturanTerkait,dt.kmperaturanTerkait]);

        setPeraturanTerkait(peraturanTerkait => [...peraturanTerkait, {
          id_per_terkait: dt.kmperaturanTerkait.id_per_terkait,
          id_peraturan: dt.kmregulasiPerpajakan.id_peraturan,
          no_regulasi: dt.kmregulasiPerpajakan.no_regulasi,
          tgl_regulasi: dt.kmregulasiPerpajakan.tgl_regulasi,
          perihal: dt.kmregulasiPerpajakan.perihal,
          status: dt.kmregulasiPerpajakan.status,
          file_upload: dt.kmregulasiPerpajakan.file_upload,
          jns_regulasi: dt.kmregulasiPerpajakan.jns_regulasi,
          id_topik: dt.kmregulasiPerpajakan.id_topik,
          body: dt.kmregulasiPerpajakan.body,

        }]);

      })

    });



  }, []);



  const btnRef = useRef();

  const saveRegulasiTerkait = values => {
  };


  const backToProposalList = () => {
      history.push(`/knowledge/putusan/monitoring`);
  };

  const setDisabled = val => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <RegulasiDetailForm
            actionsLoading={actionsLoading}
            proposal={proposal || initValues}
            peraturanTerkait={peraturanTerkait}
            idPutusan={id}
            btnRef={btnRef}
            saveRegulasiTerkait={saveRegulasiTerkait}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <RegulasiTerkaitFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></RegulasiTerkaitFooter>
      </CardFooter>
    </Card>
  );
}

export default DetailPutusanDone;
