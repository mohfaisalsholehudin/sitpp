import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import PutusanTable from "./PutusanTable";

function Putusan() {
  return (
    <>
      <Card>
        <CardHeader
          title="Penelitian Putusan Pengadilan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <PutusanTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Putusan;
