import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import FgdTable from "./FgdTable";

function Fgd() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar FGD"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <FgdTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Fgd;
