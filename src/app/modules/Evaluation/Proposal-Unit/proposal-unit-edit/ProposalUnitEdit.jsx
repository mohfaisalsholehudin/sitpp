import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
// import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import ProposalUnitEditForm from "./ProposalUnitEditForm";
import ProposalUnitEditFooter from "./ProposalUnitEditFooter";
import { saveUsulan } from "../../Api";
import swal from "sweetalert";

//12 data yang dikirim, yang belum dimasukkan 'nip_perekam', 'jns_usul' dan yang tidak dimasukkan disini adalah 'alasan_tolak'.
const initValues = {
  no_surat: "",
  tgl_surat: "",
  perihal: "",
  id_jnspajak: "",
  // unit_kerja: "",
  instansi: "",
  alamat: "",
  no_pic: "",
  nm_pic: "",
  file: "",
  jns_pengusul: "",
  status: ""
};

function ProposalUnitEdit({
  history,
  match: {
    params: { id }
  }
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [idUsulan, setIdUsulan] = useState();
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [complete, setComplete] = useState(false);


  useEffect(() => {
    let _title = id ? "Edit Usulan Evaluasi" : "Tambah Usulan Evaluasi";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);
  const btnRef = useRef();
  const saveProposal = values => {
    if (!id) {
      if(complete)
      {
        console.log(idUsulan)
        swal("Berhasil", "Data berhasil diajukan", "success")
        .then(() => {
          console.log(values)
          history.push("/evaluation/unit")     
        });
      } else {
        swal("Berhasil", "Data berhasil disimpan", "success")
        .then(() => {
          setComplete(true);
          setIdUsulan(2);
          console.log(values)     
        });
      }
      // saveUsulan(
      //   values.alamat,
      //   "",
      //   values.file.name,
      //   values.id_jnspajak,
      //   values.instansi,
      //   values.jns_pengusul,
      //   "",
      //   "",
      //   values.nm_pic,
      //   values.no_pic,
      //   values.no_surat,
      //   values.perihal,
      //   "Draft",
      //   values.tgl_surat,
      //   ""
      // ).then(({ status }) => {
      //   if (status === 201 || status === 200) {
      //     swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
      //       history.push("/evaluation/proposal");
      //     });
      //   } else {
      //     swal("Gagal", "Data gagal disimpan", "error").then(() => {
      //       history.push("/evaluation/proposal/new");
      //     });
      //   }
      // });
    } else {
      //   dispatch(actions.updateProduct(values)).then(() => backToProductsList());
      //masukkan API untuk Update
    }
  };
  const backToProposalList = () => {
    history.push(`/evaluation/unit`);
  };

  const setDisabled = val => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <ProposalUnitEditForm
            actionsLoading={actionsLoading}
            proposal={initValues}
            //if edit maka proposal nya diambil dari nilai proposal yang di get dari API nanti
            btnRef={btnRef}
            saveProposal={saveProposal}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <ProposalUnitEditFooter
          backAction={backToProposalList}
          btnRef={btnRef}
          disabled={isDisabled}
          complete={complete}
        ></ProposalUnitEditFooter>
      </CardFooter>
    </Card>
  );
}

export default ProposalUnitEdit;
