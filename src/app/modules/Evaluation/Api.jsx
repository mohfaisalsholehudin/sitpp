import axios from "axios";
const { BACKEND_URL } = window.ENV;


//API USULAN
export function getUsulan(kdkantor) {
  return axios.get(`${BACKEND_URL}/api/usulan/bykdkantor/${kdkantor}`);
}
export function getUsulanBySeksi(unitorg) {
  return axios.get(`${BACKEND_URL}/api/usulan/byseksi/${unitorg}`);
}

export function getUsulanBySubdit(unitorg) {
  return axios.get(`${BACKEND_URL}/api/usulan/bysubdit/${unitorg}`);
}

export function getUsulanByAdmin() {
  return axios.get(`${BACKEND_URL}/api/usulan/`);
}

export function getUsulanById(id) {
  return axios.get(`${BACKEND_URL}/api/usulan/${id}`);
}

export function deleteUsulan(id) {
  return axios.delete(`${BACKEND_URL}/api/usulan/${id}`);
}

export function saveUsulan(
  alamat,
  alasan_tolak,
  file_upload,
  instansi,
  jns_pajak,
  jns_pengusul,
  jns_usul,
  nip_perekam,
  nm_pic,
  no_pic,
  no_surat,
  perihal,
  tgl_surat,
  unit_kerja,
  kd_kantor,
  kd_unit_org
) {
  // return axios.post('/api/usulan',{values})
  return axios.post(`${BACKEND_URL}/api/usulan/`, {
    alamat,
    alasan_tolak,
    file_upload,
    instansi,
    jns_pajak,
    jns_pengusul,
    jns_usul,
    nip_perekam,
    nm_pic,
    no_pic,
    no_surat,
    perihal,
    tgl_surat,
    unit_kerja,
    kd_kantor,
    kd_unit_org
  });
}

export function updateUsulan(
  id,
  alamat,
  alasan_tolak,
  file_upload,
  instansi,
  jns_pajak,
  jns_pengusul,
  jns_usul,
  nip_perekam,
  nm_pic,
  no_pic,
  no_surat,
  perihal,
  tgl_surat,
  unit_kerja,
  kd_kantor,
  kd_unit_org
) {
  return axios.put(`${BACKEND_URL}/api/usulan/${id}`, {
    alamat,
    alasan_tolak,
    file_upload,
    instansi,
    jns_pajak,
    jns_pengusul,
    jns_usul,
    nip_perekam,
    nm_pic,
    no_pic,
    no_surat,
    perihal,
    tgl_surat,
    unit_kerja,
    kd_kantor,
    kd_unit_org
  });
}

export function updateStatusUsulan(id, status, alasan = null) {
  if (alasan) {
    return axios.put(
      `${BACKEND_URL}/api/usulan/updateusulanstatus/${id}/${status}`,
      {
        alasan_tolak: alasan
      }
    );
  } else {
    return axios.put(
      `${BACKEND_URL}/api/usulan/updateusulanstatus/${id}/${status}`,
      {}
    );
  }
}

export function uploadFile(formData) {
  const config = {
    headers: {
      "Content-type": "multipart/form-data"
    }
  };
  return axios.post(`${BACKEND_URL}/api/upload`, formData, config);
}

export function uploadFileNew(formData) {
  const config = {
    headers: {
      "Content-type": "multipart/form-data"
    }
  };
  return axios.post(`${BACKEND_URL}/api/newupload`, formData, config);
}

export function uploadZip(formData) {
  const config = {
    headers: {
      "Content-type": "multipart/form-data"
    }
  };
  return axios.post(`${BACKEND_URL}/api/uploadzip`, formData, config);
}

export function downloadZip(file) {
  return axios.get(`${BACKEND_URL}/api/filesdownloadzip/${file}`);
}

export function getJenisPajak() {
  return axios.get(`${BACKEND_URL}/api/jnspajak/`);
}

export function getJenisPajakById(id_jnspajak) {
  return axios.get(`${BACKEND_URL}/api/jnspajak/${id_jnspajak}`);
}

export function saveJenisPajak(kodekantor, kodeunit, nm_jnspajak, status) {
  return axios.post(`${BACKEND_URL}/api/jnspajak/`, {
    kodekantor,
    kodeunit,
    nm_jnspajak,
    status
  });
}

export function updateJenisPajak(
  id_jnspajak,
  kodekantor,
  kodeunit,
  nm_jnspajak,
  status
) {
  return axios.put(`${BACKEND_URL}/api/jnspajak/${id_jnspajak}`, {
    kodekantor,
    kodeunit,
    nm_jnspajak,
    status
  });
}

export function deleteJenisPajak(id_jnspajak) {
  return axios.delete(`${BACKEND_URL}/api/jnspajak/${id_jnspajak}`);
}

export function getJenisPeraturan() {
  return axios.get(`${BACKEND_URL}/api/jnsperaturan/`);
}

export function getJenisPeraturanByAplikasi(aplikasi) {
  return axios.get(`${BACKEND_URL}/api/jnsperaturan/byaplikasi/${aplikasi}`);
}

export function getJenisPeraturanById(id) {
  return axios.get(`${BACKEND_URL}/api/jnsperaturan/${id}`);
}

export function saveJenisPeraturan(nm_jnsperaturan, status, aplikasi) {
  return axios.post(`${BACKEND_URL}/api/jnsperaturan/`, {
    nm_jnsperaturan,
    status,
    aplikasi
  });
}

export function updateJenisPeraturan(id, nm_jnsperaturan, status, aplikasi) {
  return axios.put(`${BACKEND_URL}/api/jnsperaturan/${id}`, {
    nm_jnsperaturan,
    status,
    aplikasi
  });
}

export function deleteJenisPeraturan(id) {
  return axios.delete(`${BACKEND_URL}/api/jnsperaturan/${id}`);
}

export function getMasterJenis() {
  return axios.get(`${BACKEND_URL}/api/masterjenis/`);
}

export function saveMasterJenis(nama, keterangan, deleted='no') {
  return axios.post(`${BACKEND_URL}/api/masterjenis/`, { nama, keterangan, deleted });
}

export function deleteMasterJenis(id) {
  return axios.delete(`${BACKEND_URL}/api/masterjenis/${id}`);
}

export function getDetilJenis() {
  return axios.get(`${BACKEND_URL}/api/detiljenis/`);
}

export function getDetilJenisById(id) {
  return axios.get(`${BACKEND_URL}/api/detiljenis/${id}`);
}

export function saveDetilJenis(id_jenis, nama, keterangan, deleted='no') {
  return axios.post(`${BACKEND_URL}/api/detiljenis/`, {
    id_jenis,
    keterangan,
    nama,
    deleted
  });
}

export function updateDetilJenis(id, id_jenis, nama, keterangan , deleted='no'){
  return axios.put(`${BACKEND_URL}/api/detiljenis/${id}`,{
    id_jenis, keterangan, nama, deleted
  })
}

export function deleteDetilJenis(id) {
  return axios.delete(`${BACKEND_URL}/api/detiljenis/${id}`);
}

//API PENELITIAN
export function getPenelitianUsulan(unitorg, status) {
  return axios.get(
    `${BACKEND_URL}/api/usulan/byseksidanstatus/${unitorg}/${status}`
  );
}

// API PERENCANAAN
export function getPerencanaan(kdkantor) {
  return axios.get(`${BACKEND_URL}/api/perencanaan/bykdkantor/${kdkantor}`);
}
export function getPerencanaanSort(kdkantor, status='Terima'){
  return axios.get(`${BACKEND_URL}/api/perencanaan/bykdkantordanstatus/${kdkantor}/${status}`);
}

export function getPerencanaanSeksi(unitorg) {
  return axios.get(`${BACKEND_URL}/api/perencanaan/byseksi/${unitorg}`);
}
export function getPerencanaanSubdit(unitorg) {
  return axios.get(`${BACKEND_URL}/api/perencanaan/bysubdit/${unitorg}`);
}
export function getPerencanaanByAdmin(){
  return axios.get(`${BACKEND_URL}/api/perencanaan/`)
}
//API UNIT KERJA

export function getUnitKerja() {
  return axios.get(`${BACKEND_URL}/api/unitkerja/`);
}

export function getLogUsulan(id) {
  return axios.get(`${BACKEND_URL}/api/logstatus/findByUsulan/${id}`);
}

//API VALIDASI
export function getValidasiUsulan(unitorg) {
  // return axios.get('/usulan/bystatus/Terima')
  // return axios.get(`${BACKEND_URL}/api/validator/bykdkantor/${unitorg}`);
  return axios.get(`${BACKEND_URL}/api/validator/byseksi/${unitorg}`);
}

export function getValidasiUsulanByAdmin(){
  return axios.get(`${BACKEND_URL}/api/validator/`)
}

export function getValidasiUsulanSubdit(unitorg) {
  return axios.get(`${BACKEND_URL}/api/validator/bysubdit/${unitorg}`);
}

export function getValidasiUsulanKantor(kdkantor) {
  return axios.get(`${BACKEND_URL}/api/validator/bykdkantor/${kdkantor}`);
}

export function getValidasiSort(kdkantor, status) {
  return axios.get(`${BACKEND_URL}/api/validator/bykdkantordanstatus/${kdkantor}/${status}`)
}

// export function getJenisPeraturan() {
//   return axios.get("/jnsperaturan/");
// }

export function getValidasiById(id) {
  return axios.get(`${BACKEND_URL}/api/validator/${id}`);
}

export function saveValidasi(
  id_usulan,
  id_validator,
  alasan,
  analisa_dampak,
  file_upload,
  isu_masalah,
  jns_peraturan,
  judul_peraturan,
  konten_peraturan,
  nip_perekam,
  tentang,
  unit_incharge,
  unit_pengusul,
  nm_pegawai,
  no_peraturan,
  kd_kantor,
  tgl_evaluasi,
  kd_unit_org
) {
  return axios.put(`${BACKEND_URL}/api/validator/${id_validator}`, {
    id_usulan,
    alasan,
    analisa_dampak,
    file_upload,
    isu_masalah,
    jns_peraturan,
    judul_peraturan,
    konten_peraturan,
    nip_perekam,
    tentang,
    unit_incharge,
    unit_pengusul,
    nm_pegawai,
    no_peraturan,
    kd_kantor,
    tgl_evaluasi,
    kd_unit_org
  });
}

export function updateStatusValidasi(
  id,
  status,
  kd_kantor,
  alasan = null,
  catatan = null
) {
  if (alasan) {
    return axios.put(
      `${BACKEND_URL}/api/validator/updatevalidatorstatus/${id}/${status}/${kd_kantor}`,
      {
        alasan_tolak: alasan
      }
    );
  } else if (catatan) {
    if (status === 3) {
      return axios.put(
        `${BACKEND_URL}/api/validator/updatevalidatorstatus/${id}/${status}/${kd_kantor}`,
        {
          catatan_es4: catatan
        }
      );
    } else {
      return axios.put(
        `${BACKEND_URL}/api/validator/updatevalidatorstatus/${id}/${status}/${kd_kantor}`,
        {
          catatan_es3: catatan
        }
      );
    }
  } else {
    return axios.put(
      `${BACKEND_URL}/api/validator/updatevalidatorstatus/${id}/${status}/${kd_kantor}`,
      {}
    );
  }
}

export function checkToken(iamToken) {
  return axios.post(
    `${BACKEND_URL}/api/iam/getwhoami`,
    {},
    { headers: { Authorization: iamToken } }
  );
}

// export function getByNip(nip){
//   return axios.post(
//     `${BACKEND_URL}/api/iam/getbynip`,
//     {nip},
//     { headers: { Authorization: token } }
//   );
// }

export function savePerencanaan(
  alasan,
  analisa_dampak,
  file_kajian,
  id_usulan,
  id_validator,
  isu_masalah,
  jns_pajak,
  jns_peraturan,
  judul_peraturan,
  konten_peraturan,
  nip_perekam,
  no_evaluasi,
  no_peraturan,
  simfoni,
  tentang,
  tgl_perencanaan,
  unit_incharge,
  kd_kantor,
  nm_perekam,
  kd_unit_org
) {
  return axios.post(`${BACKEND_URL}/api/perencanaan/`, {
    alasan,
    analisa_dampak,
    file_kajian,
    id_usulan,
    id_validator,
    isu_masalah,
    jns_pajak,
    jns_peraturan,
    judul_peraturan,
    konten_peraturan,
    nip_perekam,
    no_evaluasi,
    no_peraturan,
    simfoni,
    tentang,
    tgl_perencanaan,
    unit_incharge,
    kd_kantor,
    nm_perekam,
    kd_unit_org
  });
}

export function getPerencanaanById(id) {
  return axios.get(`${BACKEND_URL}/api/perencanaan/${id}`);
}

export function deletePerencanaan(id_perencanaan) {
  return axios.delete(`${BACKEND_URL}/api/perencanaan/${id_perencanaan}`);
}

export function updatePerencanaan(
  id,
  alasan,
  analisa_dampak,
  file_kajian,
  id_usulan,
  id_validator,
  isu_masalah,
  jns_pajak,
  jns_peraturan,
  judul_peraturan,
  konten_peraturan,
  nip_perekam,
  no_evaluasi,
  no_peraturan,
  simfoni,
  tentang,
  tgl_perencanaan,
  unit_incharge,
  kd_kantor,
  nm_perekam,
  kd_unit_org
) {
  return axios.put(`${BACKEND_URL}/api/perencanaan/${id}`, {
    alasan,
    analisa_dampak,
    file_kajian,
    id_usulan,
    id_validator,
    isu_masalah,
    jns_pajak,
    jns_peraturan,
    judul_peraturan,
    konten_peraturan,
    nip_perekam,
    no_evaluasi,
    no_peraturan,
    simfoni,
    tentang,
    tgl_perencanaan,
    unit_incharge,
    kd_kantor,
    nm_perekam,
    kd_unit_org
  });
}

export function applyPerencanaan(id, kd_kantor) {
  return axios.put(
    `${BACKEND_URL}/api/perencanaan/updateperencanaanstatus/${id}/2/${kd_kantor}`,
    {}
  );
}

export function updateStatusPerencanaan(
  id,
  status,
  kd_kantor,
  alasan = null,
  catatan = null
) {
  if (alasan) {
    return axios.put(
      `${BACKEND_URL}/api/perencanaan/updateperencanaanstatus/${id}/${status}/${kd_kantor}`,
      {
        alasan_tolak: alasan
      }
    );
  } else if (catatan) {
    if (status === 3) {
      return axios.put(
        `${BACKEND_URL}/api/perencanaan/updateperencanaanstatus/${id}/${status}/${kd_kantor}`,
        {
          catatan_es4: catatan
        }
      );
    } else {
      return axios.put(
        `${BACKEND_URL}/api/perencanaan/updateperencanaanstatus/${id}/${status}/${kd_kantor}`,
        {
          catatan_es3: catatan
        }
      );
    }
  } else {
    return axios.put(
      `${BACKEND_URL}/api/perencanaan/updateperencanaanstatus/${id}/${status}/${kd_kantor}`,
      {}
    );
  }
}

export function getLogValidasi(id) {
  return axios.get(`${BACKEND_URL}/api/logstatus/findByValidator/${id}`);
}
export function getLogPerencanaan(id) {
  return axios.get(`${BACKEND_URL}/api/logstatus/findByPerencanaan/${id}`);
}

/* API PENYUSUNAN */

export function getPenyusunan(kdkantor) {
  return axios.get(`${BACKEND_URL}/api/penyusunan/bykdkantor/${kdkantor}`);
}

export function getPenyusunanByAdmin(){
  return axios.get(`${BACKEND_URL}/api/penyusunan/`)
}

export function getPenyusunanByAdminStatus(status){
  return axios.get(`${BACKEND_URL}/api/penyusunan/bystatus/${status}`)
}

export function getPenyusunanSeksi(unitorg){
  return axios.get(`${BACKEND_URL}/api/penyusunan/byseksi/${unitorg}`)
}

export function getPenyusunanSubdit(unitorg) {
  return axios.get(`${BACKEND_URL}/api/penyusunan/bysubdit/${unitorg}`);
}

export function getPenyusunanById(id) {
  return axios.get(`${BACKEND_URL}/api/penyusunan/${id}`);
}

export function getPenyusunanByStatus(status) {
  return axios.get(`${BACKEND_URL}/api/penyusunan/bystatus/${status}`);
}

export function getPenyusunanByKodeKantorDanStatus(kdkantor, status) {
  return axios.get(
    `${BACKEND_URL}/api/penyusunan/bykdkantordanstatus/${kdkantor}/${status}`
  );
}
export function getPenyusunanBySeksiDanStatus(unitorg, status) {
  return axios.get(
    `${BACKEND_URL}/api/penyusunan/byseksidanstatus/${unitorg}/${status}`
  );
}
export function getPenyusunanBySubditDanStatus(unitorg, status) {
  return axios.get(
    `${BACKEND_URL}/api/penyusunan/bysubditdanstatus/${unitorg}/${status}`
  );
}

export function deletePenyusunan(id_penyusunan) {
  return axios.put(`${BACKEND_URL}/api/penyusunan/${id_penyusunan}`, {
    delete: "yes"
  });
}

export function savePenyusunanJudul(id_penyusunan, judul_peraturan){
  return axios.put(`${BACKEND_URL}/api/penyusunan/${id_penyusunan}`,{judul_peraturan})
}

export function savePenyusunan(
  id_perencanaan,
  jns_peraturan,
  judul_peraturan,
  kd_kantor,
  no_penyusunan,
  no_peraturan,
  no_perencanan,
  simfoni,
  tentang,
  tgl_penyusunan,
  unit_incharge,
  update_file_kajian,
  kd_unit_org
) {
  // return axios.post('/api/usulan',{values})
  return axios.post(`${BACKEND_URL}/api/penyusunan/`, {
    id_perencanaan,
    jns_peraturan,
    judul_peraturan,
    kd_kantor,
    no_penyusunan,
    no_peraturan,
    no_perencanan,
    simfoni,
    tentang,
    tgl_penyusunan,
    unit_incharge,
    update_file_kajian,
    kd_unit_org
  });
}

export function updatePenyusunan(
  id,
  id_perencanaan,
  jns_peraturan,
  judul_peraturan,
  kd_kantor,
  no_penyusunan,
  no_peraturan,
  no_perencanan,
  simfoni,
  tentang,
  tgl_penyusunan,
  unit_incharge,
  update_file_kajian,
  kd_unit_org
) {
  return axios.put(`${BACKEND_URL}/api/penyusunan/${id}`, {
    id_perencanaan,
    jns_peraturan,
    judul_peraturan,
    kd_kantor,
    no_penyusunan,
    no_peraturan,
    no_perencanan,
    simfoni,
    tentang,
    tgl_penyusunan,
    unit_incharge,
    update_file_kajian,
    kd_unit_org
  });
}
export function updatePenyusunanKajianUnit(id, file_kajian_unit) {
  return axios.put(`${BACKEND_URL}/api/penyusunan/${id}`, {
    file_kajian_unit
  });
}
export function updatePenyusunanKajianSahli(id, file_kajian_sahli) {
  return axios.put(`${BACKEND_URL}/api/penyusunan/${id}`, {
    file_kajian_sahli
  });
}
export function updatePenyusunanKajianDirjen(id, file_kajian_direktur) {
  return axios.put(`${BACKEND_URL}/api/penyusunan/${id}`, {
    file_kajian_direktur
  });
}

export function updateStatusPenyusunan(id, status, kd_kantor, alasan = null, catatan_draft = null, catatan_direktorat = null, catatan_sahli= null, catatan=null) {
  if (alasan) {
    return axios.put(
      `${BACKEND_URL}/api/penyusunan/updatepenyusunanstatus/${id}/${status}/${kd_kantor}`,
      {
        alasan_tolak: alasan
      }
    );
  }else if(catatan_draft){
    return axios.put(`${BACKEND_URL}/api/penyusunan/updatepenyusunanstatus/${id}/${status}/${kd_kantor}`,{
      catatan_draft
    })
  } else if(catatan_direktorat){
    return axios.put(`${BACKEND_URL}/api/penyusunan/updatepenyusunanstatus/${id}/${status}/${kd_kantor}`,{
      catatan_direktorat
    })
  } else if(catatan_sahli){
    return axios.put(`${BACKEND_URL}/api/penyusunan/updatepenyusunanstatus/${id}/${status}/${kd_kantor}`,{
      catatan_sahli
    })
  } else if(catatan){
  if(status === 16){
    return axios.put(
      `${BACKEND_URL}/api/penyusunan/updatepenyusunanstatus/${id}/${status}/${kd_kantor}`,
      { catatan_es4: catatan }
    );
  } else {
    return axios.put(
      `${BACKEND_URL}/api/penyusunan/updatepenyusunanstatus/${id}/${status}/${kd_kantor}`,
      { catatan_es3: catatan }
    );
  }
  }
    else {
    return axios.put(
      `${BACKEND_URL}/api/penyusunan/updatepenyusunanstatus/${id}/${status}/${kd_kantor}`,
      { alasan_tolak: "" }
    );
  }
}

export function getLogPenyusunan(id) {
  return axios.get(`${BACKEND_URL}/api/logstatus/findByPenyusunan/${id}`);
}

/* API PENYUSUNAN - Surat Undangan */

export function getSurat(id) {
  return axios.get(`${BACKEND_URL}/api/surat/byidpenyusunan/${id}`);
}

export function getJenisSurat(id_jenis) {
  return axios.get(`${BACKEND_URL}/api/detiljenis/byidjenis/${id_jenis}`);
}

export function getJenisSuratById(id) {
  return axios.get(`${BACKEND_URL}/api/detiljenis/${id}`);
}

export function getSuratByIdSurat(id_surat) {
  return axios.get(`${BACKEND_URL}/api/surat/${id_surat}`);
}

export function deleteSuratMenyurat(id_surat) {
  return axios.delete(`${BACKEND_URL}/api/surat/${id_surat}`);
}

export function saveSuratUndangan(
  id_penyusunan,
  nomor_surat,
  perihal,
  tgl_surat,
  unit_penerbit,
  unit_penerima,
  id_jenis,
  jenis_instansi,
  status,
  file_upload,
  jenis_instansi_2
) {
  return axios.post(`${BACKEND_URL}/api/surat/`, {
    id_penyusunan,
    nomor_surat,
    perihal,
    tgl_surat,
    unit_penerbit,
    unit_penerima,
    id_jenis,
    jenis_instansi,
    status,
    file_upload,
    jenis_instansi_2
  });
}

export function updateSuratUndangan(
  id,
  id_penyusunan,
  nomor_surat,
  perihal,
  tgl_surat,
  unit_penerbit,
  unit_penerima,
  id_jenis,
  jenis_instansi,
  status,
  file_upload,
  jenis_instansi_2
) {
  return axios.put(`${BACKEND_URL}/api/surat/${id}`, {
    id_penyusunan,
    nomor_surat,
    perihal,
    tgl_surat,
    unit_penerbit,
    unit_penerima,
    id_jenis,
    jenis_instansi,
    status,
    file_upload,
    jenis_instansi_2
  });
}

/* API PENYUSUNAN - LHR Pembahasan */

export function getLhr(id) {
  return axios.get(`${BACKEND_URL}/api/lhr/findByPenyusunan/${id}`);
}

export function getLhrById(id) {
  return axios.get(`${BACKEND_URL}/api/lhr/${id}`);
}

export function getLhrDetilByIdDetil(id_detil) {
  return axios.get(`${BACKEND_URL}/api/lhrdetil/${id_detil}`);
}

export function getLhrDetil(id_lhr) {
  return axios.get(`${BACKEND_URL}/api/lhrdetil/byidLhr/${id_lhr}`);
}

export function deleteLhr(id_lhr) {
  return axios.delete(`${BACKEND_URL}/api/lhr/${id_lhr}`);
}

export function deleteLhrDetil(id_lhr_detil) {
  return axios.delete(`${BACKEND_URL}/api/lhrdetil/${id_lhr_detil}`);
}

export function saveLhr(
  id_penyusunan,
  id_surat,
  no_surat_lhr,
  tgl_rapat,
  tmpt_bahas,
  unit_penyelenggara,
  file_lhr,
  jenis_instansi,
  status,
  laporan_singkat
) {
  return axios.post(`${BACKEND_URL}/api/lhr/`, {
    id_penyusunan,
    id_surat,
    no_surat_lhr,
    tgl_rapat,
    tmpt_bahas,
    unit_penyelenggara,
    file_lhr,
    jenis_instansi,
    status,
    laporan_singkat
  });
}

export function updateLhr(
  id,
  id_penyusunan,
  id_surat,
  no_surat_lhr,
  tgl_rapat,
  tmpt_bahas,
  unit_penyelenggara,
  file_lhr,
  jenis_instansi,
  status,
  laporan_singkat
) {
  return axios.put(`${BACKEND_URL}/api/lhr/${id}`, {
    id_penyusunan,
    id_surat,
    no_surat_lhr,
    tgl_rapat,
    tmpt_bahas,
    unit_penyelenggara,
    file_lhr,
    jenis_instansi,
    status,
    laporan_singkat
  });
}

export function saveLhrDetil(id_lhr, jenis, pokok_pengaturan) {
  return axios.post(`${BACKEND_URL}/api/lhrdetil/`, {
    id_lhr,
    jenis,
    pokok_pengaturan
  });
}

export function updateLhrDetil(id_detil, id_lhr, jenis, pokok_pengaturan) {
  return axios.put(`${BACKEND_URL}/api/lhrdetil/${id_detil}`, {
    id_lhr,
    jenis,
    pokok_pengaturan
  });
}

/* API PENYUSUNAN - Peraturan Terkait */

export function getPeraturanTerkait(id) {
  return axios.get(`${BACKEND_URL}/api/peraturan/byid_penyusunan/${id}`);
}

export function getPeraturanTerkaitByIdPeraturan(id) {
  return axios.get(`${BACKEND_URL}/api/peraturan/${id}`);
}

export function deletePeraturanTerkait(id_perter) {
  return axios.delete(`${BACKEND_URL}/api/peraturan/${id_perter}`);
}

export function savePeraturanTerkait(
  id_penyusunan,
  nomor_peraturan,
  perihal,
  tgl_ditetapkan,
  tgl_diundangkan,
  status
) {
  return axios.post(`${BACKEND_URL}/api/peraturan/`, {
    id_penyusunan,
    nomor_peraturan,
    perihal,
    tgl_ditetapkan,
    tgl_diundangkan,
    status
  });
}

export function updatePeraturanTerkait(
  id,
  id_penyusunan,
  nomor_peraturan,
  perihal,
  tgl_ditetapkan,
  tgl_diundangkan,
  status
) {
  return axios.put(`${BACKEND_URL}/api/peraturan/${id}`, {
    id_penyusunan,
    nomor_peraturan,
    perihal,
    tgl_ditetapkan,
    tgl_diundangkan,
    status
  });
}

/* API PENYUSUNAN - DRAFTING */

export function getDraftComposingById(id) {
  return axios.get(`${BACKEND_URL}/api/draftperaturan/byidpenyusunan/${id}`);
}
// export function getDraftComposingById(id) {
//    axios.get(`/draftperaturan/byidpenyusunan/${id}`,{transformResponse: (res)=> {return res;}, responseType: 'json'});
// }

export function getDraft(id_draft) {
  return axios.get(`${BACKEND_URL}/api/draftperaturan/${id_draft}`);
}

export function saveDraftComposing(
  body_draft,
  jns_draft,
  id_penyusunan,
  fileupload,
  filelampiranr
) {
  return axios.post(`${BACKEND_URL}/api/draftperaturan/`, {
    body_draft,
    jns_draft,
    id_penyusunan,
    fileupload,
    filelampiranr
  });
}
export function saveDraftComposingCosign(
  body_cosign,
  jns_draft,
  id_penyusunan,
  fileupload_cosign,
  filelampiran_cosign
) {
  return axios.post(`${BACKEND_URL}/api/draftperaturan/`, {
    body_cosign,
    jns_draft,
    id_penyusunan,
    fileupload_cosign,
    filelampiran_cosign
  });
}
export function saveDraftComposingSahli(
  body_sahli,
  jns_draft,
  id_penyusunan,
  fieupload_sahli,
  filelampiran_sahli
) {
  return axios.post(`${BACKEND_URL}/api/draftperaturan/`, {
    body_sahli,
    jns_draft,
    id_penyusunan,
    fieupload_sahli,
    filelampiran_sahli
  });
}

export function saveDraftComposingDirjen(
  body_direktur,
  jns_draft,
  id_penyusunan,
  fileupload_direktur,
  filelampiran_direktur
) {
  return axios.post(`${BACKEND_URL}/api/draftperaturan/`, {
    body_direktur,
    jns_draft,
    id_penyusunan,
    fileupload_direktur,
    filelampiran_direktur
  });
}

export function updateDraftComposing(
  body_draft,
  id_draft,
  fileupload,
  filelampiranr,
  keterangan = 1
) {
  return axios.put(
    `${BACKEND_URL}/api/draftperaturan/${id_draft}/${keterangan}`,
    {
      body_draft,
      fileupload,
      filelampiranr
    }
  );
}
export function updateDraftComposingCosign(
  body_cosign,
  id_draft,
  fileupload_cosign,
  filelampiran_cosign,
  keterangan = 2
) {
  return axios.put(
    `${BACKEND_URL}/api/draftperaturan/${id_draft}/${keterangan}`,
    {
      body_cosign,
      fileupload_cosign,
      filelampiran_cosign
    }
  );
}
export function updateDraftComposingSahli(
  body_sahli,
  id_draft,
  fieupload_sahli,
  filelampiran_sahli,
  keterangan = 3
) {
  return axios.put(
    `${BACKEND_URL}/api/draftperaturan/${id_draft}/${keterangan}`,
    {
      body_sahli,
      fieupload_sahli,
      filelampiran_sahli
    }
  );
}

export function updateDraftComposingDirjen(
  body_direktur,
  id_draft,
  fileupload_direktur,
  filelampiran_direktur,
  keterangan = 4
) {
  return axios.put(
    `${BACKEND_URL}/api/draftperaturan/${id_draft}/${keterangan}`,
    {
      body_direktur,
      fileupload_direktur,
      filelampiran_direktur
    }
  );
}
/* API PENYUSUNAN - PROCESS */

export function getMintaJawab(id_penyusunan) {
  return axios.get(
    `${BACKEND_URL}/api/mintajawab/byidpenyusunan/${id_penyusunan}`
  );
}

export function getMintaJawabByIdMintaJawab(id_mintajawab) {
  return axios.get(`${BACKEND_URL}/api/mintajawab/${id_mintajawab}`);
}

export function deleteMintaJawab(id_mintajawab) {
  return axios.delete(`${BACKEND_URL}/api/mintajawab/${id_mintajawab}`);
}

export function saveMintaJawab(
  file_permintaan,
  id_penyusunan,
  jenis,
  nomor_surat,
  perihal,
  tgl_surat,
  tujuan,
  jenis_instansi
) {
  return axios.post(`${BACKEND_URL}/api/mintajawab/`, {
    file_permintaan,
    id_penyusunan,
    jenis,
    nomor_surat,
    perihal,
    tgl_surat,
    tujuan,
    jenis_instansi
  });
}

export function updateMintaJawab(
  id_mintajawab,
  file_permintaan,
  id_penyusunan,
  jenis,
  nomor_surat,
  perihal,
  tgl_surat,
  tujuan,
  jenis_instansi
) {
  return axios.put(`${BACKEND_URL}/api/mintajawab/${id_mintajawab}`, {
    file_permintaan,
    id_penyusunan,
    jenis,
    nomor_surat,
    perihal,
    tgl_surat,
    tujuan,
    jenis_instansi
  });
}

export function getCosignUnit(id_penyusunan) {
  return axios.get(
    `${BACKEND_URL}/api/cosignunit/byidpenyusunan/${id_penyusunan}`
  );
}

export function getCosignUnitByIdCosign(id_cosign) {
  return axios.get(`${BACKEND_URL}/api/cosignunit/${id_cosign}`);
}

export function deleteCosignUnit(id_cosign) {
  return axios.delete(`${BACKEND_URL}/api/cosignunit/${id_cosign}`);
}

export function saveCosignUnit(
  file_permintaan,
  id_minta_jawab,
  id_penyusunan,
  nama_unit,
  no_surat,
  tgl_surat
) {
  return axios.post(`${BACKEND_URL}/api/cosignunit/`, {
    file_permintaan,
    id_minta_jawab,
    id_penyusunan,
    nama_unit,
    no_surat,
    tgl_surat
  });
}

export function updateCosignUnit(
  id_cosign,
  file_permintaan,
  id_minta_jawab,
  id_penyusunan,
  nama_unit,
  no_surat,
  tgl_surat
) {
  return axios.put(`${BACKEND_URL}/api/cosignunit/${id_cosign}`, {
    file_permintaan,
    id_minta_jawab,
    id_penyusunan,
    nama_unit,
    no_surat,
    tgl_surat
  });
}

export function getCosignKementerian(id_penyusunan) {
  return axios.get(
    `${BACKEND_URL}/api/cosignkl/byidpenyusunan/${id_penyusunan}`
  );
}

export function getCosignKementerianByIdCosign(id_cosign) {
  return axios.get(`${BACKEND_URL}/api/cosignkl/${id_cosign}`);
}

export function deleteCosignKementerian(id_cosign) {
  return axios.delete(`${BACKEND_URL}/api/cosignkl/${id_cosign}`);
}

export function saveCosignKementerian(
  file_cosign,
  id_minta_jawab,
  id_penyusunan,
  nama_unit,
  no_surat,
  tgl_surat
) {
  return axios.post(`${BACKEND_URL}/api/cosignkl/`, {
    file_cosign,
    id_minta_jawab,
    id_penyusunan,
    nama_unit,
    no_surat,
    tgl_surat
  });
}

export function updateCosignKementerian(
  id_cosign,
  file_cosign,
  id_minta_jawab,
  id_penyusunan,
  nama_unit,
  no_surat,
  tgl_surat
) {
  return axios.put(`${BACKEND_URL}/api/cosignkl/${id_cosign}`, {
    file_cosign,
    id_minta_jawab,
    id_penyusunan,
    nama_unit,
    no_surat,
    tgl_surat
  });
}

export function getAnalisisHarmon(id) {
  return axios.get(`${BACKEND_URL}/api/analisisharmon/byidpenyusunan/${id}`);
}
export function getAnalisisHarmonByIdHarmon(id) {
  return axios.get(`${BACKEND_URL}/api/analisisharmon/${id}`);
}

export function saveAnalisisHarmon(
  id_penyusunan,
  draft,
  jawab1,
  jawab2,
  jawab3,
  jawab4,
  jawab5
) {
  return axios.post(`${BACKEND_URL}/api/analisisharmon/`, {
    id_penyusunan,
    draft,
    jawab1,
    jawab2,
    jawab3,
    jawab4,
    jawab5
  });
}

export function updateAnalisisHarmon(
  id_analisisharmon,
  id_penyusunan,
  draft,
  jawab1,
  jawab2,
  jawab3,
  jawab4,
  jawab5
) {
  return axios.put(`${BACKEND_URL}/api/analisisharmon/${id_analisisharmon}`, {
    id_penyusunan,
    draft,
    jawab1,
    jawab2,
    jawab3,
    jawab4,
    jawab5
  });
}

export function getCosignSahli(id_penyusunan) {
  return axios.get(
    `${BACKEND_URL}/api/cosignsahli/byidpenyusunan/${id_penyusunan}`
  );
}

export function getCosignSahliByIdCosign(id_cosign_sahli) {
  return axios.get(`${BACKEND_URL}/api/cosignsahli/${id_cosign_sahli}`);
}

export function saveCosignSahli(
  id_penyusunan,
  id_minta_jawab,
  no_surat,
  tgl_surat,
  nama_unit,
  file_permintaan
) {
  return axios.post(`${BACKEND_URL}/api/cosignsahli/`, {
    id_penyusunan,
    id_minta_jawab,
    no_surat,
    tgl_surat,
    nama_unit,
    file_permintaan
  });
}

export function saveCosignSahliCatatan(id_penyusunan, nama_unit, catatan) {
  return axios.post(`${BACKEND_URL}/api/cosignsahli/`, {
    id_penyusunan,
    nama_unit,
    catatan
  });
}

export function updateCosignSahli(
  id_penyusunan,
  id_cosign_sahli,
  id_minta_jawab,
  no_surat,
  tgl_surat,
  nama_unit,
  file_permintaan
) {
  return axios.put(`${BACKEND_URL}/api/cosignsahli/${id_cosign_sahli}`, {
    id_penyusunan,
    id_minta_jawab,
    no_surat,
    tgl_surat,
    nama_unit,
    file_permintaan
  });
}

export function updateCosignSahliCatatan(
  id_penyusunan,
  id_cosign_sahli,
  nama_unit,
  catatan
) {
  return axios.put(`${BACKEND_URL}/api/cosignsahli/${id_cosign_sahli}`, {
    id_penyusunan,
    nama_unit,
    catatan
  });
}

export function deleteCosignSahli(id_cosign_sahli){
  return axios.delete(`${BACKEND_URL}/api/cosignsahli/${id_cosign_sahli}`)
}

export function getRegulasiPerpajakanTerima() {
  return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/terima`);
}

export function getRegulasiPerpajakanById(id) {
  return axios.get(`${BACKEND_URL}/api/kmregulasiperpajakan/${id}`);
}

export function saveDraftReal(
  body_draft,
  jns_draft,
  id_penyusunan
  // fileupload,
  // filelampiranr
) {
  return axios.post(`${BACKEND_URL}/api/draftperaturan/`, {
    body_draft,
    jns_draft,
    id_penyusunan
    // fileupload,
    // filelampiranr
  });
}
