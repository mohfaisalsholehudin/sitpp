import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
// import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import ResearchEditForm from "./ResearchEditForm";
import ResearchEditFooter from "./ResearchEditFooter";

const initValues = {
  noSurat: "",
  tglSurat: "",
  perihal: "",
  jenisPajak: "",
  jenisKantor: "",
  instansi: "",
  alamat: "",
  noTelp: "",
  namaPic: "",
  file: ""
};

function ResearchEdit({
  history,
  match: {
    params: { id }
  }
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading, setActionsLoading] = useState(true);
  const [proposal, setProposal] = useState([]);
  const [isDisabled, setIsDisabled] = useState();

  useEffect(() => {
    let _title = id ? "" : "Tambah Surat Usulan";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);
  const btnRef = useRef();
  const saveProposal = values => {
    if (!id) {
      //   dispatch(actions.createProduct(values)).then(() => backToProductsList());
    } else {
      //   dispatch(actions.updateProduct(values)).then(() => backToProductsList());
    }
  };
  const backToProposalList = () => {
    history.push(`/evaluation/research`);
  };

  const setDisabled = val => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <ResearchEditForm
            actionsLoading={actionsLoading}
            proposal={initValues}
            btnRef={btnRef}
            saveProposal={saveProposal}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <ResearchEditFooter
          backAction={backToProposalList}
          btnRef={btnRef}
          disabled={isDisabled}
          />
      </CardFooter>
    </Card>
  );
}

export default ResearchEdit;
