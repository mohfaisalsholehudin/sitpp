import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
  Checkbox,
} from "../../../../helpers";
import CustomFileInput from "../../../../helpers/form/CustomFileInput";
import { getJenisPajak } from "../../Api";
import { unitKerja } from "../../../../references/UnitKerja";

function ProposalEditForm({ proposal, btnRef, saveProposal }) {
  const [jenisPajak, setJenisPajak] = useState([]);
  const [kantor, setKantor] = useState([]);
  // const [pajak, setPajak] = useState([]);
  const { user } = useSelector((state) => state.auth);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    no_surat: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(255, "Maximum 255 symbols")
      .required("Nomor Naskah Dinas atau Usulan Lainnya is required"),
    tgl_surat: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),
    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      .trim("No Whitespace allowed")
      // .max(50, "Maximum 50 symbols")
      .required("Perihal is required"),
    jns_pajak: Yup.array()
      .min(1, "Jenis Pajak is required")
      .required("Jenis Pajak is required"),
    alamat: Yup.string()
      .min(2, "Minimum 2 symbols")
      // .max(100, "Maximum 100 symbols")
      .required("Alamat is required"),
    no_pic: Yup.number().required("No Telp PIC is required"),
    unit_kerja: Yup.string().required("Unit Kerja is required"),
    nm_pic: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Nama PIC is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        // value => value && SUPPORTED_FORMATS.includes(value.type)
        (value) =>
          value && SUPPORTED_FORMATS.some((a) => value.type.includes(a))
      ),
    jns_usul: Yup.number().required("Jenis Instansi is required"),
  });
  const FILE_SIZE = 50000000;
  const SUPPORTED_FORMATS = [
    "application/pdf",
    "application/x-rar-compressed",
    "application/octet-stream",
    "application/zip",
    "application/octet-stream",
    "application/x-zip-compressed",
    "multipart/x-zip",
    "application/vnd.rar",
    "application/rar",
    "application/x-rar"
  ];

  useEffect(() => {
    getJenisPajak().then(({ data }) => {
      data.map((data) => {
        return data.status === "AKTIF"
          ? setJenisPajak((jenisPajak) => [...jenisPajak, data])
          : null;
      });
      // setJenisPajak(data);
    });

    unitKerja.map((data) => {
      return setKantor((kantor) => [
        ...kantor,
        {
          label: data.nm_UNIT_KERJA,
          value: data.kd_unit_kerja,
          alamat: data.alamat,
        },
      ]);
    });
  }, []);

  const asosiasi = () => {
    return (
      <>
        <div className="form-group row">
          <Field
            name="unit_kerja"
            component={Input}
            placeholder="Instansi"
            label="Instansi"
          />
        </div>
        <div className="form-group row">
          <Field
            name="alamat"
            component={Textarea}
            placeholder="Alamat"
            label="Alamat"
          />
        </div>
      </>
    );
  };
  const kemenkeu = () => {
    return (
      <>
        <div className="form-group row">
          <Field
            name="unit_kerja"
            component={Input}
            placeholder="Unit Kementerian Keuangan"
            label="Unit Kementerian Keuangan"
          />
        </div>
        <div className="form-group row">
          <Field
            name="alamat"
            component={Textarea}
            placeholder="Alamat"
            label="Alamat"
          />
        </div>
      </>
    );
  };

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          // console.log(values);
          saveProposal(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid,
        }) => {
          const handleChangeUnitKerja = (val) => {
            setFieldValue("unit_kerja", val.label);
            setFieldValue("alamat", val.alamat);
          };

          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          };

          const handleChangeInstansi = () => {
            setFieldValue("unit_kerja", "");
            setFieldValue("alamat", "");
          };
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    name="no_surat"
                    component={Input}
                    placeholder="Nomor Naskah Dinas atau Usulan Lainnya"
                    label="Nomor Naskah Dinas atau Usulan Lainnya"
                    onClick={() => handleChangeNip()}
                  />
                </div>
                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField name="tgl_surat" label="Tanggal Surat" />
                </div>
                {/* FIELD PERIHAL */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                  />
                </div>
                {/* FIELD JENIS PAJAK */}
                <div className="form-group row align-items-center">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Jenis Pajak
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <div className="checkbox-inline">
                      {jenisPajak.map((data, index) => (
                        <Field
                          component={Checkbox}
                          name="jns_pajak"
                          type="checkbox"
                          value={data.nm_jnspajak}
                          content={data.nm_jnspajak}
                          key={index}
                          // check={proposal.jns_pajak.split(',').includes(data.nm_jnspajak) }
                        />
                      ))}
                    </div>
                  </div>
                </div>
                {/* FIELD JENIS INSTANSI */}
                <div className="form-group row">
                  <Sel
                    name="jns_usul"
                    label="Jenis Instansi"
                    onBlur={() => handleChangeInstansi()}
                  >
                    <option>Pilih Jenis Instansi</option>
                    <option value="1">Asosiasi / KL</option>
                    <option value="3">Unit Kementerian Keuangan</option>
                    <option value="2">Unit DJP</option>
                  </Sel>
                </div>
                {values.jns_usul ? (
                  <>
                    {" "}
                    {/* FIELD INSTANSI */}
                    {/* {checkOfficeType(values.jns_usul)} */}
                    {values.jns_usul === "1" ? (
                      asosiasi()
                    ) : values.jns_usul === "3" ? (
                      kemenkeu()
                    ) : (
                      <div className="form-group row">
                        <label className="col-xl-3 col-lg-3 col-form-label">
                          Unit Kerja
                        </label>
                        <div className="col-lg-9 col-xl-6">
                          <Select
                            options={kantor}
                            onChange={(value) => handleChangeUnitKerja(value)}
                            value={kantor.filter(
                              (data) => data.label === values.unit_kerja
                            )}
                          />
                        </div>
                      </div>
                    )}
                    {/* FIELD NO TELPON */}
                    <div className="form-group row">
                      <Field
                        name="no_pic"
                        component={Input}
                        placeholder="No Telp PIC"
                        label="No Telp PIC"
                      />
                    </div>
                    {/* FIELD NAMA PIC */}
                    <div className="form-group row">
                      <Field
                        name="nm_pic"
                        component={Input}
                        placeholder="Nama PIC "
                        label="Nama PIC / Penandatangan Surat"
                      />
                    </div>
                    {/* FIELD UPLOAD FILE */}
                    <div className="form-group row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Upload File
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <Field
                          name="file"
                          component={CustomFileInput}
                          title="Select a file"
                          label="File"
                          // setFieldValue={setFieldValue}
                          // errorMessage={errors["file"] ? errors["file"] : undefined}
                          // touched={touched["file"]}
                          style={{ display: "flex" }}
                          // onBlur={handleBlur}
                        />
                      </div>
                    </div>
                  </>
                ) : null}
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ProposalEditForm;
