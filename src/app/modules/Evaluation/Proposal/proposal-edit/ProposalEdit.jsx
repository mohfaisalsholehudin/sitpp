import React, { useEffect, useState, useRef } from "react";
import { useSelector } from "react-redux";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
// import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import ProposalEditForm from "./ProposalEditForm";
import ProposalEditFooter from "./ProposalEditFooter";
import { getUsulanById, saveUsulan, updateUsulan } from "../../Api";
import swal from "sweetalert";
import { uploadFileNew } from "../../../../references/Api";


function ProposalEdit({
  history,
  match: {
    params: { id }
  }
}) {
  // Subheader
  const suhbeader = useSubheader();

  const { user } = useSelector(state => state.auth);


  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [proposal, setProposal] = useState();
  const [loading, setLoading] = useState(false);


  //12 data yang dikirim, yang belum dimasukkan 'nip_perekam', 'jns_usul' dan yang tidak dimasukkan disini adalah 'alasan_tolak'.
const initValues = {
  no_surat: "",
  tgl_surat: "",
  perihal: "",
  jns_pajak: "",
  unit_kerja: "",
  instansi: "",
  alamat: "",
  no_pic: "",
  nm_pic: "",
  file: "",
  jns_pengusul: "",
  status: "",
  nip_perekam: "",
  jns_usul: "",
  alasan_tolak: "",
  kd_kantor: user.kantorLegacyKode,
  kd_unit_org: user.unitEs4LegacyKode,
};


  useEffect(() => {
    let _title = id ? "Edit Usulan Evaluasi" : "Tambah Usulan Evaluasi";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
      getUsulanById(id).then(({ data }) => {

        setProposal({
          id_usulan: data.id_usulan,
          no_surat: data.no_surat,
          tgl_surat: data.tgl_surat,
          perihal: data.perihal,
          jns_pengusul: data.jns_pengusul,
          jns_pajak: data.jns_pajak.split(','),
          unit_kerja: data.unit_kerja,
          instansi: data.instansi,
          alamat: data.alamat,
          no_pic: data.no_pic,
          nm_pic: data.nm_pic,
          nip_perekam: data.nip_perekam,
          jns_usul: data.jns_usul,
          alasan_tolak: data.alasan_tolak,
          file_upload: data.file_upload,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
          status: data.status,
          id_tahapan: data.id_tahapan,
          instansi_unit: data.instansi_unit,
          kd_kantor: data.kd_kantor,
          kd_unit_org: data.kd_unit_org
        });
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();
  const saveProposal = values => {
    if (!id) {
      enableLoading();
      const formData = new FormData();
      if (values.instansi) {
        formData.append("file", values.file);
        uploadFileNew(formData)
          .then(({ data }) =>
          {
            disableLoading();
            saveUsulan(
              values.alamat,
              "",
              data.message,
              values.instansi,
              values.jns_pajak.toString(),
              "",
              values.jns_usul,
              values.nip_perekam,
              values.nm_pic,
              values.no_pic,
              values.no_surat,
              values.perihal,
              values.tgl_surat,
              "", 
              values.kd_kantor,
              values.kd_unit_org
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push("/evaluation/proposal");
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push("/evaluation/proposal/new");
                });
              }
            })
      })
          .catch(() => window.alert("Oops Something went wrong !"));
      } else {
        formData.append("file", values.file);
        uploadFileNew(formData)
          .then(({ data }) =>
          {
            disableLoading();
            saveUsulan(
              values.alamat,
              "",
              data.message,
              "",
              values.jns_pajak.toString(),
              "",
              values.jns_usul,
              values.nip_perekam,
              values.nm_pic,
              values.no_pic,
              values.no_surat,
              values.perihal,
              values.tgl_surat,
              values.unit_kerja,
              values.kd_kantor,
              values.kd_unit_org
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push("/evaluation/proposal");
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push("/evaluation/proposal/new");
                });
              }
            })
      })
          .catch(() => swal("Error", "Oops Something went wrong !", "error"));
      }
    } else {
      if (values.file.name) {
        enableLoading();
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFileNew(formData)
          .then(({ data }) =>
          {
            disableLoading();
            updateUsulan(
              values.id_usulan,
              values.alamat,
              "", //alasan_tolak
              data.message, //file_upload
              values.instansi,
              values.jns_pajak.toString(),
              "", //jns_Pengusul
              values.jns_usul,
              values.nip_perekam,  //nip_perekam
              values.nm_pic,
              values.no_pic,
              values.no_surat,
              values.perihal,
              values.tgl_surat,
              values.unit_kerja,
              values.kd_kantor,
              values.kd_unit_org
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push("/evaluation/proposal");
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push("/evaluation/proposal/new");
                });
              }
            })
      })
          .catch(() => window.alert("Oops Something went wrong !"));
      } else {
        updateUsulan(
          values.id_usulan,
          values.alamat,
          "", //alasan_tolak
          values.file_upload,
          values.instansi,
          values.jns_pajak.toString(),
          "",  //jns_pengusul
          values.jns_usul,
          values.nip_perekam, //nip_perekam
          values.nm_pic,
          values.no_pic,
          values.no_surat,
          values.perihal,
          values.tgl_surat,
          values.unit_kerja,
          values.kd_kantor,
          values.kd_unit_org
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push("/evaluation/proposal");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/evaluation/proposal/new");
            });
          }
        });
      }
    }
  };
  const backToProposalList = () => {
    history.push(`/evaluation/proposal`);
  };

  const setDisabled = val => {
    setIsDisabled(val);
  };

  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <ProposalEditForm
            actionsLoading={actionsLoading}
            proposal={proposal || initValues}
            btnRef={btnRef}
            saveProposal={saveProposal}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <ProposalEditFooter
          backAction={backToProposalList}
          btnRef={btnRef}
          loading={loading}
        ></ProposalEditFooter>
      </CardFooter>
    </Card>
  );
}

export default ProposalEdit;
