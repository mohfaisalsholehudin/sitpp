/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import SVG from "react-inlinesvg";
import { getPeraturanByJudul } from "../../../../references/Api";
import "./table.css";
import { useHistory } from "react-router-dom";

function SearchModal({ id, show, onHide, val, handleChangePeraturan }) {
  const [content, setContent] = useState();
  const history = useHistory()

  const addPer = (idPer, no_regulasi) => {
    // console.log(idPer);
    // console.log(no_regulasi)
    handleChangePeraturan(idPer, no_regulasi)
    history.push(`/evaluation/validate/${id}/add`)
    // addPerTerkaitById(1, id, idPer).then(({ status }) => {
    //   if (status === 201 || status === 200) {
    //     swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
    //       history.push("/dashboard");
    //       history.replace(`/knowledge/regulasi/usulan/${id}/terkait`);
    //     });
    //   } else {
    //     swal("Gagal", "Data gagal disimpan", "error").then(() => {
    //       history.push("/dashboard");
    //       history.replace(`/knowledge/regulasi/usulan/${id}/terkait`);
    //     });
    //   }
    // });

    //alert(id);
  };

  const tableWrap = {
    height: "500px",
    border: "2px solid black",
    overflow: "auto"
  }

  useEffect(() => {
    getPeraturanByJudul(1, 100, val).then(({ data }) => {
      if (data.content.length > 0) {
        setContent(data.content);
      }
    });
  }, [val]);

  return (
    <Modal
      size="xl"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Daftar Peraturan
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="tableWrap">
          <table>
            <thead>
              <tr>
                <th>NO</th>
                <th>NO PERATURAN</th>
                <th>PERIHAL</th>
                <th>AKSI</th>
              </tr>
            </thead>
            <tbody>
              {content ? (
                content.map((data, index) => {
                  return (
                    <tr key={index}>
                      <td>{index + 1}</td>
                      <td>{data.no_regulasi}</td>
                      <td>{data.perihal}</td>
                      <td style={{width:'150px'}}>
                      <button
                      type="button"
                      onClick={() => addPer(data.id_peraturan, data.no_regulasi)}
                      className="btn btn-light-primary"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                    >
                      <i className="fas fa-check-circle"></i>
                      Pilih
                    </button>
                        {/* <a
                          title="Tambah"
                          className="btn btn-icon btn-light btn-hover-success btn-sm mx-3"
                          onClick={() => addPer(data.id_peraturan)}
                        >
                          <span className="svg-icon svg-icon-md svg-icon-success">
                            <SVG
                              src={toAbsoluteUrl(
                                "/media/svg/icons/Navigation/Plus.svg"
                              )}
                            />
                          </span>
                        </a> */}
                      </td>
                    </tr>
                  );
                })
              ) : (
                <tr>
                  <td colSpan={4} style={{ textAlign: "center" }}>
                    Data Tidak Ditemukan
                  </td>
                </tr>
              )}
            </tbody>
            <tfoot>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </Modal.Body>
      <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}

export default SearchModal;
