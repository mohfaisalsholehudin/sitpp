/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useLocation } from "react-router";
import { NavLink } from "react-router-dom";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl, checkIsActive } from "../../../../_helpers";
import {
  getKownerByUnit,
  getNotifKo,
  getNotifPkp,
  getPkpByNip,
  getSmeByNip,
} from "../../../../../app/references/Api";

export function AsideMenuList({ layoutProps }) {
  const location = useLocation();
  const getMenuItemActive = (url, hasSubmenu = false) => {
    return checkIsActive(location, url)
      ? ` ${!hasSubmenu &&
          "menu-item-active"} menu-item-open menu-item-not-hightlighted`
      : "";
  };

  // const { REACT_APP_MENU_URL } = process.env;
  // const [menu, setMenu] = useState([]);
  const { role, user } = useSelector((state) => state.auth);
  const konseptor = role.includes("ROLE_PERATURAN_KONSEPTOR");
  const es4 = role.includes("ROLE_PERATURAN_PENELITI_LVL1");
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const admin = role.includes("ROLE_ADMIN_PERATURAN");
  const admin_kmi = role.includes("ROLE_ADMIN_KMI");

  const [pkp, setPkp] = useState(false);
  const [sme, setSme] = useState(false);
  const [ko, setKo] = useState(false);
  const [notifPkp, setNotifPkp] = useState("");
  const [notifKo, setNotifKo] = useState("");

  useEffect(() => {
    getPkpByNip(user.nip9).then(({ data }) => {
      data.length > 0 ? setPkp(true) : setPkp(false);
    });
    getSmeByNip(user.nip9).then(({ data }) => {
      data.length > 0 ? setSme(true) : setSme(false);
    });
    getKownerByUnit(
      user.unitEs4LegacyKode,
      user.unitEs3LegacyKode,
      user.unitEs2LegacyKode,
      user.jabatan
    ).then(({ data }) => {
      data.length > 0 ? setKo(true) : setKo(false);
    });
    getNotifPkp(user.namaPegawai).then(({ data }) => {
      setNotifPkp(data);
    });
    getNotifKo(user.namaPegawai).then(({ data }) => {
      setNotifKo(data);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // const getMenu = async () => {
  //   try {
  //     const data = {
  //       role: ["99", "8"],
  //       route: "dashboard",
  //     };
  //     const response = await axios.post(`${REACT_APP_MENU_URL}`, data);
  //     return response;
  //   } catch (err) {
  //     if (err.response) {
  //       console.log(err.response);
  //     } else {
  //       console.log(err);
  //     }
  //   }
  // };

  // useEffect(() => {
  //   // getMenu()
  //   // .then(({data: {data}})=> {
  //   //   setMenu(data.menu);
  //   //   // console.log(data.menu);
  //   //   Object.values(data.menu).map((data)=>{
  //   //     console.log(data[1]);
  //   //   })
  //   // })
  // }, []);

  // const showMenu = () => {
  //    menu.map((data, index)=>{
  //       console.log(index);
  //   })
  // }

  return (
    <>
      <ul className={`menu-nav ${layoutProps.ulClasses}`}>
        {/* {showMenu()} */}
        {/* Dashboard Pages */}
        <li
          className={`menu-item ${getMenuItemActive("/dashboard", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/dashboard">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Dashboard</span>
          </NavLink>
        </li>
        {/* End of Dashboard Pages */}

        {/* Tata Kelola Peraturan Pages */}

        {admin || konseptor || es4 || es3 ? (
          <li
            className={`menu-item menu-item-submenu ${getMenuItemActive(
              "/tata-kelola",
              true
            )}`}
            aria-haspopup="true"
            data-menu-toggle="hover"
          >
            <NavLink className="menu-link menu-toggle" to="/tata-kelola">
              <span className="svg-icon menu-icon">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Code/Compiling.svg")}
                />
              </span>
              <span className="menu-text">Tata Kelola Peraturan</span>
              <i className="menu-arrow" />
            </NavLink>
            <div className="menu-submenu">
              <i className="menu-arrow" />
              <ul className="menu-subnav">
                <li
                  className="menu-item  menu-item-parent"
                  aria-haspopup="true"
                >
                  <span className="menu-link">
                    <span className="menu-text">Tata Kelola Peraturan</span>
                  </span>
                </li>
                {/* Evaluation Pages */}

                <li
                  className={`menu-item menu-item-submenu ${getMenuItemActive(
                    "/evaluation",
                    true
                  )}`}
                  aria-haspopup="true"
                  data-menu-toggle="hover"
                >
                  <NavLink className="menu-link menu-toggle" to="/evaluation">
                    <span className="svg-icon menu-icon">
                      <SVG
                        src={toAbsoluteUrl(
                          "/media/svg/icons/Communication/Clipboard-list.svg"
                        )}
                      />
                    </span>
                    <span className="menu-text">Evaluasi / Analisis</span>
                    <i className="menu-arrow" />
                  </NavLink>
                  <div className="menu-submenu ">
                    <i className="menu-arrow" />
                    <ul className="menu-subnav">
                      <li
                        className="menu-item  menu-item-parent"
                        aria-haspopup="true"
                      >
                        <span className="menu-link">
                          <span className="menu-text">Evaluasi / Analisis</span>
                        </span>
                      </li>

                      {konseptor || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/evaluation/proposal"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/evaluation/proposal"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Usulan Asosiasi / Unit
                            </span>
                          </NavLink>
                        </li>
                      ) : null}
                      {/* <li
                className={`menu-item ${getMenuItemActive("/evaluation/unit")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link menu-toggle" to="/evaluation/unit">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Usulan Evaluasi</span>
                </NavLink>
              </li> */}

                      {es4 || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/evaluation/research"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/evaluation/research"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Penelitian Usulan</span>
                          </NavLink>
                        </li>
                      ) : null}

                      {konseptor || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/evaluation/validate"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/evaluation/validate"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Daftar Validasi Usulan
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      {es4 || es3 || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/evaluation/revalidate"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/evaluation/revalidate"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Penelitian Validasi
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      <li
                        className={`menu-item ${getMenuItemActive(
                          "/evaluation/monitoring"
                        )}`}
                        aria-haspopup="true"
                      >
                        <NavLink
                          className="menu-link menu-toggle"
                          to="/evaluation/monitoring"
                        >
                          <i className="menu-bullet menu-bullet-dot">
                            <span />
                          </i>
                          <span className="menu-text">
                            Monitoring Evaluasi / Analisis
                          </span>
                        </NavLink>
                      </li>
                    </ul>
                  </div>
                </li>
                {/*End of Evaluation Pages */}

                {/* Planning Pages */}
                <li
                  className={`menu-item menu-item-submenu ${getMenuItemActive(
                    "/plan",
                    true
                  )}`}
                  aria-haspopup="true"
                  data-menu-toggle="hover"
                >
                  <NavLink className="menu-link menu-toggle" to="/plan">
                    <span className="svg-icon menu-icon">
                      <SVG
                        src={toAbsoluteUrl(
                          "/media/svg/icons/Communication/Clipboard-check.svg"
                        )}
                      />
                    </span>
                    <span className="menu-text">Perencanaan</span>
                    <i className="menu-arrow" />
                  </NavLink>
                  <div className="menu-submenu ">
                    <i className="menu-arrow" />
                    <ul className="menu-subnav">
                      <li
                        className="menu-item  menu-item-parent"
                        aria-haspopup="true"
                      >
                        <span className="menu-link">
                          <span className="menu-text">Perencanaan</span>
                        </span>
                      </li>

                      {admin || konseptor ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/plan/proposal"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/plan/proposal"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Usulan Perencanaan
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      {admin || es4 || es3 ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/plan/research"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/plan/research"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Penelitian Perencanaan
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      <li
                        className={`menu-item ${getMenuItemActive(
                          "/plan/monitoring"
                        )}`}
                        aria-haspopup="true"
                      >
                        <NavLink
                          className="menu-link menu-toggle"
                          to="/plan/monitoring"
                        >
                          <i className="menu-bullet menu-bullet-dot">
                            <span />
                          </i>
                          <span className="menu-text">
                            Monitoring Perencanaan
                          </span>
                        </NavLink>
                      </li>
                    </ul>
                  </div>
                </li>
                {/* End of Planning Pages */}

                {/* Composing Pages */}
                <li
                  className={`menu-item menu-item-submenu ${getMenuItemActive(
                    "/compose",
                    true
                  )}`}
                  aria-haspopup="true"
                  data-menu-toggle="hover"
                >
                  <NavLink className="menu-link menu-toggle" to="/compose">
                    <span className="svg-icon menu-icon">
                      <SVG
                        src={toAbsoluteUrl(
                          "/media/svg/icons/Files/Selected-file.svg"
                        )}
                      />
                    </span>
                    <span className="menu-text">Penyusunan</span>
                    <i className="menu-arrow" />
                  </NavLink>
                  <div className="menu-submenu ">
                    <i className="menu-arrow" />
                    <ul className="menu-subnav">
                      <li
                        className="menu-item  menu-item-parent"
                        aria-haspopup="true"
                      >
                        <span className="menu-link">
                          <span className="menu-text">Penyusunan</span>
                        </span>
                      </li>
                      {konseptor || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/compose/proposal"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/compose/proposal"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Usulan Penyusunan</span>
                          </NavLink>
                        </li>
                      ) : null}

                      {es4 || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/compose/research"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/compose/research"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Penelitian Draft Usulan
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      {konseptor || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/compose/process"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/compose/process"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Proses Penyusunan</span>
                          </NavLink>
                        </li>
                      ) : null}
                      {es4 || es3 || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/compose/reprocess"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/compose/reprocess"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Penelitian Penyusunan
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      {konseptor || es4 || es3 || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/compose/monitoring"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/compose/monitoring"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Monitoring Penyusunan
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      {/* <li
                className={`menu-item ${getMenuItemActive("/compose/bkf")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link menu-toggle" to="/compose/bkf">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">
                    Inisiasi Unit Eselon 1 Kemenkeu
                  </span>
                </NavLink>
              </li> */}
                    </ul>
                  </div>
                </li>
                {/* End of Composing Pages */}
              </ul>
            </div>
          </li>
        ) : null}
        {/* End of Tata Kelola Peraturan Pages */}

        {/* Evaluation Pages */}

        {/* End of Evaluasi Pages */}

        {/* Planning Pages */}

        {/* End of Planning Pages */}
        {/* Composing Pages */}

        {/* End of Composing Pages */}

        {/* KM Pages */}
        {admin || konseptor || es4 || es3 ? (
          <li
            className={`menu-item menu-item-submenu ${getMenuItemActive(
              "/knowledge",
              true
            )}`}
            aria-haspopup="true"
            data-menu-toggle="hover"
          >
            <NavLink className="menu-link menu-toggle" to="/knowledge">
              <span className="svg-icon menu-icon">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Book-open.svg")}
                />
              </span>
              <span className="menu-text">KM SI Peraturan</span>
              <i className="menu-arrow" />
            </NavLink>
            <div className="menu-submenu ">
              <i className="menu-arrow" />
              <ul className="menu-subnav">
                <li
                  className="menu-item  menu-item-parent"
                  aria-haspopup="true"
                >
                  <span className="menu-link">
                    <span className="menu-text">KM SI Peraturan</span>
                  </span>
                </li>

                {/* Regulasi Pages */}

                <li
                  className={`menu-item menu-item-submenu ${getMenuItemActive(
                    "/regulasi",
                    true
                  )}`}
                  aria-haspopup="true"
                  data-menu-toggle="hover"
                >
                  <NavLink className="menu-link menu-toggle" to="/regulasi">
                    <span className="svg-icon menu-icon">
                      <SVG
                        src={toAbsoluteUrl("/media/svg/icons/Home/Book.svg")}
                      />
                    </span>
                    <span className="menu-text">Regulasi Perpajakan</span>
                    <i className="menu-arrow" />
                  </NavLink>
                  <div className="menu-submenu ">
                    <i className="menu-arrow" />
                    <ul className="menu-subnav">
                      <li
                        className="menu-item  menu-item-parent"
                        aria-haspopup="true"
                      >
                        <span className="menu-link">
                          <span className="menu-text">Regulasi Perpajakan</span>
                        </span>
                      </li>

                      {konseptor || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/regulasi/usulan"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/regulasi/usulan"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Tambah Regulasi Perpajakan
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      {es3 || es4 || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/regulasi/penelitian"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/regulasi/penelitian"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Penelitian Regulasi Perpajakan
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      <li
                        className={`menu-item ${getMenuItemActive(
                          "/knowledge/regulasi/monitoring"
                        )}`}
                        aria-haspopup="true"
                      >
                        <NavLink
                          className="menu-link menu-toggle"
                          to="/knowledge/regulasi/monitoring"
                        >
                          <i className="menu-bullet menu-bullet-dot">
                            <span />
                          </i>
                          <span className="menu-text">
                            Monitoring Regulasi Perpajakan
                          </span>
                        </NavLink>
                      </li>
                      {admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/regulasi/publish"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/regulasi/publish"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Monitoring Regulasi Publish
                            </span>
                          </NavLink>
                        </li>
                      ) : null}
                    </ul>
                  </div>
                </li>

                {/* Penegasan */}
                <li
                  className={`menu-item menu-item-submenu ${getMenuItemActive(
                    "/penegasan",
                    true
                  )}`}
                  aria-haspopup="true"
                  data-menu-toggle="hover"
                >
                  <NavLink className="menu-link menu-toggle" to="/penegasan">
                    <span className="svg-icon menu-icon">
                      <SVG
                        src={toAbsoluteUrl("/media/svg/icons/Home/Book.svg")}
                      />
                    </span>
                    <span className="menu-text">Surat Penegasan</span>
                    <i className="menu-arrow" />
                  </NavLink>
                  <div className="menu-submenu ">
                    <i className="menu-arrow" />
                    <ul className="menu-subnav">
                      <li
                        className="menu-item  menu-item-parent"
                        aria-haspopup="true"
                      >
                        <span className="menu-link">
                          <span className="menu-text">Surat Penegasan</span>
                        </span>
                      </li>

                      {konseptor || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/penegasan/usulan"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/penegasan/usulan"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Unggah Surat Penegasan
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      {es3 || es4 || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/penegasan/penelitian"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/penegasan/penelitian"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Penelitian Surat Penegasan
                            </span>
                          </NavLink>
                        </li>
                      ) : null}
                      <li
                        className={`menu-item ${getMenuItemActive(
                          "/knowledge/penegasan/monitoring"
                        )}`}
                        aria-haspopup="true"
                      >
                        <NavLink
                          className="menu-link menu-toggle"
                          to="/knowledge/penegasan/monitoring"
                        >
                          <i className="menu-bullet menu-bullet-dot">
                            <span />
                          </i>
                          <span className="menu-text">
                            Monitoring Surat Penegasan
                          </span>
                        </NavLink>
                      </li>
                    </ul>
                  </div>
                </li>

                {/* Kajian Pages */}

                <li
                  className={`menu-item menu-item-submenu ${getMenuItemActive(
                    "/kajian",
                    true
                  )}`}
                  aria-haspopup="true"
                  data-menu-toggle="hover"
                >
                  <NavLink className="menu-link menu-toggle" to="/kajian">
                    <span className="svg-icon menu-icon">
                      <SVG
                        src={toAbsoluteUrl("/media/svg/icons/Home/Book.svg")}
                      />
                    </span>
                    <span className="menu-text">Hasil Kajian RIA dan CBA</span>
                    <i className="menu-arrow" />
                  </NavLink>
                  <div className="menu-submenu ">
                    <i className="menu-arrow" />
                    <ul className="menu-subnav">
                      <li
                        className="menu-item  menu-item-parent"
                        aria-haspopup="true"
                      >
                        <span className="menu-link">
                          <span className="menu-text">
                            Hasil Kajian RIA dan CBA
                          </span>
                        </span>
                      </li>

                      {konseptor || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/kajian/usulan"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/kajian/usulan"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Usulan Hasil Kajian RIA dan CBA
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      {es3 || es4 || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/kajian/penelitian"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/kajian/penelitian"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Penelitian Hasil Kajian RIA dan CBA
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      <li
                        className={`menu-item ${getMenuItemActive(
                          "/knowledge/kajian/monitoring"
                        )}`}
                        aria-haspopup="true"
                      >
                        <NavLink
                          className="menu-link menu-toggle"
                          to="/knowledge/kajian/monitoring"
                        >
                          <i className="menu-bullet menu-bullet-dot">
                            <span />
                          </i>
                          <span className="menu-text">
                            Monitoring Hasil Kajian RIA dan CBA
                          </span>
                        </NavLink>
                      </li>
                    </ul>
                  </div>
                </li>

                {/* Putusan */}
                <li
                  className={`menu-item menu-item-submenu ${getMenuItemActive(
                    "/putusan",
                    true
                  )}`}
                  aria-haspopup="true"
                  data-menu-toggle="hover"
                >
                  <NavLink className="menu-link menu-toggle" to="/putusan">
                    <span className="svg-icon menu-icon">
                      <SVG
                        src={toAbsoluteUrl("/media/svg/icons/Home/Book.svg")}
                      />
                    </span>
                    <span className="menu-text">Putusan Pengadilan</span>
                    <i className="menu-arrow" />
                  </NavLink>
                  <div className="menu-submenu ">
                    <i className="menu-arrow" />
                    <ul className="menu-subnav">
                      <li
                        className="menu-item  menu-item-parent"
                        aria-haspopup="true"
                      >
                        <span className="menu-link">
                          <span className="menu-text">Putusan Pengadilan</span>
                        </span>
                      </li>

                      {konseptor || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/putusan/usulan"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/putusan/usulan"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Usulan Putusan Pengadilan
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      {es3 || es4 || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/putusan/penelitian"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/putusan/penelitian"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Penelitian Putusan Pengadilan
                            </span>
                          </NavLink>
                        </li>
                      ) : null}
                      <li
                        className={`menu-item ${getMenuItemActive(
                          "/knowledge/putusan/monitoring"
                        )}`}
                        aria-haspopup="true"
                      >
                        <NavLink
                          className="menu-link menu-toggle"
                          to="/knowledge/putusan/monitoring"
                        >
                          <i className="menu-bullet menu-bullet-dot">
                            <span />
                          </i>
                          <span className="menu-text">
                            Monitoring Putusan Pengadilan
                          </span>
                        </NavLink>
                      </li>
                    </ul>
                  </div>
                </li>

                {/* Kodefikasi Pages */}

                <li
                  className={`menu-item menu-item-submenu ${getMenuItemActive(
                    "/kodefikasi",
                    true
                  )}`}
                  aria-haspopup="true"
                  data-menu-toggle="hover"
                >
                  <NavLink className="menu-link menu-toggle" to="/kodefikasi">
                    <span className="svg-icon menu-icon">
                      <SVG
                        src={toAbsoluteUrl("/media/svg/icons/Home/Book.svg")}
                      />
                    </span>
                    <span className="menu-text">Kodifikasi</span>
                    <i className="menu-arrow" />
                  </NavLink>
                  <div className="menu-submenu ">
                    <i className="menu-arrow" />
                    <ul className="menu-subnav">
                      <li
                        className="menu-item  menu-item-parent"
                        aria-haspopup="true"
                      >
                        <span className="menu-link">
                          <span className="menu-text">Kodifikasi</span>
                        </span>
                      </li>

                      {konseptor || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/kodefikasi/usulan"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/kodefikasi/usulan"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Usulan Kodifikasi</span>
                          </NavLink>
                        </li>
                      ) : null}

                      {es3 || es4 || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/kodefikasi/penelitian"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/kodefikasi/penelitian"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Penelitian Kodifikasi
                            </span>
                          </NavLink>
                        </li>
                      ) : null}
                      <li
                        className={`menu-item ${getMenuItemActive(
                          "/knowledge/kodefikasi/monitoring"
                        )}`}
                        aria-haspopup="true"
                      >
                        <NavLink
                          className="menu-link menu-toggle"
                          to="/knowledge/kodefikasi/monitoring"
                        >
                          <i className="menu-bullet menu-bullet-dot">
                            <span />
                          </i>
                          <span className="menu-text">
                            Monitoring Kodifikasi
                          </span>
                        </NavLink>
                      </li>
                    </ul>
                  </div>
                </li>

                {/* Rekomendasi */}
                <li
                  className={`menu-item menu-item-submenu ${getMenuItemActive(
                    "/rekomendasi",
                    true
                  )}`}
                  aria-haspopup="true"
                  data-menu-toggle="hover"
                >
                  <NavLink className="menu-link menu-toggle" to="/rekomendasi">
                    <span className="svg-icon menu-icon">
                      <SVG
                        src={toAbsoluteUrl("/media/svg/icons/Home/Book.svg")}
                      />
                    </span>
                    <span className="menu-text">Rekomendasi</span>
                    <i className="menu-arrow" />
                  </NavLink>
                  <div className="menu-submenu ">
                    <i className="menu-arrow" />
                    <ul className="menu-subnav">
                      <li
                        className="menu-item  menu-item-parent"
                        aria-haspopup="true"
                      >
                        <span className="menu-link">
                          <span className="menu-text">Rekomendasi</span>
                        </span>
                      </li>

                      {konseptor || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/rekomendasi/usulan"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/rekomendasi/usulan"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Usulan Surat Rekomendasi
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      {es3 || es4 || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/rekomendasi/penelitian"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/rekomendasi/penelitian"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Penelitian Surat Rekomendasi
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      <li
                        className={`menu-item ${getMenuItemActive(
                          "/knowledge/rekomendasi/monitoring"
                        )}`}
                        aria-haspopup="true"
                      >
                        <NavLink
                          className="menu-link menu-toggle"
                          to="/knowledge/rekomendasi/monitoring"
                        >
                          <i className="menu-bullet menu-bullet-dot">
                            <span />
                          </i>
                          <span className="menu-text">
                            Monitoring Surat Rekomendasi
                          </span>
                        </NavLink>
                      </li>
                    </ul>
                  </div>
                </li>
                <li
                  className={`menu-item menu-item-submenu ${getMenuItemActive(
                    "/fgd",
                    true
                  )}`}
                  aria-haspopup="true"
                  data-menu-toggle="hover"
                >
                  <NavLink className="menu-link menu-toggle" to="/kodefikasi">
                    <span className="svg-icon menu-icon">
                      <SVG
                        src={toAbsoluteUrl("/media/svg/icons/Home/Book.svg")}
                      />
                    </span>
                    <span className="menu-text">FGD</span>
                    <i className="menu-arrow" />
                  </NavLink>
                  <div className="menu-submenu ">
                    <i className="menu-arrow" />
                    <ul className="menu-subnav">
                      <li
                        className="menu-item  menu-item-parent"
                        aria-haspopup="true"
                      >
                        <span className="menu-link">
                          <span className="menu-text">FGD</span>
                        </span>
                      </li>

                      {konseptor || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/fgd/usulan"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/fgd/usulan"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Usulan FGD</span>
                          </NavLink>
                        </li>
                      ) : null}

                      {es3 || es4 || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/fgd/penelitian"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/fgd/penelitian"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Penelitian FGD</span>
                          </NavLink>
                        </li>
                      ) : null}

                      <li
                        className={`menu-item ${getMenuItemActive(
                          "/knowledge/fgd/monitoring"
                        )}`}
                        aria-haspopup="true"
                      >
                        <NavLink
                          className="menu-link menu-toggle"
                          to="/knowledge/fgd/monitoring"
                        >
                          <i className="menu-bullet menu-bullet-dot">
                            <span />
                          </i>
                          <span className="menu-text">Monitoring FGD</span>
                        </NavLink>
                      </li>
                    </ul>
                  </div>
                </li>
                <li
                  className={`menu-item menu-item-submenu ${getMenuItemActive(
                    "/audiensi",
                    true
                  )}`}
                  aria-haspopup="true"
                  data-menu-toggle="hover"
                >
                  <NavLink className="menu-link menu-toggle" to="/audiensi">
                    <span className="svg-icon menu-icon">
                      <SVG
                        src={toAbsoluteUrl("/media/svg/icons/Home/Book.svg")}
                      />
                    </span>
                    <span className="menu-text">Audiensi</span>
                    <i className="menu-arrow" />
                  </NavLink>
                  <div className="menu-submenu ">
                    <i className="menu-arrow" />
                    <ul className="menu-subnav">
                      <li
                        className="menu-item  menu-item-parent"
                        aria-haspopup="true"
                      >
                        <span className="menu-link">
                          <span className="menu-text">Audiensi</span>
                        </span>
                      </li>

                      {konseptor || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/audiensi/usulan"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/audiensi/usulan"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Usulan Audiensi</span>
                          </NavLink>
                        </li>
                      ) : null}

                      {es3 || es4 || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/audiensi/penelitian"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/audiensi/penelitian"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">
                              Penelitian Audiensi
                            </span>
                          </NavLink>
                        </li>
                      ) : null}

                      <li
                        className={`menu-item ${getMenuItemActive(
                          "/knowledge/audiensi/monitoring"
                        )}`}
                        aria-haspopup="true"
                      >
                        <NavLink
                          className="menu-link menu-toggle"
                          to="/knowledge/audiensi/monitoring"
                        >
                          <i className="menu-bullet menu-bullet-dot">
                            <span />
                          </i>
                          <span className="menu-text">Monitoring Audiensi</span>
                        </NavLink>
                      </li>
                    </ul>
                  </div>
                </li>

                {/* Survey */}
                <li
                  className={`menu-item menu-item-submenu ${getMenuItemActive(
                    "/survei",
                    true
                  )}`}
                  aria-haspopup="true"
                  data-menu-toggle="hover"
                >
                  <NavLink className="menu-link menu-toggle" to="/survei">
                    <span className="svg-icon menu-icon">
                      <SVG
                        src={toAbsoluteUrl("/media/svg/icons/Home/Book.svg")}
                      />
                    </span>
                    <span className="menu-text">Survei</span>
                    <i className="menu-arrow" />
                  </NavLink>
                  <div className="menu-submenu ">
                    <i className="menu-arrow" />
                    <ul className="menu-subnav">
                      <li
                        className="menu-item  menu-item-parent"
                        aria-haspopup="true"
                      >
                        <span className="menu-link">
                          <span className="menu-text">Survei</span>
                        </span>
                      </li>

                      {konseptor || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/survei/usulan"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/survei/usulan"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Usulan Survei</span>
                          </NavLink>
                        </li>
                      ) : null}

                      {es3 || es4 || admin ? (
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/knowledge/survei/penelitian"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/knowledge/survei/penelitian"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Penelitian Survei</span>
                          </NavLink>
                        </li>
                      ) : null}

                      <li
                        className={`menu-item ${getMenuItemActive(
                          "/knowledge/survei/monitoring"
                        )}`}
                        aria-haspopup="true"
                      >
                        <NavLink
                          className="menu-link menu-toggle"
                          to="/knowledge/survei/monitoring"
                        >
                          <i className="menu-bullet menu-bullet-dot">
                            <span />
                          </i>
                          <span className="menu-text">Monitoring Survei</span>
                        </NavLink>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </li>
        ) : null}

        {/* End of KM Pages */}

        {/* Process Knowledge Pages */}
        {/* {admin_kmi ? ( */}
        <li
          className={`menu-item menu-item-submenu ${getMenuItemActive(
            "/process-knowledge",
            true
          )}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/process-knowledge">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Code/Settings4.svg")} />
            </span>
            <span className="menu-text">Pengelolaan Knowledge</span>
            <i className="menu-arrow" />
          </NavLink>
          <div className="menu-submenu ">
            <i className="menu-arrow" />
            <ul className="menu-subnav">
              <li className="menu-item  menu-item-parent" aria-haspopup="true">
                <span className="menu-link">
                  <span className="menu-text">Pengelolaan Knowledge</span>
                </span>
              </li>

              {/* {admin ? ( */}
              <li
                className={`menu-item ${getMenuItemActive(
                  "/process-knowledge/add-knowledge"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/process-knowledge/add-knowledge"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Tambah Knowledge</span>
                </NavLink>
              </li>

              {admin_kmi || pkp || ko ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/simple-knowledge"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/simple-knowledge"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Tambah Simpel Knowledge</span>
                  </NavLink>
                </li>
              ) : null}
              {/* // ) : null} */}
              {/* {admin ? ( */}

              {pkp || admin_kmi ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/review-pkp"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/review-pkp"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">
                      Review Penjamin Kualitas Pengetahuan
                    </span>
                    <span
                      className="navi-label"
                      style={{ marginLeft: "5px", marginTop: "8px" }}
                    >
                      <span className="label label-light-danger label-rounded font-weight-bold">
                        {notifPkp}
                      </span>
                    </span>
                  </NavLink>
                </li>
              ) : null}
              {/* ) : null} */}

              {ko || admin_kmi ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/review-ko"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/review-ko"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Review Knowledge Owner</span>
                    <span
                      className="navi-label"
                      style={{ marginLeft: "5px", marginTop: "8px" }}
                    >
                      <span className="label label-light-danger label-rounded font-weight-bold">
                        {notifKo}
                      </span>
                    </span>
                  </NavLink>
                </li>
              ) : null}
              {sme || admin_kmi ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/review-sme"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/review-sme"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">
                      Review Subject Matter Expert
                    </span>
                  </NavLink>
                </li>
              ) : null}
              {admin_kmi ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/search-knowledge"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/search-knowledge"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Pencarian Knowledge</span>
                  </NavLink>
                </li>
              ) : null}
              {pkp || admin_kmi ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/update-review"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/update-review"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Review Update Konten PKP</span>
                  </NavLink>
                </li>
              ) : null}
              {ko || admin_kmi ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/ko-update-review"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/ko-update-review"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Review Update Konten KO</span>
                  </NavLink>
                </li>
              ) : null}
              {sme || admin_kmi ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/sme-update-review"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/sme-update-review"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Review Update Konten SME</span>
                  </NavLink>
                </li>
              ) : null}
              {admin_kmi ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/monitoring-knowledge"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/monitoring-knowledge"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Monitoring Knowledge</span>
                  </NavLink>
                </li>
              ) : null}

              {admin_kmi ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/monitoring-review"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/monitoring-review"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Monitoring Review</span>
                  </NavLink>
                </li>
              ) : null}

              {admin_kmi ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/feedback"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/feedback"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Feedback</span>
                  </NavLink>
                </li>
              ) : null}
              {admin_kmi ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/rating"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/rating"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Rating</span>
                  </NavLink>
                </li>
              ) : null}
              {admin_kmi ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/mapping"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/mapping"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">
                      Mapping Peraturan Perpajakan
                    </span>
                  </NavLink>
                </li>
              ) : null}
              {admin_kmi ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/pengajuan"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/pengajuan"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">
                      Pengajuan Usulan Perubahan
                    </span>
                  </NavLink>
                </li>
              ) : null}
              {admin_kmi ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/review-pengajuan"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/review-pengajuan"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">
                      Review Pengajuan Usulan Perubahan
                    </span>
                  </NavLink>
                </li>
              ) : null}
                 {admin_kmi ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/matrix-knowledge"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/matrix-knowledge"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">
                      Matrix Knowledge
                    </span>
                  </NavLink>
                </li>
              ) : null}
              {/* {admin ? (
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/process-knowledge/review-knowledge"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/review-knowledge"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Review PKP</span>
                  </NavLink>
                </li>
              ) : null}
                 <li
                  className={`menu-item ${getMenuItemActive("/process-knowledge/knowledge-owner")}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/knowledge-owner"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Knowledge Owner</span>
                  </NavLink>
                </li>
              <li
                  className={`menu-item ${getMenuItemActive("/process-knowledge/review-sme")}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/process-knowledge/review-sme"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Review Subject Matter Expert</span>
                  </NavLink>
                </li> */}
            </ul>
          </div>
        </li>
        {/* ) : null} */}
        {/* End of Process Knowledge Pages */}

        {/* Setting SI Peraturan Pages */}
        {/* {admin ? (
          <li
            className={`menu-item menu-item-submenu ${getMenuItemActive(
              "/admin",
              true
            )}`}
            aria-haspopup="true"
            data-menu-toggle="hover"
          >
            <NavLink className="menu-link menu-toggle" to="/admin">
              <span className="svg-icon menu-icon">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Group.svg"
                  )}
                />
              </span>
              <span className="menu-text">Setting SI Peraturan</span>
              <i className="menu-arrow" />
            </NavLink>
            <div className="menu-submenu ">
              <i className="menu-arrow" />
              <ul className="menu-subnav">
                <li
                  className="menu-item  menu-item-parent"
                  aria-haspopup="true"
                >
                  <span className="menu-link">
                    <span className="menu-text">Setting SI Peraturan</span>
                  </span>
                </li>

                <li
                  className={`menu-item ${getMenuItemActive(
                    "/admin/jenis-pajak"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/admin/jenis-pajak"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Jenis Pajak</span>
                  </NavLink>
                </li>
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/admin/peraturan-jenis"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/admin/peraturan-jenis"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Jenis Peraturan</span>
                  </NavLink>
                </li>
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/admin/master-jenis"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/admin/master-jenis"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Master Jenis</span>
                  </NavLink>
                </li>
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/admin/detil-jenis"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink
                    className="menu-link menu-toggle"
                    to="/admin/detil-jenis"
                  >
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Detil Jenis</span>
                  </NavLink>
                </li>
              </ul>
            </div>
          </li>
        ) : null} */}
        {/* End of Setting SI Peraturan Pages */}

        {/* Administrator Pages */}
        {admin || admin_kmi ? (
          <li
            className={`menu-item menu-item-submenu ${getMenuItemActive(
              "/admin",
              true
            )}`}
            aria-haspopup="true"
            data-menu-toggle="hover"
          >
            <NavLink className="menu-link menu-toggle" to="/admin">
              <span className="svg-icon menu-icon">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Group.svg"
                  )}
                />
              </span>
              <span className="menu-text">Administrator</span>
              <i className="menu-arrow" />
            </NavLink>
            <div className="menu-submenu ">
              <i className="menu-arrow" />
              <ul className="menu-subnav">
                <li
                  className="menu-item  menu-item-parent"
                  aria-haspopup="true"
                >
                  <span className="menu-link">
                    <span className="menu-text">Administrator</span>
                  </span>
                </li>
                {/* {admin_kmi  ? 
                <li
                  className={`menu-item ${getMenuItemActive("/admin/role")}`}
                  aria-haspopup="true"
                >
                  <NavLink className="menu-link menu-toggle" to="/admin/role">
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Penambahan Role</span>
                  </NavLink>
                </li>
                : null} */}
                {admin_kmi ? (
                  <li
                    className={`menu-item menu-item-submenu ${getMenuItemActive(
                      "/setting",
                      true
                    )}`}
                    aria-haspopup="true"
                    data-menu-toggle="hover"
                  >
                    <NavLink className="menu-link menu-toggle" to="/setting">
                      <i className="menu-bullet menu-bullet-dot">
                        <span />
                      </i>
                      <span className="menu-text">
                        Setting Knowledge Integrator
                      </span>
                      <i className="menu-arrow" />
                    </NavLink>
                    <div className="menu-submenu ">
                      <i className="menu-arrow" />
                      <ul className="menu-subnav">
                        <li
                          className="menu-item  menu-item-parent"
                          aria-haspopup="true"
                        >
                          <span className="menu-link">
                            <span className="menu-text">
                              Setting Knowledge Integrator
                            </span>
                          </span>
                        </li>
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/admin/setting/probis"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/admin/setting/probis"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Bisnis Proses</span>
                          </NavLink>
                        </li>
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/admin/setting/sektor"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/admin/setting/sektor"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Bisnis Sektor</span>
                          </NavLink>
                        </li>
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/admin/setting/tipe-knowledge"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/admin/setting/tipe-knowledge"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Tipe Knowledge</span>
                          </NavLink>
                        </li>
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/admin/setting/tusi"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/admin/setting/tusi"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Tugas dan Fungsi</span>
                          </NavLink>
                        </li>
                        {admin_kmi ? (
                          <li
                            className={`menu-item ${getMenuItemActive(
                              "/admin/role"
                            )}`}
                            aria-haspopup="true"
                          >
                            <NavLink
                              className="menu-link menu-toggle"
                              to="/admin/role"
                            >
                              <i className="menu-bullet menu-bullet-dot">
                                <span />
                              </i>
                              <span className="menu-text">Penambahan Role</span>
                            </NavLink>
                          </li>
                        ) : null}
                        {admin_kmi ? (
                          <li
                            className={`menu-item ${getMenuItemActive(
                              "/admin/pengumuman"
                            )}`}
                            aria-haspopup="true"
                          >
                            <NavLink
                              className="menu-link menu-toggle"
                              to="/admin/pengumuman"
                            >
                              <i className="menu-bullet menu-bullet-dot">
                                <span />
                              </i>
                              <span className="menu-text">Pengumuman</span>
                            </NavLink>
                          </li>
                        ) : null}
                      </ul>
                    </div>
                  </li>
                ) : null}
                {/* Setting SI Peraturan Pages */}
                {admin ? (
                  <li
                    className={`menu-item menu-item-submenu ${getMenuItemActive(
                      "/settingsiperaturan",
                      true
                    )}`}
                    aria-haspopup="true"
                    data-menu-toggle="hover"
                  >
                    <NavLink
                      className="menu-link menu-toggle"
                      to="/settingsiperaturan"
                    >
                      <i className="menu-bullet menu-bullet-dot">
                        <span />
                      </i>
                      <span className="menu-text">Setting SI Peraturan</span>
                      <i className="menu-arrow" />
                    </NavLink>
                    <div className="menu-submenu ">
                      <i className="menu-arrow" />
                      <ul className="menu-subnav">
                        <li
                          className="menu-item  menu-item-parent"
                          aria-haspopup="true"
                        >
                          <span className="menu-link">
                            <span className="menu-text">
                              Setting SI Peraturan
                            </span>
                          </span>
                        </li>

                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/admin/jenis-pajak"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/admin/jenis-pajak"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Jenis Pajak</span>
                          </NavLink>
                        </li>
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/admin/peraturan-jenis"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/admin/peraturan-jenis"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Jenis Peraturan</span>
                          </NavLink>
                        </li>
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/admin/master-jenis"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/admin/master-jenis"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Master Jenis</span>
                          </NavLink>
                        </li>
                        <li
                          className={`menu-item ${getMenuItemActive(
                            "/admin/detil-jenis"
                          )}`}
                          aria-haspopup="true"
                        >
                          <NavLink
                            className="menu-link menu-toggle"
                            to="/admin/detil-jenis"
                          >
                            <i className="menu-bullet menu-bullet-dot">
                              <span />
                            </i>
                            <span className="menu-text">Detil Jenis</span>
                          </NavLink>
                        </li>
                      </ul>
                    </div>
                  </li>
                ) : null}
                {/* End of Setting SI Peraturan Pages */}
              </ul>
            </div>
          </li>
        ) : null}
        {/* End of Administrator Pages */}
      </ul>
    </>
  );
}
