/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,no-undef */
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { toAbsoluteUrl } from "../../../../_helpers";
import { useSelector } from "react-redux";

export function QuickUser() {
  const history = useHistory();

  const { user, jabatan } = useSelector((state) => state.auth);
  const [pegawai] = useState([]);
  // const unit = user.pegawai.jabatan_pegawais.length > 0 ? user.pegawai.jabatan_pegawais[0].nama_unit : "";
  // const jabatan = user.pegawai.jabatan_pegawais.length > 0 ? user.pegawai.jabatan_pegawais[0].nama_jabatan : "";

  // const showNamaJabatan = () => {
  //   return pegawai.map((data, index) => (
  //     <div key={index}>
  //       <div key={data.jabatan["@id"]} className="text-muted mt-1">
  //         {data.jabatan.nama}
  //       </div>
  //       <div key={data.unit["@id"]} className="text-muted mt-1">
  //         {data.unit.nama}
  //       </div>
  //     </div>
  //   ));
  // };

  const logoutClick = () => {
    const toggle = document.getElementById("kt_quick_user_toggle");
    if (toggle) {
      toggle.click();
    }

    // window.open("http://localhost:8081/logout");
    // setTimeout(() => {
    //   history.push("/logout");
    // }, 1000);

    history.push("/logout");
  };

  return (
    <div
      id="kt_quick_user"
      className="offcanvas offcanvas-right offcanvas p-10"
    >
      <div className="offcanvas-header d-flex align-items-center justify-content-between pb-5">
        <h3 className="font-weight-bold m-0">
          Profil Saya
          {/* <small className="text-muted font-size-sm ml-2">12 messages</small> */}
        </h3>
        <a
          href="#"
          className="btn btn-xs btn-icon btn-light btn-hover-primary"
          id="kt_quick_user_close"
        >
          <i className="ki ki-close icon-xs text-muted" />
        </a>
      </div>

      <div className="offcanvas-content pr-5 mr-n5">
        <div className="d-flex align-items-center mt-5">
          <div className="symbol symbol-100 mr-5">
            <div
              className="symbol-label"
              style={{
                backgroundImage: `url(${toAbsoluteUrl(
                  "/media/users/blank.png"
                )})`,
              }}
            />
            <i className="symbol-badge bg-success" />
          </div>
          <div className="d-flex flex-column">
            <a
              href="#"
              className="font-weight-bold font-size-h5 text-dark-75 text-hover-primary"
            >
              {user.namaPegawai}
            </a>
            {/* <div className="text-muted mt-1">Application Developer</div> */}
            <div className="font-weight-bold mt-1">{user.nip9}</div>
            {jabatan === "Plh/pjs" ? <h5 className="font-weight-bold m-0" style={{color: "red"}}> Plh / Pjs</h5> : null }
            {/* <div className="text-muted mt-1">Plh / Pjs</div> */}
            {/* {showNamaJabatan()} */}
            <div>
              <div className="text-muted mt-1">{user.jabatan}</div>
              <div className="text-muted mt-1">{user.unitEs4}</div>
              <div className="text-muted mt-1">{user.kantor}</div>
            </div>
            <br />

            <button
              className="btn  btn-bold"
              onClick={logoutClick}
              style={{ backgroundColor: "#FFC91B" }}
            >
              Keluar
            </button>
          </div>
        </div>

        <div className="separator separator-dashed mt-8 mb-5" />

        <div className="navi navi-spacer-x-0 p-0"></div>
        {/* <div className="mb-10">
          <h3 className="font-weight-bold m-0 mb-5">Inbox Mail</h3>
        </div> */}
      </div>
    </div>
  );
}
