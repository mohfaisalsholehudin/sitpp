/* eslint-disable no-unused-vars */
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React, { useState, useMemo, useEffect } from "react";
import { useSelector } from "react-redux";
import {
  Nav,
  Tab,
  Dropdown,
  OverlayTrigger,
  Tooltip,
  TabContent,
} from "react-bootstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import SVG from "react-inlinesvg";
import objectPath from "object-path";
import { useHtmlClassService } from "../../../_core/MetronicLayout";
import { toAbsoluteUrl } from "../../../../_helpers";
import { DropdownTopbarItemToggler } from "../../../../_partials/dropdowns";
import { getPengumumanByNip } from "../../../../../app/references/Api";
import { DateTimeFormat } from "../../../../../app/helpers/DateFormat";
import { useHistory } from "react-router-dom";

const perfectScrollbarOptions = {
  wheelSpeed: 2,
  wheelPropagation: false,
};

export function UserNotificationsDropdown() {
  const [key, setKey] = useState("Alerts");
  const [content, setContent] = useState([]);
  const bgImage = toAbsoluteUrl("/media/misc/bg-1.jpg");
  const { user } = useSelector((state) => state.auth);
  const history = useHistory();

  const uiService = useHtmlClassService();
  const layoutProps = useMemo(() => {
    return {
      offcanvas:
        objectPath.get(uiService.config, "extras.notifications.layout") ===
        "offcanvas",
    };
  }, [uiService]);
  const handleClick = (id) => {
    history.push(`/announcement/${id}/detail`);
  };
  useEffect(() => {
    getPengumumanByNip(user.nip9).then(({ data }) => {
      // setContent(data);
      data.map((dt) => {
        return dt.status === "Publish Aktif"
          ? setContent((content) => [...content, dt])
          : null;
      });
    });
  }, [user.nip9]);
  return (
    <>
      {layoutProps.offcanvas && (
        <div className="topbar-item">
          <div
            className="btn btn-icon btn-clean btn-lg mr-1 pulse pulse-light"
            id="kt_quick_notifications_toggle"
          >
            <span className="svg-icon svg-icon-xl svg-icon-light">
              <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/Communication/Group-chat.svg"
                )}
              />
            </span>
            <span className="pulse-ring"></span>
          </div>
        </div>
      )}
      {!layoutProps.offcanvas && (
        <Dropdown drop="down" alignRight>
          <Dropdown.Toggle
            as={DropdownTopbarItemToggler}
            id="kt_quick_notifications_toggle"
          >
            <OverlayTrigger
              placement="bottom"
              overlay={
                <Tooltip id="user-notification-tooltip">Lihat Pesan</Tooltip>
              }
            >
              <div
                className="btn btn-icon btn-clean btn-lg mr-1 pulse pulse-warning"
                id="kt_quick_notifications_toggle"
              >
                <span className="svg-icon svg-icon-xl svg-icon-warning">
                  <SVG
                    src={toAbsoluteUrl(
                      "/media/svg/icons/Communication/Group-chat.svg"
                    )}
                  />
                </span>
                <span className="pulse-ring"></span>
                <span className="pulse-ring" />
              </div>
            </OverlayTrigger>
          </Dropdown.Toggle>

          <Dropdown.Menu className="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
            <form>
              {/** Head */}
              <div
                className="d-flex flex-column pt-12 bgi-size-cover bgi-no-repeat rounded-top"
                style={{ backgroundColor: "#FFC91B" }}
              >
                <h4
                  className="d-flex flex-center rounded-top"
                  style={{ marginBottom: "30px" }}
                >
                  <span className="text-black">Pesan Pengguna</span>
                  <span className="btn btn-text btn-primary btn-sm font-weight-bold btn-font-md ml-2">
                    {content.length} Baru
                  </span>
                </h4>

                <Tab.Container defaultActiveKey={key}>
                  <Tab.Content className="tab-content">
                    <PerfectScrollbar
                      options={perfectScrollbarOptions}
                      className="navi navi-hover scroll my-4"
                      style={{ maxHeight: "300px", position: "relative" }}
                    >
                      {content.length > 0 ? (
                        content.map((data, index) => {
                          return (
                            <a
                              key={index}
                              onClick={() => handleClick(data.id_pengumuman)}
                              className="navi-item"
                            >
                              <div className="navi-link">
                                <div className="navi-icon mr-2">
                                  <i className="flaticon2-sms text-primary"></i>
                                </div>
                                <div className="navi-text">
                                  <div className="font-weight-bold">
                                    {data.judul}
                                  </div>
                                  <div className="text-muted">
                                    {DateTimeFormat(data.updated_at)}
                                  </div>
                                </div>
                              </div>
                            </a>
                          );
                        })
                      ) : (
                        <div className="d-flex flex-center text-center text-muted min-h-200px">
                          Tidak ada pesan.
                        </div>
                      )}
                    </PerfectScrollbar>
                  </Tab.Content>
                </Tab.Container>
              </div>
            </form>
          </Dropdown.Menu>
        </Dropdown>
      )}
    </>
  );
}
